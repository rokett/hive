package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/ldap"
	"rokett.me/hive/internal/uuid"
	"rokett.me/hive/internal/validation"
)

func (app *application) searchLdapUsers(w http.ResponseWriter, r *http.Request) {
	var users []models.User

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	var query string

	switch r.Header.Get("Hx-Target") {
	case "authorised_users":
		query = r.PostForm.Get("related_authorised_users")
	case "system_managers":
		query = r.PostForm.Get("related_system_managers")
	case "developers":
		query = r.PostForm.Get("developer")
	case "iaos":
		query = r.PostForm.Get("iao")
	}

	fuzzy := true
	includeDisabled := false

	for _, l := range app.config.LDAP {
		//TODO allow for multple hosts to be tried and the first successful one used
		res, err := ldap.SearchUserBySN(l.Hosts[0], l.Port, l.BaseDN, l.BindDN, l.BindPW, query, fuzzy, includeDisabled)
		if err != nil {
			app.serverError(w, err)
			return
		}

		users = append(users, res...)
	}

	app.render(w, r, "upns.partial.html", &templateData{
		Users: users,
	})
}

func (app *application) searchUpns(w http.ResponseWriter, r *http.Request) {
	var users []models.User

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	fuzzy := true
	includeDisabled := false

	for _, l := range app.config.LDAP {
		res, err := ldap.SearchUserByUsername(l.Hosts[0], l.Port, l.BaseDN, l.BindDN, l.BindPW, r.PostForm.Get("upn"), fuzzy, includeDisabled)
		if err != nil {
			app.serverError(w, err)
			return
		}

		users = append(users, res...)
	}

	app.render(w, r, "upns.partial.html", &templateData{
		Users: users,
	})
}

func (app *application) updateLdapUsers() {
	users, err := app.users.ListUsersWithUpn()
	if err != nil {
		app.logger.Error(
			"unable to list users with UPN",
			"error", err.Error(),
		)
		return
	}

	for _, user := range users {
		var problem bool

		for _, l := range app.config.LDAP {
			res, err := ldap.SearchUserByUpn(l.Hosts[0], l.Port, l.BaseDN, l.BindDN, l.BindPW, user.Upn, true)
			if err != nil {
				app.logger.Error(
					"searching for LDAP user",
					"upn", user.Upn,
					"error", err.Error(),
				)
				problem = true
				break
			}

			res.ID = user.ID

			err = app.users.Update(res)
			if err != nil {
				app.logger.Error(
					"updating user record",
					"upn", user.Upn,
					"error", err.Error(),
				)
				problem = true
				break
			}
		}

		if problem {
			continue
		}
	}
}

func (app *application) createApplicationUser(w http.ResponseWriter, r *http.Request) {
	APIResponse := api.Response{}
	var APIErrors api.Errors

	var applicationUser models.User

	err := json.NewDecoder(r.Body).Decode(&applicationUser)
	if err != nil {
		app.logger.Error(
			"decoding json request to create application user",
			"error", err.Error(),
		)

		apiErr := api.ErrJSONDecode
		apiErr.Detail = err.Error()
		APIResponse.Errors = APIErrors.Add(apiErr, nil)
		APIResponse.Send(app.logger, http.StatusBadRequest, w)

		return
	}

	var validationErrors validation.Errors

	if applicationUser.Username == "" {
		validationErrors = validationErrors.Add("username", "REQUIRED field")

		APIErrors = APIErrors.Add(api.ErrValidation, validationErrors)
		APIResponse.Errors = APIErrors
		APIResponse.Send(app.logger, http.StatusBadRequest, w)

		return
	}

	token, err := uuid.NewV4()
	if err != nil {
		app.logger.Error(
			"generating token for user",
			"username", applicationUser.Username,
			"error", err.Error(),
		)

		apiErr := api.ErrGeneratingUUID
		apiErr.Detail = err.Error()
		APIResponse.Errors = APIErrors.Add(apiErr, nil)
		APIResponse.Message = "unable to generate token for user"
		APIResponse.Send(app.logger, http.StatusBadRequest, w)

		return
	}

	err = app.users.Upsert(models.User{
		FirstName:   applicationUser.Username,
		Surname:     applicationUser.Username,
		Upn:         fmt.Sprintf("%s@hive.local", applicationUser.Username),
		Token:       token.String(),
		TokenExpiry: time.Now().UTC().Add(876000 * time.Hour), // 100 years
		TokenMaxAge: int(time.Until(time.Now().UTC().Add(876000 * time.Hour)).Seconds()),
	})
	if err != nil {
		app.logger.Error(
			err.Error(),
			"username", applicationUser.Username,
		)

		apiErr := models.ErrCreatingUser
		apiErr.Detail = err.Error()
		APIResponse.Errors = APIErrors.Add(apiErr, nil)

		APIResponse.Send(app.logger, http.StatusInternalServerError, w)

		return
	}

	APIResponse.Result = struct {
		Username    string    `json:"username"`
		Token       string    `json:"token"`
		TokenExpiry time.Time `json:"token_expiry"`
	}{
		Username:    applicationUser.Username,
		Token:       token.String(),
		TokenExpiry: time.Now().UTC().Add(876000 * time.Hour), // 100 years,
	}
	APIResponse.Send(app.logger, http.StatusCreated, w)
}

package main

import (
	"io/fs"
	"net/http"

	"github.com/go-chi/chi/v5"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"rokett.me/hive"
	"rokett.me/hive/internal/handlers/asset"
	"rokett.me/hive/internal/handlers/department"
	"rokett.me/hive/internal/handlers/environment"
	"rokett.me/hive/internal/handlers/ipaddress"
	"rokett.me/hive/internal/handlers/ldap"
	"rokett.me/hive/internal/handlers/location"
	"rokett.me/hive/internal/handlers/network"
	"rokett.me/hive/internal/handlers/software"
	"rokett.me/hive/internal/handlers/supplier"
	"rokett.me/hive/internal/handlers/system"
	"rokett.me/hive/internal/handlers/user"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (app *application) routes() http.Handler {
	r := chi.NewRouter()

	if len(app.config.Cors.AllowedOrigins) > 0 {
		r.Use(cors.Handler(cors.Options{
			AllowedOrigins: app.config.Cors.AllowedOrigins,
			AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
			AllowedHeaders: app.config.Cors.AllowedHeaders,
		}))
	}

	// status route is used for healthchecking and is excluded from logging
	r.Get("/status", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := w.Write([]byte("ok"))
		if err != nil {
			app.serverError(w, err)
		}
	})

	// metrics route is excluded from logging
	r.Method(http.MethodGet, "/metrics", promhttp.Handler())

	r.Group(func(r chi.Router) {
		r.Use(app.logMetrics)
		r.Use(app.recoverPanic)
		r.Use(app.getClientIP)
		r.Use(app.logRequest)
		r.Use(app.secureHeaders)
		r.Use(app.cacheControl)
		r.Use(app.session.Enable)
		r.Use(middleware.Compress(5))

		r.Get("/login", app.showLoginPage)
		r.Post("/login", app.loginUser)
	})

	r.Group(func(r chi.Router) {
		r.Use(app.logMetrics)
		r.Use(app.recoverPanic)
		r.Use(app.getClientIP)
		r.Use(app.logRequest)
		r.Use(app.secureHeaders)
		r.Use(app.cacheControl)
		r.Use(app.session.Enable)
		r.Use(middleware.Compress(5))

		r.Get("/api/assets/prometheus", asset.GetPrometheusTargets(app.db, app.logger))
		r.Get("/api/assets/checkin", asset.Checkin(app.db, app.logger))
		r.Get("/api/assets", asset.GetAll(app.db, app.logger))         //TODO Unify with UI endpoint
		r.Delete("/api/assets/{id}", asset.Delete(app.db, app.logger)) //TODO Unify with UI endpoint
		r.Get("/api/assets/search", asset.Search(app.db, app.logger))  //TODO Unify with UI endpoint
		r.Post("/api/assets", asset.Create(app.db, app.logger))        //TODO Unify with UI endpoint
		r.Patch("/api/assets/{id}", asset.Update(app.db, app.logger))

		r.Post("/api/config/application_users", app.createApplicationUser)

		r.Get("/api/departments", department.GetAll(app.db, app.logger))
		r.Get("/api/departments/search", department.Search(app.db, app.logger))

		r.Get("/api/environments/search", environment.Search(app.db, app.logger))

		r.Get("/api/ipaddress/search", ipaddress.Search(app.db, app.logger))

		r.Get("/api/ldap/search", ldap.Search(app.config, app.logger))

		r.Get("/api/locations", location.GetAll(app.db, app.logger))
		r.Get("/api/locations/search", location.Search(app.db, app.logger))

		r.Get("/api/networks/{id}", network.GetOne(app.db, app.logger))

		r.Get("/api/software/search", software.Search(app.db, app.logger))

		r.Post("/api/suppliers", supplier.Create(app.db, app.logger))

		r.Get("/api/systems", system.GetAll(app.db, app.logger))
		r.Patch("/api/systems/{id}", system.Update(app.db, app.logger))
		r.Get("/api/systems/search", system.Search(app.db, app.logger))

		r.Put("/api/users", user.CreateIfNotExists(app.db, app.logger))
	})

	r.Group(func(r chi.Router) {
		r.Use(app.logMetrics)
		r.Use(app.recoverPanic)
		r.Use(app.getClientIP)
		r.Use(app.logRequest)
		r.Use(app.secureHeaders)
		r.Use(app.cacheControl)
		r.Use(app.requireAuthentication)
		r.Use(app.session.Enable)
		r.Use(middleware.Compress(5))

		r.Get("/", app.home)
		r.Get("/api/docs", app.showAPIDocs)

		r.Get("/assets", app.showAssetsPage)
		r.Get("/assets/{id}", app.showAssetDetailPage)
		r.Put("/assets/{id}", app.updateAsset)
		r.Get("/assets/add", app.showAddAssetForm)
		r.Post("/assets/add", app.addAsset)
		r.Get("/assets/export", app.exportAssets)
		r.Delete("/assets/{id}", app.deleteAsset)

		r.Get("/departments", app.showDepartmentsPage)
		r.Get("/departments/{id}", app.showDepartmentDetailPage)
		r.Delete("/departments/{id}", app.deleteDepartment)
		r.Get("/departments/add", app.showAddDepartmentForm)
		r.Post("/departments/add", app.addDepartment)
		r.Put("/departments/{id}", app.updateDepartment)

		r.Get("/directorates", app.showDirectoratesPage)
		r.Get("/directorates/{id}", app.showDirectorateDetailPage)
		r.Delete("/directorates/{id}", app.deleteDirectorate)
		r.Get("/directorates/add", app.showAddDirectorateForm)
		r.Post("/directorates/add", app.addDirectorate)
		r.Put("/directorates/{id}", app.updateDirectorate)

		r.Get("/locations", app.showLocationsPage)
		r.Get("/locations/{id}", app.showLocationDetailPage)
		r.Delete("/locations/{id}", app.deleteLocation)
		r.Get("/locations/add", app.showAddLocationForm)
		r.Post("/locations/add", app.addLocation)
		r.Put("/locations/{id}", app.updateLocation)

		r.Get("/networks", app.showNetworksPage)
		r.Get("/networks/add", app.showAddNetworkPage)
		r.Post("/networks/add", app.addNetwork)
		r.Get("/networks/{id}", app.showNetworkDetailPage)
		r.Delete("/networks/{id}", app.deleteNetwork)

		r.Get("/networks/ip/{id}", app.showIPAddressDetailPage)
		r.Put("/networks/ip/{id}", app.updateIPAddress)

		r.Get("/organisations", app.showOrganisationsPage)
		r.Get("/organisations/{id}", app.showOrganisationDetailPage)
		r.Delete("/organisations/{id}", app.deleteOrganisation)
		r.Get("/organisations/add", app.showAddOrganisationForm)
		r.Post("/organisations/add", app.addOrganisation)
		r.Put("/organisations/{id}", app.updateOrganisation)

		r.Get("/suppliers", app.showSuppliersPage)
		r.Get("/suppliers/{id}", app.showSupplierDetailPage)
		r.Put("/suppliers/{id}", app.updateSupplier)
		r.Get("/suppliers/add", app.showAddSupplierForm)
		r.Post("/suppliers/add", app.addSupplier)
		r.Get("/suppliers/export", app.exportSuppliers)
		r.Delete("/suppliers/{id}", app.deleteSupplier)

		r.Get("/systems", app.showSystemsPage)
		r.Get("/systems/{id}", app.showSystemDetailPage)
		r.Put("/systems/{id}", app.updateSystem)
		r.Get("/systems/add", app.showAddSystemForm)
		r.Post("/systems/add", app.addSystem)
		r.Get("/systems/export", app.exportSystems)
		r.Delete("/systems/{id}", app.deleteSystem)

		r.Post("/searchAccreditations", app.searchAccreditations)
		r.Post("/searchAssets", app.searchAssets)
		r.Post("/searchDirectorates", app.searchDirectorates)
		r.Post("/searchLdapUsers", app.searchLdapUsers)
		r.Post("/searchOrganisations", app.searchOrganisations)
		r.Post("/searchServices", app.searchServices)
		r.Post("/searchSuppliers", app.searchSuppliers)
		r.Post("/searchSystemNames", app.searchSystemNames)
		r.Post("/searchUpns", app.searchUpns)
	})

	var static, err = fs.Sub(hive.UI, "ui/dist/static")
	if err != nil {
		app.logger.Error(
			"returning embedded assets",
			"error", err.Error(),
		)
	}

	r.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.FS(static))))

	return r
}

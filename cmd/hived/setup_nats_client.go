package main

import (
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/nkeys"
)

func setupNATSClient(host string, port int, streamReplicas int, NKeySeed string, logger *slog.Logger) (nc *nats.Conn, js nats.JetStreamContext) {
	totalWait := 10 * time.Minute
	reconnectDelay := 10 * time.Second
	connTimeout := 5 * time.Second

	// Create an NKey pair from the seed
	kp, err := nkeys.FromSeed([]byte(NKeySeed))
	if err != nil {
		logger.Error(
			"failed to create NKey pair from seed",
			"error", err,
		)
		os.Exit(1)
	}

	// Get the public key for the NKey (this is the user/account public key sent to the server)
	pubKey, err := kp.PublicKey()
	if err != nil {
		logger.Error(
			"failed to get the public key",
			"error", err,
		)
		os.Exit(1)
	}

	// NKeyOption requires a function for signing the nonce from the server
	sigCB := func(nonce []byte) ([]byte, error) {
		// Sign the nonce using the NKey pair (private key)
		sig, err := kp.Sign(nonce)
		if err != nil {
			logger.Error(
				"failed to sign the NKey pair",
				"error", err,
			)
			os.Exit(1)
		}
		return sig, nil
	}

	connOpts := []nats.Option{
		nats.Nkey(pubKey, sigCB),
		nats.ReconnectWait(reconnectDelay),
		nats.MaxReconnects(int(totalWait / reconnectDelay)),
		nats.Timeout(connTimeout),
		nats.DisconnectErrHandler(func(_ *nats.Conn, err error) {
			if err != nil {
				logger.Warn(
					fmt.Sprintf("Disconnected from NATS server: will attempt reconnects for %.0fm", totalWait.Minutes()),
					"error", err,
				)
			}
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			logger.Info(
				"Connected to NATS server",
				"server", nc.ConnectedUrl(),
			)
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			if nc.LastError() != nil {
				logger.Error(
					"Failed to reconnect to NATS server; stopping attempts",
					"error", nc.LastError(),
				)
			}
		}),
	}

	nc, err = nats.Connect(fmt.Sprintf("nats://%s:%d", host, port), connOpts...)
	if err != nil {
		logger.Error(
			"unable to connect to NATS server",
			"error", err,
			"nats_server", fmt.Sprintf("nats://%s:%d", host, port),
		)
		os.Exit(1)
	}

	js, err = nc.JetStream()
	if err != nil {
		nc.Close()

		logger.Error(
			"unable to create JetStream context",
			"error", err,
			"nats_server", fmt.Sprintf("nats://%s:%d", host, port),
		)
		os.Exit(1)
	}

	// The ASSETS stream is used for comms with assets.
	// We need to check if it already exists or not.
	// If it doesn't, we create it.
	assetsStream, err := js.StreamInfo("ASSETS")
	if err != nil && err.Error() != "nats: stream not found" {
		nc.Close()

		logger.Error(
			"unable to get stream info",
			"error", err,
		)
		os.Exit(1)
	}
	if assetsStream == nil {
		streamInf, err := js.AddStream(&nats.StreamConfig{
			Name:      "ASSETS",
			Subjects:  []string{"assets.*"},
			Storage:   nats.FileStorage,
			Retention: nats.WorkQueuePolicy,
			Discard:   nats.DiscardOld,
			Replicas:  streamReplicas,
		})
		if err != nil {
			nc.Close()

			logger.Error(
				"unable to add stream",
				"error", err,
				"stream_name", "ASSETS",
			)
			os.Exit(1)
		}

		logger.Debug(
			"added NATS stream",
			"stream_name", streamInf.Config.Name,
			"stream_subjects", streamInf.Config.Subjects,
			"stream_storage_policy", streamInf.Config.Storage,
			"stream_retention_policy", streamInf.Config.Retention,
			"stream_discard_policy", streamInf.Config.Discard,
			"stream_replicas", streamInf.Config.Replicas,
		)
	}

	return nc, js
}

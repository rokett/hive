package main

import (
	"html/template"
	"io/fs"
	"net/url"
	"path/filepath"
	"strings"
	"time"

	"rokett.me/hive"
	"rokett.me/hive/internal/models"
)

type templateData struct {
	Accreditations []models.Accreditation
	Action         string
	Assets         []models.AssetView
	Departments    []models.Department
	Directorates   []models.Directorate
	Environment    string
	Environments   []models.Environment
	Flash          string
	FormData       url.Values
	FormErrors     map[string]string
	IP             models.IP
	Locations      []models.Location
	Network        models.Network
	Networks       []models.Network
	Organisations  []models.Organisation
	Services       []models.Service
	Supplier       models.Supplier
	Suppliers      []models.Supplier
	Systems        []models.SystemView
	Users          []models.User
}

func newTemplateCache() (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	pages, err := fs.Glob(hive.UI, "ui/dist/html/*.page.html")
	if err != nil {
		return nil, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		ts, err := template.New(name).Funcs(functions).ParseFS(hive.UI, page)
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseFS(hive.UI, "ui/dist/html/*.layout.html")
		if err != nil {
			return nil, err
		}

		/*ts, err = ts.ParseFS(hive.UI, "ui/dist/html/*.partial.html")
		if err != nil {
			return nil, err
		}*/

		cache[name] = ts
	}

	partials, err := fs.Glob(hive.UI, "ui/dist/html/*.partial.html")
	if err != nil {
		return nil, err
	}

	for _, partial := range partials {
		name := filepath.Base(partial)

		ts, err := template.New(name).Funcs(functions).ParseFS(hive.UI, partial)
		if err != nil {
			return nil, err
		}

		cache[name] = ts
	}

	return cache, nil
}

func friendlydate(t time.Time) string {
	return t.Format("02 Jan 2006")
}

func friendlyDateTime(t time.Time) string {
	return t.Format("02 Jan 2006 @ 15:04")
}

func isAfterEpoch(t time.Time) bool {
	return t.After(time.Time{})
}

func isCheckboxSelected(selectedOpts string, opt string) bool {
	opts := strings.Split(selectedOpts, ",")

	for _, o := range opts {
		if o == opt {
			return true
		}
	}
	return false
}

func splitString(s, sep string) []string {
	return strings.Split(s, sep)
}

var functions = template.FuncMap{
	"friendlydate":       friendlydate,
	"friendlyDateTime":   friendlyDateTime,
	"isAfterEpoch":       isAfterEpoch,
	"isCheckboxSelected": isCheckboxSelected,
	"splitString":        splitString,
}

package main

import (
	"net/http"
	"strings"
)

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/assets", http.StatusSeeOther)
}

func (app *application) showAPIDocs(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "api.page.html", &templateData{})
}

func (app *application) showLoginPage(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "login.page.html", &templateData{})
}

func (app *application) loginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	conn, err := app.ldapAuth.Connect()
	if err != nil {
		app.logger.Error(err.Error())
		app.serverError(w, err)
		return
	}

	app.ldapAuth.Conn = conn
	defer app.ldapAuth.Conn.Close()

	// We replace any * characters as this is a wildcard in LDAP queries.  We only want to allow an exact match.
	u, err := app.ldapAuth.Authenticate(strings.Replace(r.PostForm.Get("upn"), "*", "", -1), r.PostForm.Get("password"), app.ldapAuth.AllowedGroupDN)
	if err != nil {
		app.logger.Error(
			err.Error(),
			"username", r.PostForm.Get("upn"),
		)

		app.session.Put(r, "flash", "Authentication failed")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	err = app.users.Upsert(u)
	if err != nil {
		app.logger.Error(
			err.Error(),
			"username", r.PostForm.Get("upn"),
		)

		app.session.Put(r, "flash", "Authentication failed")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:   "session_token",
		Value:  u.Token,
		MaxAge: u.TokenMaxAge,
		Path:   "/",
	})

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

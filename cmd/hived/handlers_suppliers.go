package main

import (
	"encoding/csv"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) searchAccreditations(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	accreditations, err := app.accreditations.Search(r.PostForm.Get("has_accreditation"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "accreditations.partial.html", &templateData{
		Accreditations: accreditations,
	})
}

func (app *application) showAddSupplierForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "supplier_detail.page.html", &templateData{
		Action: "add",
	})
}

func (app *application) showSupplierDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	rels := []string{
		"accreditations",
		"systems",
		"docs",
	}

	supplier, err := app.suppliers.Get(id, strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.FormatInt(supplier.ID, 10))
	form.Set("name", supplier.Supplier)
	form.Set("selectedAccreditations", supplier.SelectedAccreditations)
	form.Set("selectedSystems", supplier.SelectedSystems)
	form.Set("selectedDocs", supplier.SelectedDocs)

	var accounts []string
	for _, a := range *supplier.Accounts {
		accounts = append(accounts, a.Upn)
	}
	form.Set("selected_supplier_accounts", strings.Join(accounts, ","))

	app.render(w, r, "supplier_detail.page.html", &templateData{
		Action:   "view",
		FormData: form,
	})
}

func (app *application) showSuppliersPage(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"accreditations",
		"systems",
		"docs",
	}

	suppliers, err := app.suppliers.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "suppliers.page.html", &templateData{
		Suppliers: suppliers,
	})
}

func (app *application) addSupplier(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	supplier := models.Supplier{
		Supplier: r.PostForm.Get("name"),
	}

	validationErrors, err := app.suppliers.Validate(supplier)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "supplier_detail.page.html", &templateData{
			Action:     "add",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	accreditations := strings.Split(r.PostForm.Get("selected_accreditations"), ",")
	systems := strings.Split(r.PostForm.Get("selected_systems"), ",")
	accounts := strings.Split(r.PostForm.Get("selected_supplier_accounts"), ",")
	docs := strings.Split(r.PostForm.Get("selected_supplier_docs"), ",")

	err = app.suppliers.Insert(supplier, accreditations, systems, accounts, docs)
	if err != nil {
		app.serverError(w, err)
		return
	}

	go app.updateLdapUsers()

	app.session.Put(r, "flash", "Created "+supplier.Supplier)

	http.Redirect(w, r, "/suppliers", http.StatusSeeOther)
}

func (app *application) updateSupplier(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	supplier, err := app.suppliers.Get(id, "")
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	supplier.Supplier = r.PostForm.Get("name")

	validationErrors, err := app.suppliers.Validate(supplier)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		r.PostForm.Set("id", chi.URLParam(r, "id"))

		app.render(w, r, "supplier_detail.page.html", &templateData{
			Action:     "view",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	accreditations := strings.Split(r.PostForm.Get("selected_accreditations"), ",")
	systems := strings.Split(r.PostForm.Get("selected_systems"), ",")
	accounts := strings.Split(r.PostForm.Get("selected_supplier_accounts"), ",")
	docs := strings.Split(r.PostForm.Get("selected_supplier_docs"), ",")

	err = app.suppliers.Update(supplier, accreditations, systems, accounts, docs)
	if err != nil {
		app.serverError(w, err)
		return
	}

	go app.updateLdapUsers()

	app.session.Put(r, "flash", "Updated "+supplier.Supplier)

	http.Redirect(w, r, "/suppliers", http.StatusSeeOther)
}

func (app *application) deleteSupplier(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	supplier, err := app.suppliers.Get(id, "")
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.suppliers.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted "+supplier.Supplier)

	http.Redirect(w, r, "/suppliers", http.StatusSeeOther)
}

func (app *application) searchSuppliers(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	suppliers, err := app.suppliers.SearchByName(r.PostForm.Get("related_suppliers"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "suppliers.partial.html", &templateData{
		Suppliers: suppliers,
	})
}

func (app *application) exportSuppliers(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"accreditations",
		"systems",
	}

	suppliers, err := app.suppliers.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	w.Header().Set("Content-Disposition", "attachment; filename=suppliers.csv")
	w.Header().Set("Content-Type", "application/octet-stream")

	csvFile := csv.NewWriter(w)
	err = csvFile.Write([]string{
		"Supplier",
		"AD Account(s)",
		"Accreditations",
		"Related System(s)",
	})
	if err != nil {
		app.serverError(w, err)
		return
	}

	for _, supplier := range suppliers {
		var accounts []string
		for _, account := range *supplier.Accounts {
			accounts = append(accounts, account.Upn)
		}

		var accreditations []string
		for _, accreditation := range *supplier.Accreditations {
			accreditations = append(accreditations, accreditation.Accreditation)
		}

		var systems []string
		for _, system := range *supplier.Systems {
			systems = append(systems, system.System)
		}

		err = csvFile.Write([]string{
			supplier.Supplier,
			strings.Join(accounts, ":"),
			strings.Join(accreditations, ":"),
			strings.Join(systems, ":"),
		})
		if err != nil {
			app.serverError(w, err)
			return
		}
	}

	csvFile.Flush()
}

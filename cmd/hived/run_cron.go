package main

import (
	"github.com/jmoiron/sqlx"
	"github.com/robfig/cron/v3"

	"rokett.me/hive/internal/auditlog"
	"rokett.me/hive/internal/services/asset"
	"rokett.me/hive/internal/services/network"
)

func (app *application) runCron(db *sqlx.DB, debug bool) {
	c := cron.New()

	_, err := c.AddFunc("@"+app.config.Server.NetworkScanInterval, func() {
		network.Scan(app.config.Server.StaleIP, db, app.logger, debug)
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to scan network",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@every 2h", func() {
		err := app.sqlInstances.DeleteOrphaned()
		if err != nil {
			app.logger.Error(err.Error())
		}
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete orphaned SQL Server instances",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@every 2h30m", func() {
		err := app.databases.DeleteOrphaned()
		if err != nil {
			app.logger.Error(err.Error())
		}
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete orphaned databases",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@every 2h40m", func() {
		err := app.databaseFiles.DeleteOrphaned()
		if err != nil {
			app.logger.Error(err.Error())
		}
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete orphaned database files",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@every 2h", func() {
		err := app.software.DeleteOrphaned()
		if err != nil {
			app.logger.Error(err.Error())
		}
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete orphaned software",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@every 2h15m", func() {
		err := app.volumes.DeleteOrphaned()
		if err != nil {
			app.logger.Error(err.Error())
		}
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete orphaned volumes",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@every 2h30m", func() {
		err := app.documentation.DeleteOrphaned()
		if err != nil {
			app.logger.Error(err.Error())
		}
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete orphaned documentation",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@"+app.config.Server.WarrantyUpdateInterval, func() {
		asset.UpdateWarrantyDetails(app.config.Server.DellAPIURL, app.config.Server.DellAPIOauthURL, app.config.Server.DellAPIWarrantyURL, app.config.Server.DellAPIClientID, app.config.Server.DellAPIClientSecret, db, app.logger)
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to update asset warranty details",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@daily", func() {
		auditlog.DeleteOldRecords(app.config.Audit.MaxAge, db, app.logger)
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete old audit records",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@every 1m30s", func() {
		err := app.services.DeleteOrphaned()
		if err != nil {
			app.logger.Error(err.Error())
		}
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete orphaned services",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@daily", func() {
		app.updateLdapUsers()
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to update Ldap users",
			"error", err.Error(),
		)
	}

	_, err = c.AddFunc("@daily", func() {
		asset.DeleteZombies(db, app.logger)
	})
	if err != nil {
		app.logger.Error(
			"adding scheduled function to delete zombie assets",
			"error", err.Error(),
		)
	}

	c.Start()
}

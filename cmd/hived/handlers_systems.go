package main

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) searchSystemNames(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	systems, err := app.systems.SearchByName(r.PostForm.Get("related_systems"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	var systemsView []models.SystemView
	for _, system := range systems {
		systemsView = append(systemsView, models.SystemView{
			System: system.System,
		})
	}

	app.render(w, r, "systems.partial.html", &templateData{
		Systems: systemsView,
	})
}

func (app *application) showSystemsPage(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"assets",
		"asset.environments",
		"environments",
		"organisations",
		"services",
		"suppliers",
	}

	systems, err := app.systems.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	var systemsView []models.SystemView
	for _, system := range systems {

		systemsView = append(systemsView, models.SystemView{
			ID:                system.ID,
			System:            system.System,
			Description:       system.Description,
			Tier:              system.Tier,
			OnCall:            *system.OnCall,
			Websites:          system.Websites,
			DistributionLists: system.DistributionLists,
			Shortcode:         system.Shortcode,
			Documentation:     system.Documentation,
			Environments:      *system.Environments,
			Assets:            *system.Assets,
			Suppliers:         *system.Suppliers,
			Organisations:     *system.Organisations,
			Developers:        *system.Developers,
			SystemManagers:    *system.SystemManagers,
			IAOs:              *system.IAOs,
			CreatedAt:         system.CreatedAt,
			UpdatedAt:         system.UpdatedAt,
			AuthorisedUsers:   *system.AuthorisedUsers,
			Services:          *system.Services,
		})
	}

	app.render(w, r, "systems.page.html", &templateData{
		Systems: systemsView,
	})
}

func (app *application) showAddSystemForm(w http.ResponseWriter, r *http.Request) {
	environments, err := app.environments.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	organisations, err := app.organisations.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "system_detail.page.html", &templateData{
		Action:        "add",
		Environments:  environments,
		Organisations: organisations,
	})
}

func (app *application) exportSystems(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"assets",
		"asset.environments",
		"environments",
		"organisations",
		"services",
		"suppliers",
	}

	systems, err := app.systems.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	w.Header().Set("Content-Disposition", "attachment; filename=systems.csv")
	w.Header().Set("Content-Type", "application/octet-stream")

	csvFile := csv.NewWriter(w)
	err = csvFile.Write([]string{
		"System Name",
		"Documentation",
		"Shortcode",
		"Description",
		"Service(s)",
		"Supplier(s)",
		"Developer(s)",
		"System Managers",
		"IAO",
		"Authorised Users",
		"Website(s)",
		"Distribution List(s)",
		"Tier",
		"Environment(s)",
		"Organisation(s)",
		"On-Call?",
		"Related Asset(s)",
	})
	if err != nil {
		app.serverError(w, err)
		return
	}

	for _, system := range systems {
		var services []string
		for _, service := range *system.Services {
			services = append(services, service.Name)
		}

		var suppliers []string
		for _, supplier := range *system.Suppliers {
			suppliers = append(suppliers, supplier.Supplier)
		}

		var developers []string
		for _, developer := range *system.Developers {
			developers = append(developers, fmt.Sprintf("%s %s", developer.FirstName, developer.Surname))
		}

		var systemManagers []string
		for _, sm := range *system.SystemManagers {
			systemManagers = append(systemManagers, fmt.Sprintf("%s %s", sm.FirstName, sm.Surname))
		}

		var iaos []string
		for _, iao := range *system.IAOs {
			iaos = append(iaos, fmt.Sprintf("%s %s", iao.FirstName, iao.Surname))
		}

		var authorisedUsers []string
		for _, au := range *system.AuthorisedUsers {
			authorisedUsers = append(authorisedUsers, fmt.Sprintf("%s %s", au.FirstName, au.Surname))
		}

		var environments []string
		for _, env := range *system.Environments {
			environments = append(environments, env.Name)
		}

		var organisations []string
		for _, org := range *system.Organisations {
			organisations = append(organisations, org.Organisation)
		}

		var assets []string
		for _, asset := range *system.Assets {
			var envs []string

			for _, env := range *asset.Environments {
				envs = append(envs, fmt.Sprintf("(%s)", env.Name))
			}

			assets = append(assets, fmt.Sprintf("%s %s", asset.Hostname, strings.Join(envs, "")))
		}

		err = csvFile.Write([]string{
			system.System,
			system.Documentation,
			system.Shortcode,
			system.Description,
			strings.Join(services, ":"),
			strings.Join(suppliers, ":"),
			strings.Join(developers, ":"),
			strings.Join(systemManagers, ":"),
			strings.Join(iaos, ":"),
			strings.Join(authorisedUsers, ":"),
			system.Websites,
			system.DistributionLists,
			strconv.FormatInt(system.Tier, 10),
			strings.Join(environments, ":"),
			strings.Join(organisations, ":"),
			strconv.FormatBool(*system.OnCall),
			strings.Join(assets, ":"),
		})
		if err != nil {
			app.serverError(w, err)
			return
		}
	}

	csvFile.Flush()
}

func (app *application) addSystem(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	system := models.System{
		System:            r.PostForm.Get("system"),
		Description:       r.PostForm.Get("description"),
		Websites:          r.PostForm.Get("websites"),
		DistributionLists: r.PostForm.Get("distribution_lists"),
		Shortcode:         r.PostForm.Get("shortcode"),
		Documentation:     strings.Replace(app.config.Server.SystemDocumentationURL, "[system]", r.PostForm.Get("system"), -1),
	}

	tier, err := strconv.Atoi(r.PostForm.Get("tier"))
	if err != nil {
		app.serverError(w, err)
		return
	}
	system.Tier = int64(tier)

	if r.PostForm.Get("oncall") != "" {
		tr := true
		system.OnCall = &tr
	}

	environments := r.Form["environment"]
	suppliers := strings.Split(r.PostForm.Get("selected_suppliers"), ",")
	organisations := r.Form["organisation"]
	developer := r.PostForm.Get("developer")
	systemManagers := strings.Split(r.PostForm.Get("selected_system_managers"), ",")
	iao := r.PostForm.Get("iao")
	authorisedUsers := strings.Split(r.PostForm.Get("selected_authorised_users"), ",")
	services := strings.Split(r.PostForm.Get("selected_services"), ",")

	relatedAssets := strings.Split(r.PostForm.Get("selected_assets"), ",")
	assets := make(map[string][]string)
	var formAssets []string
	for _, a := range relatedAssets {
		assets[a] = r.Form[a+"_environment"]
		formAssets = append(formAssets, fmt.Sprintf("%s:%s", a, r.Form[a+"_environment"]))
	}

	validationErrors, err := app.systems.Validate(system, environments, organisations, services, systemManagers, suppliers)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		environments, err := app.environments.List()
		if err != nil {
			app.serverError(w, err)
			return
		}

		organisations, err := app.organisations.List()
		if err != nil {
			app.serverError(w, err)
			return
		}

		r.PostForm.Set("selected_environments", strings.Join(r.Form["environment"], ","))
		r.PostForm.Set("selected_organisations", strings.Join(r.Form["organisation"], ","))
		r.PostForm.Set("selected_asset_environments", strings.Join(formAssets, ","))

		app.render(w, r, "system_detail.page.html", &templateData{
			Action:        "add",
			Environments:  environments,
			FormData:      r.PostForm,
			FormErrors:    validationErrors,
			Organisations: organisations,
		})
		return
	}

	err = app.systems.Insert(system, assets, environments, suppliers, organisations, systemManagers, authorisedUsers, services, developer, iao)
	if err != nil {
		app.serverError(w, err)
		return
	}

	go app.updateLdapUsers()

	app.session.Put(r, "flash", "Created "+system.System)

	http.Redirect(w, r, "/systems", http.StatusSeeOther)
}

func (app *application) deleteSystem(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	system, err := app.systems.Get(id, "")
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.systems.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted "+system.System)

	http.Redirect(w, r, "/systems", http.StatusSeeOther)
}

func (app *application) showSystemDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	rels := []string{
		"assets",
		"asset.environments",
		"environments",
		"organisations",
		"services",
		"suppliers",
	}

	system, err := app.systems.Get(id, strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.FormatInt(system.ID, 10))
	form.Set("system", system.System)
	form.Set("description", system.Description)
	form.Set("websites", system.Websites)
	form.Set("distribution_lists", system.DistributionLists)
	form.Set("shortcode", system.Shortcode)
	form.Set("tier", strconv.FormatInt(system.Tier, 10))
	form.Set("oncall", strconv.FormatBool(*system.OnCall))
	form.Set("documentation", system.Documentation)

	var svcs []string
	for _, svc := range *system.Services {
		svcs = append(svcs, svc.Name)
	}
	form.Set("selected_services", strings.Join(svcs, ","))

	for _, dev := range *system.Developers {
		form.Set("developer", dev.Upn)
		break
	}

	var sms []string
	for _, sm := range *system.SystemManagers {
		sms = append(sms, sm.Upn)
	}
	form.Set("selected_system_managers", strings.Join(sms, ","))

	for _, iao := range *system.IAOs {
		form.Set("iao", iao.Upn)
		break
	}

	var aus []string
	for _, au := range *system.AuthorisedUsers {
		aus = append(aus, au.Upn)
	}
	form.Set("selected_authorised_users", strings.Join(aus, ","))

	var sups []string
	for _, sup := range *system.Suppliers {
		sups = append(sups, sup.Supplier)
	}
	form.Set("selected_suppliers", strings.Join(sups, ","))

	var envs []string
	for _, env := range *system.Environments {
		envs = append(envs, env.Name)
	}
	form.Set("selected_environments", strings.Join(envs, ","))

	var orgs []string
	for _, org := range *system.Organisations {
		orgs = append(orgs, org.Organisation)
	}
	form.Set("selected_organisations", strings.Join(orgs, ","))

	var assets []string
	var formAssets []string
	for _, asset := range *system.Assets {
		assets = append(assets, asset.Hostname)

		for _, env := range *asset.Environments {
			formAssets = append(formAssets, fmt.Sprintf("%s:%s", asset.Hostname, env.Name))
		}
	}
	form.Set("selected_assets", strings.Join(assets, ","))
	form.Set("selected_asset_environments", strings.Join(formAssets, ","))

	environments, err := app.environments.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	organisations, err := app.organisations.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "system_detail.page.html", &templateData{
		Action:        "view",
		Environments:  environments,
		FormData:      form,
		Organisations: organisations,
	})
}

func (app *application) updateSystem(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	system, err := app.systems.Get(id, "")
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	system.System = r.PostForm.Get("system")
	system.Description = r.PostForm.Get("description")
	system.Websites = r.PostForm.Get("websites")
	system.DistributionLists = r.PostForm.Get("distribution_lists")
	system.Shortcode = r.PostForm.Get("shortcode")
	system.Documentation = r.PostForm.Get("documentation")

	tier, err := strconv.Atoi(r.PostForm.Get("tier"))
	if err != nil {
		app.serverError(w, err)
		return
	}
	system.Tier = int64(tier)

	fls := false
	system.OnCall = &fls
	if r.PostForm.Get("oncall") != "" {
		tr := true
		system.OnCall = &tr
	}

	environments := r.Form["environment"]
	suppliers := strings.Split(r.PostForm.Get("selected_suppliers"), ",")
	organisations := r.Form["organisation"]
	developer := r.PostForm.Get("developer")
	systemManagers := strings.Split(r.PostForm.Get("selected_system_managers"), ",")
	iao := r.PostForm.Get("iao")
	authorisedUsers := strings.Split(r.PostForm.Get("selected_authorised_users"), ",")
	services := strings.Split(r.PostForm.Get("selected_services"), ",")

	relatedAssets := strings.Split(r.PostForm.Get("selected_assets"), ",")
	assets := make(map[string][]string)
	var formAssets []string
	for _, a := range relatedAssets {
		assets[a] = r.Form[a+"_environment"]
		formAssets = append(formAssets, fmt.Sprintf("%s:%s", a, r.Form[a+"_environment"]))
	}

	validationErrors, err := app.systems.Validate(system, environments, organisations, services, systemManagers, suppliers)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		environments, err := app.environments.List()
		if err != nil {
			app.serverError(w, err)
			return
		}

		organisations, err := app.organisations.List()
		if err != nil {
			app.serverError(w, err)
			return
		}

		r.PostForm.Set("selected_environments", strings.Join(r.Form["environment"], ","))
		r.PostForm.Set("selected_organisations", strings.Join(r.Form["organisation"], ","))
		r.PostForm.Set("selected_asset_environments", strings.Join(formAssets, ","))

		app.render(w, r, "system_detail.page.html", &templateData{
			Action:        "view",
			Environments:  environments,
			FormData:      r.PostForm,
			FormErrors:    validationErrors,
			Organisations: organisations,
		})
		return
	}

	err = app.systems.Update(system, assets, environments, suppliers, organisations, systemManagers, authorisedUsers, services, developer, iao)
	if err != nil {
		app.serverError(w, err)
		return
	}

	go app.updateLdapUsers()

	app.session.Put(r, "flash", "Updated "+system.System)

	http.Redirect(w, r, "/systems", http.StatusSeeOther)
}

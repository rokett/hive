package main

import (
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/microsoft/go-mssqldb"
	"rokett.me/hive/internal/models"
)

func openDB(config models.Config, logger *slog.Logger) *sqlx.DB {
	var db *sqlx.DB
	var err error

	maxAttempts := 10

	dsn := fmt.Sprintf("server=%s;port=%d;database=%s;encrypt=disable;connection timeout=30", config.Database.Host, config.Database.Port, config.Database.Name)

	if config.Database.Username != "" && config.Database.Password != "" {
		dsn = fmt.Sprintf("%s;user id=%s;password=%s", dsn, config.Database.Username, config.Database.Password)
	}

	for attempts := 1; attempts <= maxAttempts; attempts++ {
		db, err = sqlx.Connect("sqlserver", dsn)
		if err != nil {
			logger.Info(
				"unable to connect to DB; retrying",
				"error", err.Error(),
			)

			time.Sleep(time.Duration(attempts) * time.Second)
			continue
		}

		break
	}

	if err != nil {
		logger.Error(
			"run out of atttempts to connect to database",
			"error", err.Error(),
			"max_attempts", maxAttempts,
		)
		os.Exit(1)
	}

	return db
}

package main

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) showAssetsPage(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"instances",
		"systems",
		"system.environments",
	}

	assets, err := app.assets.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	var assetView []models.AssetView
	for _, asset := range assets {
		a := models.AssetView{
			ID:                 asset.ID,
			AssetType:          asset.AssetType,
			Decommissioned:     *asset.Decommissioned,
			Hostname:           asset.Hostname,
			IgnoreWarranty:     *asset.IgnoreWarranty,
			Manufacturer:       asset.Manufacturer,
			Model:              asset.Model,
			OS:                 asset.OS,
			RecoveryMethod:     asset.RecoveryMethod,
			SerialNumber:       asset.SerialNumber,
			ServicePack:        asset.ServicePack,
			ShipDate:           asset.ShipDate,
			ThirdPartyWarranty: *asset.ThirdPartyWarranty,
			WarrantyExpiryDate: asset.WarrantyExpiryDate,
			WarrantyLevel:      asset.WarrantyLevel,
			CreatedAt:          asset.CreatedAt,
			UpdatedAt:          asset.UpdatedAt,
			Tier:               asset.Tier,
			Systems:            *asset.Systems,
			SQLInstances:       *asset.SQLInstances,
		}

		if time.Until(a.WarrantyExpiryDate).Hours() > 0 && time.Until(a.WarrantyExpiryDate).Hours() < 2920 {
			a.IsWarrantyExpiringSoon = true
		}

		if a.WarrantyExpiryDate.Before(time.Now()) {
			a.IsWarrantyExpired = true
			a.IsWarrantyExpiringSoon = false
		}

		assetView = append(assetView, a)
	}

	app.render(w, r, "assets.page.html", &templateData{
		Assets: assetView,
	})
}

func (app *application) showAssetDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	rels := []string{
		"systems",
		"system.environments",
	}

	asset, err := app.assets.Get(id, strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.FormatInt(asset.ID, 10))
	form.Set("name", asset.Hostname)
	form.Set("asset_type", asset.AssetType)
	form.Set("decommissioned", strconv.FormatBool(*asset.Decommissioned))
	form.Set("ignore_warranty", strconv.FormatBool(*asset.IgnoreWarranty))
	form.Set("manufacturer", asset.Manufacturer)
	form.Set("model", asset.Model)
	form.Set("os", asset.OS)
	form.Set("recovery_method", asset.RecoveryMethod)
	form.Set("serial_number", asset.SerialNumber)
	form.Set("service_pack", asset.ServicePack)
	form.Set("ship_date", asset.ShipDate.Format("2006-01-02"))
	form.Set("third_party_warranty", strconv.FormatBool(*asset.ThirdPartyWarranty))
	form.Set("warranty_expiry_date", asset.WarrantyExpiryDate.Format("2006-01-02"))
	form.Set("warranty_level", asset.WarrantyLevel)
	form.Set("selected_systems", asset.SelectedSystems)
	form.Set("is_virtual", strconv.FormatBool(*asset.IsVirtual))
	form.Set("domain", asset.Domain)
	form.Set("tier", strconv.FormatInt(asset.Tier, 10))

	app.render(w, r, "asset_detail.page.html", &templateData{
		Action:   "view",
		FormData: form,
	})
}

func (app *application) showAddAssetForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "asset_detail.page.html", &templateData{
		Action: "add",
	})
}

func (app *application) addAsset(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	fls := false

	asset := models.Asset{
		Hostname:           r.PostForm.Get("name"),
		Decommissioned:     &fls,
		AssetType:          r.PostForm.Get("asset_type"),
		IsVirtual:          &fls,
		RecoveryMethod:     r.PostForm.Get("recovery_method"),
		OS:                 r.PostForm.Get("os"),
		Domain:             r.PostForm.Get("domain"),
		SerialNumber:       r.PostForm.Get("serial_number"),
		Manufacturer:       r.PostForm.Get("manufacturer"),
		Model:              r.PostForm.Get("model"),
		ServicePack:        r.PostForm.Get("service_pack"),
		WarrantyLevel:      r.PostForm.Get("warranty_level"),
		IgnoreWarranty:     &fls,
		ThirdPartyWarranty: &fls,
	}

	tr := true

	if r.PostForm.Get("decomissioned") != "" {
		asset.Decommissioned = &tr
	}

	if r.PostForm.Get("is_virtual") != "" {
		asset.IsVirtual = &tr
	}

	if r.PostForm.Get("ignore_warranty") != "" {
		asset.IgnoreWarranty = &tr
	}

	if r.PostForm.Get("third_party_warranty") != "" {
		asset.ThirdPartyWarranty = &tr
	}

	if r.PostForm.Get("warranty_expiry_date") != "" {
		warrantyExpiryDate, err := time.Parse("2006-01-02", r.PostForm.Get("warranty_expiry_date"))
		if err != nil {
			app.serverError(w, err)
			return
		}
		asset.WarrantyExpiryDate = warrantyExpiryDate
	}

	validationErrors, err := app.assets.Validate(asset)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "asset_detail.page.html", &templateData{
			Action:     "add",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.assets.Insert(asset)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Created "+asset.Hostname)

	http.Redirect(w, r, "/assets", http.StatusSeeOther)
}

func (app *application) updateAsset(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	asset, err := app.assets.Get(id, "systems,system.environments")
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	asset.Hostname = r.PostForm.Get("name")
	asset.AssetType = r.PostForm.Get("asset_type")
	asset.RecoveryMethod = r.PostForm.Get("recovery_method")
	asset.OS = r.PostForm.Get("os")
	asset.Domain = r.PostForm.Get("domain")
	asset.SerialNumber = r.PostForm.Get("serial_number")
	asset.Manufacturer = r.PostForm.Get("manufacturer")
	asset.Model = r.PostForm.Get("model")
	asset.ServicePack = r.PostForm.Get("service_pack")
	asset.WarrantyLevel = r.PostForm.Get("warranty_level")

	fls := false
	tr := true

	asset.Decommissioned = &fls
	if r.PostForm.Get("decommissioned") != "" {
		asset.Decommissioned = &tr
	}

	asset.IsVirtual = &fls
	if r.PostForm.Get("is_virtual") != "" {
		asset.IsVirtual = &tr
	}

	asset.IgnoreWarranty = &fls
	if r.PostForm.Get("ignore_warranty") != "" {
		asset.IgnoreWarranty = &tr
	}

	asset.ThirdPartyWarranty = &fls
	if r.PostForm.Get("third_party_warranty") != "" {
		asset.ThirdPartyWarranty = &tr
	}

	if r.PostForm.Get("warranty_expiry_date") != "" {
		warrantyExpiryDate, err := time.Parse("2006-01-02", r.PostForm.Get("warranty_expiry_date"))
		if err != nil {
			app.serverError(w, err)
			return
		}
		asset.WarrantyExpiryDate = warrantyExpiryDate
	}

	validationErrors, err := app.assets.Validate(asset)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		r.PostForm.Set("id", chi.URLParam(r, "id"))

		app.render(w, r, "asset_detail.page.html", &templateData{
			Action:     "view",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.assets.Update(asset)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Updated "+asset.Hostname)

	http.Redirect(w, r, "/assets", http.StatusSeeOther)
}

func (app *application) deleteAsset(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	asset, err := app.assets.Get(id, "")
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.assets.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted "+asset.Hostname)

	http.Redirect(w, r, "/assets", http.StatusSeeOther)
}

func (app *application) searchAssets(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	assets, err := app.assets.SearchByName(r.PostForm.Get("related_assets"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	var assetView []models.AssetView
	for _, asset := range assets {
		a := models.AssetView{
			Hostname: asset.Hostname,
		}

		assetView = append(assetView, a)
	}

	app.render(w, r, "assets.partial.html", &templateData{
		Assets: assetView,
	})
}

func (app *application) exportAssets(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"instances",
		"systems",
		"system.environments",
	}

	assets, err := app.assets.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	w.Header().Set("Content-Disposition", "attachment; filename=assets.csv")
	w.Header().Set("Content-Type", "application/octet-stream")

	csvFile := csv.NewWriter(w)
	err = csvFile.Write([]string{
		"Asset Name",
		"Decommissioned?",
		"Serial Number",
		"Shipping Date",
		"Warranty Expiry",
		"Warranty Level",
		"Ignore Warranty",
		"Third Party Warranty",
		"Manufacturer",
		"Model",
		"Asset Type",
		"OS",
		"Service Pack",
		"SQL Server Instances",
		"Recovery Method",
		"Tier",
		"Domain",
		"UUID",
		"FQDN",
		"Build Timestamp",
		"Is Virtual?",
		"Systems",
		"Created Date",
		"Updated Date",
	})
	if err != nil {
		app.serverError(w, err)
		return
	}

	for _, asset := range assets {
		var shipDate string
		if !asset.ShipDate.IsZero() {
			shipDate = asset.ShipDate.Format("2006-01-02")
		}

		var warrantyExpiryDate string
		if !asset.WarrantyExpiryDate.IsZero() {
			warrantyExpiryDate = asset.WarrantyExpiryDate.Format("2006-01-02")
		}

		var buildTimestamp string
		if !asset.BuildTimestamp.IsZero() {
			buildTimestamp = asset.BuildTimestamp.Format("2006-01-02")
		}

		var tier string
		if asset.Tier != 0 {
			tier = strconv.FormatInt(asset.Tier, 10)
		}

		var sp string
		if asset.ServicePack != "0" {
			sp = asset.ServicePack
		}

		var instances []string
		for _, instance := range *asset.SQLInstances {
			instances = append(instances, fmt.Sprintf("%s|%s", instance.SQLInstance, instance.ServiceAccount))
		}

		var systems []string
		for _, system := range *asset.Systems {
			var envs []string
			for _, env := range *system.Environments {
				envs = append(envs, fmt.Sprintf("(%s)", env.Name))
			}

			systems = append(systems, fmt.Sprintf("%s %s", system.System, strings.Join(envs, "")))
		}

		err = csvFile.Write([]string{
			asset.Hostname,
			strconv.FormatBool(*asset.Decommissioned),
			asset.SerialNumber,
			shipDate,
			warrantyExpiryDate,
			asset.WarrantyLevel,
			strconv.FormatBool(*asset.IgnoreWarranty),
			strconv.FormatBool(*asset.ThirdPartyWarranty),
			asset.Manufacturer,
			asset.Model,
			asset.AssetType,
			asset.OS,
			sp,
			strings.Join(instances, ":"),
			asset.RecoveryMethod,
			tier,
			asset.Domain,
			asset.UUID,
			asset.FQDN,
			buildTimestamp,
			strconv.FormatBool(*asset.IsVirtual),
			strings.Join(systems, ":"),
			asset.CreatedAt.Format("2006-01-02"),
			asset.UpdatedAt.Format("2006-01-02"),
		})
		if err != nil {
			app.serverError(w, err)
			return
		}
	}

	csvFile.Flush()
}

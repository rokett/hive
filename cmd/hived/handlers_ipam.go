package main

import (
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) showAddNetworkPage(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "add_network.page.html", &templateData{
		Action: "add",
	})
}

func (app *application) addNetwork(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	validationErrors := make(map[string]string)

	validNetwork := true
	_, ipnet, err := net.ParseCIDR(r.PostForm.Get("network"))
	if err != nil {
		validNetwork = false
		validationErrors["network"] = "Invalid network"
	} else {
		unique, err := app.networks.IsUnique(r.PostForm.Get("network"))
		if err != nil {
			validationErrors["network"] = "problem checking that network is unique"
			fmt.Println(err)
		} else if !unique {
			validationErrors["network"] = "network is already in use"
		}
	}

	gateway := net.ParseIP(r.PostForm.Get("gateway"))
	if validNetwork && !ipnet.Contains(gateway) {
		validationErrors["gateway"] = "The specified gateway is not valid within the defined network"
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "add_network.page.html", &templateData{
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	vlan, err := strconv.ParseInt(r.PostForm.Get("vlan"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	network := models.Network{
		Network:     r.PostForm.Get("network"),
		Description: r.PostForm.Get("description"),
		Gateway:     r.PostForm.Get("gateway"),
		VLAN:        vlan,
	}

	network.ID, err = app.networks.Insert(network)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.networks.InsertNetworkIPs(network)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Created network: "+r.PostForm.Get("network"))

	http.Redirect(w, r, "/networks", http.StatusSeeOther)
}

func (app *application) deleteNetwork(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	network, err := app.networks.Get(id, "")
	if err != nil {
		app.serverError(w, err)
		return
	}

	inuse, err := app.networks.IsInUse(id)
	if err != nil {
		app.serverError(w, err)
		return
	}
	if inuse {
		app.session.Put(r, "flash", network.Network+" network is in use so cannot be deleted")
		http.Redirect(w, r, "/networks", http.StatusSeeOther)
		return
	}

	err = app.networks.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted network: "+network.Network)

	http.Redirect(w, r, "/networks", http.StatusSeeOther)
}

func (app *application) showIPAddressDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	ip, err := app.ips.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "ip.page.html", &templateData{
		IP: ip,
	})
}

func (app *application) updateIPAddress(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	ip, err := app.ips.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if r.URL.Query().Get("action") == "clear" {
		err = app.ips.Clear(id)
		if err != nil {
			app.serverError(w, err)
			return
		}

		http.Redirect(w, r, fmt.Sprintf("/networks/%d", ip.NetworkID), http.StatusSeeOther)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	ip.Description = r.PostForm.Get("description")
	ip.Hostname = r.PostForm.Get("hostname")

	ip.Reserved = false
	if r.PostForm.Get("reserved") != "" {
		ip.Reserved = true
	}

	ip.Static = false
	if r.PostForm.Get("static") != "" {
		ip.Static = true
	}

	err = app.ips.Update(ip)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Updated: "+ip.IPAddress)

	http.Redirect(w, r, fmt.Sprintf("/networks/%d", ip.NetworkID), http.StatusSeeOther)
}

func (app *application) showNetworkDetailPage(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"ips",
	}

	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	network, err := app.networks.Get(id, strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "network_detail.page.html", &templateData{
		Network: network,
	})
}

func (app *application) showNetworksPage(w http.ResponseWriter, r *http.Request) {
	networks, err := app.networks.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "networks.page.html", &templateData{
		Networks: networks,
	})
}

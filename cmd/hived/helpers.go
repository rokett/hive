package main

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

func (app *application) render(w http.ResponseWriter, r *http.Request, name string, td *templateData) {
	ts, ok := app.templateCache[name]
	if !ok {
		app.serverError(w, fmt.Errorf("the %s template does not exist", name))
		return
	}

	buf := new(bytes.Buffer)

	err := ts.Execute(buf, app.addDefaultData(td, r))
	if err != nil {
		app.serverError(w, err)
		return
	}

	w.Header().Set("Content-Type", http.DetectContentType(buf.Bytes()))

	_, err = buf.WriteTo(w)
	if err != nil {
		app.serverError(w, err)
		return
	}
}

func (app *application) serverError(w http.ResponseWriter, err error) {
	app.logger.Error(err.Error())

	httpErrorsTotal.WithLabelValues("5xx").Inc()

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func (app *application) clientError(w http.ResponseWriter, status int) {
	// We don't really care about specific status codes, just the type
	code := strconv.Itoa(status)
	switch code[0:1] {
	case "2":
		code = "2xx"
	case "3":
		code = "3xx"
	case "4":
		code = "4xx"
	case "5":
		code = "5xx"
	}

	httpErrorsTotal.WithLabelValues(code).Inc()

	http.Error(w, http.StatusText(status), status)
}

func (app *application) addDefaultData(td *templateData, r *http.Request) *templateData {
	if td == nil {
		td = &templateData{}
	}

	td.Flash = app.session.PopString(r, "flash")
	td.Environment = app.environment

	return td
}

func (app *application) isAuthenticated(r *http.Request) error {
	var missingToken bool

	token, err := r.Cookie("session_token")
	if err != nil && err != http.ErrNoCookie {
		return fmt.Errorf("unable to retrieve session token: %w", err)
	}
	if err != nil {
		missingToken = true
	}

	var tokenValue string

	if r.Header.Get("vnd-hive-application-token") != "" || r.Header.Get("Authorization") != "" {
		if r.Header.Get("vnd-hive-application-token") != "" {
			tokenValue = r.Header.Get("vnd-hive-application-token")
		}

		if r.Header.Get("Authorization") != "" {
			tokenValue = strings.Split(r.Header.Get("Authorization"), " ")[1]
		}

		// If we have a session token and a header token, and the values do not match, we need to error
		if !missingToken && token.Value != tokenValue {
			return errors.New("have both 'session_token' cookie and 'vnd-hive-application-token' header which do not match")
		}

		missingToken = false
	}

	if missingToken {
		return errors.New("token missing")
	}

	if tokenValue == "" {
		tokenValue = token.Value
	}

	valid, user, err := app.users.IsTokenValid(tokenValue)
	if err != nil {
		return err
	}
	if !valid {
		return errors.New("session token is not valid")
	}

	// If the token is being passed in the header, it is an application user not a real person.
	// The token has been verified as valid by this point, and we don't need to check LDAP for the user, so we can return now.
	if r.Header.Get("vnd-hive-application-token") != "" || r.Header.Get("Authorization") != "" {
		return nil
	}

	enabled, u, err := app.ldapAuth.IsUserEnabled(user.Upn)
	if err != nil {
		return err
	}
	if !enabled {
		return errors.New("user account is not enabled in ldap directory")
	}

	err = app.ldapAuth.IsInAllowedGroup(app.ldapAuth.AllowedGroupDN, u.GetAttributeValues("memberOf"))
	if err != nil {
		return errors.New("access denied")
	}

	return nil
}

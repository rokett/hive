package main

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) showLocationsPage(w http.ResponseWriter, r *http.Request) {
	locations, err := app.locations.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "locations.page.html", &templateData{
		Locations: locations,
	})
}

func (app *application) showAddLocationForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "location_detail.page.html", &templateData{
		Action: "add",
	})
}

func (app *application) addLocation(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	location := models.Location{
		Office:    r.PostForm.Get("office"),
		FirstLine: r.PostForm.Get("firstLine"),
		Street:    r.PostForm.Get("street"),
		City:      r.PostForm.Get("city"),
		County:    r.PostForm.Get("county"),
		PostCode:  r.PostForm.Get("postCode"),
		Telephone: strings.ReplaceAll(r.PostForm.Get("telephone"), " ", ""),
	}

	validationErrors, err := app.locations.Validate(location)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "location_detail.page.html", &templateData{
			Action:     "add",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.locations.Insert(location)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Created "+location.Office)

	http.Redirect(w, r, "/locations", http.StatusSeeOther)
}

func (app *application) updateLocation(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	location, err := app.locations.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	location.Office = r.PostForm.Get("office")
	location.FirstLine = r.PostForm.Get("firstLine")
	location.Street = r.PostForm.Get("street")
	location.City = r.PostForm.Get("city")
	location.County = r.PostForm.Get("county")
	location.PostCode = r.PostForm.Get("postCode")
	location.Telephone = strings.ReplaceAll(r.PostForm.Get("telephone"), " ", "")

	validationErrors, err := app.locations.Validate(location)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		r.PostForm.Set("id", chi.URLParam(r, "id"))

		app.render(w, r, "location_detail.page.html", &templateData{
			Action:     "view",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.locations.Update(location)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Updated "+location.Office)

	http.Redirect(w, r, "/locations", http.StatusSeeOther)
}

func (app *application) showLocationDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	location, err := app.locations.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.FormatInt(location.ID, 10))
	form.Set("office", location.Office)
	form.Set("firstLine", location.FirstLine)
	form.Set("street", location.Street)
	form.Set("city", location.City)
	form.Set("county", location.County)
	form.Set("postCode", location.PostCode)
	form.Set("telephone", location.Telephone)

	app.render(w, r, "location_detail.page.html", &templateData{
		Action:   "view",
		FormData: form,
	})
}

func (app *application) deleteLocation(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	location, err := app.locations.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.locations.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted "+location.Office)

	http.Redirect(w, r, "/locations", http.StatusSeeOther)
}

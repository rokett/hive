package main

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) showDepartmentsPage(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"directorate",
		"organisation",
	}

	departments, err := app.departments.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "departments.page.html", &templateData{
		Departments: departments,
	})
}

func (app *application) showAddDepartmentForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "department_detail.page.html", &templateData{
		Action: "add",
	})
}

func (app *application) addDepartment(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	department := models.Department{
		Name: r.PostForm.Get("name"),
	}

	validationErrors, err := app.departments.Validate(department, r.PostForm.Get("directorate"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "department_detail.page.html", &templateData{
			Action:     "add",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.departments.Insert(department, r.PostForm.Get("directorate"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Created "+department.Name)

	http.Redirect(w, r, "/departments", http.StatusSeeOther)
}

func (app *application) updateDepartment(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	department, err := app.departments.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	department.Name = r.PostForm.Get("name")

	validationErrors, err := app.departments.Validate(department, r.PostForm.Get("directorate"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		r.PostForm.Set("id", chi.URLParam(r, "id"))

		app.render(w, r, "department_detail.page.html", &templateData{
			Action:     "view",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.departments.Update(department, r.PostForm.Get("directorate"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Updated "+department.Name)

	http.Redirect(w, r, "/departments", http.StatusSeeOther)
}

func (app *application) showDepartmentDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	department, err := app.departments.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	dir, err := app.directorates.Get(department.DirectorateID)
	if err != nil {
		app.serverError(w, err)
		return
	}

	org, err := app.organisations.Get(dir.OrganisationID)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.FormatInt(department.ID, 10))
	form.Set("name", department.Name)
	form.Set("directorate", dir.Name)
	form.Set("organisation", org.Organisation)

	app.render(w, r, "department_detail.page.html", &templateData{
		Action:   "view",
		FormData: form,
	})
}

func (app *application) deleteDepartment(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	department, err := app.departments.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.departments.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted "+department.Name)

	http.Redirect(w, r, "/departments", http.StatusSeeOther)
}

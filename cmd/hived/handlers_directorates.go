package main

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) showDirectoratesPage(w http.ResponseWriter, r *http.Request) {
	rels := []string{
		"departments",
		"organisation",
	}

	directorates, err := app.directorates.List(strings.Join(rels, ","))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "directorates.page.html", &templateData{
		Directorates: directorates,
	})
}

func (app *application) showAddDirectorateForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "directorate_detail.page.html", &templateData{
		Action: "add",
	})
}

func (app *application) showDirectorateDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	directorate, err := app.directorates.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	org, err := app.organisations.Get(directorate.OrganisationID)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.FormatInt(directorate.ID, 10))
	form.Set("name", directorate.Name)
	form.Set("organisation", org.Organisation)

	app.render(w, r, "directorate_detail.page.html", &templateData{
		Action:   "view",
		FormData: form,
	})
}

func (app *application) addDirectorate(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	directorate := models.Directorate{
		Name: r.PostForm.Get("name"),
	}

	validationErrors, err := app.directorates.Validate(directorate, r.PostForm.Get("organisation"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "directorate_detail.page.html", &templateData{
			Action:     "add",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.directorates.Insert(directorate, r.PostForm.Get("organisation"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Created "+directorate.Name)

	http.Redirect(w, r, "/directorates", http.StatusSeeOther)
}

func (app *application) updateDirectorate(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	directorate, err := app.directorates.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	directorate.Name = r.PostForm.Get("name")

	validationErrors, err := app.directorates.Validate(directorate, r.PostForm.Get("organisation"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		r.PostForm.Set("id", chi.URLParam(r, "id"))

		app.render(w, r, "directorate_detail.page.html", &templateData{
			Action:     "view",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.directorates.Update(directorate, r.PostForm.Get("organisation"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Updated "+directorate.Name)

	http.Redirect(w, r, "/directorates", http.StatusSeeOther)
}

func (app *application) deleteDirectorate(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	directorate, err := app.directorates.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.directorates.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted "+directorate.Name)

	http.Redirect(w, r, "/directorates", http.StatusSeeOther)
}

func (app *application) searchDirectorates(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	directorates, err := app.directorates.SearchByName(r.PostForm.Get("directorate"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "directorates.partial.html", &templateData{
		Directorates: directorates,
	})
}

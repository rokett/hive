package main

import (
	"net/http"
	"net/url"
	"strconv"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/models"
)

func (app *application) showOrganisationsPage(w http.ResponseWriter, r *http.Request) {
	organisations, err := app.organisations.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "organisations.page.html", &templateData{
		Organisations: organisations,
	})
}

func (app *application) showOrganisationDetailPage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	organisation, err := app.organisations.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.FormatInt(organisation.ID, 10))
	form.Set("organisation", organisation.Organisation)

	app.render(w, r, "organisation_detail.page.html", &templateData{
		Action:   "view",
		FormData: form,
	})
}

func (app *application) deleteOrganisation(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	organisation, err := app.organisations.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.organisations.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Deleted "+organisation.Organisation)

	http.Redirect(w, r, "/organisations", http.StatusSeeOther)
}

func (app *application) showAddOrganisationForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "organisation_detail.page.html", &templateData{
		Action: "add",
	})
}

func (app *application) addOrganisation(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	organisation := models.Organisation{
		Organisation: r.PostForm.Get("organisation"),
	}

	validationErrors, err := app.organisations.Validate(organisation)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "organisation_detail.page.html", &templateData{
			Action:     "add",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.organisations.Insert(organisation)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Created "+organisation.Organisation)

	http.Redirect(w, r, "/organisations", http.StatusSeeOther)
}

func (app *application) searchOrganisations(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	organisations, err := app.organisations.SearchByName(r.PostForm.Get("organisation"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "organisations.partial.html", &templateData{
		Organisations: organisations,
	})
}

func (app *application) updateOrganisation(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	organisation, err := app.organisations.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	organisation.Organisation = r.PostForm.Get("organisation")

	validationErrors, err := app.organisations.Validate(organisation)
	if err != nil {
		app.serverError(w, err)
		return
	}

	if len(validationErrors) > 0 {
		r.PostForm.Set("id", chi.URLParam(r, "id"))

		app.render(w, r, "organisation_detail.page.html", &templateData{
			Action:     "view",
			FormData:   r.PostForm,
			FormErrors: validationErrors,
		})
		return
	}

	err = app.organisations.Update(organisation)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Updated "+organisation.Organisation)

	http.Redirect(w, r, "/organisations", http.StatusSeeOther)
}

package main

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"strings"
	"time"

	"rokett.me/hive"
	"rokett.me/hive/internal/models"

	"github.com/BurntSushi/toml"
)

func loadConfig(configFile string, logger *slog.Logger) models.Config {
	var config models.Config

	if configFile == "" {
		exe, err := os.Executable()
		if err != nil {
			logger.Error(
				"get executable path",
				"error", err.Error(),
			)
			os.Exit(1)
		}
		path := filepath.Dir(exe)
		configFile = fmt.Sprintf("%s/config.toml", path)
	}

	// Load/create the config file
	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		logger.Error(
			"the configuration file does not exist; creating template config file",
			"error", err.Error(),
		)

		tmpl := hive.ConfigTmpl

		f, err := os.Create("config.toml")
		if err != nil {
			logger.Error(
				"create config file",
				"error", err.Error(),
				"config_file", "config.toml2",
			)
			os.Exit(1)
		}
		defer f.Close()

		_, err = f.Write(tmpl)
		if err != nil {
			logger.Error(
				"write config template to file",
				"error", err.Error(),
			)
			os.Exit(1)
		}

		err = f.Sync()
		if err != nil {
			logger.Error(
				"commit config template to file",
				"error", err.Error(),
			)
			os.Exit(1)
		}

		fmt.Println("Config file created.  Check the configuration before running this application again")

		os.Exit(0)
	}

	_, err := toml.DecodeFile(configFile, &config)
	if err != nil {
		logger.Error(
			"decode config file",
			"error", err.Error(),
		)
		os.Exit(1)
	}

	// Only daily, hourly or every x is allowed for the network scan interval
	NWScanIntervalParts := strings.Split(config.Server.NetworkScanInterval, " ")
	switch NWScanIntervalParts[0] {
	case "daily", "hourly":

	case "every":
		_, err := time.ParseDuration(NWScanIntervalParts[1])
		if err != nil {
			logger.Error(
				"parse network_scan_interval config",
				"error", err.Error(),
				"network_scan_interval", config.Server.NetworkScanInterval,
			)
			os.Exit(1)
		}
	default:
		logger.Error(
			"invalid network_scan_interval",
			"error", err.Error(),
			"network_scan_interval", config.Server.NetworkScanInterval,
		)
		os.Exit(1)
	}

	// Only daily, hourly or every x is allowed for the warranty update interval
	WUpdateIntervalParts := strings.Split(config.Server.WarrantyUpdateInterval, " ")
	switch WUpdateIntervalParts[0] {
	case "daily", "hourly":

	case "every":
		_, err := time.ParseDuration(WUpdateIntervalParts[1])
		if err != nil {
			logger.Error(
				"parse warranty_update_interval config",
				"error", err.Error(),
				"warranty_update_interval", config.Server.WarrantyUpdateInterval,
			)
			os.Exit(1)
		}
	default:
		logger.Error(
			"invalid warranty_update_interval",
			"error", err.Error(),
			"warranty_update_interval", config.Server.WarrantyUpdateInterval,
		)
		os.Exit(1)
	}

	if len(config.Cors.AllowedHeaders) == 0 {
		config.Cors.AllowedHeaders = []string{"*"}
	}

	return config
}

package main

import "net/http"

func (app *application) searchServices(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	services, err := app.services.SearchByName(r.PostForm.Get("related_services"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "services.partial.html", &templateData{
		Services: services,
	})
}

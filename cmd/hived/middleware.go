package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"

	"github.com/felixge/httpsnoop"
)

func (app *application) logMetrics(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Call the httpsnoop.CaptureMetrics() function, passing in the next handler in
		// the chain along with the existing http.ResponseWriter and http.Request.
		metrics := httpsnoop.CaptureMetrics(next, w, r)

		// We don't really care about specific status codes, just the type
		status := strconv.Itoa(metrics.Code)
		switch status[0:1] {
		case "2":
			status = "2xx"
		case "3":
			status = "3xx"
		case "4":
			status = "4xx"
		case "5":
			status = "5xx"
		}

		httpRequestsTotal.WithLabelValues(r.URL.Path, r.Method, status).Inc()
		httpRequestDurationSeconds.WithLabelValues(r.URL.Path, r.Method).Observe(metrics.Duration.Seconds())
	})
}

func (app *application) cacheControl(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.String(), "static") {
			w.Header().Add("Cache-Control", "public, max-age=31536000")
		}

		next.ServeHTTP(w, r)
	})
}

func (app *application) secureHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-XSS-Protection", "1; mode=block")
		w.Header().Set("X-Frame-Options", "deny")

		next.ServeHTTP(w, r)
	})
}

func (app *application) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/status" {
			app.logger.Info(
				"incoming request",
				"client_ip", r.Context().Value(clientIPCtxKey).(string),
				"method", r.Method,
				"url", r.URL.String(),
				"host", r.Host,
				"referer", r.Header.Get("Referer"),
			)
		}

		next.ServeHTTP(w, r)
	})
}

func (app *application) recoverPanic(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Create a deferred function (which will always be run in the event
		// of a panic as Go unwinds the stack).
		defer func() {
			// Use the builtin recover function to check if there has been a
			// panic or not. If there has...
			if err := recover(); err != nil {
				// Set a "Connection: close" header on the response.
				w.Header().Set("Connection", "close")
				// Call the app.serverError helper method to return a 500
				// Internal Server response.
				app.serverError(w, fmt.Errorf("%s", err))
			}
		}()

		next.ServeHTTP(w, r)
	})
}

func (app *application) getClientIP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Retrieve the client IP from the remote address of the request
		clientIP, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			app.logger.Error(
				"retrieving 'RemoteAddr' HTTP header",
				"error", err.Error(),
			)

			clientIP = "0.0.0.0"
		}

		// If the request has passed through a proxy, the RemoteAddr may actually end up being the IP of the proxy.
		// Depending on how the proxy is configured, the X-Forwarded-For header may have been inserted to show the REAL client IP.
		// If it's there, we'll grab it and use it.
		// X-Forwarded-For could be a comma separated list, so we'll split it and then select the first entry which is the real client IP.
		// See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For for more info.
		if r.Header.Get("X-Forwarded-For") != "" {
			xForwardedFor := strings.Split(r.Header.Get("X-Forwarded-For"), ",")
			clientIP = strings.TrimSpace(xForwardedFor[0])
		}

		ctx := context.WithValue(r.Context(), clientIPCtxKey, clientIP)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (app *application) requireAuthentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		conn, err := app.ldapAuth.Connect()
		if err != nil {
			app.logger.Error(err.Error())
			app.serverError(w, err)
			return
		}

		app.ldapAuth.Conn = conn
		defer app.ldapAuth.Conn.Close()

		err = app.isAuthenticated(r)
		if err != nil {
			app.logger.Warn(err.Error())
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}

		// Otherwise set the "Cache-Control: no-store" header so that pages
		// require authentication are not stored in the users browser cache (or
		// other intermediary cache).
		w.Header().Add("Cache-Control", "no-store")

		next.ServeHTTP(w, r.WithContext(r.Context()))
	})
}

package main

import (
	"html/template"
	"log/slog"

	"github.com/golangcollege/sessions"
	"github.com/jmoiron/sqlx"
	"github.com/nats-io/nats.go"
	"rokett.me/hive/internal/ldap"
	"rokett.me/hive/internal/models"
)

type application struct {
	accreditations *models.AccreditationModel
	assets         *models.AssetModel
	config         models.Config
	databases      *models.DatabaseModel
	databaseFiles  *models.DatabaseFileModel
	db             *sqlx.DB
	departments    *models.DepartmentModel
	directorates   *models.DirectorateModel
	documentation  *models.DocumentationModel
	environment    string
	environments   *models.EnvironmentModel
	js             nats.JetStreamContext
	ldapAuth       *ldap.Config
	locations      *models.LocationModel
	logger         *slog.Logger
	ips            *models.IPModel
	networks       *models.NetworkModel
	organisations  *models.OrganisationModel
	session        *sessions.Session
	services       *models.ServiceModel
	software       *models.SoftwareModel
	sqlInstances   *models.SQLInstanceModel
	suppliers      *models.SupplierModel
	systems        *models.SystemModel
	templateCache  map[string]*template.Template
	users          *models.UserModel
	volumes        *models.VolumeModel
}

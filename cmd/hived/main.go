package main

import (
	"flag"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"time"

	"rokett.me/hive/internal/ldap"
	"rokett.me/hive/internal/models"

	"github.com/golangcollege/sessions"
)

type contextKeyType string

const clientIPCtxKey contextKeyType = "client_ip"

var (
	app     = "Hive"
	version string
	build   string

	debugging = flag.Bool("debug", false, "Enable debugging?")
	dev       = flag.Bool("dev", false, "Signifies running in development; useful for plain text logging")
)

func main() {
	configFile := flag.String("config", "", "Path to config file, including filename")
	versionFlg := flag.Bool("version", false, "Display application version")
	helpFlg := flag.Bool("help", false, "Display application help")

	flag.Parse()

	if *versionFlg {
		fmt.Printf("%s v%s build %s\n", app, version, build)
		os.Exit(0)
	}

	if *helpFlg {
		flag.PrintDefaults()
		os.Exit(0)
	}

	logger := setupLogger(app, version, build, *dev)

	//audit.App = app
	//audit.Version = version
	//audit.Build = build

	config := loadConfig(*configFile, logger)

	db := openDB(config, logger)
	defer db.Close()

	err := runMigrations(db, logger)
	if err != nil {
		os.Exit(1)
	}

	// Servers communicate with assets via NATS.
	// NATS should be running as an external service.
	// Let's setup the client so that messages can be published and received.
	nc, js := setupNATSClient(config.NATS.Host, config.NATS.Port, config.NATS.StreamReplicas, config.NATS.NKeySeed, logger)
	defer nc.Close()

	templateCache, err := newTemplateCache()
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	session := sessions.New([]byte(config.Server.SessionSecret))
	session.Lifetime = 12 * time.Hour

	app := &application{
		accreditations: &models.AccreditationModel{
			DB: db,
		},
		assets: &models.AssetModel{
			DB: db,
		},
		config: config,
		databases: &models.DatabaseModel{
			DB:     db,
			Logger: logger,
		},
		databaseFiles: &models.DatabaseFileModel{
			DB:     db,
			Logger: logger,
		},
		db: db,
		departments: &models.DepartmentModel{
			DB: db,
		},
		directorates: &models.DirectorateModel{
			DB: db,
		},
		documentation: &models.DocumentationModel{
			DB:     db,
			Logger: logger,
		},
		environment: config.Server.Environment,
		environments: &models.EnvironmentModel{
			DB: db,
		},
		js: js,
		ldapAuth: &ldap.Config{
			DirectoryServers: config.Auth.Hosts,
			Port:             config.Auth.Port,
			BaseDN:           config.Auth.BaseDN,
			BindDN:           config.Auth.BindDN,
			BindPW:           config.Auth.BindPW,
			AllowedGroupDN:   config.Auth.AllowedGroupDN,
		},
		locations: &models.LocationModel{
			DB: db,
		},
		logger: logger,
		ips: &models.IPModel{
			DB: db,
		},
		networks: &models.NetworkModel{
			DB: db,
		},
		organisations: &models.OrganisationModel{
			DB: db,
		},
		session: session,
		services: &models.ServiceModel{
			DB:     db,
			Logger: logger,
		},
		software: &models.SoftwareModel{
			DB:     db,
			Logger: logger,
		},
		sqlInstances: &models.SQLInstanceModel{
			DB:     db,
			Logger: logger,
		},
		suppliers: &models.SupplierModel{
			DB: db,
		},
		systems: &models.SystemModel{
			DB: db,
		},
		templateCache: templateCache,
		users: &models.UserModel{
			DB: db,
		},
		volumes: &models.VolumeModel{
			DB:     db,
			Logger: logger,
		},
	}

	// Test that LDAP config is correct
	conn, err := app.ldapAuth.Connect()
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
	conn.Close()

	app.runCron(db, *debugging)

	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", config.Server.Port),
		ErrorLog:     slog.NewLogLogger(app.logger.Handler(), slog.LevelError),
		Handler:      app.routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	app.logger.Info(
		"running server",
		"port", config.Server.Port,
	)
	err = srv.ListenAndServe()
	app.logger.Error(err.Error())
	os.Exit(1)
}

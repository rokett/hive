# Hive

## How to setup local Dev environment
1. Install NodeJS (v12.13.0 or higher)
2. Install SQL Server 2017 Express.
3. Configure services to startup automatically.
4. SQL Server Configuration Manager -> SQL Server Network Configuration -> Protocols for SQLEXPRESS -> Enable TCP/IP
5. Create database in SQL
6. Apply db_datareader, db_datawriter, db_ddladmin permissions to the user you'll be running Hive as.
7. Start hived in order to create the config file
8. Update the config file
9. Start hived again to run migrations and start server

## Are you behind a corporate proxy?
Stuff doesn't work that well behind a proxy server, Bower for example.  In order to tell Bower that it's behind a proxy you need to create a `.bowerrc` file in the project root with content similar to the following:

````json
{
    "proxy": "http://<proxy_url>:<port>",
    "https-proxy": "http://<proxy_url>:<port>"
}
````

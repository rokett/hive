package auditlog

import (
	"log/slog"

	"github.com/jmoiron/sqlx"
)

var (
	// App contains application name injected from func main
	App string

	// Version contains application version number injected from func main
	Version string

	// Build contains application build GUID injected from func main
	Build string
)

// Log adds an entry to the audit log
func Log(action string, op int64, oldData string, newData string, object string, id int64, db *sqlx.DB, logger *slog.Logger) {
	_, err := db.Exec("INSERT INTO audit (action, user_id, old_data, new_data, object, object_id) VALUES(@p1, @p2, @p3, @p4, @p5, @p6);", action, op, oldData, newData, object, id)
	if err != nil {
		logger.Error(
			"adding new audit entry",
			"error", err.Error(),
		)
	}
}

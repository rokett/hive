package auditlog

import (
	"log/slog"
	"time"

	"github.com/jmoiron/sqlx"
)

// DeleteOldRecords removes audit logs from the database which are older than the maximum age passed in
func DeleteOldRecords(maxAge int, db *sqlx.DB, logger *slog.Logger) {
	olderThan := time.Now().AddDate(0, 0, -maxAge)

	_, err := db.Exec("DELETE FROM audit WHERE created_at < @p1", olderThan)
	if err != nil {
		logger.Error(
			"deleting stale audit logs",
			"error", err.Error(),
		)
	}
}

package supplier

import (
	"encoding/json"
	"fmt"
	"log/slog"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
	"rokett.me/hive/internal/handlers/authentication"
	"rokett.me/hive/internal/models"

	"net/http"

	"rokett.me/hive/internal/services/supplier"
	"rokett.me/hive/internal/services/user"
	"rokett.me/hive/internal/validation"

	"github.com/jmoiron/sqlx"
)

// Create creates a new supplier
func Create(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		var suppliers []models.Supplier
		var returnedSuppliers []models.Supplier

		validParams := map[string]bool{
			"fields":  true,
			"include": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		//TODO Should do something better here for unauthenticated access really.
		var operator int64
		if r.Context().Value(authentication.OperatorCtxKey) != nil {
			operator = r.Context().Value(authentication.OperatorCtxKey).(int64)
		} else {
			var fuzzy bool
			user, err := user.SearchUserByUsername("system", "username", fuzzy, db)
			if err != nil {
				logger.Error("unable to find system user",
					"error", err.Error(),
				)

				apiErr := models.ErrSearchingUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			operator = user.ID
		}

		err := json.NewDecoder(r.Body).Decode(&suppliers)
		if err != nil {
			logger.Error("decoding json request to create supplier account",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONDecode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		for _, s := range suppliers {
			var validationErrors validation.Errors

			if s.Supplier == "" {
				validationErrors = validationErrors.Add("supplier", "You must provide the name of the supplier")
			}

			uniqueSupplier, _, err := supplier.IsUnique(s.Supplier, db)
			if err != nil {
				logger.Error("checking supplier is unique",
					"error", err.Error(),
				)

				apiErr := models.ErrIsUniqueSupplier
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}
			if s.Supplier != "" && !uniqueSupplier {
				msg := fmt.Sprintf("The supplier, %s, already exists", s.Supplier)
				validationErrors = validationErrors.Add("supplier", msg)
			}

			if len(validationErrors) > 0 {
				APIErrors = APIErrors.Add(api.ErrValidation, validationErrors)

				continue
			}

			id, err := supplier.Create(s, db)
			if err != nil {
				logger.Error("adding new supplier",
					"error", err.Error(),
				)

				apiErr := models.ErrCreatingSupplier
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)

				continue
			}

			if s.Accounts != nil {
				for _, a := range *s.Accounts {
					unique, _, err := supplier.IsUniqueSupplierUserRelationship(id, a.ID, db)
					if err != nil {
						logger.Error("checking that the supplier/user relationship is unique",
							"error", err.Error(),
						)

						apiErr := models.ErrCreatingSupplier
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
					if unique {
						err = supplier.CreateSupplierUserRelationship(id, a.ID, db)
						if err != nil {
							logger.Error("adding supplier/user relationship",
								"error", err.Error(),
							)

							apiErr := models.ErrCreatingSupplier
							apiErr.Detail = err.Error()
							APIResponse.Errors = APIErrors.Add(apiErr, nil)
							APIResponse.Send(logger, http.StatusInternalServerError, w)

							return
						}
					}
				}
			}

			if s.Accreditations != nil {
				for _, a := range *s.Accreditations {
					unique, _, err := supplier.IsUniqueSupplierAccreditationRelationship(id, a.ID, db)
					if err != nil {
						logger.Error("checking that the supplier/accreditation relationship is unique",
							"error", err.Error(),
						)

						apiErr := models.ErrCreatingSupplier
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
					if unique {
						err = supplier.CreateSupplierAccreditationRelationship(id, a.ID, db)
						if err != nil {
							logger.Error("adding supplier/accreditation relationship",
								"error", err.Error(),
							)

							apiErr := models.ErrCreatingSupplier
							apiErr.Detail = err.Error()
							APIResponse.Errors = APIErrors.Add(apiErr, nil)
							APIResponse.Send(logger, http.StatusInternalServerError, w)

							return
						}
					}
				}
			}

			if s.Systems != nil {
				for _, sys := range *s.Systems {
					unique, _, err := supplier.IsUniqueSupplierSystemRelationship(id, sys.ID, db)
					if err != nil {
						logger.Error("checking that the supplier/system relationship is unique",
							"error", err.Error(),
						)

						apiErr := models.ErrCreatingSupplier
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
					if unique {
						err = supplier.CreateSupplierSystemRelationship(id, sys.ID, db)
						if err != nil {
							logger.Error("adding supplier/system relationship",
								"error", err.Error(),
							)

							apiErr := models.ErrCreatingSupplier
							apiErr.Detail = err.Error()
							APIResponse.Errors = APIErrors.Add(apiErr, nil)
							APIResponse.Send(logger, http.StatusInternalServerError, w)

							return
						}
					}
				}
			}

			newSupplier, err := supplier.View(id, r.URL.Query().Get("fields"), r.URL.Query().Get("include"), db)
			if err != nil {
				logger.Error("viewing new supplier",
					"error", err.Error(),
					"id", id,
				)

				apiErr := models.ErrGetSupplier
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)

				continue
			}

			b, _ := json.Marshal(newSupplier)
			go auditlog.Log("create", operator, "", string(b[:]), "supplier", id, db, logger)

			returnedSuppliers = append(returnedSuppliers, newSupplier)
		}

		httpStatus := http.StatusCreated

		if len(APIErrors) > 0 {
			APIResponse.Errors = APIErrors

			httpStatus = http.StatusBadRequest
		}

		if len(returnedSuppliers) > 0 {
			APIResponse.Result = returnedSuppliers

			if len(APIErrors) > 0 {
				httpStatus = http.StatusMultiStatus
			}
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

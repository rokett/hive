package asset

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

// Checkin sends a request to an asset to checkin, and optionally returns the response to the caller
func Checkin(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		if r.URL.Query().Get("name") == "" {
			logger.Error("'name' is a required parameter when asking an asset to check-in")

			apiErr := models.ErrAssetNameRequired
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		returnJSON := false
		if r.URL.Query().Get("return_json") != "" {
			b, err := strconv.ParseBool(r.URL.Query().Get("return_json"))
			if err != nil {
				logger.Error("parsing 'return_json' querystring argument",
					"error", err.Error(),
				)

				apiErr := api.ErrParseQuerystring
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			returnJSON = b
		}

		// send request to client
		client := &http.Client{
			Timeout: 120 * time.Second,
		}

		params := url.Values{}
		params.Add("return_json", strconv.FormatBool(returnJSON))

		url := url.URL{
			Scheme:   "http",
			Host:     fmt.Sprintf("%s:1021", r.URL.Query().Get("name")),
			Path:     "checkin",
			RawQuery: params.Encode(),
		}

		resp, err := client.Get(url.String())
		if err != nil {
			logger.Error("asset check-in",
				"error", err.Error(),
				"asset", r.URL.Query().Get("name"),
				"url", url.String(),
			)

			apiErr := models.ErrAssetCheckin
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)

			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}
		defer resp.Body.Close()

		if returnJSON {
			asset := models.Asset{}
			err = json.NewDecoder(resp.Body).Decode(&asset)
			if err != nil {
				logger.Error("return asset details to check-in source",
					"error", err.Error(),
				)

				apiErr := models.ErrAssetCheckin
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)

				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			APIResponse.Result = asset
		}

		APIResponse.Send(logger, http.StatusOK, w)
	})
}

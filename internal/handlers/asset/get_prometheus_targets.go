package asset

import (
	"encoding/json"
	"log/slog"
	"net/http"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/asset"

	"github.com/jmoiron/sqlx"
)

// GetPrometheusTargets is a service discovery endpoint for Prometheus, which returns a specially formatted JSON response for service discovery purposes.
// See https://prometheus.io/docs/prometheus/latest/http_sd/.
func GetPrometheusTargets(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"environments": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		assets, err := asset.ListPrometheusTargets(r.URL.Query().Get("environments"), db)
		if err != nil {
			logger.Error("listing Prometheus targets",
				"error", err.Error(),
			)

			apiErr := models.ErrGetAsset
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)

			APIResponse.Send(logger, http.StatusBadRequest, w)
			return
		}

		json, _ := json.Marshal(assets)

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(json)
		if err != nil {
			logger.Error("sending Prometheus targets",
				"error", err.Error(),
			)
		}
	})
}

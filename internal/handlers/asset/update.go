package asset

import (
	"encoding/json"
	"log/slog"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
	"rokett.me/hive/internal/handlers/authentication"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/asset"
	"rokett.me/hive/internal/services/software"
	"rokett.me/hive/internal/services/sqlinstance"
	"rokett.me/hive/internal/services/user"
	"rokett.me/hive/internal/services/volume"

	"github.com/jmoiron/sqlx"
)

//TODO catch errors for reporting back, but there should be no need to throw us out as soon as an error occurs.  If we let the flow continue we will at least get some data updated.

// Update is used to update an asset
func Update(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		//TODO Should do something better here for unauthenticated access really.
		var operator int64
		if r.Context().Value(authentication.OperatorCtxKey) != nil {
			operator = r.Context().Value(authentication.OperatorCtxKey).(int64)
		} else {
			var fuzzy bool
			user, err := user.SearchUserByUsername("system", "username", fuzzy, db)
			if err != nil {
				logger.Error("unable to find system user",
					"error", err.Error(),
				)

				apiErr := models.ErrSearchingUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			operator = user.ID
		}

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			logger.Error("parsing ID of asset to be updated",
				"error", err.Error(),
			)

			apiErr := models.ErrGetOrganisation
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)

			APIResponse.Send(logger, http.StatusBadRequest, w)
			return
		}

		validParams := map[string]bool{
			"include": true,
			"fields":  true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		var a models.Asset

		err = json.NewDecoder(r.Body).Decode(&a)
		if err != nil {
			logger.Error("decoding json request to update asset",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONDecode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		oldDataStruct, err := asset.View(id, "", "", db)
		if err != nil {
			logger.Error("retrieving asset details to be updated",
				"error", err.Error(),
			)

			apiErr := models.ErrGetAsset
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}
		oldData, err := json.Marshal(oldDataStruct)
		if err != nil {
			logger.Error("marshalling struct to json when updating asset",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONEncode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		err = asset.Update(a, id, db)
		if err != nil {
			logger.Error("updating asset",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingAsset
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if a.Software != nil {
			err = asset.DeleteAssetSoftwareRelationship(id, db)
			if err != nil {
				logger.Error("deleting asset/software relationship when updating asset",
					"error", err.Error(),
				)

				apiErr := models.ErrUpdatingAsset
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			err = software.Create(*a.Software, db)
			if err != nil {
				logger.Error("creating software records when updating asset",
					"error", err.Error(),
				)

				apiErr := models.ErrCreatingSoftware
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			err = asset.CreateAssetSoftwareRelationship(id, *a.Software, db)
			if err != nil {
				logger.Error("adding asset/software relationships when updating asset",
					"error", err.Error(),
				)

				apiErr := models.ErrUpdatingAsset
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}
		}

		if a.Volumes != nil {
			err = volume.Upsert(*a.Volumes, id, db)
			if err != nil {
				logger.Error("handling volume records when updating asset",
					"error", err.Error(),
				)

				apiErr := models.ErrUpsertVolume
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}
		}

		if a.SQLInstances != nil {
			err = sqlinstance.Upsert(*a.SQLInstances, id, db)
			if err != nil {
				logger.Error("handling SQL Instance records when updating asset",
					"error", err.Error(),
				)

				apiErr := models.ErrUpsertSQLInstance
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}
		}

		// We don't want to include the existing events in the new event which gets logged; the log would end up very big and confusing
		includes := r.URL.Query().Get("include")
		includes = strings.Replace(includes, "events", "", -1)
		includes = strings.Replace(includes, ",,", ",", -1)

		updatedAsset, err := asset.View(id, r.URL.Query().Get("fields"), includes, db)
		if err != nil {
			logger.Error("retrieving updated asset details",
				"error", err.Error(),
			)

			apiErr := models.ErrGetAsset
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}
		updatedAss, err := json.Marshal(updatedAsset)
		if err != nil {
			logger.Error("marshalling updated asset struct to json",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONEncode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		go auditlog.Log("change", operator, string(oldData[:]), string(updatedAss[:]), "asset", id, db, logger)

		httpStatus := http.StatusOK

		if len(APIErrors) > 0 {
			APIResponse.Errors = APIErrors

			httpStatus = http.StatusBadRequest
		}

		if (updatedAsset != models.Asset{}) {
			APIResponse.Result = updatedAsset

			if len(APIErrors) > 0 {
				httpStatus = http.StatusMultiStatus
			}
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

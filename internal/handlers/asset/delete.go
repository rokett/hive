package asset

import (
	"encoding/json"
	"log/slog"
	"net/http"
	"strconv"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
	"rokett.me/hive/internal/handlers/authentication"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/asset"
	"rokett.me/hive/internal/services/user"

	"github.com/go-chi/chi/v5"
	"github.com/jmoiron/sqlx"
)

// Delete deletes an existing asset
func Delete(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"force": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		//TODO Should do something better here for unauthenticated access really.
		var operator int64
		if r.Context().Value(authentication.OperatorCtxKey) != nil {
			operator = r.Context().Value(authentication.OperatorCtxKey).(int64)
		} else {
			var fuzzy bool
			user, err := user.SearchUserByUsername("system", "username", fuzzy, db)
			if err != nil {
				logger.Error("finding system user",
					"error", err.Error(),
				)

				apiErr := models.ErrSearchingUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			operator = user.ID
		}

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			logger.Error("parsing ID of asset to be deleted",
				"error", err.Error(),
			)

			apiErr := models.ErrGetOrganisation
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)

			APIResponse.Send(logger, http.StatusBadRequest, w)
		}

		// If the force flag is not set then we want to carry out some safety checks
		if r.URL.Query().Get("force") == "" {
			inUse, err := asset.IsInUse(id, db)
			if err != nil {
				logger.Error("checking whether asset to be deleted is in use",
					"error", err.Error(),
				)

				apiErr := models.ErrIsAssetInUse
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			if inUse {
				apiErr := models.ErrAssetInUse
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}
		}

		a, err := asset.View(id, "", "", db)
		if err != nil {
			logger.Error("retrieving asset to be deleted",
				"error", err.Error(),
			)

			apiErr := models.ErrGetAsset
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		err = asset.Delete(id, db)
		if err != nil {
			logger.Error("deleting asset",
				"error", err.Error(),
			)

			apiErr := models.ErrDeletingAsset
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		b, _ := json.Marshal(a)
		go auditlog.Log("delete", operator, string(b[:]), "", "asset", id, db, logger)

		APIResponse.Send(logger, http.StatusNoContent, w)
	})
}

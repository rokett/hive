package asset

import (
	"log/slog"
	"net/http"
	"strconv"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/asset"

	"github.com/jmoiron/sqlx"
)

// Search returns a list of assets matching the search criteria
func Search(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		var httpStatus int

		validParams := map[string]bool{
			"fuzzy":       true,
			"name":        true,
			"include":     true,
			"os":          true,
			"asset_type":  true,
			"environment": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		fuzzy := false
		if r.URL.Query().Get("fuzzy") != "" {
			b, err := strconv.ParseBool(r.URL.Query().Get("fuzzy"))
			if err != nil {
				logger.Error("parsing 'fuzzy' querystring argument",
					"error", err.Error(),
				)

				apiErr := api.ErrParseFuzzy
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			fuzzy = b
		}

		assets, err := asset.Search(r.URL.Query().Get("name"), r.URL.Query().Get("include"), r.URL.Query().Get("os"), r.URL.Query().Get("asset_type"), r.URL.Query().Get("environment"), fuzzy, db)
		if err != nil {
			logger.Error("searching for assets",
				"error", err.Error(),
			)

			apiErr := models.ErrSearchAsset
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)

			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		if len(assets) == 0 {
			httpStatus = http.StatusNotFound
		} else {
			httpStatus = http.StatusOK

			APIResponse.Result = assets
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

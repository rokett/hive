package asset

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"

	"github.com/jmoiron/sqlx"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
	"rokett.me/hive/internal/handlers/authentication"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/asset"
	"rokett.me/hive/internal/services/software"
	"rokett.me/hive/internal/services/sqlinstance"
	"rokett.me/hive/internal/services/user"
	"rokett.me/hive/internal/services/volume"
	"rokett.me/hive/internal/validation"
)

// Create adds a new asset
func Create(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var assets []models.Asset
		var returnedAssets []models.Asset

		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"fields":  true,
			"include": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		//TODO Should do something better here for unauthenticated access really.
		var operator int64
		if r.Context().Value(authentication.OperatorCtxKey) != nil {
			operator = r.Context().Value(authentication.OperatorCtxKey).(int64)
		} else {
			var fuzzy bool
			user, err := user.SearchUserByUsername("system", "username", fuzzy, db)
			if err != nil {
				logger.Error("unable to find system user",
					"error", err.Error(),
				)

				apiErr := models.ErrSearchingUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			operator = user.ID
		}

		err := json.NewDecoder(r.Body).Decode(&assets)
		if err != nil {
			logger.Error("decoding json request to create asset",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONDecode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		for _, a := range assets {
			var validationErrors validation.Errors

			if a.Hostname == "" {
				validationErrors = validationErrors.Add("hostname", "You must provide an asset name")
			}

			uniqueHostname, _, err := asset.IsUniqueHostname(a.Hostname, db)
			if err != nil {
				logger.Error("checking that the asset hostname is unique",
					"error", err.Error(),
				)

				apiErr := models.ErrIsUnique
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}
			if a.Hostname != "" && !uniqueHostname {
				msg := fmt.Sprintf("Asset, %s, already exists", a.Hostname)
				validationErrors = validationErrors.Add("hostname", msg)
			}

			if a.SerialNumber != "" {
				uniqueSN, _, err := asset.IsUniqueSN(a.SerialNumber, db)
				if err != nil {
					logger.Error("checking that the asset serial number is unique",
						"error", err.Error(),
					)

					apiErr := models.ErrIsUniqueSN
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if !uniqueSN {
					msg := fmt.Sprintf("Serial number, %s, already exists", a.SerialNumber)
					validationErrors = validationErrors.Add("serial_number", msg)
				}
			}

			if a.AssetType == "" {
				validationErrors = validationErrors.Add("asset_type", "You must provide the type of asset")
			}

			if a.AssetType == "server" && a.Domain == "" {
				validationErrors = validationErrors.Add("domain", "You must provide the domain the asset is a member of")
			}

			if len(validationErrors) > 0 {
				APIErrors = APIErrors.Add(api.ErrValidation, validationErrors)

				continue
			}

			id, err := asset.Create(a, db)
			if err != nil {
				logger.Error("adding new asset via API",
					"error", err.Error(),
				)

				apiErr := models.ErrCreatingAsset
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)

				continue
			}

			if a.Software != nil {
				err = software.Create(*a.Software, db)
				if err != nil {
					logger.Error("creating software records for new asset",
						"error", err.Error(),
					)

					apiErr := models.ErrCreatingSoftware
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}

				err = asset.CreateAssetSoftwareRelationship(id, *a.Software, db)
				if err != nil {
					logger.Error("adding asset/software relationships",
						"error", err.Error(),
					)

					apiErr := models.ErrCreatingAsset
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
			}

			if a.Volumes != nil {
				err = volume.Upsert(*a.Volumes, id, db)
				if err != nil {
					logger.Error("handling volume records when creating new asset",
						"error", err.Error(),
					)

					apiErr := models.ErrUpsertVolume
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
			}

			if a.SQLInstances != nil {
				err = sqlinstance.Upsert(*a.SQLInstances, id, db)
				if err != nil {
					logger.Error("handling SQL Instance records when creating new asset",
						"error", err.Error(),
					)

					apiErr := models.ErrUpsertSQLInstance
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
			}

			if a.Systems != nil {
				for _, sys := range *a.Systems {
					var envs []models.Environment

					if len(*sys.Environments) == 0 {
						envs = append(envs, models.Environment{
							ID: 0,
						})
					} else {
						envs = *sys.Environments
					}

					for _, env := range envs {
						unique, _, err := asset.IsUniqueAssetSystemRelationship(id, sys.ID, env.ID, db)
						if err != nil {
							logger.Error("checking that the asset/system relationship is unique",
								"error", err.Error(),
							)

							apiErr := models.ErrCreatingAsset
							apiErr.Detail = err.Error()
							APIResponse.Errors = APIErrors.Add(apiErr, nil)
							APIResponse.Send(logger, http.StatusInternalServerError, w)

							return
						}
						if unique {
							err = asset.CreateAssetSystemRelationship(id, sys.ID, env.ID, db)
							if err != nil {
								logger.Error("adding asset/system relationship",
									"error", err.Error(),
								)

								apiErr := models.ErrCreatingAsset
								apiErr.Detail = err.Error()
								APIResponse.Errors = APIErrors.Add(apiErr, nil)
								APIResponse.Send(logger, http.StatusInternalServerError, w)

								return
							}
						}
					}
				}
			}

			newAsset, err := asset.View(id, r.URL.Query().Get("fields"), r.URL.Query().Get("include"), db)
			if err != nil {
				logger.Error("viewing newly created asset",
					"error", err.Error(),
					"id", id,
				)

				apiErr := models.ErrGetAsset
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)

				continue
			}

			b, _ := json.Marshal(newAsset)
			go auditlog.Log("create", operator, "", string(b[:]), "asset", id, db, logger)

			returnedAssets = append(returnedAssets, newAsset)
		}

		httpStatus := http.StatusCreated

		if len(APIErrors) > 0 {
			APIResponse.Errors = APIErrors

			httpStatus = http.StatusBadRequest
		}

		if len(returnedAssets) > 0 {
			APIResponse.Result = returnedAssets

			if len(APIErrors) > 0 {
				httpStatus = http.StatusMultiStatus
			}
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

package system

import (
	"log/slog"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"

	"net/http"

	"rokett.me/hive/internal/services/system"

	"github.com/jmoiron/sqlx"
)

// GetAll returns all systems
func GetAll(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"fields":  true,
			"include": true,
			"sort":    true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		var httpStatus int

		systems, err := system.List(r.URL.Query().Get("fields"), r.URL.Query().Get("include"), r.URL.Query().Get("sort"), db)
		if err != nil {
			logger.Error("retrieving all systems",
				"error", err.Error(),
			)

			apiErr := models.ErrGetSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			httpStatus = http.StatusBadRequest
		} else {
			httpStatus = http.StatusOK

			APIResponse.Result = systems
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

package system

import (
	"encoding/json"
	"log/slog"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
	"rokett.me/hive/internal/handlers/authentication"
	"rokett.me/hive/internal/models"

	"net/http"
	"strconv"
	"strings"

	"rokett.me/hive/internal/services/system"
	"rokett.me/hive/internal/services/user"

	"github.com/jmoiron/sqlx"
)

// Update updates a system
func Update(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"fields":  true,
			"include": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		//TODO Should do something better here for unauthenticated access really.
		var operator int64
		if r.Context().Value(authentication.OperatorCtxKey) != nil {
			operator = r.Context().Value(authentication.OperatorCtxKey).(int64)
		} else {
			var fuzzy bool
			user, err := user.SearchUserByUsername("system", "username", fuzzy, db)
			if err != nil {
				logger.Error("unable to find system user",
					"error", err.Error(),
				)

				apiErr := models.ErrSearchingUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			operator = user.ID
		}

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			logger.Error("parsing ID of system to be updated",
				"error", err.Error(),
			)

			apiErr := models.ErrGetOrganisation
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)

			APIResponse.Send(logger, http.StatusBadRequest, w)
			return
		}

		var s models.System

		err = json.NewDecoder(r.Body).Decode(&s)
		if err != nil {
			logger.Error("error decoding JSON request",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONDecode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		oldDataStruct, err := system.View(id, "", "", db)
		if err != nil {
			logger.Error("retrieving system details to be updated",
				"error", err.Error(),
			)

			apiErr := models.ErrGetSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}
		oldData, err := json.Marshal(oldDataStruct)
		if err != nil {
			logger.Error("marshalling struct to json when updating system",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONEncode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		err = system.Update(s, id, db)
		if err != nil {
			logger.Error("updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		err = system.DeleteSystemServiceRelationship(id, db)
		if err != nil {
			logger.Error("deleting service/system relationship when updating asset",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.Services != nil {
			for _, svc := range *s.Services {
				unique, _, err := system.IsUniqueSystemServiceRelationship(id, svc.ID, db)
				if err != nil {
					logger.Error("checking that service/system relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemServiceRelationship(id, svc.ID, db)
					if err != nil {
						logger.Error("adding service/system relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		err = system.DeleteSystemAssetRelationship(id, db)
		if err != nil {
			logger.Error("deleting asset/system relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.Assets != nil {
			for _, a := range *s.Assets {
				var envs []models.Environment

				if a.Environments != nil {
					if len(*a.Environments) == 0 {
						envs = append(envs, models.Environment{
							ID: 0,
						})
					} else {
						envs = *a.Environments
					}

					for _, env := range envs {
						unique, _, err := system.IsUniqueSystemAssetRelationship(id, a.ID, env.ID, db)
						if err != nil {
							logger.Error("checking that system/asset relationship is unique when updating system",
								"error", err.Error(),
							)

							apiErr := models.ErrUpdatingSystem
							apiErr.Detail = err.Error()
							APIResponse.Errors = APIErrors.Add(apiErr, nil)
							APIResponse.Send(logger, http.StatusInternalServerError, w)

							return
						}
						if unique {
							err = system.CreateSystemAssetRelationship(id, a.ID, env.ID, db)
							if err != nil {
								logger.Error("adding system/asset relationship when updating system",
									"error", err.Error(),
								)

								apiErr := models.ErrUpdatingSystem
								apiErr.Detail = err.Error()
								APIResponse.Errors = APIErrors.Add(apiErr, nil)
								APIResponse.Send(logger, http.StatusInternalServerError, w)

								return
							}
						}
					}
				}
			}
		}

		err = system.DeleteSystemSupplierRelationship(id, db)
		if err != nil {
			logger.Error("deleting system/supplier relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.Suppliers != nil {
			for _, sup := range *s.Suppliers {
				unique, _, err := system.IsUniqueSystemSupplierRelationship(id, sup.ID, db)
				if err != nil {
					logger.Error("checking that system/supplier relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemSupplierRelationship(id, sup.ID, db)
					if err != nil {
						logger.Error("adding system/supplier relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		err = system.DeleteSystemOrganisationRelationship(id, db)
		if err != nil {
			logger.Error("deleting system/organisation relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.Organisations != nil {
			for _, org := range *s.Organisations {
				unique, _, err := system.IsUniqueSystemOrganisationRelationship(id, org.ID, db)
				if err != nil {
					logger.Error("checking that system/organisation relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemOrganisationRelationship(id, org.ID, db)
					if err != nil {
						logger.Error("adding system/organisation relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		err = system.DeleteSystemUserRelationship(id, "developer", db)
		if err != nil {
			logger.Error("deleting system/user developer relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.Developers != nil {
			for _, u := range *s.Developers {
				unique, _, err := system.IsUniqueSystemUserRelationship(id, u.ID, "developer", db)
				if err != nil {
					logger.Error("checking that system/user developer relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemUserRelationship(id, u.ID, "developer", db)
					if err != nil {
						logger.Error("adding system/user developer relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		err = system.DeleteSystemUserRelationship(id, "system manager", db)
		if err != nil {
			logger.Error("deleting system/user system manager relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.SystemManagers != nil {
			for _, u := range *s.SystemManagers {
				unique, _, err := system.IsUniqueSystemUserRelationship(id, u.ID, "system manager", db)
				if err != nil {
					logger.Error("checking that system/user system manager relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemUserRelationship(id, u.ID, "system manager", db)
					if err != nil {
						logger.Error("adding system/user system manager relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		err = system.DeleteSystemUserRelationship(id, "iao", db)
		if err != nil {
			logger.Error("deleting system/user iao relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.IAOs != nil {
			for _, u := range *s.IAOs {
				unique, _, err := system.IsUniqueSystemUserRelationship(id, u.ID, "iao", db)
				if err != nil {
					logger.Error("checking that system/user iao relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemUserRelationship(id, u.ID, "iao", db)
					if err != nil {
						logger.Error("adding system/user iao relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		err = system.DeleteSystemUserRelationship(id, "authorised user", db)
		if err != nil {
			logger.Error("deleting system/user authorised user relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.AuthorisedUsers != nil {
			for _, u := range *s.AuthorisedUsers {
				unique, _, err := system.IsUniqueSystemUserRelationship(id, u.ID, "authorised user", db)
				if err != nil {
					logger.Error("checking that system/user authorised user relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemUserRelationship(id, u.ID, "authorised user", db)
					if err != nil {
						logger.Error("adding system/user authorised user relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		err = system.DeleteSystemEnvironmentRelationship(id, db)
		if err != nil {
			logger.Error("deleting system/environment relationship when updating system",
				"error", err.Error(),
			)

			apiErr := models.ErrUpdatingSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		if s.Environments != nil {
			for _, org := range *s.Environments {
				unique, _, err := system.IsUniqueSystemEnvironmentRelationship(id, org.ID, db)
				if err != nil {
					logger.Error("checking that system/environment relationship is unique when updating system",
						"error", err.Error(),
					)

					apiErr := models.ErrUpdatingSystem
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}
				if unique {
					err = system.CreateSystemEnvironmentRelationship(id, org.ID, db)
					if err != nil {
						logger.Error("adding system/environment relationship when updating system",
							"error", err.Error(),
						)

						apiErr := models.ErrUpdatingSystem
						apiErr.Detail = err.Error()
						APIResponse.Errors = APIErrors.Add(apiErr, nil)
						APIResponse.Send(logger, http.StatusInternalServerError, w)

						return
					}
				}
			}
		}

		// We don't want to include the existing events in the new event which gets logged; the log would end up very big and confusing
		includes := r.URL.Query().Get("include")
		includes = strings.Replace(includes, "events", "", -1)
		includes = strings.Replace(includes, ",,", ",", -1)

		updatedSystem, err := system.View(id, r.URL.Query().Get("fields"), includes, db)
		if err != nil {
			logger.Error("retrieving updated system details",
				"error", err.Error(),
			)

			apiErr := models.ErrGetSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}
		updatedSys, err := json.Marshal(updatedSystem)
		if err != nil {
			logger.Error("marshalling updated system struct to json",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONEncode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusInternalServerError, w)

			return
		}

		go auditlog.Log("change", operator, string(oldData[:]), string(updatedSys[:]), "system", id, db, logger)

		httpStatus := http.StatusOK

		if len(APIErrors) > 0 {
			APIResponse.Errors = APIErrors

			httpStatus = http.StatusBadRequest
		}

		if (updatedSystem != models.System{}) {
			APIResponse.Result = updatedSystem

			if len(APIErrors) > 0 {
				httpStatus = http.StatusMultiStatus
			}
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

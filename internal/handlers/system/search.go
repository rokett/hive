package system

import (
	"log/slog"
	"strconv"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"

	"net/http"

	"rokett.me/hive/internal/services/system"

	"github.com/jmoiron/sqlx"
)

// Search returns a list of systems matching the search criteria
func Search(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		var httpStatus int

		validParams := map[string]bool{
			"fuzzy":     true,
			"system":    true,
			"shortcode": true,
			"include":   true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		fuzzy := false
		if r.URL.Query().Get("fuzzy") != "" {
			b, err := strconv.ParseBool(r.URL.Query().Get("fuzzy"))
			if err != nil {
				logger.Error("parsing 'fuzzy' querystring argument",
					"error", err.Error(),
				)

				apiErr := api.ErrParseFuzzy
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			fuzzy = b
		}

		systems, err := system.Search(r.URL.Query().Get("system"), r.URL.Query().Get("shortcode"), r.URL.Query().Get("include"), fuzzy, db)
		if err != nil {
			logger.Error("searching for systems",
				"error", err.Error(),
			)

			apiErr := models.ErrSearchSystem
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			httpStatus = http.StatusBadRequest

			APIResponse.Send(logger, httpStatus, w)

			return
		}

		if len(systems) == 0 {
			httpStatus = http.StatusNotFound
		} else {
			httpStatus = http.StatusOK

			APIResponse.Result = systems
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

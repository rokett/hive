package ldap

import (
	"log/slog"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"

	"net/http"
	"strconv"

	"rokett.me/hive/internal/services/ldap"
)

// Search carries out a search against an LDAP server
func Search(cfg models.Config, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"fuzzy":           true,
			"includeDisabled": true,
			"sn":              true,
			"username":        true,
			"computername":    true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		fuzzy := false
		if r.URL.Query().Get("fuzzy") != "" {
			b, err := strconv.ParseBool(r.URL.Query().Get("fuzzy"))
			if err != nil {
				logger.Error("parsing 'fuzzy' querystring argument",
					"error", err.Error(),
				)

				apiErr := api.ErrParseFuzzy
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			fuzzy = b
		}

		var includeDisabled bool
		if r.URL.Query().Get("includeDisabled") != "" {
			b, err := strconv.ParseBool(r.URL.Query().Get("includeDisabled"))
			if err != nil {
				logger.Error("parsing 'includeDisabled' querystring argument",
					"error", err.Error(),
				)

				apiErr := api.ErrParseIncludeDisabled
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			includeDisabled = b
		}

		var users []models.User
		var computers []models.LDAPComputer

		if r.URL.Query().Get("sn") != "" {
			for _, l := range cfg.LDAP {
				res, err := ldap.SearchUserBySN(l.Hosts[0], l.Port, l.BaseDN, l.BindDN, l.BindPW, r.URL.Query().Get("sn"), fuzzy, includeDisabled)
				if err != nil {
					logger.Error("searching LDAP directory for user by sn",
						"error", err.Error(),
						"surname", r.URL.Query().Get("sn"),
					)

					apiErr := models.ErrSearchingLDAPUser
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}

				users = append(users, res...)
			}

			APIResponse.Result = users
		}

		if r.URL.Query().Get("username") != "" {
			for _, l := range cfg.LDAP {
				res, err := ldap.SearchUserByUsername(l.Hosts[0], l.Port, l.BaseDN, l.BindDN, l.BindPW, r.URL.Query().Get("username"), fuzzy, includeDisabled)
				if err != nil {
					logger.Error("searching LDAP directory for user by username",
						"error", err.Error(),
						"username", r.URL.Query().Get("username"),
					)

					apiErr := models.ErrSearchingLDAPUser
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}

				users = append(users, res...)
			}

			APIResponse.Result = users
		}

		if r.URL.Query().Get("computername") != "" {
			for _, l := range cfg.LDAP {
				res, err := ldap.SearchComputerByComputerName(l.Hosts[0], l.Port, l.BaseDN, l.BindDN, l.BindPW, r.URL.Query().Get("computername"), fuzzy)
				if err != nil {
					logger.Error("searching LDAP directory for computer by computer name",
						"error", err.Error(),
						"computer name", r.URL.Query().Get("computername"),
					)

					apiErr := models.ErrSearchingLDAPComputer
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)
					APIResponse.Send(logger, http.StatusInternalServerError, w)

					return
				}

				computers = append(computers, res...)
			}

			APIResponse.Result = computers
		}

		APIResponse.Send(logger, http.StatusOK, w)
	})
}

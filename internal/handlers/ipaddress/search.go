package ipaddress

import (
	"log/slog"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"

	"net/http"
	"strconv"

	"rokett.me/hive/internal/services/ipaddress"

	"github.com/jmoiron/sqlx"
)

// Search returns a list of IP addresses matching the search criteria
func Search(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"fuzzy":   true,
			"ip":      true,
			"include": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		var httpStatus int

		fuzzy := false
		if r.URL.Query().Get("fuzzy") != "" {
			b, err := strconv.ParseBool(r.URL.Query().Get("fuzzy"))
			if err != nil {
				logger.Error("parsing 'fuzzy' querystring argument",
					"error", err.Error(),
				)

				apiErr := api.ErrParseFuzzy
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			fuzzy = b
		}

		ips, err := ipaddress.Search(r.URL.Query().Get("ip"), r.URL.Query().Get("include"), fuzzy, db)
		if err != nil {
			logger.Error("searching for IP address",
				"error", err.Error(),
			)

			apiErr := models.ErrSearchIPAddress
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			httpStatus = http.StatusBadRequest

			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		if len(ips) == 0 {
			httpStatus = http.StatusNotFound
		} else {
			httpStatus = http.StatusOK

			APIResponse.Result = ips
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

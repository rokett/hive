package network

import (
	"log/slog"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"

	"rokett.me/hive/internal/services/network"

	"github.com/jmoiron/sqlx"
)

// GetOne retrieves a single network
func GetOne(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		validParams := map[string]bool{
			"fields":  true,
			"include": true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			logger.Error("converting id from stringt to int64",
				"error", err.Error(),
			)

			apiErr := models.ErrGetNetwork
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)

			APIResponse.Send(logger, http.StatusBadRequest, w)
		}

		var httpStatus int

		n, err := network.View(id, r.URL.Query().Get("fields"), r.URL.Query().Get("include"), db)
		if err != nil {
			logger.Error("retrieving network",
				"error", err.Error(),
			)

			apiErr := models.ErrGetNetwork
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			httpStatus = http.StatusBadRequest
		} else {
			httpStatus = http.StatusOK

			APIResponse.Result = n
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

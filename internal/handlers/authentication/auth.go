package authentication

type contextKeyType string

const OperatorCtxKey contextKeyType = "operator_id"

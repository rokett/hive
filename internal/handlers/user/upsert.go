package user

import (
	"encoding/json"
	"log/slog"
	"net/http"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
	"rokett.me/hive/internal/handlers/authentication"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/user"
	"rokett.me/hive/internal/validation"
)

// CreateIfNotExists creates a new user if they do not already exist
func CreateIfNotExists(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		var users []models.User
		var returnedUsers []models.User

		//TODO Should do something better here for unauthenticated access really.
		var operator int64
		if r.Context().Value(authentication.OperatorCtxKey) != nil {
			operator = r.Context().Value(authentication.OperatorCtxKey).(int64)
		} else {
			var fuzzy bool
			user, err := user.SearchUserByUsername("system", "username", fuzzy, db)
			if err != nil {
				logger.Error("unable to find system user",
					"error", err.Error(),
				)

				apiErr := models.ErrSearchingUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			operator = user.ID
		}

		err := json.NewDecoder(r.Body).Decode(&users)
		if err != nil {
			logger.Error("decoding json request to create asset",
				"error", err.Error(),
			)

			apiErr := api.ErrJSONDecode
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		for _, u := range users {
			var validationErrors validation.Errors
			if u.Upn == "" {
				validationErrors = validationErrors.Add("upn", "You must provide the user's UPN")
			}

			if len(validationErrors) > 0 {
				APIErrors = APIErrors.Add(api.ErrValidation, validationErrors)

				continue
			}

			unique, id, err := user.IsUnique(u.Upn, db)
			if err != nil {
				logger.Error("checking if user is unique",
					"error", err.Error(),
					"upn", u.Upn,
				)

				apiErr := models.ErrCreatingUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)

				continue
			}

			if unique {
				id, err = user.Create(u, db)
				if err != nil {
					logger.Error("creating user",
						"error", err.Error(),
						"upn", u.Upn,
					)

					apiErr := models.ErrCreatingUser
					apiErr.Detail = err.Error()
					APIResponse.Errors = APIErrors.Add(apiErr, nil)

					continue
				}
			}

			newUser, err := user.View(id, db)
			if err != nil {
				logger.Error("viewing newly created user",
					"error", err.Error(),
					"id", id,
				)

				apiErr := models.ErrGetUser
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)

				continue
			}

			b, _ := json.Marshal(newUser)
			go auditlog.Log("create", operator, "", string(b[:]), "asset", id, db, logger)

			returnedUsers = append(returnedUsers, newUser)
		}

		httpStatus := http.StatusCreated

		if len(APIErrors) > 0 {
			APIResponse.Errors = APIErrors

			httpStatus = http.StatusBadRequest
		}

		if len(returnedUsers) > 0 {
			APIResponse.Result = returnedUsers

			if len(APIErrors) > 0 {
				httpStatus = http.StatusMultiStatus
			}
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

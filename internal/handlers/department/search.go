package department

import (
	"log/slog"
	"net/http"
	"strconv"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/department"

	"github.com/jmoiron/sqlx"
)

// Search returns a list of departments matching the search criteria
func Search(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}

		var APIErrors api.Errors

		var httpStatus int

		validParams := map[string]bool{
			"fuzzy":      true,
			"department": true,
			"include":    true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		fuzzy := false
		if r.URL.Query().Get("fuzzy") != "" {
			b, err := strconv.ParseBool(r.URL.Query().Get("fuzzy"))
			if err != nil {
				logger.Error("parsing 'fuzzy' querystring argument",
					"error", err.Error(),
				)

				apiErr := api.ErrParseFuzzy
				apiErr.Detail = err.Error()
				APIResponse.Errors = APIErrors.Add(apiErr, nil)
				APIResponse.Send(logger, http.StatusInternalServerError, w)

				return
			}

			fuzzy = b
		}

		departments, err := department.Search(r.URL.Query().Get("department"), r.URL.Query().Get("include"), fuzzy, db)
		if err != nil {
			logger.Error("searching for departments",
				"error", err.Error(),
			)

			apiErr := models.ErrSearchDepartment
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			httpStatus = http.StatusBadRequest

			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		if len(departments) == 0 {
			httpStatus = http.StatusNotFound
		} else {
			httpStatus = http.StatusOK

			APIResponse.Result = departments
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

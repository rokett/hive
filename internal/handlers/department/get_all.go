package department

import (
	"log/slog"
	"net/http"

	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/department"

	"github.com/jmoiron/sqlx"
)

// GetAll returns all departments
func GetAll(db *sqlx.DB, logger *slog.Logger) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		APIResponse := api.Response{}
		var APIErrors api.Errors

		var httpStatus int

		validParams := map[string]bool{
			"fuzzy":   true,
			"include": true,
			"sort":    true,
		}

		paramsValid := true
		params := r.URL.Query()
		for k := range params {
			if !validParams[k] {
				paramsValid = false
				apiErr := api.ErrInvalidQuerystringParameter
				apiErr.Detail = k
				APIErrors = APIErrors.Add(apiErr, nil)
			}
		}

		if !paramsValid {
			APIResponse.Errors = APIErrors
			APIResponse.Send(logger, http.StatusBadRequest, w)

			return
		}

		departments, err := department.List(r.URL.Query().Get("fields"), r.URL.Query().Get("include"), r.URL.Query().Get("sort"), db)
		if err != nil {
			logger.Error("retrieving all departments",
				"error", err.Error(),
			)

			apiErr := models.ErrGetDepartment
			apiErr.Detail = err.Error()
			APIResponse.Errors = APIErrors.Add(apiErr, nil)
			httpStatus = http.StatusBadRequest
		} else {
			httpStatus = http.StatusOK

			APIResponse.Result = departments
		}

		APIResponse.Send(logger, httpStatus, w)
	})
}

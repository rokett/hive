package ldap

import (
	"fmt"
	"time"

	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/uuid"
)

func (l *Config) Authenticate(upn, password, allowedGroupDN string) (models.User, error) {
	enabled, user, err := l.IsUserEnabled(upn)
	if err != nil {
		return models.User{}, err
	}
	if !enabled {
		return models.User{}, ErrAccountDisabled
	}

	// Attemp to bind as the user; this is what tells us if they can authenticate or not
	err = l.Conn.Bind(user.DN, password)
	if err != nil {
		return models.User{}, fmt.Errorf("ldap authentication failed: %w", err)
	}

	err = l.IsInAllowedGroup(allowedGroupDN, user.GetAttributeValues("memberOf"))
	if err != nil {
		return models.User{}, ErrAccessDenied
	}

	token, err := uuid.NewV4()
	if err != nil {
		return models.User{}, fmt.Errorf("unable to generate token for user: %w", err)
	}

	tokenExpiry := time.Now().UTC().Add(2592000 * time.Second)

	return models.User{
		Upn:             user.GetAttributeValue("userPrincipalName"),
		Token:           token.String(),
		TokenExpiry:     tokenExpiry,
		TokenMaxAge:     int(time.Until(tokenExpiry).Seconds()),
		LastLogin:       time.Now().UTC(),
		AccountDisabled: false,
	}, nil
}

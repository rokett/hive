package ldap

import (
	"fmt"
	"strings"

	"github.com/go-ldap/ldap/v3"
)

type Config struct {
	DirectoryServers []string
	Port             int
	BaseDN           string
	BindDN           string
	BindPW           string
	Conn             *ldap.Conn
	AllowedGroupDN   string
}

func (l *Config) Connect() (*ldap.Conn, error) {
	var err error
	var conn *ldap.Conn

	for _, ds := range l.DirectoryServers {
		conn, err = ldap.DialURL(fmt.Sprintf("ldap://%s:%d", strings.TrimSpace(ds), l.Port))
		if err != nil {
			continue
		}

		err := conn.Bind(l.BindDN, l.BindPW)
		if err != nil {
			return nil, fmt.Errorf("error during LDAP bind: %w", err)
		}

		return conn, nil
	}

	return nil, fmt.Errorf("error connecting to LDAP directory server: %w", err)
}

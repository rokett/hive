package ldap

import (
	"fmt"

	"github.com/go-ldap/ldap/v3"
)

func (l *Config) getGroupMembers(dn string) ([]string, error) {
	searchRequest := ldap.NewSearchRequest(
		l.BaseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectCategory=Group)(distinguishedName=%s))", ldap.EscapeFilter(dn)),
		[]string{"cn", "memberOf"},
		nil,
	)

	sr, err := l.Conn.Search(searchRequest)
	if err != nil {
		return []string{}, fmt.Errorf("error searching for group details %s: %w", dn, err)
	}

	return sr.Entries[0].GetAttributeValues("memberOf"), nil
}

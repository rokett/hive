package validation

// Error models a validation error
type Error struct {
	Parameter string `json:"parameter"`
	Error     string `json:"error"`
}

// Errors contains an array of Error objects
type Errors []Error

// Add entries to the ValidationErrors array
func (ve Errors) Add(param string, err string) Errors {
	ve = append(ve, Error{
		Parameter: param,
		Error:     err,
	})

	return ve
}

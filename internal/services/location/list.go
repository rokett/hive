package location

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// List returns all locations
func List(fields string, include string, sort string, db *sqlx.DB) ([]models.Location, error) {
	l := []models.Location{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, sort)

	err := db.Select(&l, q)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.Location{}, errors.New("there are no locations")
	}
	if err != nil {
		return []models.Location{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, location := range l {
		l[k], err = getRelationships(location, rels, db)
		if err != nil {
			return []models.Location{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return l, nil
}

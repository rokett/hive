package location

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Search returns all locations matching the office passed
func Search(office string, include string, fuzzy bool, db *sqlx.DB) ([]models.Location, error) {
	l := []models.Location{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery("", "")

	if fuzzy {
		q = fmt.Sprintf(`%s WHERE office LIKE @p1`, q)
		office = fmt.Sprintf(`%%%s%%`, office)
	} else {
		q = fmt.Sprintf(`%s WHERE office = @p1`, q)
	}

	err := db.Select(&l, q, office)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.Location{}, errors.New("there are no locations")
	}
	if err != nil {
		return []models.Location{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, location := range l {
		l[k], err = getRelationships(location, rels, db)
		if err != nil {
			return []models.Location{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return l, nil
}

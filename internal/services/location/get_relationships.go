package location

import (
	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(loc models.Location, rels map[string]bool, db *sqlx.DB) (models.Location, error) {
	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'location' AND object_id = @p1;", loc.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'location' AND object_id = %d;", loc.ID)
			return models.Location{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		loc.EventLog = &evt
	}*/

	return loc, nil
}

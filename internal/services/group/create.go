package group

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create a new group
func Create(group models.Group, db *sqlx.DB) (int64, error) {
	var id int64

	err := db.Get(&id, "INSERT INTO groups ([group], group_dn) VALUES(@p1, @p2);SELECT id = convert(bigint, SCOPE_IDENTITY());", group.Group, group.GroupDN)
	if err != nil {
		return 0, fmt.Errorf("error adding new group: %w", err)
	}

	return id, nil
}

package group

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// IsUnique checks that a group does not exist
func IsUnique(groupDN string, db *sqlx.DB) (bool, int64, error) {
	g := models.Group{}

	query := fmt.Sprintf("SELECT id FROM groups WHERE group_dn = '%s';", groupDN)

	err := db.Get(&g, "SELECT id FROM groups WHERE group_dn = @p1;", groupDN)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, g.ID, nil
}

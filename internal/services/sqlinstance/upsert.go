package sqlinstance

import (
	"fmt"
	"strings"

	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/database"

	"github.com/jmoiron/sqlx"
)

// Upsert either updates an existing record or inserts a new one.  As an added extra, it also deletes records which are not part of the patch.
func Upsert(instances []models.SQLInstance, assetID int64, db *sqlx.DB) error {
	// We need to retrieve the existing checksums of SQL instances related to the asset.
	// This allows us to check whether an instance already exists, in which case we update the record, or needs to be created.
	// We can also track which existing records have been updated, meaning that records which have not been updated can be deleted.
	instChecksums := []models.SQLInstance{}

	err := db.Select(&instChecksums, "SELECT id, checksum FROM sqlInstances WHERE asset_id = @p1", assetID)
	if err != nil {
		return fmt.Errorf("unable to retrieve SQL Instance checksums: %w", err)
	}

	// Making a map of the checksums allows us to check whether a hash already exists much easier than iterating over the slice each time.
	checksums := make(map[string]int64)

	for _, c := range instChecksums {
		checksums[c.Checksum] = c.ID
	}

	for _, inst := range instances {
		// Create a checksum for comparison.
		checksum, err := getChecksum(inst, assetID)
		if err != nil {
			return fmt.Errorf("unable to calculate SQL instance checksum: %w", err)
		}

		// If the checksum already exists we remove it from the map and update the record.
		// Otherwise we create a new record.
		if _, ok := checksums[checksum]; ok {
			// There is nothing to update with an instance.
			// All we're interested in here is getting the ID for dealing with databases, and removing the checksum entry so that we can clear up old instances.
			inst.ID = checksums[checksum]
			delete(checksums, checksum)
		} else {
			inst.ID, err = Create(inst, assetID, db)
			if err != nil {
				return fmt.Errorf("unable to create new SQL Instance: %w", err)
			}
		}

		if inst.Databases != nil {
			err = database.Upsert(*inst.Databases, inst.ID, db)
			if err != nil {
				return fmt.Errorf("unable to handle database records: %w", err)
			}
		}
	}

	for _, id := range checksums {
		var stmts []string

		stmts = append(stmts, "DELETE FROM databases WHERE instance_id = @p1")
		stmts = append(stmts, "DELETE FROM sqlInstances WHERE id = @p1")

		stmt := strings.Join(stmts, ";")

		_, err := db.Exec(stmt, id)
		if err != nil {
			return fmt.Errorf("error deleting SQL Instance with ID %d: %w", id, err)
		}
	}

	return nil
}

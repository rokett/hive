package sqlinstance

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteInstanceDatabaseRelationship an existing SQL instance/database relationship
func DeleteInstanceDatabaseRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM databases WHERE instance_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting SQL instance/database relationship with Asset ID %d: %w", id, err)
	}

	return nil
}

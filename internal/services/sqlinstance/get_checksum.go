package sqlinstance

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"

	"rokett.me/hive/internal/models"
)

func getChecksum(inst models.SQLInstance, assetID int64) (string, error) {
	bs, err := json.Marshal([]string{
		inst.SQLInstance,
		inst.ServiceAccount,
		strconv.FormatInt(assetID, 10),
	})
	if err != nil {
		return "", fmt.Errorf("unable to marshal SQL Instance information to calculate checksum: %w", err)
	}

	checksum := sha256.Sum256(bs)

	return hex.EncodeToString(checksum[:]), nil
}

package sqlinstance

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create new SQL instance
func Create(inst models.SQLInstance, assetID int64, db *sqlx.DB) (int64, error) {
	// Create a checksum.  We will use this for comparison purposes in order to know which records should be updated
	checksum, err := getChecksum(inst, assetID)
	if err != nil {
		return 0, fmt.Errorf("unable to calculate SQL Instance checksum: %w", err)
	}

	var id int64

	err = db.Get(&id, "INSERT INTO sqlInstances (name, service_account, asset_id, checksum) VALUES(@p1, @p2, @p3, @p4);SELECT id = convert(bigint, SCOPE_IDENTITY());", inst.SQLInstance, inst.ServiceAccount, assetID, checksum)
	if err != nil {
		return 0, fmt.Errorf("error adding new SQL instance: %w", err)
	}

	return id, nil
}

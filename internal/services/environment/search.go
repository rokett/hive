package environment

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Search returns all organisations matching the name passed
func Search(environment string, include string, fuzzy bool, db *sqlx.DB) ([]models.Environment, error) {
	e := []models.Environment{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery("name", "")

	if fuzzy {
		q = fmt.Sprintf(`%s WHERE name LIKE @p1`, q)
		environment = fmt.Sprintf(`%%%s%%`, environment)
	} else {
		q = fmt.Sprintf(`%s WHERE name = @p1`, q)
	}

	err := db.Select(&e, q, environment)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.Environment{}, errors.New("there are no environments")
	}
	if err != nil {
		return []models.Environment{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, env := range e {
		e[k], err = getRelationships(env, rels, db)
		if err != nil {
			return []models.Environment{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return e, nil
}

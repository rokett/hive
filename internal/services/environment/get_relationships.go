package environment

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(env models.Environment, rels map[string]bool, db *sqlx.DB) (models.Environment, error) {
	if rels["systems"] || rels["*"] {
		s := []models.System{}

		err := db.Select(&s, "SELECT systems.id, systems.name FROM environment_system INNER JOIN systems ON environment_system.system_id = systems.id WHERE (environment_system.environment_id = @p1)", env.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT systems.id, systems.name FROM environment_system INNER JOIN systems ON environment_system.system_id = systems.id WHERE (environment_system.environment_id = %d)", env.ID)
			return models.Environment{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		env.Systems = &s
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'environment' AND object_id = @p1;", env.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'environment' AND object_id = %d;", env.ID)
			return models.Environment{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		env.EventLog = &evt
	}*/

	return env, nil
}

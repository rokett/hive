package supplier

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSupplierUserRelationship checks that supplier/user relationship does not exist
func IsUniqueSupplierUserRelationship(supplierID, userID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT supplier_id FROM supplier_user WHERE supplier_id = @p1 AND user_id = @p2;", supplierID, userID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT supplier_id FROM supplier_user WHERE supplier_id = %d AND user_id = %d;", supplierID, userID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

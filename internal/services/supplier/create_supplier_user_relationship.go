package supplier

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSupplierUserRelationship creates an entry in the supplier_user pivot table to link an user to a supplier
func CreateSupplierUserRelationship(supplierID, userID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO supplier_user (supplier_id, user_id) VALUES(@p1, @p2);", supplierID, userID)
	if err != nil {
		return fmt.Errorf("error adding new supplier/user relationship: %w", err)
	}

	return nil
}

package supplier

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSupplierSystemRelationship checks that supplier/system relationship does not exist
func IsUniqueSupplierSystemRelationship(supplierID int64, sysID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT supplier_id FROM supplier_system WHERE supplier_id = @p1 AND system_id = @p2;", supplierID, sysID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT supplier_id FROM supplier_system WHERE supplier_id = %d AND system_id = %d;", supplierID, sysID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

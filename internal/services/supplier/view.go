package supplier

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// View returns a single supplier by ID
func View(id int64, fields string, include string, db *sqlx.DB) (models.Supplier, error) {
	s := models.Supplier{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, "")
	q = fmt.Sprintf(`%s WHERE ID = @p1`, q)

	err := db.Get(&s, q, id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return models.Supplier{}, errors.New("supplier does not exist")
	}
	if err != nil {
		q = buildQuery(fields, "")
		q = fmt.Sprintf(`%s WHERE ID = %d`, q, id)

		return models.Supplier{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	s, err = getRelationships(s, rels, db)
	if err != nil {
		return models.Supplier{}, fmt.Errorf("unable to get relationships: %w", err)
	}

	return s, nil
}

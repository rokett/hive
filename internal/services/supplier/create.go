package supplier

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create a new supplier
func Create(supplier models.Supplier, db *sqlx.DB) (int64, error) {
	var id int64

	err := db.Get(&id, "INSERT INTO suppliers (supplier) VALUES(@p1);SELECT id = convert(bigint, SCOPE_IDENTITY());", supplier.Supplier)
	if err != nil {
		return 0, fmt.Errorf("error adding new supplier: %w", err)
	}

	return id, nil
}

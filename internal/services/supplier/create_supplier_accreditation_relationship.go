package supplier

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSupplierAccreditationRelationship creates an entry in the accreditation_supplier pivot table to link an accreditation to a supplier
func CreateSupplierAccreditationRelationship(supplierID int64, accreditationID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO accreditation_supplier (accreditation_id, supplier_id) VALUES(@p1, @p2);", accreditationID, supplierID)
	if err != nil {
		return fmt.Errorf("error adding new supplier/accreditation relationship: %w", err)
	}

	return nil
}

package supplier

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUnique checks that a supplier does not exist
func IsUnique(supplier string, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT id FROM suppliers WHERE supplier = @p1;", supplier)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT id FROM suppliers WHERE supplier = %s;", supplier)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

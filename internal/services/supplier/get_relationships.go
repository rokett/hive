package supplier

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(s models.Supplier, rels map[string]bool, db *sqlx.DB) (models.Supplier, error) {
	if rels["accounts"] || rels["*"] {
		u := []models.User{}

		err := db.Select(&u, "SELECT users.id, users.upn FROM supplier_user INNER JOIN users ON supplier_user.user_id = users.id WHERE (supplier_user.supplier_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT users.id, users.upn FROM supplier_user INNER JOIN users ON supplier_user.user_id = users.id WHERE (supplier_user.supplier_id = %d)", s.ID)
			return models.Supplier{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.Accounts = &u
	}

	if rels["accreditations"] || rels["*"] {
		a := []models.Accreditation{}

		err := db.Select(&a, "SELECT accreditations.id, accreditations.accreditation FROM accreditation_supplier INNER JOIN accreditations ON accreditation_supplier.accreditation_id = accreditations.id WHERE (accreditation_supplier.supplier_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT accreditations.id, accreditations.accreditation FROM accreditation_supplier INNER JOIN accreditations ON accreditation_supplier.accreditation_id = accreditations.id WHERE (accreditation_supplier.supplier_id = %d)", s.ID)
			return models.Supplier{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.Accreditations = &a
	}

	if rels["systems"] || rels["*"] {
		sys := []models.System{}

		err := db.Select(&sys, "SELECT systems.id, systems.name FROM supplier_system INNER JOIN systems ON supplier_system.system_id = systems.id WHERE (supplier_system.supplier_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT systems.id, systems.name FROM supplier_system INNER JOIN systems ON supplier_system.system_id = systems.id WHERE (supplier_system.supplier_id = %d)", s.ID)
			return models.Supplier{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.Systems = &sys
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'supplier' AND object_id = @p1;", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'supplier' AND object_id = %d;", s.ID)
			return models.Supplier{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.EventLog = &evt
	}*/

	return s, nil
}

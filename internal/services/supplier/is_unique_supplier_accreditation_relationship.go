package supplier

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSupplierAccreditationRelationship checks that supplier/accreditation relationship does not exist
func IsUniqueSupplierAccreditationRelationship(supplierID int64, accreditationID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT supplier_id FROM accreditation_supplier WHERE supplier_id = @p1 AND accreditation_id = @p2;", supplierID, accreditationID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT supplier_id FROM accreditation_supplier WHERE supplier_id = %d AND accreditation_id = %d;", supplierID, accreditationID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

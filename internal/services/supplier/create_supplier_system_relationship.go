package supplier

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSupplierSystemRelationship creates an entry in the supplier_system pivot table to link a supplier to a system
func CreateSupplierSystemRelationship(supplierID int64, sysID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO supplier_system (supplier_id, system_id) VALUES(@p1, @p2);", supplierID, sysID)
	if err != nil {
		return fmt.Errorf("error adding new supplier/system relationship: %w", err)
	}

	return nil
}

package department

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(dept models.Department, rels map[string]bool, db *sqlx.DB) (models.Department, error) {
	if rels["directorate"] || rels["*"] {
		dir := models.Directorate{}

		err := db.Get(&dir, "SELECT directorates.name, directorates.id FROM directorates INNER JOIN departments ON directorates.id = departments.directorate_id WHERE (departments.id = @p1)", dept.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT directorates.name, directorates.id FROM directorates INNER JOIN departments ON directorates.id = departments.directorate_id WHERE (departments.id = %d)", dept.ID)
			return models.Department{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		dept.Directorate = &dir
	}

	if rels["organisation"] || rels["*"] {
		org := models.Organisation{}

		err := db.Get(&org, "SELECT organisations.organisation FROM departments INNER JOIN directorates ON departments.directorate_id = dbo.directorates.id INNER JOIN organisations ON directorates.organisation_id = organisations.id WHERE (departments.id = @p1)", dept.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.organisation FROM departments INNER JOIN directorates ON departments.directorate_id = dbo.directorates.id INNER JOIN organisations ON directorates.organisation_id = organisations.id WHERE (departments.id = %d)", dept.ID)
			return models.Department{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		dept.Organisation = &org
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'department' AND object_id = @p1;", dept.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'department' AND object_id = %d;", dept.ID)
			return models.Department{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		dept.EventLog = &evt
	}*/

	return dept, nil
}

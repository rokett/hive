package department

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// List returns all departments
func List(fields string, include string, sort string, db *sqlx.DB) ([]models.Department, error) {
	d := []models.Department{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, sort)

	err := db.Select(&d, q)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.Department{}, errors.New("there are no departments")
	}
	if err != nil {
		return []models.Department{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, department := range d {
		d[k], err = getRelationships(department, rels, db)
		if err != nil {
			return []models.Department{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return d, nil
}

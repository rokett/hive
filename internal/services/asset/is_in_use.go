package asset

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsInUse returns true if an asset is part of a system
func IsInUse(id int64, db *sqlx.DB) (bool, error) {
	var count int64

	err := db.Get(&count, "SELECT COUNT(asset_id) FROM asset_system WHERE asset_id = @p1", id)
	if err != nil {
		q := fmt.Sprintf("SELECT COUNT(asset_id) FROM asset_system WHERE asset_id = %d", id)

		return true, fmt.Errorf("problem with query %s: %w", q, err)
	}

	if count > 0 {
		return true, nil
	}

	return false, nil
}

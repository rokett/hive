package asset

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueAssetSoftwareRelationship checks that an asset/software relationship does not exist
func IsUniqueAssetSoftwareRelationship(assetID int64, swID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT asset_id FROM asset_software WHERE asset_id = @p1 AND software_id = @p2;", assetID, swID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT asset_id FROM asset_software WHERE asset_id = %d AND software_id = %d;", assetID, swID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

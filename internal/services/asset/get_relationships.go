package asset

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(asset models.Asset, rels map[string]bool, sys []models.System, db *sqlx.DB) (models.Asset, error) {
	if rels["software"] || rels["*"] {
		sw := []models.Software{}

		err := db.Select(&sw, "SELECT software.name, software.version, software.id FROM asset_software INNER JOIN software ON asset_software.software_id = software.id WHERE (asset_software.asset_id = @p1)", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT software.name, software.version, software.id FROM asset_software INNER JOIN software ON asset_software.software_id = software.id WHERE (asset_software.asset_id = %d)", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.Software = &sw
	}

	if rels["volumes"] || rels["*"] {
		vols := []models.Volume{}

		err := db.Select(&vols, "SELECT id, block_size, capacity, drive_letter, filesystem, free_space, label, disk_model FROM volumes WHERE asset_id = @p1;", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, block_size, capacity, drive_letter, filesystem, free_space, label, disk_model FROM volumes WHERE asset_id = %d;", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.Volumes = &vols
	}

	if rels["organisations"] || rels["*"] {
		org := []models.Organisation{}

		err := db.Select(&org, "SELECT organisations.organisation, organisations.id FROM asset_organisation INNER JOIN organisations ON asset_organisation.organisation_id = organisations.id WHERE (asset_organisation.asset_id = @p1)", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.organisation, organisations.id FROM asset_organisation INNER JOIN organisations ON asset_organisation.organisation_id = organisations.id WHERE (asset_organisation.asset_id = %d)", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.Organisation = &org
	}

	if rels["system.environments"] || rels["*"] {
		for k, s := range sys {
			envs := []models.Environment{}

			err := db.Select(&envs, "SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = @p1 AND asset_system.system_id = @p2) ORDER BY environments.id", asset.ID, s.ID)
			if err != nil && !errors.Is(err, sql.ErrNoRows) {
				q := fmt.Sprintf("SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = %d AND asset_system.system_id = %d) ORDER BY environments.id", asset.ID, s.ID)
				return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
			}

			sys[k].Environments = &envs
		}
	}

	if rels["system.services"] || rels["*"] {
		for k, s := range sys {
			svcs := []models.Service{}

			err := db.Select(&svcs, "SELECT services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = @p1) ORDER BY services.id", s.ID)
			if err != nil && !errors.Is(err, sql.ErrNoRows) {
				q := fmt.Sprintf("services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = %d) ORDER BY services.id", s.ID)
				return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
			}

			sys[k].Services = &svcs
		}
	}

	if rels["systems"] || rels["*"] {
		asset.Systems = &sys
	}

	if rels["instances"] || rels["*"] {
		inst := []models.SQLInstance{}

		err := db.Select(&inst, "SELECT id, name, service_account FROM sqlInstances WHERE asset_id = @p1;", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, name, service_account FROM sqlInstances WHERE asset_id = %d;", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		if rels["instance.databases"] || rels["*"] {
			for k, i := range inst {
				dbs := []models.Database{}

				err = db.Select(&dbs, "SELECT id, name, status, recovery_model, read_only, standby, preferred_backup_replica, last_full_backup, last_log_backup, is_primary_replica, compatibility_level, size FROM databases WHERE instance_id = @p1;", i.ID)
				if err != nil && !errors.Is(err, sql.ErrNoRows) {
					q := fmt.Sprintf("SELECT id, name, status, recovery_model, read_only, standby, preferred_backup_replica, last_full_backup, last_log_backup, is_primary_replica, compatibility_level, size FROM databases WHERE instance_id = %d;", i.ID)
					return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
				}

				if rels["instance.database.files"] || rels["*"] {
					for k, d := range dbs {
						files := []models.DatabaseFile{}

						err = db.Select(&files, "SELECT id, name, path, type FROM database_files WHERE database_id = @p1;", d.ID)
						if err != nil && !errors.Is(err, sql.ErrNoRows) {
							q := fmt.Sprintf("SELECT id, name, path, type FROM database_files WHERE database_id = %d;", d.ID)
							return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
						}

						dbs[k].DatabaseFiles = &files
					}
				}

				inst[k].Databases = &dbs
			}
		}

		asset.SQLInstances = &inst
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'asset' AND object_id = @p1;", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'asset' AND object_id = %d;", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.EventLog = &evt
	}*/

	return asset, nil
}

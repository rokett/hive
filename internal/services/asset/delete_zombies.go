package asset

import (
	"database/sql"
	"errors"
	"log/slog"
	"os/exec"
	"strings"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

// DeleteZombies removes assets, and their relationships, which are no longer active.
// No longer active means that the record has not been updated within the last 30 days.
func DeleteZombies(db *sqlx.DB, logger *slog.Logger) {
	logger.Info("starting zombie asset purge")

	assets := []models.Asset{}

	// We need to find assets which were last updated > 30 day ago, and have Asset Collector installed; these should update automatically every day.
	q := "SELECT DISTINCT assets.name, assets.id FROM assets INNER JOIN asset_software ON assets.id = asset_software.asset_id INNER JOIN software ON asset_software.software_id = software.id WHERE (software.name LIKE N'%asset-collector%');"

	err := db.Select(&assets, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		logger.Error(
			"retrieving zombied assets prior to deletion",
			"error", err.Error(),
		)

		return
	}

	for _, a := range assets {
		cmd := exec.Command("ping", "-c", "3", a.Hostname)

		out, err := cmd.CombinedOutput()
		if strings.Contains(string(out), "timed out") || strings.Contains(string(out), "unreachable") || strings.Contains(string(out), "0 packets received") {
			logger.Info(
				"deleting zombie asset",
				"asset", a.Hostname,
			)

			//TODO To be enabled after a period of testing
			/*err = Delete(a.ID, db)
			if err != nil {
				logger.Error(
					"deleting zombie asset",
					"error", err.Error(),
					"asset", a.Hostname,
				)
				continue
			}

			b, _ := json.Marshal(a)
			go auditlog.Log("delete", 1, string(b[:]), "", "asset", a.ID, db, logger)*/

			continue
		}
		if err != nil {
			logger.Error(
				"checking if zombie asset responds to ping",
				"error", err.Error(),
				"asset", a.Hostname,
			)
			continue
		}

		logger.Info(
			"asset is alive but not checking in",
			"asset", a.Hostname,
		)
	}

	logger.Info("completed zombie asset purge")
}

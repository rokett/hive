package asset

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

type oauthResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	Scope       string `json:"scope"`
}

type warrantyResponse []struct {
	ID                     int         `json:"id"`
	ServiceTag             string      `json:"serviceTag"`
	OrderBuid              int         `json:"orderBuid"`
	ShipDate               time.Time   `json:"shipDate"`
	ProductCode            string      `json:"productCode"`
	LocalChannel           interface{} `json:"localChannel"`
	ProductID              interface{} `json:"productId"`
	ProductLineDescription string      `json:"productLineDescription"`
	ProductFamily          interface{} `json:"productFamily"`
	SystemDescription      interface{} `json:"systemDescription"`
	ProductLobDescription  string      `json:"productLobDescription"`
	CountryCode            string      `json:"countryCode"`
	Duplicated             bool        `json:"duplicated"`
	Invalid                bool        `json:"invalid"`
	Entitlements           []struct {
		ItemNumber              string      `json:"itemNumber"`
		StartDate               time.Time   `json:"startDate"`
		EndDate                 time.Time   `json:"endDate"`
		EntitlementType         string      `json:"entitlementType"`
		ServiceLevelCode        string      `json:"serviceLevelCode"`
		ServiceLevelDescription string      `json:"serviceLevelDescription"`
		ServiceLevelGroup       interface{} `json:"serviceLevelGroup"`
	} `json:"entitlements"`
}

// UpdateWarrantyDetails for assets
func UpdateWarrantyDetails(apiURL string, oauthURL string, warrantyURL string, clientID string, clientSecret string, db *sqlx.DB, logger *slog.Logger) {
	logger.Info("starting asset warranty update")

	assets := []models.Asset{}

	err := db.Select(&assets, "SELECT id, serial_number FROM assets WHERE manufacturer LIKE '%Dell%' AND serial_number IS NOT NULL AND third_party_warranty = 0;")
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := "SELECT id, serial_number FROM assets WHERE manufacturer LIKE '%Dell%' AND serial_number IS NOT NULL AND third_party_warranty = 0;"

		logger.Error(
			"retrieving all Dell assets",
			"error", err.Error(),
			"query", q,
		)

		return
	}

	if len(assets) == 0 {
		logger.Error("no Dell assets returned")

		return
	}

	client := &http.Client{
		Timeout: 60 * time.Second,
	}

	// The Dell API uses OAUTH for authentication, so we first need to pass our credentials to receive a token.
	resp, err := client.PostForm(fmt.Sprintf("%s%s", apiURL, oauthURL), url.Values{
		"grant_type":    {"client_credentials"},
		"client_id":     {clientID},
		"client_secret": {clientSecret},
	})
	logger = logger.With(
		"oauth_response_status", resp.Status,
		"oauth_response_status_code", resp.StatusCode,
	)
	if err != nil {
		logger.Error(
			"making OAUTH request to Dell API",
			"error", err.Error(),
		)

		return
	}

	var or oauthResponse
	err = json.NewDecoder(resp.Body).Decode(&or)
	if err != nil {
		logger.Error(
			"decode Dell API oauth response",
			"error", err.Error(),
		)

		return
	}
	resp.Body.Close()

	// We set a base end date for checking against the asset entitlement dates later
	baseEndDate, err := time.Parse("2006-01-02T15:04:05", "2000-01-01T00:00:01")
	if err != nil {
		logger.Error(
			"setting base end date",
			"error", err.Error(),
		)

		return
	}

	for {
		if len(assets) == 0 {
			break
		}

		// We need to get the first 20 assets, assuming there are more than 20, and concat serial numbers into a comma separated string for the query
		// We do this to limit the length of the querystring, yet also process serial numbers in batches to minimise requests.
		// We then remove the first 20 assets from the slice.  The next time this loop runs, the first 20 it picks will be new ones.
		// If there are less than 20 assets left in the slice, then we've just completed the last loop and can exit at the start of the next one.
		var a []models.Asset
		if len(assets) > 20 {
			a = assets[:20]
			assets = assets[20:]
		} else {
			a = assets[:]
			assets = assets[len(assets):]
		}

		var serials []string

		for _, v := range a {
			serials = append(serials, strings.TrimSpace(v.SerialNumber))
		}

		joinedSerials := strings.Join(serials, ",")

		// Make a request to the API for warranty info
		uri := fmt.Sprintf("%s%s?servicetags=%s", apiURL, warrantyURL, joinedSerials)
		req, err := http.NewRequest("GET", uri, nil)
		if err != nil {
			logger.Error(
				"creating HTTP request to warranty endpoint",
				"error", err.Error(),
			)

			continue
		}

		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", or.AccessToken))
		req.Header.Add("Accept", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			logger.Error(
				"making request to Dell warranty endpoint",
				"error", err.Error(),
				"uri", uri,
				"warranty_response_status", resp.Status,
				"warranty_response_status_code", resp.StatusCode,
			)

			continue
		}

		var r warrantyResponse
		err = json.NewDecoder(resp.Body).Decode(&r)
		if err != nil {
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				logger.Error(
					"decode warranty response payload",
					"error", err.Error(),
					"uri", uri,
					"warranty_response_status", resp.Status,
					"warranty_response_status_code", resp.StatusCode,
				)

				continue
			}

			logger.Error(
				"decode warranty response payload",
				"error", err.Error(),
				"response", string(body),
				"uri", uri,
				"warranty_response_status", resp.Status,
				"warranty_response_status_code", resp.StatusCode,
			)

			continue
		}
		resp.Body.Close()

		// Now we can range over the response.  Each entry is one of the assets we have requested information for.
		// We can then pull out the warranty end date, of which there may be multiple entries, compare them against the previous
		// entry, and if it is newer update the end date.  Whatever we are left with is the true end date.
		for _, w := range r {
			// Each asset could have many entitlements, and we need to find the latest end date.
			// endDate is an in-loop variable used to ensure we get the latest end date from all of the asset entitlements.
			// We start with the base end date as defined further up in the function.
			endDate := baseEndDate

			var warrantyLevel string

			for _, e := range w.Entitlements {
				diff := e.EndDate.Sub(endDate)

				if diff > 0 {
					endDate = e.EndDate

					serviceLevel := strings.ToLower(e.ServiceLevelDescription)

					if strings.Contains(serviceLevel, "4 hour") {
						warrantyLevel = "4 Hr"
					}

					if strings.Contains(serviceLevel, "8 hour") {
						warrantyLevel = "8 Hr"
					}

					if strings.Contains(serviceLevel, "next business day") {
						warrantyLevel = "NBD"
					}

					// If there is no service level defined then we can assume the asset has no active maintenance.
					// The warranty end date will be set to the base end date.
					if warrantyLevel == "" {
						endDate = baseEndDate
					}
				}
			}

			// If the current warranty end date has not progressed since the base date we set, then we don't want to update the record
			if endDate.Equal(baseEndDate) {
				continue
			}

			// Now it's time to update the asset record
			_, err = db.Exec("UPDATE assets SET warranty_expiry_date = @p1, warranty_level = @p2, ship_date = @p3 WHERE serial_number = @p4", endDate, warrantyLevel, w.ShipDate, w.ServiceTag)
			if err != nil {
				logger.Error(
					"updating asset warranty details",
					"error", err.Error(),
					"service_tag", w.ServiceTag,
				)
			}
		}
	}

	logger.Info("completed asset warranty update")
}

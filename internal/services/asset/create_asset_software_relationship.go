package asset

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// CreateAssetSoftwareRelationship builds a batch INSERT statement from a slice of software.
// This ensures that the relationships are inserted in a single statement.
func CreateAssetSoftwareRelationship(id int64, sw []models.Software, db *sqlx.DB) error {
	if len(sw) == 0 {
		return nil
	}

	var q string

	entry := 1
	var entries []interface{}

	for _, v := range sw {
		tmp := "INSERT INTO asset_software (asset_id, software_id) VALUES"

		tmp = fmt.Sprintf("%s (@p%d,", tmp, entry)
		entries = append(entries, id)
		entry++

		tmp = fmt.Sprintf("%s (SELECT id FROM software WHERE name = @p%d", tmp, entry)
		entries = append(entries, v.Name)
		entry++

		tmp = fmt.Sprintf("%s AND version = @p%d));", tmp, entry)
		entries = append(entries, v.Version)
		entry++

		q = fmt.Sprintf("%s%s", q, tmp)

		// There have occassionally been issues whereby the number of parameters exceeds 2100 which is a hard limit in SQL Server.
		// Therefore we commit the data in chunks of 1000 to get around the problem.
		if entry > 2000 {
			err := insertAssetSoftwareRelationship(q, entries, db)
			if err != nil {
				return err
			}

			// These need to be reset back to default values in order to start the process again.
			entry = 1
			q = ""
			entries = nil
		}
	}

	err := insertAssetSoftwareRelationship(q, entries, db)
	if err != nil {
		return err
	}

	return nil
}

func insertAssetSoftwareRelationship(q string, entries []interface{}, db *sqlx.DB) error {
	_, err := db.Exec(q, entries...)
	if err != nil {
		return fmt.Errorf("error creating asset/software relationship: %w", err)
	}

	return nil
}

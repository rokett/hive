package asset

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Update an existing asset
func Update(a models.Asset, id int64, db *sqlx.DB) error {
	q := "UPDATE assets SET"

	param := 1
	var params []interface{}

	if a.Hostname != "" {
		q = fmt.Sprintf("%s name = @p%d,", q, param)
		params = append(params, a.Hostname)
		param++
	}

	if a.Manufacturer != "" {
		q = fmt.Sprintf("%s manufacturer = @p%d,", q, param)
		params = append(params, a.Manufacturer)
		param++
	}

	if a.Model != "" {
		q = fmt.Sprintf("%s model = @p%d,", q, param)
		params = append(params, a.Model)
		param++
	}

	if a.SerialNumber != "" {
		q = fmt.Sprintf("%s serial_number = @p%d,", q, param)
		params = append(params, a.SerialNumber)
		param++
	}

	if a.OS != "" {
		q = fmt.Sprintf("%s os = @p%d,", q, param)
		params = append(params, a.OS)
		param++
	}

	if a.ServicePack != "" {
		q = fmt.Sprintf("%s service_pack = @p%d,", q, param)
		params = append(params, a.ServicePack)
		param++
	}

	if !a.WarrantyExpiryDate.IsZero() {
		q = fmt.Sprintf("%s warranty_expiry_date = @p%d,", q, param)
		params = append(params, a.WarrantyExpiryDate)
		param++
	}

	if a.IgnoreWarranty != nil {
		q = fmt.Sprintf("%s ignore_warranty = @p%d,", q, param)
		params = append(params, a.IgnoreWarranty)
		param++
	}

	if a.ThirdPartyWarranty != nil {
		q = fmt.Sprintf("%s third_party_warranty = @p%d,", q, param)
		params = append(params, a.ThirdPartyWarranty)
		param++
	}

	if a.RecoveryMethod != "" {
		q = fmt.Sprintf("%s recovery_method = @p%d,", q, param)
		params = append(params, a.RecoveryMethod)
		param++
	}

	if a.Domain != "" {
		q = fmt.Sprintf("%s domain = @p%d,", q, param)
		params = append(params, a.Domain)
		param++
	}

	if a.UUID != "" {
		q = fmt.Sprintf("%s uuid = @p%d,", q, param)
		params = append(params, a.UUID)
		param++
	}

	if a.FQDN != "" {
		q = fmt.Sprintf("%s fqdn = @p%d,", q, param)
		params = append(params, a.FQDN)
		param++
	}

	if !a.BuildTimestamp.IsZero() {
		q = fmt.Sprintf("%s build_timestamp = @p%d,", q, param)
		params = append(params, a.BuildTimestamp)
		param++
	}

	if a.IsVirtual != nil {
		q = fmt.Sprintf("%s is_virtual = @p%d,", q, param)
		params = append(params, a.IsVirtual)
		param++
	}

	if a.WarrantyLevel != "" {
		q = fmt.Sprintf("%s warranty_level = @p%d,", q, param)
		params = append(params, a.WarrantyLevel)
		param++
	}

	if !a.ShipDate.IsZero() {
		q = fmt.Sprintf("%s ship_date = @p%d,", q, param)
		params = append(params, a.ShipDate)
		param++
	}

	if a.AssetType != "" {
		q = fmt.Sprintf("%s asset_type = @p%d,", q, param)
		params = append(params, a.AssetType)
		param++
	}

	if a.Decommissioned != nil {
		q = fmt.Sprintf("%s decommissioned = @p%d,", q, param)
		params = append(params, a.Decommissioned)
		param++
	}

	if a.NTFSTrimEnabled != nil {
		q = fmt.Sprintf("%s ntfs_trim_enabled = @p%d,", q, param)
		params = append(params, a.NTFSTrimEnabled)
		param++
	}

	if a.ReFSTrimEnabled != nil {
		q = fmt.Sprintf("%s refs_trim_enabled = @p%d,", q, param)
		params = append(params, a.ReFSTrimEnabled)
		param++
	}

	q = strings.TrimRight(q, ",")

	q = fmt.Sprintf("%s WHERE id = @p%d;", q, param)
	params = append(params, id)

	_, err := db.Exec(q, params...)
	if err != nil {
		return fmt.Errorf("updating asset %s: %w", a.Hostname, err)
	}

	return nil
}

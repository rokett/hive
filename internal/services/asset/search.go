package asset

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Search returns all assets matching the name passed
func Search(asset string, include string, os string, assetType string, environment string, fuzzy bool, db *sqlx.DB) ([]models.Asset, error) {
	a := []models.Asset{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery("", "")
	q = fmt.Sprintf(`%s WHERE`, q)

	if environment != "" {
		envs := strings.Split(environment, ",")

		q = strings.TrimSuffix(q, " WHERE")

		q = fmt.Sprintf(`%s INNER JOIN asset_system ON assets.id = asset_system.asset_id INNER JOIN environments ON asset_system.environment_id = environments.id`, q)

		q = fmt.Sprintf(`%s WHERE (`, q)

		for _, v := range envs {
			q = fmt.Sprintf(`%s(environments.name = '%s') OR`, q, v)
		}

		q = strings.TrimSuffix(q, " OR")
		q = fmt.Sprintf(`%s)`, q)
	}

	if asset != "" {
		if strings.Contains(q, "environments") {
			q = fmt.Sprintf(`%s AND (`, q)
		} else {
			q = fmt.Sprintf(`%s (`, q)
		}

		if fuzzy {
			q = fmt.Sprintf(`%s assets.name LIKE @p1`, q)
			asset = fmt.Sprintf(`%%%s%%`, asset)
		} else {
			q = fmt.Sprintf(`%s assets.name = @p1`, q)
		}

		q = fmt.Sprintf(`%s)`, q)
	}

	if os != "" {
		osVersions := strings.Split(os, ",")

		if strings.Contains(q, "environments") || strings.Contains(q, "assets.name LIKE") {
			q = fmt.Sprintf(`%s AND (`, q)
		} else {
			q = fmt.Sprintf(`%s (`, q)
		}

		for _, v := range osVersions {
			q = fmt.Sprintf(`%s assets.OS LIKE '%%%s%%' OR `, q, v)
		}

		q = strings.TrimSuffix(q, " OR ")
		q = fmt.Sprintf(`%s)`, q)
	}

	if assetType != "" {
		assetTypes := strings.Split(assetType, ",")

		if strings.Contains(q, "environments") || strings.Contains(q, "assets.name LIKE") || strings.Contains(q, "assets.OS") {
			q = fmt.Sprintf(`%s AND (`, q)
		} else {
			q = fmt.Sprintf(`%s (`, q)
		}

		for _, v := range assetTypes {
			q = fmt.Sprintf(`%s assets.asset_type = '%s' OR `, q, v)
		}

		q = strings.TrimSuffix(q, " OR ")
		q = fmt.Sprintf(`%s)`, q)
	}

	err := db.Select(&a, q, asset)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.Asset{}, errors.New("there are no assets")
	}
	if err != nil {
		return []models.Asset{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, asset := range a {
		// We get the systems here, rather than checking to see if that relationship has been requested,
		// because in order to find out the asset tier we need to find the related system that has the highest associated tier.
		// If we waited to check whether the relationship had been requested, we'd have to request that relationship in order to see the asset tier.
		sys := []models.System{}

		err := db.Select(&sys, "SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = @p1)", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = %d)", asset.ID)
			return []models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		var tier int64
		tier = 99
		for _, s := range sys {
			if s.Tier < tier {
				tier = s.Tier
			}
		}

		// If tier is still 99 then it doesn't really have one so we can use the nil value to exclude it from the response
		if tier == 99 {
			tier = 0
		}

		a[k], err = getRelationships(asset, rels, sys, db)
		if err != nil {
			return []models.Asset{}, fmt.Errorf("unable to get relationships: %w", err)
		}

		a[k].Tier = tier
	}

	return a, nil
}

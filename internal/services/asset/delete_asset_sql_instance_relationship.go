package asset

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteAssetSQLInstanceRelationship an existing asset/SQL instance relationship
func DeleteAssetSQLInstanceRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM sqlInstances WHERE asset_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting asset/SQL instance relationship with Asset ID %d: %w", id, err)
	}

	return nil
}

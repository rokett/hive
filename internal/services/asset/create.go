package asset

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create a new asset
func Create(asset models.Asset, db *sqlx.DB) (int64, error) {
	var id int64

	fls := false

	if asset.IgnoreWarranty == nil {
		asset.IgnoreWarranty = &fls
	}

	if asset.IsVirtual == nil {
		asset.IsVirtual = &fls
	}

	if asset.ThirdPartyWarranty == nil {
		asset.ThirdPartyWarranty = &fls
	}

	if asset.NTFSTrimEnabled == nil {
		asset.NTFSTrimEnabled = &fls
	}

	if asset.ReFSTrimEnabled == nil {
		asset.ReFSTrimEnabled = &fls
	}

	err := db.Get(&id, "INSERT INTO assets (name, manufacturer, model, serial_number, os, service_pack, warranty_expiry_date, ignore_warranty, third_party_warranty, recovery_method, domain, uuid, fqdn, build_timestamp, is_virtual, warranty_level, ship_date, asset_type, ntfs_trim_enabled, refs_trim_enabled) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20);SELECT id = convert(bigint, SCOPE_IDENTITY());", asset.Hostname, asset.Manufacturer, asset.Model, asset.SerialNumber, asset.OS, asset.ServicePack, asset.WarrantyExpiryDate, asset.IgnoreWarranty, asset.ThirdPartyWarranty, asset.RecoveryMethod, asset.Domain, asset.UUID, asset.FQDN, asset.BuildTimestamp, asset.IsVirtual, asset.WarrantyLevel, asset.ShipDate, asset.AssetType, asset.NTFSTrimEnabled, asset.ReFSTrimEnabled)
	if err != nil {
		return 0, fmt.Errorf("error adding new asset: %w", err)
	}

	return id, nil
}

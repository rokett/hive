package asset

import (
	"fmt"
	"strings"
)

func buildQuery(fields string, sort string) string {
	q := "SELECT"

	if fields != "" {
		reqFields := strings.Split(fields, ",")

		flds := "id"

		for _, fld := range reqFields {
			flds = flds + "," + fld
		}

		q = fmt.Sprintf(`%s %s`, q, flds)
	} else {
		q = fmt.Sprintf(`%s assets.id, assets.name, assets.manufacturer, assets.model, assets.asset_type, assets.serial_number, assets.os, assets.ntfs_trim_enabled, assets.refs_trim_enabled, assets.service_pack, assets.warranty_expiry_date, assets.ignore_warranty, assets.third_party_warranty, assets.recovery_method, assets.domain, assets.uuid, assets.fqdn, assets.build_timestamp, assets.is_virtual, assets.warranty_level, assets.ship_date, assets.decommissioned, assets.created_at, assets.updated_at`, q)
	}

	q = fmt.Sprintf(`%s FROM assets`, q)

	if sort != "" {
		var sortString string

		reqSort := strings.Split(sort, ",")

		for _, v := range reqSort {
			var parts []string

			if strings.Contains(v, ":") {
				parts = strings.Split(v, ":")
			} else {
				parts = []string{v, "asc"}
			}

			sortString = fmt.Sprintf("%s %s %s,", sortString, parts[0], parts[1])
		}

		sortString = strings.TrimSuffix(sortString, ",")

		q = fmt.Sprintf("%s ORDER BY %s", q, sortString)
	}

	return q
}

package asset

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteAssetSoftwareRelationship an existing asset/software relationship
func DeleteAssetSoftwareRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM asset_software WHERE asset_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting asset/software relationship with Asset ID %d: %w", id, err)
	}

	return nil
}

package asset

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueHostname checks that an asset does not exist
func IsUniqueHostname(hostname string, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT id FROM assets WHERE name = @p1;", hostname)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT id FROM assets WHERE name = %s;", hostname)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

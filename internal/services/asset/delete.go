package asset

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
)

// Delete an existing asset
func Delete(id int64, db *sqlx.DB) error {
	var stmts []string

	stmts = append(stmts, "DELETE FROM asset_software WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM asset_organisation WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM sqlInstances WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM asset_system WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM volumes WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM assets WHERE id = @p1")

	stmt := strings.Join(stmts, ";")

	_, err := db.Exec(stmt, id)
	if err != nil {
		return fmt.Errorf("error deleting asset, and associated relationships, with ID %d: %w", id, err)
	}

	return nil
}

package asset

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSN checks that an asset serial number does not exist
func IsUniqueSN(sn string, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT id FROM assets WHERE serial_number = @p1;", sn)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT id FROM assets WHERE serial_number = %s;", sn)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

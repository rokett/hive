package asset

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// List returns all assets
func List(fields string, include string, sort string, db *sqlx.DB) ([]models.Asset, error) {
	a := []models.Asset{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, sort)

	err := db.Select(&a, q)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.Asset{}, errors.New("there are no assets")
	}
	if err != nil {
		return []models.Asset{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, asset := range a {
		// We get the systems here, rather than checking to see if that relationship has been requested,
		// because in order to find out the asset tier we need to find the related system that has the highest associated tier.
		// If we waited to check whether the relationship had been requested, we'd have to request that relationship in order to see the asset tier.
		sys := []models.System{}

		err := db.Select(&sys, "SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = @p1)", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = %d)", asset.ID)
			return []models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		var tier int64
		tier = 99
		for _, s := range sys {
			if s.Tier < tier {
				tier = s.Tier
			}
		}

		// If tier is still 99 then it doesn't really have one so we can use the nil value to exclude it from the response
		if tier == 99 {
			tier = 0
		}

		a[k], err = getRelationships(asset, rels, sys, db)
		if err != nil {
			return []models.Asset{}, fmt.Errorf("unable to get relationships: %w", err)
		}

		a[k].Tier = tier
	}

	return a, nil
}

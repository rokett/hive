package asset

import (
	"database/sql"
	"fmt"
	"strings"

	"rokett.me/hive/internal/models"

	"errors"

	"github.com/jmoiron/sqlx"
)

type prometheusAsset struct {
	Targets []string `json:"targets"`
	Labels  struct {
		Systems      string `json:"__meta_systems"`
		Services     string `json:"__meta_services"`
		Environments string `json:"__meta_environments"`
	} `json:"labels"`
}

// ListPrometheusTargets returns all assets
func ListPrometheusTargets(environments string, db *sqlx.DB) ([]prometheusAsset, error) {
	a := []models.Asset{}
	ret := []prometheusAsset{}

	// We can filter the list by specific environments if needed
	environments = strings.ToLower(environments)
	e := strings.Split(environments, ",")
	envs := make(map[string]bool)
	for _, v := range e {
		if v == "" {
			continue
		}
		envs[v] = true
	}

	rels := map[string]bool{
		"system.environments": true,
		"system.services":     true,
		"systems":             true,
	}

	q := buildQuery("name,domain,fqdn", "")
	q = fmt.Sprintf(`%s WHERE decommissioned = 0 AND asset_type = 'server' AND (OS LIKE '%%Linux%%' OR OS LIKE '%%centos%%' OR OS LIKE '%%RHEL%%' OR OS LIKE '%%Red Hat%%' OR OS LIKE '%%Ubuntu%%' OR OS LIKE '%%windows%%')`, q)

	err := db.Select(&a, q)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []prometheusAsset{}, errors.New("there are no assets")
	}
	if err != nil {
		return []prometheusAsset{}, fmt.Errorf("problem with query %s: %w", q, err)
	}

	for k, asset := range a {
		// We get the systems here, rather than checking to see if that relationship has been requested,
		// because in order to find out the asset tier we need to find the related system that has the highest associated tier.
		// If we waited to check whether the relationship had been requested, we'd have to request that relationship in order to see the asset tier.
		relatedSystems := []models.System{}

		err := db.Select(&relatedSystems, "SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = @p1) ORDER BY systems.id", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = %d) ORDER BY systems.id", asset.ID)
			return []prometheusAsset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		a[k], err = getRelationships(asset, rels, relatedSystems, db)
		if err != nil {
			return []prometheusAsset{}, fmt.Errorf("unable to get relationships: %w", err)
		}

		// We need to store the systems, services and environments in a slice so that we can later turn them into a comma separated string for Prometheus.
		var systems []string
		var services []string
		var environments []string

		for _, system := range *a[k].Systems {
			if isNotInSlice(systems, system.System) {
				systems = append(systems, system.System)
			}

			for _, svc := range *system.Services {
				if isNotInSlice(services, svc.Name) {
					services = append(services, svc.Name)
				}
			}

			for _, env := range *system.Environments {
				// If we are not filtering for any environments then we always want to add the asset environments to the output.
				// Otherwise we only add assets with matching environments.
				if len(envs) == 0 {
					if isNotInSlice(environments, env.Name) {
						environments = append(environments, env.Name)
					}
					continue
				}

				if envs[strings.ToLower(env.Name)] {
					if isNotInSlice(environments, env.Name) {
						environments = append(environments, env.Name)
					}
				}
			}
		}

		// If no environments are set, AND we are filtering for specific environments, then we skip adding this asset to the output
		if len(envs) > 0 && len(environments) == 0 {
			continue
		}

		pa := prometheusAsset{
			Targets: []string{fmt.Sprintf("%s.%s:9126", strings.ToLower(a[k].Hostname), strings.ToLower(a[k].Domain))},
		}

		pa.Labels.Systems = strings.ToLower(strings.Join(systems, ","))
		pa.Labels.Services = strings.ToLower(strings.Join(services, ","))
		pa.Labels.Environments = strings.ToLower(strings.Join(environments, ","))

		ret = append(ret, pa)
	}

	return ret, nil
}

func isNotInSlice(source []string, value string) bool {
	for _, item := range source {
		if item == value {
			return false
		}
	}
	return true
}

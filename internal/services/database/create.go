package database

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create new database
func create(dbase models.Database, instanceID int64, db *sqlx.DB) (int64, error) {
	// Create a checksum.  We will use this for comparison purposes in order to know which records should be updated
	checksum, err := getChecksum(dbase, instanceID)
	if err != nil {
		return 0, fmt.Errorf("unable to calculate database checksum: %w", err)
	}

	var id int64

	err = db.Get(&id, "INSERT INTO databases (name, status, recovery_model, read_only, standby, preferred_backup_replica, last_full_backup, last_log_backup, is_primary_replica, instance_id, checksum, compatibility_level, size) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13);SELECT id = convert(bigint, SCOPE_IDENTITY());", dbase.Name, dbase.Status, dbase.RecoveryModel, dbase.ReadOnly, dbase.Standby, dbase.PreferredBackupReplica, dbase.LastFullBackup, dbase.LastLogBackup, dbase.IsPrimaryReplica, instanceID, checksum, dbase.CompatibilityLevel, dbase.Size)
	if err != nil {
		q := fmt.Sprintf("INSERT INTO databases (name, status, recovery_model, read_only, standby, preferred_backup_replica, last_full_backup, last_log_backup, is_primary_replica, instance_id, checksum, compatibility_level, size) VALUES('%s', '%s', '%s', %t, %t, %t, '%s', '%s', %t, %d, '%s', %d, %d);SELECT id = convert(bigint, SCOPE_IDENTITY());", dbase.Name, dbase.Status, dbase.RecoveryModel, dbase.ReadOnly, dbase.Standby, dbase.PreferredBackupReplica, dbase.LastFullBackup, dbase.LastLogBackup, dbase.IsPrimaryReplica, instanceID, checksum, dbase.CompatibilityLevel, dbase.Size)
		return 0, fmt.Errorf("adding new datatabase with query; %s: %w", q, err)
	}

	return id, nil
}

package database

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"

	"rokett.me/hive/internal/models"
)

func getChecksum(db models.Database, instID int64) (string, error) {
	bs, err := json.Marshal([]string{
		db.Name,
		strconv.FormatInt(instID, 10),
	})
	if err != nil {
		return "", fmt.Errorf("unable to marshal database information to calculate checksum: %w", err)
	}

	checksum := sha256.Sum256(bs)

	return hex.EncodeToString(checksum[:]), nil
}

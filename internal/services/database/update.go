package database

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

func update(dbase models.Database, id int64, db *sqlx.DB) error {
	_, err := db.Exec("UPDATE databases SET status = @p1, recovery_model = @p2, read_only = @p3, standby = @p4, preferred_backup_replica = @p5, is_primary_replica = @p6, last_full_backup = @p7, last_log_backup = @p8, compatibility_level = @p9, size = @p10 WHERE id = @p11", dbase.Status, dbase.RecoveryModel, dbase.ReadOnly, dbase.Standby, dbase.PreferredBackupReplica, dbase.IsPrimaryReplica, dbase.LastFullBackup, dbase.LastLogBackup, dbase.CompatibilityLevel, dbase.Size, id)
	if err != nil {
		return fmt.Errorf("updating database with ID %d: %w", id, err)
	}

	return nil
}

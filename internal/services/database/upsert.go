package database

import (
	"fmt"
	"strings"

	"rokett.me/hive/internal/models"
	"rokett.me/hive/internal/services/databasefile"

	"github.com/jmoiron/sqlx"
)

// Upsert either updates an existing record or inserts a new one.  As an added extra, it also deletes records which are not part of the patch.
func Upsert(databases []models.Database, instID int64, db *sqlx.DB) error {
	// We need to retrieve the existing checksums of databases related to the SQL Instance.
	// This allows us to check whether a database already exists, in which case we update the record, or needs to be created.
	// We can also track which existing records have been updated, meaning that records which have not been updated can be deleted.
	dbChecksums := []models.Database{}

	err := db.Select(&dbChecksums, "SELECT id, checksum FROM databases WHERE instance_id = @p1", instID)
	if err != nil {
		return fmt.Errorf("unable to retrieve database checksums: %w", err)
	}

	// Making a map of the checksums allows us to check whether a hash already exists much easier than iterating over the slice each time.
	checksums := make(map[string]int64)

	for _, c := range dbChecksums {
		checksums[c.Checksum] = c.ID
	}

	for _, database := range databases {
		// Create a checksum for comparison.
		checksum, err := getChecksum(database, instID)
		if err != nil {
			return fmt.Errorf("unable to calculate database checksum: %w", err)
		}

		// If the checksum already exists we remove it from the map and update the record.
		// Otherwise we create a new record.
		if _, ok := checksums[checksum]; ok {
			err = update(database, checksums[checksum], db)
			if err != nil {
				return fmt.Errorf("unable to update database with ID %d: %w", checksums[checksum], err)
			}

			database.ID = checksums[checksum]
			delete(checksums, checksum)
		} else {
			database.ID, err = create(database, instID, db)
			if err != nil {
				return fmt.Errorf("unable to create new database: %w", err)
			}
		}

		if database.DatabaseFiles != nil {
			err = databasefile.Upsert(*database.DatabaseFiles, database.ID, db)
			if err != nil {
				return fmt.Errorf("unable to handle database file records: %w", err)
			}
		}
	}

	for _, id := range checksums {
		var stmts []string

		stmts = append(stmts, "DELETE FROM database_files WHERE database_id = @p1")
		stmts = append(stmts, "DELETE FROM databases WHERE id = @p1")

		stmt := strings.Join(stmts, ";")

		_, err := db.Exec(stmt, id)
		if err != nil {
			return fmt.Errorf("error deleting database with ID %d: %w", id, err)
		}
	}

	return nil
}

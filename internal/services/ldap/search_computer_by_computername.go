package ldap

import (
	"fmt"
	"strings"

	"github.com/go-ldap/ldap/v3"
	"rokett.me/hive/internal/models"
)

// SearchComputerByComputerName returns all computer accounts where the computername matches
func SearchComputerByComputerName(DCs string, port int, baseDN string, bindDN string, bindPwd string, computername string, fuzzy bool) ([]models.LDAPComputer, error) {
	l, err := ldap.DialURL(fmt.Sprintf("ldap://%s:%d", strings.TrimSpace(DCs), port))
	if err != nil {
		return []models.LDAPComputer{}, fmt.Errorf("error dialing LDAP server: %w", err)
	}
	defer l.Close()

	err = l.Bind(bindDN, bindPwd)
	if err != nil {
		return []models.LDAPComputer{}, fmt.Errorf("error during LDAP bind: %w", err)
	}

	filter := "(&(objectCategory=Computer)(cn=" + computername
	if fuzzy {
		filter = filter + "*"
	}
	filter = filter + "))"

	searchRequest := ldap.NewSearchRequest(
		baseDN, // The base dn to search
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		filter,         // The filter to apply
		[]string{"cn"}, // A list attributes to retrieve
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		return []models.LDAPComputer{}, fmt.Errorf("error during LDAP search: %w", err)
	}

	var computers []models.LDAPComputer

	for _, entry := range sr.Entries {
		computer := models.LDAPComputer{
			CN: entry.GetAttributeValue("cn"),
		}

		computers = append(computers, computer)
	}

	return computers, nil
}

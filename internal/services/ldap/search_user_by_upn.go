package ldap

import (
	"fmt"
	"regexp"
	"strings"

	"rokett.me/hive/internal/models"

	"github.com/go-ldap/ldap/v3"
)

// SearchUserByUpn returns the user account which matches the given UPN
func SearchUserByUpn(DCs string, port int, baseDN string, bindDN string, bindPwd string, upn string, includeDisabled bool) (models.User, error) {
	l, err := ldap.DialURL(fmt.Sprintf("ldap://%s:%d", strings.TrimSpace(DCs), port))
	if err != nil {
		return models.User{}, fmt.Errorf("error dialing LDAP server: %w", err)
	}
	defer l.Close()

	err = l.Bind(bindDN, bindPwd)
	if err != nil {
		return models.User{}, fmt.Errorf("error during LDAP bind: %w", err)
	}

	searchRequest := ldap.NewSearchRequest(
		baseDN, // The base dn to search
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectCategory=Person)(userPrincipalName=%s))", upn),                     // The filter to apply
		[]string{"givenName", "sn", "sAMAccountName", "userPrincipalName", "userAccountControl"}, // A list attributes to retrieve
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		return models.User{}, fmt.Errorf("error during LDAP search: %w", err)
	}

	var user models.User

	for _, entry := range sr.Entries {
		// 514 = Disabled account
		// 546 = Disabled, password not required
		// 66050 = Disabled, Password Doesn’t Expire
		// 66082 = Disabled, Password Doesn’t Expire & Not Required
		// 262658 = Disabled, Smartcard Required
		// 262690 = Disabled, Smartcard Required, Password Not Required
		// 328194 = Disabled, Smartcard Required, Password Doesn’t Expire
		// 328226 = Disabled, Smartcard Required, Password Doesn’t Expire & Not Required
		var re = regexp.MustCompile(`(?m)^(?:514|546|66050|66082|262658|262690|328194|328226)$`)
		if len(re.FindStringIndex(entry.GetAttributeValue("userAccountControl"))) > 0 {
			user.AccountDisabled = true
		}

		if !includeDisabled {
			continue
		}

		upn := strings.Split(entry.GetAttributeValue("userPrincipalName"), "@")

		user.FirstName = entry.GetAttributeValue("givenName")
		user.Surname = entry.GetAttributeValue("sn")
		user.DistinguishedName = entry.DN
		user.Upn = entry.GetAttributeValue("userPrincipalName")

		if len(upn) == 2 {
			user.Domain = upn[1]
		}

		break
	}

	return user, nil
}

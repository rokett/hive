package ldap

import (
	"fmt"
	"regexp"
	"strings"

	"rokett.me/hive/internal/models"

	"github.com/go-ldap/ldap/v3"
)

// SearchUserByUsername returns all user accounts where the username matches
func SearchUserByUsername(DCs string, port int, baseDN string, bindDN string, bindPwd string, username string, fuzzy bool, includeDisabled bool) ([]models.User, error) {
	l, err := ldap.DialURL(fmt.Sprintf("ldap://%s:%d", strings.TrimSpace(DCs), port))
	if err != nil {
		return []models.User{}, fmt.Errorf("error dialing LDAP server: %w", err)
	}
	defer l.Close()

	err = l.Bind(bindDN, bindPwd)
	if err != nil {
		return []models.User{}, fmt.Errorf("error during LDAP bind: %w", err)
	}

	filter := "(&(objectCategory=Person)(sAMAccountName=" + username
	if fuzzy {
		filter = filter + "*"
	}
	filter = filter + "))"

	searchRequest := ldap.NewSearchRequest(
		baseDN, // The base dn to search
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		filter, // The filter to apply
		[]string{"givenName", "sn", "sAMAccountName", "userPrincipalName", "userAccountControl"}, // A list attributes to retrieve
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		return []models.User{}, fmt.Errorf("error during LDAP search: %w", err)
	}

	var users []models.User

	for _, entry := range sr.Entries {
		if !includeDisabled {
			// 514 = Disabled account
			// 546 = Disabled, password not required
			// 66050 = Disabled, Password Doesn’t Expire
			// 66082 = Disabled, Password Doesn’t Expire & Not Required
			// 262658 = Disabled, Smartcard Required
			// 262690 = Disabled, Smartcard Required, Password Not Required
			// 328194 = Disabled, Smartcard Required, Password Doesn’t Expire
			// 328226 = Disabled, Smartcard Required, Password Doesn’t Expire & Not Required

			var re = regexp.MustCompile(`(?m)^(?:514|546|66050|66082|262658|262690|328194|328226)$`)
			if len(re.FindStringIndex(entry.GetAttributeValue("userAccountControl"))) > 0 {
				continue
			}
		}

		upn := strings.Split(entry.GetAttributeValue("userPrincipalName"), "@")

		user := models.User{
			FirstName:         entry.GetAttributeValue("givenName"),
			Surname:           entry.GetAttributeValue("sn"),
			DistinguishedName: entry.DN,
			Upn:               entry.GetAttributeValue("userPrincipalName"),
		}

		if len(upn) == 2 {
			user.Domain = upn[1]
		}

		users = append(users, user)
	}

	return users, nil
}

package ipaddress

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

func getRelationships(ip models.IP, rels map[string]bool, db *sqlx.DB) (models.IP, error) {
	if rels["network"] || rels["*"] {
		n := models.Network{}

		err := db.Get(&n, "SELECT networks.network, networks.description, networks.gateway, networks.subnet_mask, networks.vlan_id FROM networks WHERE id = @p1", ip.NetworkID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT networks.network, networks.description, networks.gateway, networks.subnet_mask, networks.vlan_id FROM networks WHERE id = %d", ip.NetworkID)
			return models.IP{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		ip.Network = &n
	}

	return ip, nil
}

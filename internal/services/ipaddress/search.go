package ipaddress

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Search returns all IP addresses matching the IP passed
func Search(address string, include string, fuzzy bool, db *sqlx.DB) ([]models.IP, error) {
	ips := []models.IP{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery("")

	if fuzzy {
		q = fmt.Sprintf(`%s WHERE ip LIKE @p1`, q)
		address = fmt.Sprintf(`%%%s%%`, address)
	} else {
		q = fmt.Sprintf(`%s WHERE ip = @p1`, q)
	}

	err := db.Select(&ips, q, address)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.IP{}, errors.New("there are no IP addresses")
	}
	if err != nil {
		return []models.IP{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, ip := range ips {
		ips[k], err = getRelationships(ip, rels, db)
		if err != nil {
			return []models.IP{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return ips, nil
}

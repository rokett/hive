package ipaddress

import (
	"fmt"
	"strings"
)

func buildQuery(fields string) string {
	q := "SELECT"

	if fields != "" {
		reqFields := strings.Split(fields, ",")

		flds := "id"

		for _, fld := range reqFields {
			flds = flds + "," + fld
		}

		q = fmt.Sprintf(`%s %s`, q, flds)
	} else {
		q = fmt.Sprintf(`%s id, ip, hostname, description, static, reserved, stale, network_id, created_at, updated_at`, q)
	}

	q = fmt.Sprintf(`%s FROM ipAddresses`, q)

	return q
}

package databasefile

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create new database file
func create(file models.DatabaseFile, databaseID int64, db *sqlx.DB) (int64, error) {
	// Create a checksum.  We will use this for comparison purposes in order to know which records should be updated
	checksum, err := getChecksum(file, databaseID)
	if err != nil {
		return 0, fmt.Errorf("unable to calculate database file checksum: %w", err)
	}

	var id int64

	err = db.Get(&id, "INSERT INTO database_files (name, path, type, database_id, checksum) VALUES(@p1, @p2, @p3, @p4, @p5);SELECT id = convert(bigint, SCOPE_IDENTITY());", file.Name, file.Path, file.Type, databaseID, checksum)
	if err != nil {
		q := fmt.Sprintf("INSERT INTO database_files (name, path, type, database_id, checksum) VALUES('%s', '%s', '%s', %d, '%s');SELECT id = convert(bigint, SCOPE_IDENTITY());", file.Name, file.Path, file.Type, databaseID, checksum)
		return 0, fmt.Errorf("adding new datatabase file with query; %s: %w", q, err)
	}

	return id, nil
}

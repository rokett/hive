package databasefile

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Upsert either updates an existing record or inserts a new one.  As an added extra, it also deletes records which are not part of the patch.
func Upsert(files []models.DatabaseFile, dbID int64, db *sqlx.DB) error {
	// We need to retrieve the existing checksums of database files related to the database.
	// This allows us to check whether a database file already exists, in which case we update the record, or needs to be created.
	// We can also track which existing records have been updated, meaning that records which have not been updated can be deleted.
	fileChecksums := []models.DatabaseFile{}

	err := db.Select(&fileChecksums, "SELECT id, checksum FROM database_files WHERE database_id = @p1", dbID)
	if err != nil {
		return fmt.Errorf("unable to retrieve database file checksums: %w", err)
	}

	// Making a map of the checksums allows us to check whether a hash already exists much easier than iterating over the slice each time.
	checksums := make(map[string]int64)

	for _, c := range fileChecksums {
		checksums[c.Checksum] = c.ID
	}

	for _, file := range files {
		// Create a checksum for comparison.
		checksum, err := getChecksum(file, dbID)
		if err != nil {
			return fmt.Errorf("unable to calculate database file checksum: %w", err)
		}

		// If the checksum already exists we remove it from the map and update the record.
		// Otherwise we create a new record.
		if _, ok := checksums[checksum]; ok {
			err = update(file, checksums[checksum], db)
			if err != nil {
				return fmt.Errorf("unable to update database file with ID %d: %w", checksums[checksum], err)
			}

			delete(checksums, checksum)
		} else {
			_, err = create(file, dbID, db)
			if err != nil {
				return fmt.Errorf("unable to create new database file: %w", err)
			}
		}
	}

	for _, id := range checksums {
		_, err := db.Exec("DELETE FROM database_files WHERE id = @p1", id)
		if err != nil {
			return fmt.Errorf("error deleting database file with ID %d: %w", id, err)
		}
	}

	return nil
}

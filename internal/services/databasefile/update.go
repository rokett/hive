package databasefile

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

func update(file models.DatabaseFile, id int64, db *sqlx.DB) error {
	_, err := db.Exec("UPDATE database_files SET name = @p1, path = @p2, type = @p3 WHERE id = @p4", file.Name, file.Path, file.Type, id)
	if err != nil {
		return fmt.Errorf("unable to update database file with ID %d: %w", id, err)
	}

	return nil
}

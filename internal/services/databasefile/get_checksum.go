package databasefile

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"

	"rokett.me/hive/internal/models"
)

func getChecksum(file models.DatabaseFile, dbID int64) (string, error) {
	bs, err := json.Marshal([]string{
		file.Name,
		strconv.FormatInt(dbID, 10),
	})
	if err != nil {
		return "", fmt.Errorf("unable to marshal database file information to calculate checksum: %w", err)
	}

	checksum := sha256.Sum256(bs)

	return hex.EncodeToString(checksum[:]), nil
}

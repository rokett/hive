package user

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create a new user
func Create(user models.User, db *sqlx.DB) (int64, error) {
	var id int64

	//TODO Username is deprecated in favour of upn.  Remove when we're ready to remove it from the front end.
	err := db.Get(&id, "INSERT INTO users (firstname, surname, upn, token, token_expiry, token_max_age, last_login) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7);SELECT id = convert(bigint, SCOPE_IDENTITY());", user.FirstName, user.Surname, user.Upn, user.Token, user.TokenExpiry, user.TokenMaxAge, user.LastLogin)
	if err != nil {
		return 0, fmt.Errorf("error adding new user: %w", err)
	}

	return id, nil
}

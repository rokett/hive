package user

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// UpdateUserGroups first deletes any user/group relationships, and then adds new ones
func UpdateUserGroups(id int64, groups []models.Group, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM group_user WHERE user_id = @p1", id)
	if err != nil {
		q := fmt.Sprintf("DELETE FROM group_user WHERE user_id = %d", id)

		return fmt.Errorf("probem with query %s: %w", q, err)
	}

	for _, g := range groups {
		_, err := db.Exec("INSERT INTO group_user (group_id, user_id) VALUES (@p1, @p2);", g.ID, id)
		if err != nil {
			q := fmt.Sprintf("INSERT INTO group_user (group_id, user_id) VALUES (%d, %d);", g.ID, id)

			return fmt.Errorf("error updating user/group relationships: %s: %w", q, err)
		}
	}

	return nil
}

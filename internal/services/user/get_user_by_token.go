package user

import (
	"database/sql"
	"fmt"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// GetUserByToken returns the user record whose token value matches that provided to the function
func GetUserByToken(token string, db *sqlx.DB) (models.User, error) {
	u := models.User{}

	query := fmt.Sprintf("SELECT id, firstname, surname, username, token, token_expiry, token_max_age, last_login, created_at, updated_at FROM users WHERE token = %s;", token)

	err := db.Get(&u, "SELECT id, firstname, surname, username, token, token_expiry, token_max_age, last_login, created_at, updated_at FROM users WHERE token = @p1;", token)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return models.User{}, errors.New("user does not exist")
	}
	if err != nil {
		return models.User{}, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return u, nil
}

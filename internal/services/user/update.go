package user

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Update an existing user
func Update(u models.User, id int64, db *sqlx.DB) error {
	_, err := db.Exec("UPDATE users SET firstname = @p1, surname = @p2, token = @p3, token_expiry = @p4, token_max_age = @p5, last_login = @p6 WHERE ID = @p7;", u.FirstName, u.Surname, u.Token, u.TokenExpiry, u.TokenMaxAge, u.LastLogin, &id)
	if err != nil {
		return fmt.Errorf("error updating user: %w", err)
	}

	return nil
}

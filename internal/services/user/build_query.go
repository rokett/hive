package user

import (
	"fmt"
	"strings"
)

func buildQuery(fields string, sort string) string {
	q := "SELECT"

	if fields != "" {
		reqFields := strings.Split(fields, ",")

		flds := "id"

		for _, fld := range reqFields {
			flds = flds + "," + fld
		}

		q = fmt.Sprintf(`%s %s`, q, flds)
	} else {
		//TODO Username is deprecated in favour of upn.  Remove when we're ready to remove it from the front end.
		q = fmt.Sprintf(`%s id, firstname, surname, username, upn, token, token_expiry, token_max_age, last_login, created_at, updated_at`, q)
	}

	q = fmt.Sprintf(`%s FROM users`, q)

	if sort != "" {
		var sortString string

		reqSort := strings.Split(sort, ",")

		for _, v := range reqSort {
			var parts []string

			if strings.Contains(v, ":") {
				parts = strings.Split(v, ":")
			} else {
				parts = []string{v, "asc"}
			}

			sortString = fmt.Sprintf("%s %s %s,", sortString, parts[0], parts[1])
		}

		sortString = strings.TrimSuffix(sortString, ",")

		q = fmt.Sprintf("%s ORDER BY %s", q, sortString)
	}

	return q
}

package user

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsInUse returns true if a user has any role on a system
func IsInUse(id int64, db *sqlx.DB) (bool, error) {
	var count int64

	err := db.Get(&count, "SELECT COUNT(user_id) FROM system_user_role WHERE user_id = @p1", id)
	if err != nil {
		q := fmt.Sprintf("SELECT COUNT(user_id) FROM system_user_role WHERE user_id = %d", id)

		return true, fmt.Errorf("problem with query %s: %w", q, err)
	}

	if count > 0 {
		return true, nil
	}

	return false, nil
}

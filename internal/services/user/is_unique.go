package user

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUnique checks that a user does not exist
func IsUnique(upn string, db *sqlx.DB) (bool, int64, error) {
	var id int64

	query := fmt.Sprintf("SELECT id FROM users WHERE upn = %s;", upn)

	err := db.Get(&id, "SELECT id FROM users WHERE upn = @p1;", upn)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

package user

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// List returns all users
func List(fields string, include string, sort string, db *sqlx.DB) ([]models.User, error) {
	u := []models.User{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, sort)

	err := db.Select(&u, q)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.User{}, errors.New("there are no users")
	}
	if err != nil {
		return []models.User{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, user := range u {
		u[k], err = getRelationships(user, rels, db)
		if err != nil {
			return []models.User{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return u, nil
}

package user

import (
	"database/sql"
	"fmt"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// View returns a single user by ID
func View(id int64, db *sqlx.DB) (models.User, error) {
	u := models.User{}

	query := fmt.Sprintf("SELECT id, firstname, surname, upn, token, token_expiry, token_max_age, last_login, created_at, updated_at FROM users WHERE ID = %d;", id)

	err := db.Get(&u, "SELECT id, firstname, surname, upn, token, token_expiry, token_max_age, last_login, created_at, updated_at FROM users WHERE ID = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return models.User{}, errors.New("user does not exist")
	}
	if err != nil {
		return models.User{}, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return u, nil
}

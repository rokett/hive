package user

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

// UpdateLastLogin sets the last_login timestamp field
func UpdateLastLogin(id int64, db *sqlx.DB) error {
	_, err := db.Exec("UPDATE users SET last_login = @p1 WHERE ID = @p2;", time.Now(), &id)
	if err != nil {
		return fmt.Errorf("error updating user last login timestamp: %w", err)
	}

	return nil
}

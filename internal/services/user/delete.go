package user

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
)

// Delete an existing user
func Delete(id int64, db *sqlx.DB) error {
	var stmts []string

	stmts = append(stmts, "DELETE FROM system_user_role WHERE user_id = @p1")
	stmts = append(stmts, "DELETE FROM users WHERE id = @p1")

	stmt := strings.Join(stmts, ";")

	_, err := db.Exec(stmt, id)
	if err != nil {
		return fmt.Errorf("error deleting user with ID %d: %w", id, err)
	}

	return nil
}

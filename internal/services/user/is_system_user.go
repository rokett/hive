package user

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsSystemUser returns true if a user is the system user
func IsSystemUser(id int64, db *sqlx.DB) (bool, error) {
	var username string

	err := db.Get(&username, "SELECT username FROM users WHERE id = @p1", id)
	if err != nil {
		q := fmt.Sprintf("SELECT username FROM users WHERE id = %d", id)

		return true, fmt.Errorf("problem with query %s: %w", q, err)
	}

	if username == "system" {
		return true, nil
	}

	return false, nil
}

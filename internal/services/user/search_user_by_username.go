package user

import (
	"database/sql"
	"fmt"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// SearchUserByUsername returns the user object specified by the username
func SearchUserByUsername(u string, fields string, fuzzy bool, db *sqlx.DB) (models.User, error) {
	user := models.User{}

	q := buildQuery(fields, "")

	if fuzzy {
		q = fmt.Sprintf(`%s WHERE username LIKE @p1`, q)
		u = fmt.Sprintf(`%%%s%%`, u)
	} else {
		q = fmt.Sprintf(`%s WHERE username = @p1`, q)
	}

	err := db.Get(&user, q, u)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return models.User{}, errors.New("user does not exist")
	}
	if err != nil {
		q = buildQuery(fields, "")

		q = fmt.Sprintf(`%s WHERE username = %s`, q, u)

		return models.User{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	return user, nil
}

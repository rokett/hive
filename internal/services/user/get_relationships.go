package user

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(u models.User, rels map[string]bool, db *sqlx.DB) (models.User, error) {
	if rels["groups"] || rels["*"] {
		g := []models.Group{}

		err := db.Select(&g, "SELECT groups.id, groups.[group], groups.group_dn FROM group_user INNER JOIN groups ON group_user.group_id = groups.id WHERE (group_user.user_id = @p1)", u.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT groups.id, groups.[group], groups.group_dn FROM group_user INNER JOIN groups ON group_user.group_id = groups.id WHERE (group_user.user_id = %d)", u.ID)
			return models.User{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		u.Groups = &g
	}

	return u, nil
}

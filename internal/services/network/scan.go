package network

import (
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Scan runs a check over each of the IPs listed in the DB to see if they are alive. If they are alive the hostname is found (or attempted) and the record updated.
func Scan(stale int, db *sqlx.DB, logger *slog.Logger, debug bool) {
	// We're marking the start time here so that we can clear out stale records later
	start := time.Now().UTC()

	logger.Info("starting network scan")

	ips := []models.IP{}

	err := db.Select(&ips, "SELECT id, ip, hostname, description FROM ipAddresses WHERE static = 0 AND reserved = 0;")
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := "SELECT id, ip, hostname, description FROM ipAddresses WHERE static = 0 AND reserved = 0;"

		logger.Error(
			"retrieving IP addresses",
			"error", err.Error(),
			"query", q,
		)

		return
	}

	if len(ips) == 0 {
		logger.Error("no IP addresses returned")

		return
	}

	// We want to limit the number of concurrent goroutines to concurrentMax.
	// Once a goroutine finishes, the next one can start.
	concurrentMax := 100
	pingChan := make(chan models.IP, concurrentMax)
	pongChan := make(chan models.IP, len(ips))
	doneChan := make(chan []models.IP)

	// Now we can spin up the concurrent goroutines, up to concurrentMax, which will wait for a task to be sent in.
	for i := 0; i < concurrentMax; i++ {
		go ping(pingChan, pongChan, logger)
	}

	// The last step in the setup is to spin up a goroutine to receive alive IPs
	go receivePong(len(ips), pongChan, doneChan)

	// Now the work starts.  We loop through all of the IPs and send the struct through to pingChan.
	// This is picked up by one of the goroutines we spun up above.
	// Once the concurrentMax is reached, the code blocks until a goroutine is available.
	originalIPs := make(map[string]string)
	for _, ip := range ips {
		if debug {
			logger.Info(
				"scanning IP",
				"ip", ip.IPAddress,
			)
		}

		originalIPs[ip.IPAddress] = ip.Hostname

		pingChan <- ip
	}

	// The code blocks here until the contents of doneChan are sent.  aliveIPs is of type []models.IP.
	aliveIPs := <-doneChan

	for _, ip := range aliveIPs {
		if debug {
			logger.Info(
				"updating IP",
				"ip", ip.IPAddress,
				"hostname", ip.Hostname,
				"id", ip.ID,
			)
		}

		if originalIPs[ip.IPAddress] != ip.Hostname {
			ip.Description = ""
		}

		_, err := db.NamedExec("UPDATE ipAddresses SET hostname = :hostname, description = :description, stale = 0 WHERE id = :id;", ip)
		if err != nil {
			q := fmt.Sprintf("UPDATE ipAddresses SET hostname = '%s', description = '%s', stale = 0 WHERE id = %d;", ip.Hostname, ip.Description, ip.ID)

			logger.Error(
				"updating IP addresses from scan",
				"error", err.Error(),
				"query", q,
			)

			return
		}
	}

	// Now we need to find stale IPs; those which are not static or reserved.
	// If the records were updated within the last 10 days we mark the entry as stale.
	uDate := start.AddDate(0, 0, -stale)
	_, err = db.Exec("UPDATE ipAddresses SET stale = 1 WHERE hostname <> '' AND static = 0 and reserved = 0 AND updated_at > @p1 AND updated_at < @p2", uDate, start)
	if err != nil {
		q := fmt.Sprintf("UPDATE ipAddresses SET stale = 1 WHERE hostname <> '' AND static = 0 AND reserved = 0 AND updated_at > '%s' AND updated_at < '%s';", uDate, start)

		logger.Error(
			"clearing stale IP addresses",
			"error", err.Error(),
			"query", q,
		)

		return
	}

	// If the records have not been updated within the last 10 days the IPs are cleared out.
	_, err = db.Exec("UPDATE ipAddresses SET hostname = '', description = '', stale = 0 WHERE hostname <> '' AND static = 0 AND reserved = 0 AND updated_at < @p1;", uDate)
	if err != nil {
		q := fmt.Sprintf("UPDATE ipAddresses SET hostname = '', description = '', stale = 0 WHERE hostname <> '' AND static = 0 AND reserved = 0 AND updated_at < '%s';", uDate)

		logger.Error(
			"clearing stale IP addresses",
			"error", err.Error(),
			"query", q,
		)

		return
	}

	logger.Info("completed network scan")
}

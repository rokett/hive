package network

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// View returns a single network by ID
func View(id int64, fields string, include string, db *sqlx.DB) (models.Network, error) {
	n := models.Network{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, "")
	q = fmt.Sprintf(`%s WHERE ID = @p1`, q)

	err := db.Get(&n, q, id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return models.Network{}, errors.New("network does not exist")
	}
	if err != nil {
		q = buildQuery(fields, "")
		q = fmt.Sprintf(`%s WHERE ID = %d`, q, id)

		return models.Network{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	n, err = getRelationships(n, rels, db)
	if err != nil {
		return models.Network{}, fmt.Errorf("unable to get relationships: %w", err)
	}

	return n, nil
}

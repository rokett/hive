package network

import (
	"bytes"
	"fmt"
	"log/slog"
	"net"
	"os/exec"
	"strings"

	"rokett.me/hive/internal/models"
)

func ping(ips <-chan models.IP, pongChan chan<- models.IP, logger *slog.Logger) {
	for ip := range ips {
		cmd := exec.Command("ping", "-c", "4", ip.IPAddress)

		var out bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &stderr
		err := cmd.Run()

		//TODO Could do with catching errors here and potentially logging them
		if err != nil || strings.Contains(out.String(), "timed out") || strings.Contains(out.String(), "unreachable") {
			pongChan <- models.IP{}
		} else {
			hostnames, err := net.LookupAddr(ip.IPAddress)

			if len(hostnames) > 0 {
				ip.Hostname = strings.TrimSuffix(hostnames[0], ".")
			}

			if err != nil {
				logger.Error(
					fmt.Sprintf("reverse DNS lookup for %s", ip.IPAddress),
					"error", err.Error(),
				)
			}

			if err != nil || len(hostnames) == 0 {
				ip.Hostname = "unknown"
			}

			pongChan <- ip
		}
	}
}

func receivePong(pongNum int, pongChan <-chan models.IP, doneChan chan<- []models.IP) {
	var aliveIPs []models.IP

	for i := 0; i < pongNum; i++ {
		pong := <-pongChan

		if pong.Hostname != "" {
			aliveIPs = append(aliveIPs, pong)
		}

	}

	doneChan <- aliveIPs
}

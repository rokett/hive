package network

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(nw models.Network, rels map[string]bool, db *sqlx.DB) (models.Network, error) {
	if rels["ips"] || rels["*"] {
		ips := []models.IP{}

		err := db.Select(&ips, "SELECT id, ip, hostname, description, static, reserved, stale FROM ipAddresses WHERE network_id = @p1;", nw.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, ip, hostname, description, static, reserved, stale FROM ipAddresses WHERE network_id = %d;", nw.ID)
			return models.Network{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		nw.IPs = ips
	}

	if rels["organisations"] || rels["*"] {
		org := []models.Organisation{}

		err := db.Select(&org, "SELECT organisations.organisation, organisations.id FROM network_organisation INNER JOIN organisations ON network_organisation.organisation_id = organisations.id WHERE (network_organisation.network_id = @p1)", nw.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.organisation, organisations.id FROM network_organisation INNER JOIN organisations ON network_organisation.organisation_id = organisations.id WHERE (network_organisation.network_id = %d)", nw.ID)
			return models.Network{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		nw.Organisation = org
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'network' AND object_id = @p1;", nw.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'network' AND object_id = %d;", nw.ID)
			return models.Network{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		nw.EventLog = &evt
	}*/

	return nw, nil
}

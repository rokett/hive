package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSystemUserRelationship creates an entry in the system_user_role pivot table to link a user to a system with a specific role
func CreateSystemUserRelationship(sysID int64, uID int64, role string, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO system_user_role (system_id, user_id, role) VALUES(@p1, @p2, @p3);", sysID, uID, role)
	if err != nil {
		return fmt.Errorf("error adding new system/user relationship: %w", err)
	}

	return nil
}

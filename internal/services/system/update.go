package system

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Update an existing system
func Update(s models.System, id int64, db *sqlx.DB) error {
	q := "UPDATE systems SET"

	param := 1
	var params []interface{}

	if s.System != "" {
		q = fmt.Sprintf("%s name = @p%d,", q, param)
		params = append(params, s.System)
		param++
	}

	if s.Description != "" {
		q = fmt.Sprintf("%s description = @p%d,", q, param)
		params = append(params, s.Description)
		param++
	}

	if s.Tier != 0 {
		q = fmt.Sprintf("%s tier = @p%d,", q, param)
		params = append(params, s.Tier)
		param++
	}

	if s.OnCall != nil {
		q = fmt.Sprintf("%s oncall = @p%d,", q, param)
		params = append(params, s.OnCall)
		param++
	}

	if s.Websites != "" {
		q = fmt.Sprintf("%s websites = @p%d,", q, param)
		params = append(params, s.Websites)
		param++
	}

	if s.DistributionLists != "" {
		q = fmt.Sprintf("%s distribution_lists = @p%d,", q, param)
		params = append(params, s.DistributionLists)
		param++
	}

	if s.Shortcode != "" {
		q = fmt.Sprintf("%s shortcode = @p%d", q, param)
		params = append(params, strings.ToUpper(s.Shortcode))
		param++
	}

	q = strings.TrimRight(q, ",")

	q = fmt.Sprintf("%s WHERE id = @p%d;", q, param)
	params = append(params, id)

	_, err := db.Exec(q, params...)
	if err != nil {
		return fmt.Errorf("error updating system %s: %w", s.System, err)
	}

	return nil
}

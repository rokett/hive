package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteSystemSupplierRelationship an existing system/supplier relationship
func DeleteSystemSupplierRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM supplier_system WHERE system_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting system/supplier relationship with System ID %d: %w", id, err)
	}

	return nil
}

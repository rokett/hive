package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSystemEnvironmentRelationship creates an entry in the environment_system pivot table to link a system to an environment
func CreateSystemEnvironmentRelationship(sysID int64, envID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO environment_system (environment_id, system_id) VALUES(@p1, @p2);", envID, sysID)
	if err != nil {
		return fmt.Errorf("error adding new system/environment relationship: %w", err)
	}

	return nil
}

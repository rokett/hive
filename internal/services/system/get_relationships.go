package system

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(s models.System, rels map[string]bool, db *sqlx.DB) (models.System, error) {
	devs := []models.User{}

	//TODO Username is deprecated in favour of upn.  Remove when we're ready to remove it from the front end.
	err := db.Select(&devs, "SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'developer')", s.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'developer')", s.ID)
		return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
	}

	s.Developers = &devs

	systemManagers := []models.User{}

	//TODO Username is deprecated in favour of upn.  Remove when we're ready to remove it from the front end.
	err = db.Select(&systemManagers, "SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'system manager')", s.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'system manager')", s.ID)
		return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
	}

	s.SystemManagers = &systemManagers

	iaos := []models.User{}

	//TODO Username is deprecated in favour of upn.  Remove when we're ready to remove it from the front end.
	err = db.Select(&iaos, "SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'iao')", s.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'iao')", s.ID)
		return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
	}

	s.IAOs = &iaos

	authUsers := []models.User{}

	//TODO Username is deprecated in favour of upn.  Remove when we're ready to remove it from the front end.
	err = db.Select(&authUsers, "SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'authorised user')", s.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.username, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'authorised user')", s.ID)
		return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
	}

	s.AuthorisedUsers = &authUsers

	if rels["assets"] || rels["*"] {
		a := []models.Asset{}

		err := db.Select(&a, "SELECT DISTINCT assets.name, assets.id, assets.domain, assets.fqdn FROM asset_system INNER JOIN assets ON asset_system.asset_id = assets.id WHERE (asset_system.system_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT DISTINCT assets.name, assets.id, assets.domain, assets.fqdn FROM asset_system INNER JOIN assets ON asset_system.asset_id = assets.id WHERE (asset_system.system_id = %d)", s.ID)
			return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		if rels["asset.environments"] || rels["*"] {
			for k := range a {
				envs := []models.Environment{}

				err := db.Select(&envs, "SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = @p1 AND asset_system.system_id = @p2)", a[k].ID, s.ID)
				if err != nil && !errors.Is(err, sql.ErrNoRows) {
					q := fmt.Sprintf("SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = %d AND asset_system.system_id = %d)", a[k].ID, s.ID)
					return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
				}

				a[k].Environments = &envs
			}
		}

		s.Assets = &a
	}

	if rels["suppliers"] || rels["*"] {
		sup := []models.Supplier{}

		err := db.Select(&sup, "SELECT suppliers.id, suppliers.supplier FROM supplier_system INNER JOIN suppliers ON supplier_system.supplier_id = suppliers.id WHERE (supplier_system.system_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT suppliers.id, suppliers.supplier FROM supplier_system INNER JOIN suppliers ON supplier_system.supplier_id = suppliers.id WHERE (supplier_system.system_id = %d)", s.ID)
			return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		if rels["supplier.accounts"] || rels["*"] {
			for k := range sup {
				u := []models.User{}

				err := db.Select(&u, "SELECT users.id, users.upn FROM supplier_user INNER JOIN users ON supplier_user.user_id = users.id WHERE (supplier_user.supplier_id = @p1)", sup[k].ID)
				if err != nil && !errors.Is(err, sql.ErrNoRows) {
					q := fmt.Sprintf("SELECT users.id, users.upn FROM supplier_user INNER JOIN users ON supplier_user.user_id = users.id WHERE (supplier_user.supplier_id = %d)", sup[k].ID)
					return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
				}

				sup[k].Accounts = &u
			}
		}

		s.Suppliers = &sup
	}

	if rels["organisations"] || rels["*"] {
		org := []models.Organisation{}

		err := db.Select(&org, "SELECT organisations.id, organisations.organisation FROM organisation_system INNER JOIN organisations ON organisation_system.organisation_id = organisations.id WHERE (organisation_system.system_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.id, organisations.organisation FROM organisation_system INNER JOIN organisations ON organisation_system.organisation_id = organisations.id WHERE (organisation_system.system_id = %d)", s.ID)
			return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.Organisations = &org
	}

	if rels["environments"] || rels["*"] {
		env := []models.Environment{}

		err := db.Select(&env, "SELECT environments.id, environments.name FROM environment_system INNER JOIN environments ON environment_system.environment_id = environments.id WHERE (environment_system.system_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT environments.id, environments.name FROM environment_system INNER JOIN environments ON environment_system.environment_id = environments.id WHERE (environment_system.system_id = %d)", s.ID)
			return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.Environments = &env
	}

	if rels["services"] || rels["*"] {
		svc := []models.Service{}

		err := db.Select(&svc, "SELECT services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = @p1)", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = %d)", s.ID)
			return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.Services = &svc
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'system' AND object_id = @p1;", s.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'system' AND object_id = %d;", s.ID)
			return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		s.EventLog = &evt
	}*/

	return s, nil
}

package system

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSystemServiceRelationship checks that a system/service relationship does not exist
func IsUniqueSystemServiceRelationship(sysID int64, svcID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT system_id FROM service_system WHERE system_id = @p1 AND service_id = @p2;", sysID, svcID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT system_id FROM service_system WHERE system_id = %d AND service_id = %d;", sysID, svcID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

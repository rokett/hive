package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteSystemEnvironmentRelationship an existing system/environment relationship
func DeleteSystemEnvironmentRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM environment_system WHERE system_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting system/environment relationship with System ID %d: %w", id, err)
	}

	return nil
}

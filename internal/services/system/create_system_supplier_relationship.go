package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSystemSupplierRelationship creates an entry in the supplier_system pivot table to link a system to a supplier
func CreateSystemSupplierRelationship(sysID int64, supplierID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO supplier_system (supplier_id, system_id) VALUES(@p1, @p2);", supplierID, sysID)
	if err != nil {
		return fmt.Errorf("error adding new system/supplier relationship: %w", err)
	}

	return nil
}

package system

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSystemEnvironmentRelationship checks that system/environment relationship does not exist
func IsUniqueSystemEnvironmentRelationship(sysID int64, envID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT system_id FROM environment_system WHERE environment_id = @p1 AND system_id = @p2;", envID, sysID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT system_id FROM environment_system WHERE environment_id = %d AND system_id = %d;", envID, sysID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteSystemAssetRelationship an existing system/asset relationship
func DeleteSystemAssetRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM asset_system WHERE system_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting system/asset relationship with System ID %d: %w", id, err)
	}

	return nil
}

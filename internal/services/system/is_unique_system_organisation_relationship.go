package system

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSystemOrganisationRelationship checks that system/organisation relationship does not exist
func IsUniqueSystemOrganisationRelationship(sysID int64, orgID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT system_id FROM organisation_system WHERE organisation_id = @p1 AND system_id = @p2;", orgID, sysID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT system_id FROM organisation_system WHERE organisation_id = %d AND system_id = %d;", orgID, sysID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

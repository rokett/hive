package system

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSystemUserRelationship checks that system/user relationship does not exist
func IsUniqueSystemUserRelationship(sysID int64, uID int64, role string, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT system_id FROM system_user_role WHERE system_id = @p1 AND user_id = @p2 AND role = @p3;", sysID, uID, role)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT system_id FROM system_user_role WHERE system_id = %d AND user_id = %d AND role = %s;", sysID, uID, role)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

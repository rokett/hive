package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSystemServiceRelationship creates an entry in the service_system pivot table to link a service to a system
func CreateSystemServiceRelationship(sysID int64, svcID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO service_system (system_id, service_id) VALUES(@p1, @p2);", sysID, svcID)
	if err != nil {
		return fmt.Errorf("error adding new service/system relationship: %w", err)
	}

	return nil
}

package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteSystemUserRelationship an existing system/user relationship
func DeleteSystemUserRelationship(id int64, role string, db *sqlx.DB) error {
	q := "DELETE FROM system_user_role WHERE system_id = @p1"

	if role != "" {
		q = fmt.Sprintf("%s AND role = '%s'", q, role)
	}

	q = fmt.Sprintf("%s;", q)

	_, err := db.Exec(q, id)
	if err != nil {
		return fmt.Errorf("error deleting system/user relationship with System ID %d: %w", id, err)
	}

	return nil
}

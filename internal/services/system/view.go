package system

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// View returns a single system by ID
func View(id int64, fields string, include string, db *sqlx.DB) (models.System, error) {
	s := models.System{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, "")
	q = fmt.Sprintf(`%s WHERE ID = @p1`, q)

	err := db.Get(&s, q, id)
	if err != nil && err == sql.ErrNoRows {
		return models.System{}, errors.New("system does not exist")
	}
	if err != nil {
		q = buildQuery(fields, "")
		q = fmt.Sprintf(`%s WHERE ID = %d`, q, id)

		return models.System{}, fmt.Errorf("problem with query %s: %w", q, err)
	}

	s, err = getRelationships(s, rels, db)
	if err != nil {
		return models.System{}, fmt.Errorf("unable to get relationships: %w", err)
	}

	return s, nil
}

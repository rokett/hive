package system

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// List returns all systems
func List(fields string, include string, sort string, db *sqlx.DB) ([]models.System, error) {
	s := []models.System{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery(fields, sort)

	err := db.Select(&s, q)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.System{}, errors.New("there are no systems")
	}
	if err != nil {
		return []models.System{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, sys := range s {
		s[k], err = getRelationships(sys, rels, db)
		if err != nil {
			return []models.System{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return s, nil
}

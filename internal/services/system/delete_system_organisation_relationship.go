package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteSystemOrganisationRelationship an existing system/organisation relationship
func DeleteSystemOrganisationRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM organisation_system WHERE system_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting system/organisation relationship with System ID %d: %w", id, err)
	}

	return nil
}

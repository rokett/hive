package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSystemAssetRelationship creates an entry in the asset_system pivot table to link an asset to a system
func CreateSystemAssetRelationship(sysID int64, assetID int64, envID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO asset_system (asset_id, system_id, environment_id) VALUES(@p1, @p2, @p3);", assetID, sysID, envID)
	if err != nil {
		return fmt.Errorf("error adding new system/asset relationship: %w", err)
	}

	return nil
}

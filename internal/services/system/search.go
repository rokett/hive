package system

import (
	"database/sql"
	"fmt"
	"strings"

	"errors"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Search returns all systems matching the name passed
func Search(system string, shortcode string, include string, fuzzy bool, db *sqlx.DB) ([]models.System, error) {
	s := []models.System{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery("", "")
	q = fmt.Sprintf(`%s WHERE`, q)

	paramIndex := 1
	var params []interface{}

	if system != "" {
		if fuzzy {
			q = fmt.Sprintf(`%s (name LIKE @p%d)`, q, paramIndex)
			params = append(params, fmt.Sprintf(`%%%s%%`, system))
			paramIndex++
		} else {
			q = fmt.Sprintf(`%s (name = @p%d)`, q, paramIndex)
			params = append(params, system)
			paramIndex++
		}
	}

	if shortcode != "" {
		if strings.Contains(q, "name LIKE") {
			q = fmt.Sprintf(`%s AND (`, q)
		} else {
			q = fmt.Sprintf(`%s (`, q)
		}

		q = fmt.Sprintf(`%sshortcode = @p%d)`, q, paramIndex)
		params = append(params, shortcode)
	}

	err := db.Select(&s, q, params...)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.System{}, errors.New("there are no systems")
	}
	if err != nil {
		return []models.System{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, sys := range s {
		s[k], err = getRelationships(sys, rels, db)
		if err != nil {
			return []models.System{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return s, nil
}

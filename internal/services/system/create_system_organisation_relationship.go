package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// CreateSystemOrganisationRelationship creates an entry in the organisation_system pivot table to link a system to an organisation
func CreateSystemOrganisationRelationship(sysID int64, orgID int64, db *sqlx.DB) error {
	_, err := db.Exec("INSERT INTO organisation_system (organisation_id, system_id) VALUES(@p1, @p2);", orgID, sysID)
	if err != nil {
		return fmt.Errorf("error adding new system/organisation relationship: %w", err)
	}

	return nil
}

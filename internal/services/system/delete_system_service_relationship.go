package system

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// DeleteSystemServiceRelationship an existing system/service relationship
func DeleteSystemServiceRelationship(id int64, db *sqlx.DB) error {
	_, err := db.Exec("DELETE FROM service_system WHERE system_id = @p1;", id)
	if err != nil {
		return fmt.Errorf("error deleting system/service relationship with System ID %d: %w", id, err)
	}

	return nil
}

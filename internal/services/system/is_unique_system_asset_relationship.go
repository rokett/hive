package system

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// IsUniqueSystemAssetRelationship checks that a system/asset relationship does not exist
func IsUniqueSystemAssetRelationship(sysID int64, assetID int64, envID int64, db *sqlx.DB) (bool, int64, error) {
	var id int64

	err := db.Get(&id, "SELECT system_id FROM asset_system WHERE asset_id = @p1 AND system_id = @p2 AND environment_id = @p3;", assetID, sysID, envID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT system_id FROM asset_system WHERE asset_id = %d AND system_id = %d AND environment_id = %d;", assetID, sysID, envID)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}

package volume

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"

	"rokett.me/hive/internal/models"
)

func getChecksum(volume models.Volume, assetID int64) (string, error) {
	bs, err := json.Marshal([]string{
		strconv.FormatUint(volume.BlockSize, 10),
		volume.DriveLetter,
		volume.Filesystem,
		volume.Label,
		strconv.FormatInt(assetID, 10),
	})
	if err != nil {
		return "", fmt.Errorf("unable to marshal volume information to calculate checksum: %w", err)
	}

	checksum := sha256.Sum256(bs)

	return hex.EncodeToString(checksum[:]), nil
}

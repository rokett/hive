package volume

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Upsert either updates an existing record or inserts a new one.  As an added extra, it also deletes records which are not part of the patch.
func Upsert(volumes []models.Volume, assetID int64, db *sqlx.DB) error {
	// We need to retrieve the existing checksums of volume related to the asset.
	// This allows us to check whether a volume already exists, in which case we update the record, or needs to be created.
	// We can also track which existing record volumes have been updated, meaning that records which have not been updated can be deleted.
	volChecksums := []models.Volume{}

	err := db.Select(&volChecksums, "SELECT id, checksum FROM volumes WHERE asset_id = @p1", assetID)
	if err != nil {
		return fmt.Errorf("unable to retrieve volume checksums: %w", err)
	}

	// Making a map of the checksums allows us to check whether a hash exists much easier than iterating over the slice each time.
	checksums := make(map[string]int64)

	for _, v := range volChecksums {
		checksums[v.Checksum] = v.ID
	}

	for _, vol := range volumes {
		// Create a checksum for comparison.
		checksum, err := getChecksum(vol, assetID)
		if err != nil {
			return fmt.Errorf("unable to calculate volume checksum: %w", err)
		}

		// If the checksum already exists we remove it from the map and update the record.
		// Otherwise we create a new volume record.
		if _, ok := checksums[checksum]; ok {
			err = update(vol, checksums[checksum], db)
			if err != nil {
				return fmt.Errorf("unable to update volume with ID %d: %w", checksums[checksum], err)
			}

			delete(checksums, checksum)
		} else {
			err = create(vol, assetID, db)
			if err != nil {
				return fmt.Errorf("unable to create new volume: %w", err)
			}
		}
	}

	for _, id := range checksums {
		_, err := db.Exec("DELETE FROM volumes WHERE id = @p1", id)
		if err != nil {
			return fmt.Errorf("error deleting volume with ID %d: %w", id, err)
		}
	}

	return nil
}

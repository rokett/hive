package volume

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

func update(volume models.Volume, id int64, db *sqlx.DB) error {
	_, err := db.Exec("UPDATE volumes SET capacity = @p1, free_space = @p2, disk_model = @p3 WHERE id = @p4", volume.Capacity, volume.FreeSpace, volume.DiskModel, id)
	if err != nil {
		return fmt.Errorf("updating volume with ID %d: %w", id, err)
	}

	return nil
}

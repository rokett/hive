package volume

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/models"
)

// Create adds a new record for a volume
func create(volume models.Volume, assetID int64, db *sqlx.DB) error {
	// Create a checksum.  We will use this for comparison purposes in order to know which records should be updated
	checksum, err := getChecksum(volume, assetID)
	if err != nil {
		return fmt.Errorf("unable to calculate volume checksum: %w", err)
	}

	_, err = db.Exec("INSERT INTO volumes (block_size, capacity, drive_letter, filesystem, free_space, label, disk_model, checksum, asset_id) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9);", volume.BlockSize, volume.Capacity, volume.DriveLetter, volume.Filesystem, volume.FreeSpace, volume.Label, volume.DiskModel, checksum, assetID)
	if err != nil {
		return fmt.Errorf("error adding new volume: %w", err)
	}

	return nil
}

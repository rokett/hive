package software

import (
	"database/sql"
	"errors"
	"fmt"

	"rokett.me/hive/internal/models"

	"github.com/jmoiron/sqlx"
)

func getRelationships(sw models.Software, rels map[string]bool, db *sqlx.DB) (models.Software, error) {
	if rels["assets"] || rels["*"] {
		a := []models.Asset{}

		err := db.Select(&a, "SELECT assets.name, assets.id, assets.domain, assets.fqdn FROM asset_software INNER JOIN assets ON asset_software.asset_id = assets.id WHERE (asset_software.software_id = @p1)", sw.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT assets.name, assets.id, assets.domain, assets.fqdn FROM asset_software INNER JOIN assets ON asset_software.asset_id = assets.id WHERE (asset_software.software_id = %d)", sw.ID)
			return models.Software{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		sw.Assets = &a
	}

	return sw, nil
}

package software

import (
	"database/sql"
	"fmt"
	"strings"

	"rokett.me/hive/internal/models"

	"errors"

	"github.com/jmoiron/sqlx"
)

// Search returns all software matching the name passed
func Search(name string, include string, fuzzy bool, db *sqlx.DB) ([]models.Software, error) {
	s := []models.Software{}

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	q := buildQuery("")

	if fuzzy {
		q = fmt.Sprintf(`%s WHERE name LIKE @p1`, q)
		name = fmt.Sprintf(`%%%s%%`, name)
	} else {
		q = fmt.Sprintf(`%s WHERE name = @p1`, q)
	}

	err := db.Select(&s, q, name)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return []models.Software{}, errors.New("there is no software")
	}
	if err != nil {
		return []models.Software{}, fmt.Errorf("probem with query %s: %w", q, err)
	}

	for k, sw := range s {
		s[k], err = getRelationships(sw, rels, db)
		if err != nil {
			return []models.Software{}, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return s, nil
}

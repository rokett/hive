package software

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	mssql "github.com/microsoft/go-mssqldb"
	"rokett.me/hive/internal/models"
)

// Create adds software to the database.  If a duplicate exists, the software is not inserted and we ignore the error.
func Create(sw []models.Software, db *sqlx.DB) error {
	if len(sw) == 0 {
		return nil
	}

	var q string

	entry := 1
	var entries []interface{}

	for _, v := range sw {
		tmp := fmt.Sprintf("IF NOT EXISTS (SELECT 1 FROM software WHERE name = @p%d AND version = @p%d) INSERT INTO software (name, version) VALUES", entry, entry+1)

		tmp = fmt.Sprintf("%s (@p%d,", tmp, entry)
		entries = append(entries, v.Name)
		entry++

		tmp = fmt.Sprintf("%s @p%d);", tmp, entry)
		entries = append(entries, v.Version)
		entry++

		q = fmt.Sprintf("%s%s", q, tmp)

		// There have occassionally been issues whereby the number of parameters exceeds 2100 which is a hard limit in SQL Server.
		// Therefore we commit the data in chunks of 2000 to get around the problem.
		if entry > 2000 {
			err := insertSoftware(q, entries, db)
			if err != nil {
				return err
			}

			// These need to be reset back to default values in order to start the process again.
			entry = 1
			q = ""
			entries = nil
		}
	}

	err := insertSoftware(q, entries, db)
	if err != nil {
		return err
	}

	return nil
}

func insertSoftware(q string, entries []interface{}, db *sqlx.DB) error {
	_, err := db.Exec(q, entries...)
	if err != nil {
		if err, ok := err.(mssql.Error); ok {
			// Error code 2627 occurs when trying to insert a record which would be a duplicate (which shouldn't happen as we check the record doesn't exist first).
			// In this case it means that a software item already exists with the same name; that's ok for us so we can just ignore the error.
			if err.SQLErrorNumber() == 2627 {
				return nil
			}
		}

		return fmt.Errorf("error creating software entries: %w", err)
	}

	return nil
}

package software

import (
	"fmt"
	"strings"
)

func buildQuery(fields string) string {
	q := "SELECT"

	if fields != "" {
		reqFields := strings.Split(fields, ",")

		flds := "id"

		for _, fld := range reqFields {
			flds = flds + "," + fld
		}

		q = fmt.Sprintf(`%s %s`, q, flds)
	} else {
		q = fmt.Sprintf(`%s id, name, version, created_at, updated_at`, q)
	}

	q = fmt.Sprintf(`%s FROM software`, q)

	return q
}

package models

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

type Directorate struct {
	ID             int64        `db:"id" json:"is"`
	Name           string       `db:"name" json:"name"`
	OrganisationID int64        `db:"organisation_id"`
	Organisation   Organisation `db:"organisation" json:"organisation"`
	Departments    []Department `db:"departments" json:"departments"`
	//EventLog     []Event      `db:"events"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type DirectorateModel struct {
	DB          *sqlx.DB
	Directorate Directorate
}

func (d *DirectorateModel) List(include string) ([]Directorate, error) {
	var directorates []Directorate

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	err := d.DB.Select(&directorates, "SELECT id, name, created_at, updated_at FROM directorates ORDER BY name")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return directorates, ErrNoRecords
	}
	if err != nil {
		return directorates, fmt.Errorf("unable to get directorates: %w", err)
	}

	for k, directorate := range directorates {
		directorates[k], err = d.getRelationships(directorate, rels)
		if err != nil {
			return directorates, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return directorates, nil
}

func (d *DirectorateModel) getRelationships(directorate Directorate, rels map[string]bool) (Directorate, error) {
	if rels["organisation"] || rels["*"] {
		var org Organisation

		err := d.DB.Get(&org, "SELECT organisations.organisation, organisations.id FROM organisations INNER JOIN directorates ON organisations.id = directorates.organisation_id WHERE (directorates.id = @p1)", directorate.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.organisation, organisations.id FROM organisations INNER JOIN directorates ON organisations.id = directorates.organisation_id WHERE (directorates.id = %d)", directorate.ID)
			return directorate, fmt.Errorf("problem with query %s: %w", q, err)
		}

		directorate.Organisation = org
	}

	if rels["departments"] || rels["*"] {
		var depts []Department

		err := d.DB.Select(&depts, "SELECT departments.name, directorates.id FROM directorates INNER JOIN departments ON dbo.directorates.id = dbo.departments.directorate_id WHERE (directorates.id = @p1);", directorate.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT departments.name, directorates.id FROM directorates INNER JOIN departments ON dbo.directorates.id = dbo.departments.directorate_id WHERE (directorates.id = %d);", directorate.ID)
			return directorate, fmt.Errorf("problem with query %s: %w", q, err)
		}

		directorate.Departments = depts
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'directorate' AND object_id = @p1;", directorate.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'directorate' AND object_id = %d;", directorate.ID)
			return directorate, fmt.Errorf("problem with query %s: %w", q, err)
		}

		directorate.EventLog = evt
	}*/

	return directorate, nil
}

func (d *DirectorateModel) Insert(directorate Directorate, organisation string) error {
	_, err := d.DB.Exec("INSERT INTO directorates (name, organisation_id) VALUES(@p1, (SELECT id FROM organisations WHERE organisation = @p2));", directorate.Name, organisation)
	if err != nil {
		return fmt.Errorf("creating directorate: %w", err)
	}

	return nil
}

func (d *DirectorateModel) Update(directorate Directorate, organisation string) error {
	_, err := d.DB.Exec("UPDATE directorates SET name = @p1, organisation_id = (SELECT id FROM organisations WHERE organisation = @p2) WHERE id = @p3", directorate.Name, organisation, directorate.ID)
	if err != nil {
		return fmt.Errorf("updating directorate with ID %d: %w", directorate.ID, err)
	}

	return nil
}

func (d *DirectorateModel) Validate(directorate Directorate, organisation string) (map[string]string, error) {
	validationErrors := make(map[string]string)

	if directorate.Name == "" {
		validationErrors["name"] = "Directorate name MUST be specified"
	}

	unique, err := d.isUnique(directorate, organisation)
	if err != nil {
		return validationErrors, fmt.Errorf("validating directorate: %w", err)
	}

	if !unique {
		validationErrors["name"] = "Directorate already exists"
	}

	return validationErrors, nil
}

func (d *DirectorateModel) isUnique(directorate Directorate, organisation string) (bool, error) {
	var id int64

	err := d.DB.Get(&id, "SELECT id FROM directorates WHERE name = @p1 AND organisation_id = (SELECT id FROM organisations WHERE organisation = @p2);", directorate.Name, organisation)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM directorates WHERE name = '%s' AND organisation_id = (SELECT id FROM organisations WHERE organisation = '%s'): %w", directorate.Name, organisation, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == directorate.ID {
		return true, nil
	}

	return false, nil
}

func (d *DirectorateModel) Get(id int64) (Directorate, error) {
	var directorate Directorate

	err := d.DB.Get(&directorate, "SELECT id, name, organisation_id FROM directorates WHERE id = @p1", id)
	if err != nil {
		return directorate, fmt.Errorf("unable to get directorate with id %d: %w", id, err)
	}

	return directorate, nil
}

func (d *DirectorateModel) Delete(id int64) error {
	_, err := d.DB.Exec("DELETE FROM directorates WHERE id = @p1", id)
	if err != nil {
		return fmt.Errorf("error deleting directorate with ID %d: %w", id, err)
	}

	return nil
}

func (d *DirectorateModel) SearchByName(query string) ([]Directorate, error) {
	var directorates []Directorate

	err := d.DB.Select(&directorates, "SELECT name FROM directorates WHERE name LIKE @p1 ORDER BY name", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return directorates, ErrNoRecords
	}
	if err != nil {
		return directorates, fmt.Errorf("unable to get directorates: %w", err)
	}

	return directorates, nil
}

package models

import (
	"database/sql"
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type Network struct {
	ID           int64          `db:"id" json:"id"`
	Network      string         `db:"network" json:"network"`
	Description  string         `db:"description" json:"description"`
	Gateway      string         `db:"gateway" json:"gateway"`
	SubnetMask   string         `db:"subnet_mask" json:"subnet_mask"`
	VLAN         int64          `db:"vlan_id" json:"vlan_id"`
	IPs          []IP           `json:"ips"`
	Organisation []Organisation `json:"organisations"`
	CreatedAt    time.Time      `db:"created_at" json:"created_at"`
	UpdatedAt    time.Time      `db:"updated_at" json:"updated_at"`
	//EventLog     *[]Event `json:"events"`
}

type NetworkModel struct {
	DB      *sqlx.DB
	Network Network
}

// ErrGetNetwork represents an error when attempting to get network details
var ErrGetNetwork = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5005,
	Title:    "Error getting network",
	MoreInfo: "https://api.fff.com/docs/errors/5005",
}

func (n *NetworkModel) List() ([]Network, error) {
	var networks []Network

	err := n.DB.Select(&networks, "SELECT id, network, description, subnet_mask, gateway, vlan_id, created_at, updated_at FROM networks ORDER BY network")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return networks, ErrNoRecords
	}
	if err != nil {
		return networks, fmt.Errorf("unable to get networks: %w", err)
	}

	return networks, nil
}

func (n *NetworkModel) Get(id int64, include string) (Network, error) {
	var network Network

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	err := n.DB.Get(&network, "SELECT id, network, description, subnet_mask, gateway, vlan_id, created_at, updated_at FROM networks WHERE id = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return network, ErrNoRecords
	}
	if err != nil {
		return network, fmt.Errorf("unable to get network with id %d: %w", id, err)
	}

	network, err = n.getRelationships(network, rels)
	if err != nil {
		return network, fmt.Errorf("unable to get relationships: %w", err)
	}

	return network, nil
}

func (n *NetworkModel) getRelationships(network Network, rels map[string]bool) (Network, error) {
	if rels["ips"] || rels["*"] {
		var ips []IP

		err := n.DB.Select(&ips, "SELECT id, ip, hostname, description, static, reserved, stale FROM ipAddresses WHERE network_id = @p1;", network.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, ip, hostname, description, static, reserved, stale FROM ipAddresses WHERE network_id = %d;", network.ID)
			return network, fmt.Errorf("problem with query %s: %w", q, err)
		}

		network.IPs = ips
	}

	/*if rels["organisations"] || rels["*"] {
		var org []Organisation

		err := n.DB.Select(&org, "SELECT organisations.organisation, organisations.id FROM network_organisation INNER JOIN organisations ON network_organisation.organisation_id = organisations.id WHERE (network_organisation.network_id = @p1)", network.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.organisation, organisations.id FROM network_organisation INNER JOIN organisations ON network_organisation.organisation_id = organisations.id WHERE (network_organisation.network_id = %d)", network.ID)
			return network, fmt.Errorf("problem with query %s: %w", q, err)
		}

		network.Organisation = &org
	}

	if rels["events"] {
		var evt []Event

		err := n.DB.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'network' AND object_id = @p1;", network.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'network' AND object_id = %d;", network.ID)
			return network, fmt.Errorf("problem with query %s: %w", q, err)
		}

		network.EventLog = &evt
	}*/

	return network, nil
}

func (n *NetworkModel) IsUnique(network string) (bool, error) {
	var id int64

	err := n.DB.Get(&id, "SELECT id FROM networks WHERE network = @p1;", network)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query SELECT id FROM networks WHERE network = '%s': %w", network, err)
	}

	return false, nil
}

func (n *NetworkModel) Insert(network Network) (int64, error) {
	network.SubnetMask = network.calculateNetmask().String()

	var id int64

	err := n.DB.Get(&id, "INSERT INTO networks (network, subnet_mask, description, vlan_id, gateway) VALUES(@p1, @p2, @p3, @p4, @p5);SELECT id = convert(bigint, SCOPE_IDENTITY());", network.Network, network.SubnetMask, network.Description, network.VLAN, network.Gateway)
	if err != nil {
		return id, fmt.Errorf("error adding new network: %w", err)
	}

	return id, nil
}

func (n Network) calculateNetmask() net.IP {
	_, ipnet, _ := net.ParseCIDR(n.Network)

	if len(ipnet.Mask) == 0 {
		return nil
	}

	return net.IPv4(ipnet.Mask[0], ipnet.Mask[1], ipnet.Mask[2], ipnet.Mask[3])
}

// InsertNetworkIPs calculates and creates the IP addresses associated with a given network
func (n *NetworkModel) InsertNetworkIPs(network Network) error {
	IPs, err := network.calculateNetworkIPs()
	if err != nil {
		return fmt.Errorf("error calculating network IPs: %w", err)
	}

	for _, ip := range IPs {
		_, err := n.DB.Exec("INSERT INTO ipAddresses (ip, network_id, hostname, description, static, reserved) VALUES (@p1, @p2, @p3, @p4, @p5, @p6);", ip.IPAddress, network.ID, ip.Hostname, ip.Description, ip.Static, ip.Reserved)
		if err != nil {
			q := fmt.Sprintf("INSERT INTO ipAddresses (ip, network_id, hostname, description, static, reserved) VALUES ('%s', %d, '%s', '%s', %t, %t);", ip.IPAddress, network.ID, ip.Hostname, ip.Description, ip.Static, ip.Reserved)

			return fmt.Errorf("error adding IP address: %s: %w", q, err)
		}
	}

	return nil
}

func (n Network) calculateNetworkIPs() ([]IP, error) {
	var IPs []IP

	gateway := "Gateway"
	broadcast := "Broadcast"

	ipAddress := IP{
		Reserved: false,
		Static:   false,
	}

	// Get an array of IP addresses which belong to the given network CIDR
	hosts := n.calculateNetworkHostAddrs()
	for _, ip := range hosts {
		if ip == n.Gateway {
			continue
		}

		ipAddress.IPAddress = ip

		IPs = append(IPs, ipAddress)
	}

	ipAddress.Static = true

	ipAddress.IPAddress = n.Gateway
	ipAddress.Hostname = gateway
	ipAddress.Description = gateway

	IPs = append(IPs, ipAddress)

	broadcastAddr, err := n.calculateBroadcastAddr()
	if err != nil {
		return nil, err
	}

	ipAddress.IPAddress = broadcastAddr.String()
	ipAddress.Hostname = broadcast
	ipAddress.Description = broadcast

	IPs = append(IPs, ipAddress)

	return IPs, nil
}

func (n Network) calculateNetworkHostAddrs() []string {
	var ips []string

	ip, ipnet, _ := net.ParseCIDR(n.Network)

	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); incrementIP(ip) {
		ips = append(ips, ip.String())
	}

	// remove network address and broadcast address
	return ips[1 : len(ips)-1]
}

func incrementIP(ip net.IP) {
	for index := len(ip) - 1; index >= 0; index-- {
		ip[index]++

		if ip[index] > 0 {
			break
		}
	}
}

// The broadcast address is the last entry in a range.  If we parse a CIDR string we can find the broadcast address.
func (n Network) calculateBroadcastAddr() (net.IP, error) {
	_, ipnet, _ := net.ParseCIDR(n.Network)

	if ipnet.IP.To4() == nil {
		return net.IP{}, errors.New("does not support IPv6 addresses")
	}

	ip := make(net.IP, len(ipnet.IP.To4()))

	binary.BigEndian.PutUint32(ip, binary.BigEndian.Uint32(ipnet.IP.To4())|^binary.BigEndian.Uint32(net.IP(ipnet.Mask).To4()))

	return ip, nil
}

// IsInUse returns true if a network has any IP addresses which are allocated
func (n *NetworkModel) IsInUse(id int64) (bool, error) {
	var count int64

	err := n.DB.Get(&count, "SELECT COUNT(id) FROM ipAddresses WHERE network_id = @p1 AND hostname <> '' AND hostname <> 'Broadcast' AND hostname <> 'Gateway'", id)
	if err != nil {
		q := fmt.Sprintf("SELECT COUNT(id) FROM ipAddresses WHERE network_id = %d AND hostname <> '' AND hostname <> 'Broadcast' AND hostname <> 'Gateway'", id)

		return true, fmt.Errorf("problem with query %s: %w", q, err)
	}

	if count > 0 {
		return true, nil
	}

	return false, nil
}

func (n *NetworkModel) Delete(id int64) error {
	var stmts []string

	stmts = append(stmts, "DELETE FROM networks WHERE id = @p1")
	stmts = append(stmts, "DELETE FROM ipAddresses WHERE network_id = @p1")

	stmt := strings.Join(stmts, ";")

	_, err := n.DB.Exec(stmt, id)
	if err != nil {
		return fmt.Errorf("error deleting network with ID %d and associated IP addresses: %w", id, err)
	}

	return nil
}

package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type IP struct {
	ID          int64     `db:"id" json:"id"`
	IPAddress   string    `db:"ip" json:"ip_address"`
	Hostname    string    `db:"hostname" json:"hostname"`
	Description string    `db:"description" json:"description"`
	Static      bool      `db:"static" json:"static_ip"`
	Reserved    bool      `db:"reserved" json:"reserved_ip"`
	Stale       bool      `db:"stale" json:"stale"`
	NetworkID   int64     `db:"network_id"`
	Network     *Network  `db:"network" json:"network"`
	CreatedAt   time.Time `db:"created_at" json:"created_at"`
	UpdatedAt   time.Time `db:"updated_at" json:"updated_at"`
}

type IPModel struct {
	DB *sqlx.DB
	IP IP
}

// ErrSearchIPAddress represents an error when attempting to search for an IP address
var ErrSearchIPAddress = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5014,
	Title:    "Error searching for IP addresses",
	MoreInfo: "https://api.fff.com/docs/errors/5014",
}

func (i *IPModel) Get(id int64) (IP, error) {
	var ip IP

	err := i.DB.Get(&ip, "SELECT id, ip, hostname, description, static, reserved, stale, network_id, created_at, updated_at FROM ipAddresses WHERE id = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return ip, ErrNoRecords
	}
	if err != nil {
		return ip, fmt.Errorf("unable to get IP with id %d: %w", id, err)
	}

	return ip, nil
}

func (i *IPModel) Clear(id int64) error {
	_, err := i.DB.Exec("UPDATE ipAddresses SET hostname = '', description = '', static = 0, reserved = 0, stale = 0 WHERE id = @p1;", id)
	if err != nil {
		return fmt.Errorf("unable to clear IP with id %d: %w", id, err)
	}

	return nil
}

func (i *IPModel) Update(ip IP) error {
	_, err := i.DB.NamedExec("UPDATE ipAddresses SET hostname = :hostname, description = :description, static = :static, reserved = :reserved WHERE id = :id", ip)
	if err != nil {
		return fmt.Errorf("error updating IP address: %w", err)
	}

	return nil
}

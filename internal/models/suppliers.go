package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type Supplier struct {
	ID                     int64            `db:"id" json:"id"`
	Supplier               string           `db:"supplier" json:"supplier"`
	Accounts               *[]User          `json:"accounts"`
	Accreditations         *[]Accreditation `json:"accreditations"`
	SelectedAccreditations string           `json:"-"`
	Systems                *[]System        `json:"systems"`
	SelectedSystems        string           `json:"-"`
	Documentation          *[]Documentation `json:"documentation"`
	SelectedDocs           string           `json:"-"`
	//EventLog       *[]Event
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type SupplierModel struct {
	DB       *sqlx.DB
	Supplier Supplier
}

// ErrIsUniqueSupplier represents an error when attempting to check that a new supplier is unique before creating it
var ErrIsUniqueSupplier = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5209,
	Title:    "Error checking that supplier is unique",
	MoreInfo: "https://api.fff.com/docs/errors/5109",
}

// ErrCreatingSupplier represents an error when attempting to create a new supplier
var ErrCreatingSupplier = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5210,
	Title:    "Error creating supplier",
	MoreInfo: "https://api.fff.com/docs/errors/5210",
}

// ErrGetSupplier represents an error when attempting to get supplier details
var ErrGetSupplier = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5212,
	Title:    "Error getting supplier",
	MoreInfo: "https://api.fff.com/docs/errors/5212",
}

func (s *SupplierModel) List(include string) ([]Supplier, error) {
	var suppliers []Supplier

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	err := s.DB.Select(&suppliers, "SELECT id, supplier, created_at, updated_at FROM suppliers ORDER BY supplier")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return suppliers, ErrNoRecords
	}
	if err != nil {
		return suppliers, fmt.Errorf("unable to get suppliers: %w", err)
	}

	for k, supplier := range suppliers {
		suppliers[k], err = s.getRelationships(supplier, rels)
		if err != nil {
			return suppliers, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return suppliers, nil
}

func (s *SupplierModel) getRelationships(supplier Supplier, rels map[string]bool) (Supplier, error) {
	var documentation []Documentation

	err := s.DB.Select(&documentation, "SELECT documentation.id, documentation.type, documentation.path FROM documentation_supplier INNER JOIN documentation ON documentation_supplier.documentation_id = documentation.id WHERE (documentation_supplier.supplier_id = @p1)", supplier.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT documentation.id, documentation.type, documentation.path FROM documentation_supplier INNER JOIN documentation ON documentation_supplier.documentation_id = documentation.id WHERE (documentation_supplier.supplier_id = %d)", supplier.ID)
		return supplier, fmt.Errorf("problem with query %s: %w", q, err)
	}

	supplier.Documentation = &documentation

	var accounts []User

	err = s.DB.Select(&accounts, "SELECT users.id, users.firstname, users.surname, users.upn FROM supplier_user INNER JOIN users ON supplier_user.user_id = users.id WHERE (supplier_user.supplier_id = @p1)", supplier.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.upn FROM supplier_user INNER JOIN users ON supplier_user.user_id = users.id WHERE (supplier_user.supplier_id = %d)", supplier.ID)
		return supplier, fmt.Errorf("problem with query %s: %w", q, err)
	}

	supplier.Accounts = &accounts

	if rels["docs"] || rels["*"] {
		var d []Documentation

		err := s.DB.Select(&d, "SELECT documentation.id, documentation.type, documentation.path FROM documentation_supplier INNER JOIN documentation ON documentation_supplier.documentation_id = documentation.id WHERE (documentation_supplier.supplier_id = @p1)", supplier.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT documentation.id, documentation.type, documentation.path FROM documentation_supplier INNER JOIN documentation ON documentation_supplier.documentation_id = documentation.id WHERE (documentation_supplier.supplier_id = %d)", supplier.ID)
			return supplier, fmt.Errorf("problem with query %s: %w", q, err)
		}

		supplier.Documentation = &d
	}

	if rels["accreditations"] || rels["*"] {
		var a []Accreditation

		err := s.DB.Select(&a, "SELECT accreditations.id, accreditations.accreditation FROM accreditation_supplier INNER JOIN accreditations ON accreditation_supplier.accreditation_id = accreditations.id WHERE (accreditation_supplier.supplier_id = @p1)", supplier.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT accreditations.id, accreditations.accreditation FROM accreditation_supplier INNER JOIN accreditations ON accreditation_supplier.accreditation_id = accreditations.id WHERE (accreditation_supplier.supplier_id = %d)", supplier.ID)
			return supplier, fmt.Errorf("problem with query %s: %w", q, err)
		}

		supplier.Accreditations = &a
	}

	if rels["systems"] || rels["*"] {
		var sys []System

		err := s.DB.Select(&sys, "SELECT systems.id, systems.name FROM supplier_system INNER JOIN systems ON supplier_system.system_id = systems.id WHERE (supplier_system.supplier_id = @p1)", supplier.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT systems.id, systems.name FROM supplier_system INNER JOIN systems ON supplier_system.system_id = systems.id WHERE (supplier_system.supplier_id = %d)", supplier.ID)
			return supplier, fmt.Errorf("problem with query %s: %w", q, err)
		}

		supplier.Systems = &sys
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := s.DB.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'supplier' AND object_id = @p1;", supplier.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'supplier' AND object_id = %d;", supplier.ID)
			return supplier, fmt.Errorf("problem with query %s: %w", q, err)
		}

		supplier.EventLog = evt
	}*/

	return supplier, nil
}

func (s *SupplierModel) Get(id int64, include string) (Supplier, error) {
	var supplier Supplier

	err := s.DB.Get(&supplier, "SELECT id, supplier, created_at, updated_at FROM suppliers WHERE id = @p1", id)
	if err != nil {
		return supplier, fmt.Errorf("unable to get supplier with id %d: %w", id, err)
	}

	rels := make(map[string]bool)
	for _, v := range strings.Split(include, ",") {
		rels[v] = true
	}

	supplier, err = s.getRelationships(supplier, rels)
	if err != nil {
		return supplier, fmt.Errorf("unable to get relationships: %w", err)
	}

	var t []string

	if supplier.Documentation != nil {
		for _, a := range *supplier.Documentation {
			t = append(t, fmt.Sprintf("%s:%s", a.Type, a.Path))
		}
		supplier.SelectedDocs = strings.Join(t, ",")
		t = nil
	}

	if supplier.Accreditations != nil {
		for _, a := range *supplier.Accreditations {
			t = append(t, a.Accreditation)
		}
		supplier.SelectedAccreditations = strings.Join(t, ",")
		t = nil
	}

	if supplier.Systems != nil {
		for _, a := range *supplier.Systems {
			t = append(t, a.System)
		}
		supplier.SelectedSystems = strings.Join(t, ",")
	}

	return supplier, nil
}

func (s *SupplierModel) Insert(supplier Supplier, accreditations, systems, accounts, docs []string) error {
	var id int64

	err := s.DB.Get(&id, "INSERT INTO suppliers (supplier) VALUES(@p1);SELECT id = convert(bigint, SCOPE_IDENTITY());", supplier.Supplier)
	if err != nil {
		return fmt.Errorf("error creating supplier: %w", err)
	}

	supplier.ID = id

	err = s.upsertDocumentationRelationships(supplier, docs)
	if err != nil {
		return fmt.Errorf("error creating supplier documentation relationships: %w", err)
	}

	err = s.upsertAccreditationRelationships(supplier, accreditations)
	if err != nil {
		return fmt.Errorf("error creating supplier accreditation relationships: %w", err)
	}

	err = s.upsertSystemRelationships(supplier, systems)
	if err != nil {
		return fmt.Errorf("error creating supplier systems relationships: %w", err)
	}

	err = s.upsertAccountRelationships(supplier, accounts)
	if err != nil {
		return fmt.Errorf("error creating supplier account relationships: %w", err)
	}

	return nil
}

func (s *SupplierModel) Update(supplier Supplier, accreditations, systems, accounts, docs []string) error {
	_, err := s.DB.NamedExec("UPDATE suppliers SET supplier = :supplier WHERE id = :id", supplier)
	if err != nil {
		return fmt.Errorf("error updating supplier: %w", err)
	}

	err = s.upsertDocumentationRelationships(supplier, docs)
	if err != nil {
		return fmt.Errorf("error creating supplier documentation relationships: %w", err)
	}

	err = s.upsertAccreditationRelationships(supplier, accreditations)
	if err != nil {
		return fmt.Errorf("error updating supplier accreditations: %w", err)
	}

	err = s.upsertSystemRelationships(supplier, systems)
	if err != nil {
		return fmt.Errorf("error updating supplier systems relationships: %w", err)
	}

	err = s.upsertAccountRelationships(supplier, accounts)
	if err != nil {
		return fmt.Errorf("error creating supplier account relationships: %w", err)
	}

	return nil
}

func (s *SupplierModel) Delete(id int64) error {
	var stmts []string

	stmts = append(stmts, "DELETE FROM documentation_supplier WHERE supplier_id = @p1")
	stmts = append(stmts, "DELETE FROM accreditation_supplier WHERE supplier_id = @p1")
	stmts = append(stmts, "DELETE FROM supplier_system WHERE supplier_id = @p1")
	stmts = append(stmts, "DELETE FROM supplier_user WHERE supplier_id = @p1")
	stmts = append(stmts, "DELETE FROM suppliers WHERE id = @p1")

	stmt := strings.Join(stmts, ";")

	_, err := s.DB.Exec(stmt, id)
	if err != nil {
		return fmt.Errorf("error deleting supplier with ID %d: %w", id, err)
	}

	return nil
}

func (s *SupplierModel) Validate(supplier Supplier) (map[string]string, error) {
	validationErrors := make(map[string]string)

	unique, err := s.isUniqueName(supplier)
	if err != nil {
		return validationErrors, fmt.Errorf("validating supplier: %w", err)
	}

	if !unique {
		validationErrors["name"] = "Supplier name is already in use"
	}

	return validationErrors, nil
}

func (s *SupplierModel) isUniqueName(supplier Supplier) (bool, error) {
	var id int64

	err := s.DB.Get(&id, "SELECT id FROM suppliers WHERE supplier = @p1;", supplier.Supplier)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM suppliers WHERE supplier = '%s': %w", supplier.Supplier, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == supplier.ID {
		return true, nil
	}

	return false, nil
}

func (s *SupplierModel) upsertDocumentationRelationships(supplier Supplier, docs []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT documentation_id FROM documentation_supplier WHERE supplier_id = @p1", supplier.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing documenation relationships: %w", err)
	}

	// Making a map of the current documentation allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentDocumentation := make(map[int64]bool)

	for _, v := range t {
		currentDocumentation[v] = true
	}

	for _, v := range docs {
		if v == "" {
			continue
		}

		var id int64

		parts := strings.Split(v, ":")

		err := s.DB.Get(&id, "SELECT id from documentation where type = @p1 AND path = @p2", parts[0], parts[1])
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			// If the documentation record does not already exists then we create it
			err := s.DB.Get(&id, "INSERT INTO documentation (type, path) VALUES(@p1, @p2);SELECT id = convert(bigint, SCOPE_IDENTITY());", parts[0], parts[1])
			if err != nil {
				return fmt.Errorf("error creating documentation record (%s > %s): %w", parts[0], parts[1], err)
			}
		} else if err != nil {
			return fmt.Errorf("unable to get documentation record (%s > %s): %w", parts[0], parts[1], err)
		}

		if _, ok := currentDocumentation[id]; ok {
			delete(currentDocumentation, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO documentation_supplier (documentation_id, supplier_id) VALUES (@p1, @p2);", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error adding new documentation/supplier relationship: %w", err)
		}

		delete(currentDocumentation, id)
	}

	for id := range currentDocumentation {
		_, err := s.DB.Exec("DELETE FROM documentation_supplier WHERE documentation_id = @p1 AND supplier_id = @p2", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error deleting documentation (%d)/supplier (%d) link: %w", id, supplier.ID, err)
		}
	}

	return nil
}

func (s *SupplierModel) upsertAccreditationRelationships(supplier Supplier, accreditations []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT accreditation_id FROM accreditation_supplier WHERE supplier_id = @p1", supplier.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing accreditations: %w", err)
	}

	// Making a map of the current accreditations allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentAccreditations := make(map[int64]bool)

	for _, v := range t {
		currentAccreditations[v] = true
	}

	for _, a := range accreditations {
		if a == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM accreditations WHERE accreditation = @p1", a)
		if err != nil {
			return fmt.Errorf("unable to get accreditation: %w", err)
		}

		if _, ok := currentAccreditations[id]; ok {
			delete(currentAccreditations, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO accreditation_supplier (accreditation_id, supplier_id) VALUES(@p1, @p2);", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error adding new supplier/accreditation relationship: %w", err)
		}

		delete(currentAccreditations, id)
	}

	for id := range currentAccreditations {
		_, err := s.DB.Exec("DELETE FROM accreditation_supplier WHERE accreditation_id = @p1 AND supplier_id = @p2", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error deleting accreditation (%d)/supplier (%d) link: %w", id, supplier.ID, err)
		}
	}

	return nil
}

func (s *SupplierModel) upsertSystemRelationships(supplier Supplier, systems []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT system_id FROM supplier_system WHERE supplier_id = @p1", supplier.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing systems: %w", err)
	}

	// Making a map of the current systems allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentSystems := make(map[int64]bool)

	for _, v := range t {
		currentSystems[v] = true
	}

	for _, sys := range systems {
		if sys == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM systems WHERE name = @p1", sys)
		if err != nil {
			return fmt.Errorf("unable to get system: %w", err)
		}

		if _, ok := currentSystems[id]; ok {
			delete(currentSystems, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO supplier_system (system_id, supplier_id) VALUES(@p1, @p2);", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error adding new supplier/system relationship: %w", err)
		}

		delete(currentSystems, id)
	}

	for id := range currentSystems {
		_, err := s.DB.Exec("DELETE FROM supplier_system WHERE system_id = @p1 AND supplier_id = @p2", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error deleting system (%d)/supplier (%d) link: %w", id, supplier.ID, err)
		}
	}

	return nil
}

func (s *SupplierModel) upsertAccountRelationships(supplier Supplier, accounts []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT user_id FROM supplier_user WHERE supplier_id = @p1", supplier.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing supplier accounts: %w", err)
	}

	// Making a map of the current authorised users allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentAccounts := make(map[int64]bool)

	for _, v := range t {
		currentAccounts[v] = true
	}

	for _, u := range accounts {
		if u == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM users WHERE upn = @p1", u)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			// If the user record does not already exists then we create it
			err := s.DB.Get(&id, "INSERT INTO users (upn, firstname, surname, username) VALUES(@p1, '', '', '');SELECT id = convert(bigint, SCOPE_IDENTITY());", u)
			if err != nil {
				return fmt.Errorf("error creating user (%s): %w", u, err)
			}
		} else if err != nil {
			return fmt.Errorf("unable to get user (%s): %w", u, err)
		}

		if _, ok := currentAccounts[id]; ok {
			delete(currentAccounts, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO supplier_user (user_id, supplier_id) VALUES(@p1, @p2);", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error adding new supplier/account relationship: %w", err)
		}

		delete(currentAccounts, id)
	}

	for id := range currentAccounts {
		_, err := s.DB.Exec("DELETE FROM supplier_user WHERE user_id = @p1 AND supplier_id = @p2", id, supplier.ID)
		if err != nil {
			return fmt.Errorf("error deleting user account (%d)/supplier (%d) link: %w", id, supplier.ID, err)
		}
	}

	return nil
}

func (s *SupplierModel) SearchByName(query string) ([]Supplier, error) {
	var suppliers []Supplier

	err := s.DB.Select(&suppliers, "SELECT supplier FROM suppliers WHERE supplier LIKE @p1 ORDER BY supplier", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return suppliers, ErrNoRecords
	}
	if err != nil {
		return suppliers, fmt.Errorf("unable to get suppliers: %w", err)
	}

	return suppliers, nil
}

package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
)

// Volume represents a Windows volume
type Volume struct {
	ID          int64     `json:"id"`
	BlockSize   uint64    `db:"block_size" json:"block_size"`
	Capacity    uint64    `json:"capacity"`
	DriveLetter string    `db:"drive_letter" json:"drive_letter"`
	Filesystem  string    `json:"filesystem"`
	FreeSpace   uint64    `db:"free_space" json:"free_space"`
	Label       string    `json:"label"`
	DiskModel   string    `db:"disk_model" json:"disk_model"`
	Checksum    string    `json:"-"`
	AssetID     int64     `db:"asset_id" json:"asset_id"`
	CreatedAt   time.Time `db:"created_at"`
	UpdatedAt   time.Time `db:"updated_at"`
}

type VolumeModel struct {
	DB     *sqlx.DB
	Logger *slog.Logger
	Volume Volume
}

// ErrUpsertVolume represents an error when attempting to carry out an upsert operation on a volume
var ErrUpsertVolume = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5101,
	Title:    "Error upserting volume",
	MoreInfo: "https://api.fff.com/docs/errors/5101",
}

// DeleteOrphaned deletes any volumess which are related to an asset which no longer exists
func (v *VolumeModel) DeleteOrphaned() error {
	var volumes []Volume

	// We need to find instances which were last updated > 1 day ago, and are not linked to any assets
	q := "SELECT id, block_size, capacity, drive_letter, filesystem, free_space, label, disk_model FROM volumes WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM assets WHERE id = volumes.asset_id);"

	err := v.DB.Select(&volumes, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("retrieving orphaned volumes to be deleted: %w", err)
	}

	_, err = v.DB.Exec("DELETE FROM volumes WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM assets WHERE id = volumes.asset_id);")
	if err != nil {
		return fmt.Errorf("deleting orphaned volumes: %w", err)
	}

	for _, vol := range volumes {
		b, _ := json.Marshal(vol)
		go auditlog.Log("delete", 1, string(b[:]), "", "volume", vol.ID, v.DB, v.Logger)
	}

	return nil
}

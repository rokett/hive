package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type Organisation struct {
	ID           int64      `db:"id" json:"id"`
	Organisation string     `db:"organisation" json:"organisation"`
	CreatedAt    time.Time  `db:"created_at" json:"created_at"`
	UpdatedAt    time.Time  `db:"updated_at" json:"updated_at"`
	Networks     *[]Network `json:"networks"`
	Systems      *[]System  `json:"systems"`
	Assets       *[]Asset   `json:"assets"`
}

type OrganisationModel struct {
	DB           *sqlx.DB
	Organisation Organisation
}

// ErrGetOrganisation represents an error when attempting to get organisation details
var ErrGetOrganisation = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5312,
	Title:    "Error getting organisation",
	MoreInfo: "https://api.fff.com/docs/errors/5312",
}

func (o *OrganisationModel) List() ([]Organisation, error) {
	var organisations []Organisation

	err := o.DB.Select(&organisations, "SELECT id, organisation, created_at, updated_at FROM organisations ORDER BY organisation")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return organisations, ErrNoRecords
	}
	if err != nil {
		return organisations, fmt.Errorf("unable to get organisations: %w", err)
	}

	return organisations, nil
}

func (o *OrganisationModel) Get(id int64) (Organisation, error) {
	var organisation Organisation

	err := o.DB.Get(&organisation, "SELECT id, organisation FROM organisations WHERE id = @p1", id)
	if err != nil {
		return organisation, fmt.Errorf("unable to get organisation with id %d: %w", id, err)
	}

	return organisation, nil
}

func (o *OrganisationModel) Delete(id int64) error {
	var stmts []string

	stmts = append(stmts, "DELETE FROM organisation_system WHERE organisation_id = @p1")
	stmts = append(stmts, "DELETE FROM organisations WHERE id = @p1")

	stmt := strings.Join(stmts, ";")

	_, err := o.DB.Exec(stmt, id)
	if err != nil {
		return fmt.Errorf("error deleting organisation with ID %d: %w", id, err)
	}

	return nil
}

func (o *OrganisationModel) Validate(organisation Organisation) (map[string]string, error) {
	validationErrors := make(map[string]string)

	unique, err := o.isUniqueName(organisation)
	if err != nil {
		return validationErrors, fmt.Errorf("validating organisation: %w", err)
	}

	if !unique {
		validationErrors["organisation"] = "Organisation name is already in use"
	}

	return validationErrors, nil
}

func (o *OrganisationModel) isUniqueName(organisation Organisation) (bool, error) {
	var id int64

	err := o.DB.Get(&id, "SELECT id FROM organisations WHERE organisation = @p1;", organisation.Organisation)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM organisations WHERE organisation = '%s': %w", organisation.Organisation, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == organisation.ID {
		return true, nil
	}

	return false, nil
}

func (o *OrganisationModel) Insert(organisation Organisation) error {
	_, err := o.DB.NamedExec("INSERT INTO organisations (organisation) VALUES(:organisation);", organisation)
	if err != nil {
		return fmt.Errorf("error creating organisation: %w", err)
	}

	return nil
}

func (o *OrganisationModel) SearchByName(query string) ([]Organisation, error) {
	var organisations []Organisation

	err := o.DB.Select(&organisations, "SELECT organisation FROM organisations WHERE organisation LIKE @p1 ORDER BY organisation", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return organisations, ErrNoRecords
	}
	if err != nil {
		return organisations, fmt.Errorf("unable to get organisations: %w", err)
	}

	return organisations, nil
}

func (o *OrganisationModel) Update(organisation Organisation) error {
	_, err := o.DB.NamedExec("UPDATE organisations SET organisation = :organisation WHERE id = :id", organisation)
	if err != nil {
		return fmt.Errorf("updating organisation with ID %d: %w", organisation.ID, err)
	}

	return nil
}

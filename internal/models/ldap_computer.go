package models

import (
	"net/http"

	"rokett.me/hive/internal/api"
)

// LDAPComputer represents a user
type LDAPComputer struct {
	CN string `json:"cn"`
}

// ErrSearchingLDAPComputer represents an error when attempting to search LDAP directory for a computer
var ErrSearchingLDAPComputer = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5111,
	Title:    "Error searching LDAP for computer",
	MoreInfo: "https://api.fff.com/docs/errors/5111",
}

package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/auditlog"
)

type Service struct {
	ID      int64     `db:"id" json:"id"`
	Name    string    `db:"name" json:"name"`
	Systems *[]System `json:"systems"`
	//EventLog  *[]Event  `json:"events"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type ServiceModel struct {
	DB      *sqlx.DB
	Logger  *slog.Logger
	Service Service
}

func (s *ServiceModel) SearchByName(query string) ([]Service, error) {
	var services []Service

	err := s.DB.Select(&services, "SELECT name FROM services WHERE name LIKE @p1 ORDER BY name", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return services, ErrNoRecords
	}
	if err != nil {
		return services, fmt.Errorf("unable to get services: %w", err)
	}

	return services, nil
}

// DeleteOrphaned deletes services which are not related to any systems
func (s *ServiceModel) DeleteOrphaned() error {
	var services []Service

	// We need to find services which were created > 1 day ago and which do not exist in the link table, therefore meaning they are not related to any systems
	q := "SELECT services.id, services.name FROM services LEFT OUTER JOIN service_system ON (services.id = service_system.service_id) WHERE services.created_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM service_system WHERE service_system.service_id = services.id);"
	err := s.DB.Select(&services, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("retrieving orphaned services to be deleted: %w", err)
	}

	_, err = s.DB.Exec("DELETE FROM services WHERE services.created_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM service_system WHERE service_system.service_id = services.id);")
	if err != nil {
		return fmt.Errorf("deleting orphaned services: %w", err)
	}

	for _, svc := range services {
		b, _ := json.Marshal(svc)
		go auditlog.Log("delete", 1, string(b[:]), "", "service", svc.ID, s.DB, s.Logger)
	}

	return nil
}

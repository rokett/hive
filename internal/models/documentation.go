package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/auditlog"
)

type Documentation struct {
	ID        int64     `db:"id" json:"id"`
	Type      string    `db:"type" json:"type"`
	Path      string    `db:"path" json:"path"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type DocumentationModel struct {
	DB            *sqlx.DB
	Logger        *slog.Logger
	Documentation Documentation
}

// DeleteOrphaned deletes any documentation record which is related to a supplier which no longer exists
func (s *DocumentationModel) DeleteOrphaned() error {
	var documentation []Documentation

	// We need to find documentation which was last created > 1 day ago, and is not linked to any suppliers
	q := "SELECT id, type, path, created_at, updated_at FROM documentation WHERE created_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM documentation_supplier WHERE documentation_id = documentation.id);"

	err := s.DB.Select(&documentation, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("retrieving orphaned documentation to be deleted: %w", err)
	}

	_, err = s.DB.Exec("DELETE FROM documentation WHERE created_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM documentation_supplier WHERE documentation_id = documentation.id);")
	if err != nil {
		return fmt.Errorf("deleting orphaned documentation: %w", err)
	}

	for _, doc := range documentation {
		b, _ := json.Marshal(doc)
		go auditlog.Log("delete", 1, string(b[:]), "", "documentation", doc.ID, s.DB, s.Logger)
	}

	return nil
}

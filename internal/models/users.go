package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type User struct {
	ID                int64     `db:"id" json:"id"`
	FirstName         string    `db:"firstname" json:"firstname"`
	Surname           string    `db:"surname" json:"surname"`
	Username          string    `json:"username"`
	Upn               string    `db:"upn" json:"upn"`
	DistinguishedName string    `json:"distinguished_name"`
	Domain            string    `json:"domain"`
	AccountDisabled   bool      `db:"account_disabled"`
	Token             string    `json:"-"`
	TokenExpiry       time.Time `db:"token_expiry" json:"token_expiry"`
	TokenMaxAge       int       `db:"token_max_age" json:"token_max_age"`
	LastLogin         time.Time `db:"last_login" json:"last_login"`
	Groups            *[]Group  `db:"member_of" json:"member_of"`
	CreatedAt         time.Time `db:"created_at" json:"created_at"`
	UpdatedAt         time.Time `db:"updated_at" json:"updated_at"`
}

type UserModel struct {
	DB   *sqlx.DB
	User User
}

// ErrIsUniqueUser represents an error when attempting to check that a new user is unique before creating it
var ErrIsUniqueUser = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5309,
	Title:    "Error checking that user is unique",
	MoreInfo: "https://api.fff.com/docs/errors/5309",
}

// ErrCreatingUser represents an error when attempting to create a new user
var ErrCreatingUser = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5310,
	Title:    "Error creating user",
	MoreInfo: "https://api.fff.com/docs/errors/5310",
}

// ErrSettingUserToken represents an error when updating a user record to set the token and token expiry timestamp
var ErrSettingUserToken = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5311,
	Title:    "Error setting token",
	MoreInfo: "https://api.fff.com/docs/errors/5311",
}

// ErrAddingUserGroupRelationship represents an error when adding a relationship between user and groups
var ErrAddingUserGroupRelationship = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5312,
	Title:    "Error adding user/group relationship",
	MoreInfo: "https://api.fff.com/docs/errors/5312",
}

// ErrSearchingUser represents an error when attempting to search for a user
var ErrSearchingUser = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5211,
	Title:    "Error searching for user",
	MoreInfo: "https://api.fff.com/docs/errors/5211",
}

// ErrUpdatingLastLogin represents an error when attempting to update the last login timestamp for a user
var ErrUpdatingLastLogin = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5211,
	Title:    "Unable to update last login timestamp",
	MoreInfo: "https://api.fff.com/docs/errors/5211",
}

// ErrSearchingLDAPUser represents an error when attempting to search LDAP directory for a user
var ErrSearchingLDAPUser = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5211,
	Title:    "Error searching LDAP for user",
	MoreInfo: "https://api.fff.com/docs/errors/5211",
}

// ErrUserAlreadyExists represents an error due to trying to create a user which already exists
var ErrUserAlreadyExists = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5211,
	Title:    "User already exists",
	MoreInfo: "https://api.fff.com/docs/errors/5211",
}

// ErrGetUser represents an error when attempting to get user details
var ErrGetUser = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5112,
	Title:    "Error getting user",
	MoreInfo: "https://api.fff.com/docs/errors/5112",
}

func (u *UserModel) ListUsersWithUpn() ([]User, error) {
	var users []User

	err := u.DB.Select(&users, "SELECT id, upn, token, token_expiry, token_max_age, last_login, account_disabled FROM users WHERE upn != '' AND upn NOT LIKE '%@hive.local';")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return users, ErrNoRecords
	}
	if err != nil {
		return users, fmt.Errorf("unable to get users with upn: %w", err)
	}

	return users, nil
}

func (u *UserModel) Upsert(user User) error {
	err := u.DB.Get(&user.ID, "SELECT id FROM users WHERE upn = @p1", user.Upn)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("unable to check for user: %w", err)
	}

	if user.ID == 0 {
		err = u.create(user)
		if err != nil {
			return fmt.Errorf("unable to create new user record: %w", err)
		}

		return nil
	}

	err = u.Update(user)
	if err != nil {
		return fmt.Errorf("unable to update user record: %w", err)
	}

	return nil
}

func (u *UserModel) create(user User) error {
	_, err := u.DB.NamedExec("INSERT INTO users (upn, token, token_expiry, token_max_age, last_login, account_disabled) VALUES (:upn, :token, :token_expiry, :token_max_age, :last_login, :account_disabled);", user)
	if err != nil {
		return fmt.Errorf("error adding new user: %w", err)
	}

	return nil
}

func (u *UserModel) Update(user User) error {
	// Update is called from multiple places, which contain different parameters.
	// If called via a login, it includes the token details.  If it isn't, then it will not include those details.
	// If we don't have a different query when the token is not there, the token fields are cleared which forces everyone to need to log in again.
	q := "UPDATE users SET firstname = :firstname, surname = :surname, upn = :upn, account_disabled = :account_disabled, token = :token, token_expiry = :token_expiry, token_max_age = :token_max_age, last_login = :last_login WHERE id = :id"
	if user.Token == "" {
		q = "UPDATE users SET firstname = :firstname, surname = :surname, upn = :upn WHERE id = :id"
	}
	_, err := u.DB.NamedExec(q, user)
	if err != nil {
		return err
	}

	return nil
}

func (u *UserModel) IsTokenValid(token string) (bool, User, error) {
	var user User

	err := u.DB.Get(&user, "SELECT upn, token_expiry FROM users WHERE token = @p1", token)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return false, User{}, nil
	}
	if err != nil {
		return false, User{}, fmt.Errorf("checking for session token in database: %w", err)
	}

	if time.Now().UTC().After(user.TokenExpiry) {
		return false, User{}, nil
	}

	return true, user, nil
}

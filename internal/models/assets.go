package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

var ErrNoRecords = errors.New("no matching assets found")

type Asset struct {
	ID int64 `db:"id" json:"id"`

	AssetType          string    `db:"asset_type" json:"asset_type"`
	BuildTimestamp     time.Time `db:"build_timestamp" json:"build_timestamp"`
	Decommissioned     *bool     `db:"decommissioned" json:"decommissioned"`
	Domain             string    `db:"domain" json:"domain"`
	FQDN               string    `db:"fqdn" json:"FQDN"`
	Hostname           string    `db:"name" json:"hostname"`
	IgnoreWarranty     *bool     `db:"ignore_warranty" json:"ignore_warranty"`
	IsVirtual          *bool     `db:"is_virtual" json:"is_virtual"`
	Manufacturer       string    `db:"manufacturer" json:"manufacturer"`
	Model              string    `db:"model" json:"model"`
	NTFSTrimEnabled    *bool     `db:"ntfs_trim_enabled" json:"ntfs_trim_enabled"`
	OS                 string    `db:"os" json:"OS"`
	RawIP              string    `db:"ip" json:"IP"`
	RecoveryMethod     string    `db:"recovery_method" json:"recovery_method"`
	ReFSTrimEnabled    *bool     `db:"refs_trim_enabled" json:"refs_trim_enabled"`
	SerialNumber       string    `db:"serial_number" json:"serial_number"`
	ServicePack        string    `db:"service_pack" json:"service_pack"`
	ShipDate           time.Time `db:"ship_date" json:"ship_date"`
	ThirdPartyWarranty *bool     `db:"third_party_warranty" json:"third_party_warranty"`
	UUID               string    `db:"uuid" json:"uuid"`
	WarrantyExpiryDate time.Time `db:"warranty_expiry_date" json:"warranty_expiry_date"`
	WarrantyLevel      string    `db:"warranty_level" json:"warranty_level"`

	CreatedAt       time.Time `db:"created_at" json:"created_at"`
	UpdatedAt       time.Time `db:"updated_at" json:"updated_at"`
	Tier            int64     `json:"tier"`
	Systems         *[]System `json:"systems"`
	SelectedSystems string
	Environments    *[]Environment  `json:"environments"`
	SQLInstances    *[]SQLInstance  `json:"sqlInstances"`
	Volumes         *[]Volume       `json:"volumes"`
	Software        *[]Software     `json:"software"`
	Organisation    *[]Organisation `json:"organisation"`
}

type AssetView struct {
	ID                     int64
	AssetType              string
	Decommissioned         bool
	Hostname               string
	IgnoreWarranty         bool
	IsWarrantyExpired      bool
	IsWarrantyExpiringSoon bool
	Manufacturer           string
	Model                  string
	OS                     string
	RecoveryMethod         string
	SerialNumber           string
	ServicePack            string
	ShipDate               time.Time
	ThirdPartyWarranty     bool
	WarrantyExpiryDate     time.Time
	WarrantyLevel          string
	CreatedAt              time.Time
	UpdatedAt              time.Time
	Tier                   int64
	Systems                []System
	Environments           []Environment
	SQLInstances           []SQLInstance
}

type AssetModel struct {
	DB    *sqlx.DB
	Asset Asset
}

// ErrCreatingAsset represents an error when attempting to create a new asset
var ErrCreatingAsset = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5101,
	Title:    "Error creating asset",
	MoreInfo: "https://api.fff.com/docs/errors/5101",
}

// ErrDeletingAsset represents an error when attempting to delete an asset
var ErrDeletingAsset = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5102,
	Title:    "Error deleting asset",
	MoreInfo: "https://api.fff.com/docs/errors/5102",
}

// ErrIsUnique represents an error when attempting to check that a new asset hostname is unique before creating it
var ErrIsUnique = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5103,
	Title:    "Error checking that asset hostname is unique",
	MoreInfo: "https://api.fff.com/docs/errors/5103",
}

// ErrIsUniqueSN represents an error when attempting to check that a new asset serial number is unique before creating it
var ErrIsUniqueSN = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5104,
	Title:    "Error checking that asset serial number is unique",
	MoreInfo: "https://api.fff.com/docs/errors/5104",
}

// ErrGetAsset represents an error when attempting to get asset details
var ErrGetAsset = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5105,
	Title:    "Error getting asset",
	MoreInfo: "https://api.fff.com/docs/errors/5105",
}

// ErrUpdatingAsset represents an error when attempting to update the properties of an asset
var ErrUpdatingAsset = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5108,
	Title:    "Error updating the asset details",
	MoreInfo: "https://api.fff.com/docs/errors/5108",
}

// ErrIsAssetInUse represents an error when attempting to check whether or not an asset is in use before deleting it
var ErrIsAssetInUse = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5109,
	Title:    "Error checking whether an asset is in use",
	MoreInfo: "https://api.fff.com/docs/errors/5109",
}

// ErrAssetInUse represents an error when attempting to delete an asset which is in use (assigned to a system)
var ErrAssetInUse = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5110,
	Title:    "Asset in use",
	Detail:   "The asset is assigned to a system and cannot be deleted",
	MoreInfo: "https://api.fff.com/docs/errors/5110",
}

// ErrSearchAsset represents an error when attempting to search for a asset
var ErrSearchAsset = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5114,
	Title:    "Error searching for asset",
	MoreInfo: "https://api.fff.com/docs/errors/5114",
}

// ErrAssetNameRequired represents an error when attempting to call an endpoint which requires the asset name in the querystring
var ErrAssetNameRequired = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5115,
	Title:    "Asset 'name' is missing",
	Detail:   "The endpoint requires the 'name' parameter to be passed in the querystring",
	MoreInfo: "https://api.fff.com/docs/errors/5115",
}

// ErrAssetCheckin represents an error when asking an asset to check-in
var ErrAssetCheckin = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5116,
	Title:    "Error when asking asset to check-in",
	MoreInfo: "https://api.fff.com/docs/errors/5116",
}

func (a *AssetModel) List(include string) ([]Asset, error) {
	var assets []Asset

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	err := a.DB.Select(&assets, "SELECT assets.id, assets.name, assets.manufacturer, assets.model, assets.asset_type, assets.serial_number, assets.os, assets.ntfs_trim_enabled, assets.refs_trim_enabled, assets.service_pack, assets.warranty_expiry_date, assets.ignore_warranty, assets.third_party_warranty, assets.recovery_method, assets.domain, assets.uuid, assets.fqdn, assets.build_timestamp, assets.is_virtual, assets.warranty_level, assets.ship_date, assets.decommissioned, assets.created_at, assets.updated_at FROM assets ORDER BY assets.name;")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return assets, ErrNoRecords
	}
	if err != nil {
		return assets, fmt.Errorf("unable to get assets: %w", err)
	}

	for k, asset := range assets {
		// We get the systems here, rather than checking to see if that relationship has been requested,
		// because in order to find out the asset tier we need to find the related system that has the highest associated tier.
		// If we waited to check whether the relationship had been requested, we'd have to request that relationship in order to see the asset tier.
		var sys []System

		err := a.DB.Select(&sys, "SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = @p1)", asset.ID)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = %d)", asset.ID)
			return assets, fmt.Errorf("problem with query - %s: %w", q, err)
		}

		var tier int64
		tier = 99
		for _, s := range sys {
			if s.Tier < tier {
				tier = s.Tier
			}
		}

		// If tier is still 99 then it doesn't really have one so we can use the nil value to exclude it from the response
		if tier == 99 {
			tier = 0
		}

		assets[k], err = a.getRelationships(asset, rels, sys)
		if err != nil {
			return assets, fmt.Errorf("unable to get relationships: %w", err)
		}

		assets[k].Tier = tier
	}

	return assets, nil
}

func (a *AssetModel) getRelationships(asset Asset, rels map[string]bool, sys []System) (Asset, error) {
	/*if rels["software"] || rels["*"] {
		sw := []models.Software{}

		err := db.Select(&sw, "SELECT software.name, software.version, software.id FROM asset_software INNER JOIN software ON asset_software.software_id = software.id WHERE (asset_software.asset_id = @p1)", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT software.name, software.version, software.id FROM asset_software INNER JOIN software ON asset_software.software_id = software.id WHERE (asset_software.asset_id = %d)", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.Software = &sw
	}

	if rels["volumes"] || rels["*"] {
		vols := []models.Volume{}

		err := db.Select(&vols, "SELECT id, block_size, capacity, drive_letter, filesystem, free_space, label, disk_model FROM volumes WHERE asset_id = @p1;", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, block_size, capacity, drive_letter, filesystem, free_space, label, disk_model FROM volumes WHERE asset_id = %d;", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.Volumes = &vols
	}

	if rels["organisations"] || rels["*"] {
		org := []models.Organisation{}

		err := db.Select(&org, "SELECT organisations.organisation, organisations.id FROM asset_organisation INNER JOIN organisations ON asset_organisation.organisation_id = organisations.id WHERE (asset_organisation.asset_id = @p1)", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.organisation, organisations.id FROM asset_organisation INNER JOIN organisations ON asset_organisation.organisation_id = organisations.id WHERE (asset_organisation.asset_id = %d)", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.Organisation = &org
	}*/

	if rels["system.environments"] || rels["*"] {
		for k, s := range sys {
			var envs []Environment

			err := a.DB.Select(&envs, "SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = @p1 AND asset_system.system_id = @p2) ORDER BY environments.id", asset.ID, s.ID)
			if err != nil && !errors.Is(err, sql.ErrNoRows) {
				q := fmt.Sprintf("SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = %d AND asset_system.system_id = %d) ORDER BY environments.id", asset.ID, s.ID)
				return asset, fmt.Errorf("problem with query %s: %w", q, err)
			}

			sys[k].Environments = &envs
		}
	}

	/*if rels["system.services"] || rels["*"] {
		for k, s := range sys {
			svcs := []models.Service{}

			err := db.Select(&svcs, "SELECT services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = @p1) ORDER BY services.id", s.ID)
			if err != nil && !errors.Is(err, sql.ErrNoRows) {
				q := fmt.Sprintf("services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = %d) ORDER BY services.id", s.ID)
				return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
			}

			sys[k].Services = &svcs
		}
	}*/

	if rels["systems"] || rels["*"] {
		asset.Systems = &sys
	}

	if rels["instances"] || rels["*"] {
		var inst []SQLInstance

		err := a.DB.Select(&inst, "SELECT id, name, service_account FROM sqlInstances WHERE asset_id = @p1;", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, name, service_account FROM sqlInstances WHERE asset_id = %d;", asset.ID)
			return asset, fmt.Errorf("problem with query %s: %w", q, err)
		}

		if rels["instance.databases"] || rels["*"] {
			for k, i := range inst {
				var dbs []Database

				err = a.DB.Select(&dbs, "SELECT id, name, status, recovery_model, read_only, standby, preferred_backup_replica, last_full_backup, last_log_backup, is_primary_replica, compatibility_level, size FROM databases WHERE instance_id = @p1;", i.ID)
				if err != nil && !errors.Is(err, sql.ErrNoRows) {
					q := fmt.Sprintf("SELECT id, name, status, recovery_model, read_only, standby, preferred_backup_replica, last_full_backup, last_log_backup, is_primary_replica, compatibility_level, size FROM databases WHERE instance_id = %d;", i.ID)
					return asset, fmt.Errorf("problem with query %s: %w", q, err)
				}

				if rels["instance.database.files"] || rels["*"] {
					for k, d := range dbs {
						var files []DatabaseFile

						err = a.DB.Select(&files, "SELECT id, name, path, type FROM database_files WHERE database_id = @p1;", d.ID)
						if err != nil && !errors.Is(err, sql.ErrNoRows) {
							q := fmt.Sprintf("SELECT id, name, path, type FROM database_files WHERE database_id = %d;", d.ID)
							return asset, fmt.Errorf("problem with query %s: %w", q, err)
						}

						dbs[k].DatabaseFiles = &files
					}
				}

				inst[k].Databases = &dbs
			}
		}

		asset.SQLInstances = &inst
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := db.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'asset' AND object_id = @p1;", asset.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'asset' AND object_id = %d;", asset.ID)
			return models.Asset{}, fmt.Errorf("problem with query %s: %w", q, err)
		}

		asset.EventLog = &evt
	}*/

	return asset, nil
}

func (a *AssetModel) Get(id int64, include string) (Asset, error) {
	var asset Asset

	err := a.DB.Get(&asset, "SELECT id, name, manufacturer, model, serial_number, os, service_pack, warranty_expiry_date, ignore_warranty, third_party_warranty, recovery_method, domain, fqdn, is_virtual, warranty_level, ship_date, asset_type, decommissioned FROM assets WHERE id = @p1", id)
	if err != nil {
		return asset, fmt.Errorf("unable to get asset with id %d: %w", id, err)
	}

	rels := make(map[string]bool)
	for _, v := range strings.Split(include, ",") {
		rels[v] = true
	}

	// We get the systems here, rather than checking to see if that relationship has been requested,
	// because in order to find out the asset tier we need to find the related system that has the highest associated tier.
	// If we waited to check whether the relationship had been requested, we'd have to request that relationship in order to see the asset tier.
	var sys []System

	err = a.DB.Select(&sys, "SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = @p1)", asset.ID)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = %d)", asset.ID)
		return asset, fmt.Errorf("problem with query - %s: %w", q, err)
	}

	var tier int64
	tier = 99
	for _, s := range sys {
		if s.Tier < tier {
			tier = s.Tier
		}
	}

	// If tier is still 99 then it doesn't really have one so we can use the nil value to exclude it from the response
	if tier == 99 {
		tier = 0
	}

	asset, err = a.getRelationships(asset, rels, sys)
	if err != nil {
		return asset, fmt.Errorf("unable to get relationships: %w", err)
	}

	asset.Tier = tier

	var t []string

	if asset.Systems != nil {
		for _, s := range *asset.Systems {
			var envs []string
			for _, e := range *s.Environments {
				envs = append(envs, e.Name)
			}
			t = append(t, fmt.Sprintf("%s:%s", s.System, strings.Join(envs, "|")))
		}
		asset.SelectedSystems = strings.Join(t, ",")
	}

	return asset, nil
}

func (a *AssetModel) Validate(asset Asset) (map[string]string, error) {
	validationErrors := make(map[string]string)

	unique, err := a.isUniqueName(asset)
	if err != nil {
		return validationErrors, fmt.Errorf("validating asset: %w", err)
	}

	if !unique {
		validationErrors["name"] = "Asset name is already in use"
	}

	if asset.SerialNumber != "" {
		unique, err = a.isUniqueSerialNumber(asset)
		if err != nil {
			return validationErrors, fmt.Errorf("validating asset: %w", err)
		}

		if !unique {
			validationErrors["serial_number"] = "Serial number is already in use"
		}
	}

	if (asset.AssetType == "server" || asset.AssetType == "appliance") && asset.RecoveryMethod == "" {
		validationErrors["recovery_method"] = "You must define the server/appliance recovery method"
	}

	if asset.AssetType == "server" && asset.Domain == "" {
		validationErrors["domain"] = "You must provide the domain the asset is a member of"
	}

	return validationErrors, nil
}

func (a *AssetModel) isUniqueName(asset Asset) (bool, error) {
	var id int64

	err := a.DB.Get(&id, "SELECT id FROM assets WHERE name = @p1;", asset.Hostname)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM assets WHERE name = '%s': %w", asset.Hostname, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == asset.ID {
		return true, nil
	}

	return false, nil
}

func (a *AssetModel) isUniqueSerialNumber(asset Asset) (bool, error) {
	var id int64

	err := a.DB.Get(&id, "SELECT id FROM assets WHERE serial_number = @p1;", asset.SerialNumber)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM assets WHERE serial_number = '%s': %w", asset.SerialNumber, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == asset.ID {
		return true, nil
	}

	return false, nil
}

func (a *AssetModel) Insert(asset Asset) error {
	var id int64

	err := a.DB.Get(&id, "INSERT INTO assets (name, manufacturer, model, serial_number, os, service_pack, warranty_expiry_date, ignore_warranty, third_party_warranty, recovery_method, domain, is_virtual, warranty_level, ship_date, asset_type) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15);SELECT id = convert(bigint, SCOPE_IDENTITY());", asset.Hostname, asset.Manufacturer, asset.Model, asset.SerialNumber, asset.OS, asset.ServicePack, asset.WarrantyExpiryDate, asset.IgnoreWarranty, asset.ThirdPartyWarranty, asset.RecoveryMethod, asset.Domain, asset.IsVirtual, asset.WarrantyLevel, asset.ShipDate, asset.AssetType)
	if err != nil {
		return fmt.Errorf("error creating asset: %w", err)
	}

	return nil
}

func (a *AssetModel) Delete(id int64) error {
	var stmts []string

	stmts = append(stmts, "DELETE FROM asset_organisation WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM asset_software WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM asset_system WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM sqlInstances WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM volumes WHERE asset_id = @p1")
	stmts = append(stmts, "DELETE FROM assets WHERE id = @p1")

	stmt := strings.Join(stmts, ";")

	_, err := a.DB.Exec(stmt, id)
	if err != nil {
		return fmt.Errorf("error deleting asset with ID %d: %w", id, err)
	}

	return nil
}

func (a *AssetModel) Update(asset Asset) error {
	_, err := a.DB.NamedExec("UPDATE assets SET name = :name, manufacturer = :manufacturer, model = :model, serial_number = :serial_number, os = :os, service_pack = :service_pack, warranty_expiry_date = :warranty_expiry_date, ignore_warranty = :ignore_warranty, third_party_warranty = :third_party_warranty, recovery_method = :recovery_method, domain = :domain, is_virtual = :is_virtual, warranty_level = :warranty_level, ship_date = :ship_date, asset_type = :asset_type, decommissioned = :decommissioned WHERE id = :id", asset)
	if err != nil {
		return fmt.Errorf("error updating supplier: %w", err)
	}

	return nil
}

func (a *AssetModel) SearchByName(query string) ([]Asset, error) {
	var assets []Asset

	err := a.DB.Select(&assets, "SELECT name FROM assets WHERE name LIKE @p1 ORDER BY name", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return assets, ErrNoRecords
	}
	if err != nil {
		return assets, fmt.Errorf("unable to get assets: %w", err)
	}

	return assets, nil
}

package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
)

type SQLInstance struct {
	ID             int64       `db:"id" json:"id"`
	SQLInstance    string      `db:"name" json:"name"`
	ServiceAccount string      `db:"service_account" json:"serviceAccount"`
	AssetID        int64       `db:"asset_id" json:"asset_id"`
	Checksum       string      `json:"-"`
	Databases      *[]Database `json:"databases,omitempty"`
	CreatedAt      time.Time   `db:"created_at" json:"created_at"`
	UpdatedAt      time.Time   `db:"updated_at" json:"updated_at"`
}

type SQLInstanceModel struct {
	DB          *sqlx.DB
	Logger      *slog.Logger
	SQLInstance SQLInstance
}

// ErrUpsertSQLInstance represents an error when attempting to carry out an upsert operation on a SQL Instance
var ErrUpsertSQLInstance = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5101,
	Title:    "Error upserting SQL Instance",
	MoreInfo: "https://api.fff.com/docs/errors/5101",
}

func (s *SQLInstanceModel) DeleteOrphaned() error {
	var instances []SQLInstance

	// We need to find instances which were last updated > 1 day ago, and are not linked to any SQL assets
	q := "SELECT id, name, service_account, asset_id, created_at, updated_at FROM sqlInstances WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM assets WHERE id = sqlInstances.asset_id);"

	err := s.DB.Select(&instances, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("retrieving SQL Instances to be deleted: %w", err)
	}

	_, err = s.DB.Exec("DELETE FROM sqlInstances WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM assets WHERE id = sqlInstances.asset_id);")
	if err != nil {
		return fmt.Errorf("deleting orphaned SQL Instances: %w", err)
	}

	for _, i := range instances {
		b, _ := json.Marshal(i)
		go auditlog.Log("delete", 1, string(b[:]), "", "sql instance", i.ID, s.DB, s.Logger)
	}

	return nil
}

package models

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type Accreditation struct {
	ID            int64     `db:"id" json:"id"`
	Accreditation string    `db:"accreditation" json:"accreditation"`
	CreatedAt     time.Time `db:"created_at" json:"created_at"`
	UpdatedAt     time.Time `db:"updated_at" json:"updated_at"`
}

type AccreditationModel struct {
	DB *sqlx.DB
}

func (a *AccreditationModel) Search(query string) ([]Accreditation, error) {
	var accred []Accreditation

	err := a.DB.Select(&accred, "SELECT id, accreditation, created_at, updated_at FROM accreditations WHERE accreditation LIKE @p1 ORDER BY accreditation", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return accred, ErrNoRecords
	}
	if err != nil {
		return accred, fmt.Errorf("unable to get accreditations: %w", err)
	}

	return accred, nil
}

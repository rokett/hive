package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
	"rokett.me/hive/internal/auditlog"
)

// Software represents a piece of software
type Software struct {
	ID        int64     `json:"id"`
	Name      string    `json:"name"`
	Version   string    `json:"version"`
	Assets    *[]Asset  `json:"assets"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type SoftwareModel struct {
	DB       *sqlx.DB
	Logger   *slog.Logger
	Software Software
}

// ErrCreatingSoftware represents an error when attempting to create new software
var ErrCreatingSoftware = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5101,
	Title:    "Error creating software",
	MoreInfo: "https://api.fff.com/docs/errors/5101",
}

// ErrSearchSoftware represents an error when attempting to search for software
var ErrSearchSoftware = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5114,
	Title:    "Error searching for software",
	MoreInfo: "https://api.fff.com/docs/errors/5114",
}

// DeleteOrphaned deletes any software which is related to an asset which no longer exists
func (s *SoftwareModel) DeleteOrphaned() error {
	var software []Software

	// We need to find software which was last created > 1 day ago, and is not linked to any assets
	q := "SELECT id, name, version, created_at, updated_at FROM software WHERE created_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM asset_software WHERE software_id = software.id);"

	err := s.DB.Select(&software, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("retrieving orphaned software to be deleted: %w", err)
	}

	_, err = s.DB.Exec("DELETE FROM software WHERE created_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM asset_software WHERE software_id = software.id);")
	if err != nil {
		return fmt.Errorf("deleting orphaned software: %w", err)
	}

	for _, sw := range software {
		b, _ := json.Marshal(sw)
		go auditlog.Log("delete", 1, string(b[:]), "", "software", sw.ID, s.DB, s.Logger)
	}

	return nil
}

package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type Environment struct {
	ID      int64     `db:"id" json:"id"`
	Name    string    `db:"name" json:"name"`
	Systems *[]System `db:"systems" json:"systems"`
	//EventLog  *[]Event  `db:"events" json:"events"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type EnvironmentModel struct {
	DB          *sqlx.DB
	Environment Environment
}

// ErrSearchEnvironment represents an error when attempting to search for an environment
var ErrSearchEnvironment = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5314,
	Title:    "Error searching for environment",
	MoreInfo: "https://api.fff.com/docs/errors/5314",
}

func (e *EnvironmentModel) List() ([]Environment, error) {
	var environments []Environment

	err := e.DB.Select(&environments, "SELECT id, name FROM environments ORDER BY name")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return environments, ErrNoRecords
	}
	if err != nil {
		return environments, fmt.Errorf("unable to get environments: %w", err)
	}

	return environments, nil
}

package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type Location struct {
	ID        int64  `db:"id" json:"id"`
	Office    string `db:"office" json:"office"`
	FirstLine string `db:"first_line" json:"first_line"`
	Street    string `db:"street" json:"street"`
	City      string `db:"city" json:"city"`
	County    string `db:"county" json:"county"`
	PostCode  string `db:"post_code" json:"post_code"`
	Telephone string `db:"telephone" json:"telephone"`
	//EventLog  *[]Event  `db:"events"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// ErrGetLocation represents an error when attempting to get location details
var ErrGetLocation = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5312,
	Title:    "Error getting location",
	MoreInfo: "https://api.fff.com/docs/errors/5312",
}

// ErrSearchLocation represents an error when attempting to search for a location
var ErrSearchLocation = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5314,
	Title:    "Error searching for location",
	MoreInfo: "https://api.fff.com/docs/errors/5314",
}

type LocationModel struct {
	DB       *sqlx.DB
	Location Location
}

func (l *LocationModel) List() ([]Location, error) {
	var locations []Location

	err := l.DB.Select(&locations, "SELECT id, office, first_line, street, city, county, post_code, telephone, created_at, updated_at FROM locations ORDER BY office")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return locations, ErrNoRecords
	}
	if err != nil {
		return locations, fmt.Errorf("unable to get locations: %w", err)
	}

	return locations, nil
}

func (l *LocationModel) Validate(location Location) (map[string]string, error) {
	validationErrors := make(map[string]string)

	unique, err := l.isUniqueOffice(location)
	if err != nil {
		return validationErrors, fmt.Errorf("validating location: %w", err)
	}

	if !unique {
		validationErrors["office"] = "Office name is already in use"
	}

	if location.Office == "" {
		validationErrors["office"] = "Office is required"
	}

	if location.Street == "" {
		validationErrors["street"] = "Street is required"
	}

	if location.City == "" {
		validationErrors["city"] = "City is required"
	}

	if location.County == "" {
		validationErrors["county"] = "County is required"
	}

	if location.PostCode == "" {
		validationErrors["postCode"] = "Post Code is required"
	}

	return validationErrors, nil
}

func (l *LocationModel) isUniqueOffice(location Location) (bool, error) {
	var id int64

	err := l.DB.Get(&id, "SELECT id FROM locations WHERE office = @p1;", location.Office)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM locations WHERE office = '%s': %w", location.Office, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == location.ID {
		return true, nil
	}

	return false, nil
}

func (l *LocationModel) Insert(location Location) error {
	_, err := l.DB.NamedExec("INSERT INTO locations (office, first_line, street, city, county, post_code, telephone) VALUES(:office, :first_line, :street, :city, :county, :post_code, :telephone);", location)
	if err != nil {
		return fmt.Errorf("creating location: %w", err)
	}

	return nil
}

func (l *LocationModel) Update(location Location) error {
	_, err := l.DB.NamedExec("UPDATE locations SET office = :office, first_line = :first_line, street = :street, city = :city, county = :county, post_code = :post_code, telephone = :telephone WHERE id = :id;", location)
	if err != nil {
		return fmt.Errorf("updating location with ID %d: %d", err, location.ID)
	}

	return nil
}

func (l *LocationModel) Get(id int64) (Location, error) {
	var location Location

	err := l.DB.Get(&location, "SELECT id, office, first_line, street, city, county, post_code, telephone FROM locations WHERE id = @p1", id)
	if err != nil {
		return location, fmt.Errorf("unable to get location with id %d: %w", id, err)
	}

	return location, nil
}

func (l *LocationModel) Delete(id int64) error {
	_, err := l.DB.Exec("DELETE FROM locations WHERE id = @p1;", id)
	if err != nil {
		return fmt.Errorf("deleting location with ID %d: %w", id, err)
	}

	return nil
}

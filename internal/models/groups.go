package models

import (
	"net/http"
	"time"

	"rokett.me/hive/internal/api"
)

// Group represents an LDAP security group
type Group struct {
	ID        int64     `json:"id"`
	Group     string    `json:"group"`
	GroupDN   string    `db:"group_dn" json:"group_dn"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

// ErrIsUniqueGroup represents an error when attempting to check that a new group is unique before creating it
var ErrIsUniqueGroup = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5409,
	Title:    "Error checking that group is unique",
	MoreInfo: "https://api.fff.com/docs/errors/5409",
}

// ErrCreatingGroup represents an error when attempting to create a new group
var ErrCreatingGroup = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5410,
	Title:    "Error creating group",
	MoreInfo: "https://api.fff.com/docs/errors/5410",
}

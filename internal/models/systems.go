package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type System struct {
	ID                int64           `db:"id" json:"id"`
	System            string          `db:"name" json:"name"`
	Description       string          `db:"description" json:"description"`
	Tier              int64           `db:"tier" json:"tier"`
	OnCall            *bool           `db:"oncall" json:"oncall"`
	Websites          string          `db:"websites" json:"websites"`
	DistributionLists string          `db:"distribution_lists" json:"distribution_lists"`
	Shortcode         string          `db:"shortcode" json:"shortcode"`
	Documentation     string          `db:"documentation" json:"documentation"`
	Environments      *[]Environment  `json:"environments"`
	Assets            *[]Asset        `json:"assets"`
	Suppliers         *[]Supplier     `json:"suppliers"`
	Organisations     *[]Organisation `json:"organisations"`
	Developers        *[]User         `json:"developers"`
	SystemManagers    *[]User         `json:"system_managers"`
	IAOs              *[]User         `json:"iaos"`
	AuthorisedUsers   *[]User         `json:"authorised_users"`
	Services          *[]Service      `json:"services"`
	//EventLog                *[]Event
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type SystemView struct {
	ID                int64
	System            string
	Description       string
	Tier              int64
	OnCall            bool
	Websites          string
	DistributionLists string
	Shortcode         string
	Documentation     string
	Environments      []Environment
	Assets            []Asset
	Suppliers         []Supplier
	Organisations     []Organisation
	Developers        []User
	SystemManagers    []User
	IAOs              []User
	AuthorisedUsers   []User
	Services          []Service
	CreatedAt         time.Time
	UpdatedAt         time.Time
}

type SystemModel struct {
	DB     *sqlx.DB
	System System
}

// ErrGetSystem represents an error when attempting to get system details
var ErrGetSystem = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5112,
	Title:    "Error getting system",
	MoreInfo: "https://api.fff.com/docs/errors/5112",
}

// ErrSearchSystem represents an error when attempting to search for a system
var ErrSearchSystem = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5114,
	Title:    "Error searching for system",
	MoreInfo: "https://api.fff.com/docs/errors/5114",
}

// ErrUpdatingSystem represents an error when attempting to update the properties of a system
var ErrUpdatingSystem = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5113,
	Title:    "Error updating the system details",
	MoreInfo: "https://api.fff.com/docs/errors/5113",
}

func (s *SystemModel) List(include string) ([]System, error) {
	var systems []System

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	err := s.DB.Select(&systems, "SELECT id, name, description, tier, oncall, websites, distribution_lists, shortcode, documentation, created_at, updated_at FROM systems ORDER BY name;")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return systems, ErrNoRecords
	}
	if err != nil {
		return systems, fmt.Errorf("unable to get systems: %w", err)
	}

	for k, system := range systems {
		systems[k], err = s.getRelationships(system, rels)
		if err != nil {
			return systems, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return systems, nil
}

func (s *SystemModel) getRelationships(system System, rels map[string]bool) (System, error) {
	var devs []User

	err := s.DB.Select(&devs, "SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'developer')", system.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'developer')", system.ID)
		return system, fmt.Errorf("problem with query %s: %w", q, err)
	}

	system.Developers = &devs

	var systemManagers []User

	err = s.DB.Select(&systemManagers, "SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'system manager')", system.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'system manager')", system.ID)
		return system, fmt.Errorf("problem with query %s: %w", q, err)
	}

	system.SystemManagers = &systemManagers

	var iaos []User

	err = s.DB.Select(&iaos, "SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'iao')", system.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'iao')", system.ID)
		return system, fmt.Errorf("problem with query %s: %w", q, err)
	}

	system.IAOs = &iaos

	var authUsers []User

	err = s.DB.Select(&authUsers, "SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = @p1 AND system_user_role.role = 'authorised user')", system.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		q := fmt.Sprintf("SELECT users.id, users.firstname, users.surname, users.upn FROM system_user_role INNER JOIN users ON system_user_role.user_id = users.id WHERE (system_user_role.system_id = %d AND system_user_role.role = 'authorised user')", system.ID)
		return system, fmt.Errorf("problem with query %s: %w", q, err)
	}

	system.AuthorisedUsers = &authUsers

	if rels["assets"] || rels["*"] {
		var a []Asset

		err := s.DB.Select(&a, "SELECT DISTINCT assets.name, assets.id, assets.domain FROM asset_system INNER JOIN assets ON asset_system.asset_id = assets.id WHERE (asset_system.system_id = @p1)", system.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT DISTINCT assets.name, assets.id, assets.domain FROM asset_system INNER JOIN assets ON asset_system.asset_id = assets.id WHERE (asset_system.system_id = %d)", system.ID)
			return system, fmt.Errorf("problem with query %s: %w", q, err)
		}

		if rels["asset.environments"] || rels["*"] {
			for k := range a {
				var envs []Environment

				err := s.DB.Select(&envs, "SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = @p1 AND asset_system.system_id = @p2)", a[k].ID, system.ID)
				if err != nil && !errors.Is(err, sql.ErrNoRows) {
					q := fmt.Sprintf("SELECT environments.id, environments.name FROM asset_system INNER JOIN environments ON asset_system.environment_id = environments.id WHERE (asset_system.asset_id = %d AND asset_system.system_id = %d)", a[k].ID, system.ID)
					return system, fmt.Errorf("problem with query %s: %w", q, err)
				}

				a[k].Environments = &envs
			}
		}

		system.Assets = &a
	}

	if rels["suppliers"] || rels["*"] {
		var sups []Supplier

		err := s.DB.Select(&sups, "SELECT suppliers.id, suppliers.supplier FROM supplier_system INNER JOIN suppliers ON supplier_system.supplier_id = suppliers.id WHERE (supplier_system.system_id = @p1)", system.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT suppliers.id, suppliers.supplier FROM supplier_system INNER JOIN suppliers ON supplier_system.supplier_id = suppliers.id WHERE (supplier_system.system_id = %d)", system.ID)
			return system, fmt.Errorf("problem with query %s: %w", q, err)
		}

		system.Suppliers = &sups
	}

	if rels["organisations"] || rels["*"] {
		var orgs []Organisation

		err := s.DB.Select(&orgs, "SELECT organisations.id, organisations.organisation FROM organisation_system INNER JOIN organisations ON organisation_system.organisation_id = organisations.id WHERE (organisation_system.system_id = @p1)", system.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.id, organisations.organisation FROM organisation_system INNER JOIN organisations ON organisation_system.organisation_id = organisations.id WHERE (organisation_system.system_id = %d)", system.ID)
			return system, fmt.Errorf("problem with query %s: %w", q, err)
		}

		system.Organisations = &orgs
	}

	if rels["environments"] || rels["*"] {
		var envs []Environment

		err := s.DB.Select(&envs, "SELECT environments.id, environments.name FROM environment_system INNER JOIN environments ON environment_system.environment_id = environments.id WHERE (environment_system.system_id = @p1)", system.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT environments.id, environments.name FROM environment_system INNER JOIN environments ON environment_system.environment_id = environments.id WHERE (environment_system.system_id = %d)", system.ID)
			return system, fmt.Errorf("problem with query %s: %w", q, err)
		}

		system.Environments = &envs
	}

	if rels["services"] || rels["*"] {
		var svcs []Service

		err := s.DB.Select(&svcs, "SELECT services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = @p1)", system.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT services.id, services.name FROM service_system INNER JOIN services ON service_system.service_id = services.id WHERE (service_system.system_id = %d)", system.ID)
			return system, fmt.Errorf("problem with query %s: %w", q, err)
		}

		system.Services = &svcs
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := s.DB.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'system' AND object_id = @p1;", system.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'system' AND object_id = %d;", system.ID)
			return system, fmt.Errorf("problem with query %s: %w", q, err)
		}

		system.EventLog = &evt
	}*/

	return system, nil
}

func (s *SystemModel) SearchByName(query string) ([]System, error) {
	var systems []System

	err := s.DB.Select(&systems, "SELECT name FROM systems WHERE name LIKE @p1 ORDER BY name", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return systems, ErrNoRecords
	}
	if err != nil {
		return systems, fmt.Errorf("unable to get systems: %w", err)
	}

	return systems, nil
}

func (s *SystemModel) Insert(system System, assets map[string][]string, environments, suppliers, organisations, systemManagers, authorisedUsers, services []string, developer, iao string) error {
	var id int64

	oncall := false
	if system.OnCall != nil {
		oncall = true
	}

	err := s.DB.Get(&id, "INSERT INTO systems (name, description, tier, oncall, websites, distribution_lists, shortcode, documentation) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8);SELECT id = convert(bigint, SCOPE_IDENTITY());", system.System, system.Description, system.Tier, oncall, system.Websites, system.DistributionLists, system.Shortcode, system.Documentation)
	if err != nil {
		return fmt.Errorf("error creating system: %w", err)
	}

	system.ID = id

	err = s.upsertEnvironmentRelationships(system, environments)
	if err != nil {
		return fmt.Errorf("error creating system environments relationships: %w", err)
	}

	err = s.upsertAssetRelationships(system, assets)
	if err != nil {
		return fmt.Errorf("error creating system assets relationships: %w", err)
	}

	err = s.upsertSupplierRelationships(system, suppliers)
	if err != nil {
		return fmt.Errorf("error creating system suppliers relationships: %w", err)
	}

	err = s.upsertOrganisationRelationships(system, organisations)
	if err != nil {
		return fmt.Errorf("error creating system organisations relationships: %w", err)
	}

	err = s.upsertDeveloperRelationship(system, developer)
	if err != nil {
		return fmt.Errorf("error creating system developer relationship: %w", err)
	}

	err = s.upsertSystemManagerRelationships(system, systemManagers)
	if err != nil {
		return fmt.Errorf("error creating system system manager relationships: %w", err)
	}

	err = s.upsertIAORelationship(system, iao)
	if err != nil {
		return fmt.Errorf("error creating system iao relationship: %w", err)
	}

	err = s.upsertAuthorisedUserRelationships(system, authorisedUsers)
	if err != nil {
		return fmt.Errorf("error creating system authorised user relationships: %w", err)
	}

	err = s.upsertServiceRelationships(system, services)
	if err != nil {
		return fmt.Errorf("error creating system services relationships: %w", err)
	}

	return nil
}

func (s *SystemModel) upsertAssetRelationships(system System, assets map[string][]string) error {
	var relatedAssets []struct {
		AssetID       int64 `db:"asset_id"`
		EnvironmentID int64 `db:"environment_id"`
	}

	err := s.DB.Select(&relatedAssets, "SELECT asset_id, environment_id FROM asset_system WHERE system_id = @p1", system.ID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("unable to get assets: %w", err)
	}

	for asset, envs := range assets {
		if asset == "" {
			continue
		}

		var assetID int64

		err := s.DB.Get(&assetID, "SELECT id FROM assets WHERE name = @p1", asset)
		if err != nil {
			return fmt.Errorf("unable to get asset (%s): %w", asset, err)
		}

		for _, env := range envs {
			var envID int64
			err := s.DB.Get(&envID, "SELECT id from environments WHERE name = @p1", env)
			if err != nil {
				return fmt.Errorf("unable to get environment (%s): %w", env, err)
			}

			var found bool

			for k, v := range relatedAssets {
				if v.AssetID == assetID && v.EnvironmentID == envID {
					relatedAssets = append(relatedAssets[:k], relatedAssets[k+1:]...)
					found = true
					break
				}
			}

			if found {
				continue
			}

			_, err = s.DB.Exec("INSERT INTO asset_system (asset_id, system_id, environment_id) VALUES(@p1, @p2, @p3);", assetID, system.ID, envID)
			if err != nil {
				return fmt.Errorf("error adding new asset/system relationship: %w", err)
			}
		}
	}

	for _, v := range relatedAssets {
		_, err := s.DB.Exec("DELETE FROM asset_system WHERE asset_id = @p1 AND system_id = @p2 AND environment_id = @p3", v.AssetID, system.ID, v.EnvironmentID)
		if err != nil {
			return fmt.Errorf("error deleting asset (%d)/system (%d) link with environment (%d): %w", v.AssetID, system.ID, v.EnvironmentID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertEnvironmentRelationships(system System, environments []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT environment_id FROM environment_system WHERE system_id = @p1", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing environments: %w", err)
	}

	// Making a map of the current environments allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentEnvironments := make(map[int64]bool)

	for _, v := range t {
		currentEnvironments[v] = true
	}

	for _, env := range environments {
		if env == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM environments WHERE name = @p1", env)
		if err != nil {
			return fmt.Errorf("unable to get environment (%s): %w", env, err)
		}

		if _, ok := currentEnvironments[id]; ok {
			delete(currentEnvironments, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO environment_system (environment_id, system_id) VALUES(@p1, @p2);", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/environmnet relationship: %w", err)
		}

		delete(currentEnvironments, id)
	}

	for id := range currentEnvironments {
		_, err := s.DB.Exec("DELETE FROM environment_system WHERE environment_id = @p1 AND system_id = @p2", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting environment (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertSupplierRelationships(system System, suppliers []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT supplier_id FROM supplier_system WHERE system_id = @p1", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing suppliers: %w", err)
	}

	// Making a map of the current suppliers allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentSuppliers := make(map[int64]bool)

	for _, v := range t {
		currentSuppliers[v] = true
	}

	for _, sup := range suppliers {
		if sup == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM suppliers WHERE supplier = @p1", sup)
		if err != nil {
			return fmt.Errorf("unable to get supplier (%s): %w", sup, err)
		}

		if _, ok := currentSuppliers[id]; ok {
			delete(currentSuppliers, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO supplier_system (supplier_id, system_id) VALUES(@p1, @p2);", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/supplier relationship: %w", err)
		}

		delete(currentSuppliers, id)
	}

	for id := range currentSuppliers {
		_, err := s.DB.Exec("DELETE FROM supplier_system WHERE supplier_id = @p1 AND system_id = @p2", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting supplier (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertOrganisationRelationships(system System, organisations []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT organisation_id FROM organisation_system WHERE system_id = @p1", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing organisations: %w", err)
	}

	// Making a map of the current organisations allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentOrganisations := make(map[int64]bool)

	for _, v := range t {
		currentOrganisations[v] = true
	}

	for _, org := range organisations {
		if org == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM organisations WHERE organisation = @p1", org)
		if err != nil {
			return fmt.Errorf("unable to get organisation (%s): %w", org, err)
		}

		if _, ok := currentOrganisations[id]; ok {
			delete(currentOrganisations, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO organisation_system (organisation_id, system_id) VALUES(@p1, @p2);", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/organisation relationship: %w", err)
		}

		delete(currentOrganisations, id)
	}

	for id := range currentOrganisations {
		_, err := s.DB.Exec("DELETE FROM organisation_system WHERE organisation_id = @p1 AND system_id = @p2", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting organisation (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertDeveloperRelationship(system System, developer string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT user_id FROM system_user_role WHERE system_id = @p1 AND role = 'Developer'", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing developer: %w", err)
	}

	// Making a map of the current developer allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentDevelopers := make(map[int64]bool)

	for _, v := range t {
		currentDevelopers[v] = true
	}

	developers := []string{
		developer,
	}

	for _, dev := range developers {
		if dev == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM users WHERE upn = @p1", dev)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			// If the user record does not already exists then we create it
			err := s.DB.Get(&id, "INSERT INTO users (upn, firstname, surname, username) VALUES(@p1, '', '', '');SELECT id = convert(bigint, SCOPE_IDENTITY());", dev)
			if err != nil {
				return fmt.Errorf("error creating user (%s): %w", dev, err)
			}
		} else if err != nil {
			return fmt.Errorf("unable to get user (%s): %w", dev, err)
		}

		if _, ok := currentDevelopers[id]; ok {
			delete(currentDevelopers, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO system_user_role (user_id, system_id, role) VALUES(@p1, @p2, 'Developer');", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/developer relationship: %w", err)
		}

		delete(currentDevelopers, id)
	}

	for id := range currentDevelopers {
		_, err := s.DB.Exec("DELETE FROM system_user_role WHERE user_id = @p1 AND system_id = @p2 AND role = 'Developer'", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting developer (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertSystemManagerRelationships(system System, systemManagers []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT user_id FROM system_user_role WHERE system_id = @p1 AND role = 'System Manager'", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing system managers: %w", err)
	}

	// Making a map of the current system managers allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentSystemManagers := make(map[int64]bool)

	for _, v := range t {
		currentSystemManagers[v] = true
	}

	for _, sm := range systemManagers {
		if sm == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM users WHERE upn = @p1", sm)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			// If the user record does not already exists then we create it
			err := s.DB.Get(&id, "INSERT INTO users (upn, firstname, surname, username) VALUES(@p1, '', '', '');SELECT id = convert(bigint, SCOPE_IDENTITY());", sm)
			if err != nil {
				return fmt.Errorf("error creating user (%s): %w", sm, err)
			}
		} else if err != nil {
			return fmt.Errorf("unable to get user (%s): %w", sm, err)
		}

		if _, ok := currentSystemManagers[id]; ok {
			delete(currentSystemManagers, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO system_user_role (user_id, system_id, role) VALUES(@p1, @p2, 'System Manager');", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/system manager relationship: %w", err)
		}

		delete(currentSystemManagers, id)
	}

	for id := range currentSystemManagers {
		_, err := s.DB.Exec("DELETE FROM system_user_role WHERE user_id = @p1 AND system_id = @p2 AND role = 'System Manager'", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting system manager (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertIAORelationship(system System, iao string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT user_id FROM system_user_role WHERE system_id = @p1 AND role = 'IAO'", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing IAO: %w", err)
	}

	// Making a map of the current IAO allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentIAOs := make(map[int64]bool)

	for _, v := range t {
		currentIAOs[v] = true
	}

	iaos := []string{
		iao,
	}

	for _, iao := range iaos {
		if iao == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM users WHERE upn = @p1", iao)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			// If the user record does not already exists then we create it
			err := s.DB.Get(&id, "INSERT INTO users (upn, firstname, surname, username) VALUES(@p1, '', '', '');SELECT id = convert(bigint, SCOPE_IDENTITY());", iao)
			if err != nil {
				return fmt.Errorf("error creating user (%s): %w", iao, err)
			}
		} else if err != nil {
			return fmt.Errorf("unable to get user (%s): %w", iao, err)
		}

		if _, ok := currentIAOs[id]; ok {
			delete(currentIAOs, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO system_user_role (user_id, system_id, role) VALUES(@p1, @p2, 'IAO');", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/IAO relationship: %w", err)
		}

		delete(currentIAOs, id)
	}

	for id := range currentIAOs {
		_, err := s.DB.Exec("DELETE FROM system_user_role WHERE user_id = @p1 AND system_id = @p2 AND role = 'IAO'", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting IAO (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertAuthorisedUserRelationships(system System, authorisedUsers []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT user_id FROM system_user_role WHERE system_id = @p1 AND role = 'Authorised User'", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing authorised users: %w", err)
	}

	// Making a map of the current authorised users allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentAuthUsers := make(map[int64]bool)

	for _, v := range t {
		currentAuthUsers[v] = true
	}

	for _, u := range authorisedUsers {
		if u == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM users WHERE upn = @p1", u)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			// If the user record does not already exists then we create it
			err := s.DB.Get(&id, "INSERT INTO users (upn, firstname, surname, username) VALUES(@p1, '', '', '');SELECT id = convert(bigint, SCOPE_IDENTITY());", u)
			if err != nil {
				return fmt.Errorf("error creating user (%s): %w", u, err)
			}
		} else if err != nil {
			return fmt.Errorf("unable to get user (%s): %w", u, err)
		}

		if _, ok := currentAuthUsers[id]; ok {
			delete(currentAuthUsers, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO system_user_role (user_id, system_id, role) VALUES(@p1, @p2, 'Authorised User');", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/authorised user relationship: %w", err)
		}

		delete(currentAuthUsers, id)
	}

	for id := range currentAuthUsers {
		_, err := s.DB.Exec("DELETE FROM system_user_role WHERE user_id = @p1 AND system_id = @p2 AND role = 'Authorised User'", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting authorised user (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) upsertServiceRelationships(system System, services []string) error {
	var t []int64

	err := s.DB.Select(&t, "SELECT service_id FROM service_system WHERE system_id = @p1", system.ID)
	if err != nil {
		return fmt.Errorf("unable to retrieve existing services: %w", err)
	}

	// Making a map of the current services allows us to check whether a relationship exists much easier than iterating over the slice each time.
	currentServices := make(map[int64]bool)

	for _, v := range t {
		currentServices[v] = true
	}

	for _, svc := range services {
		if svc == "" {
			continue
		}

		var id int64

		err := s.DB.Get(&id, "SELECT id FROM services WHERE name = @p1", svc)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			// If the service record does not already exists then we create it
			err := s.DB.Get(&id, "INSERT INTO services (name) VALUES(@p1);SELECT id = convert(bigint, SCOPE_IDENTITY());", svc)
			if err != nil {
				return fmt.Errorf("error creating service (%s): %w", svc, err)
			}
		} else if err != nil {
			return fmt.Errorf("unable to get service (%s): %w", svc, err)
		}

		if _, ok := currentServices[id]; ok {
			delete(currentServices, id)
			continue
		}

		_, err = s.DB.Exec("INSERT INTO service_system (service_id, system_id) VALUES(@p1, @p2);", id, system.ID)
		if err != nil {
			return fmt.Errorf("error adding new system/service relationship: %w", err)
		}

		delete(currentServices, id)
	}

	for id := range currentServices {
		_, err := s.DB.Exec("DELETE FROM service_system WHERE service_id = @p1 AND system_id = @p2", id, system.ID)
		if err != nil {
			return fmt.Errorf("error deleting service (%d)/system (%d) link: %w", id, system.ID, err)
		}
	}

	return nil
}

func (s *SystemModel) Validate(system System, environments, organisations, services, systemManagers, suppliers []string) (map[string]string, error) {
	validationErrors := make(map[string]string)

	unique, err := s.isUniqueName(system)
	if err != nil {
		return validationErrors, fmt.Errorf("validating system: %w", err)
	}

	if !unique {
		validationErrors["system"] = "System name is already in use"
	}

	if len(environments) == 0 {
		validationErrors["environments"] = "At least one environment MUST be selected"
	}

	if len(organisations) == 0 {
		validationErrors["organisations"] = "At least one organisation MUST be selected"
	}

	var found bool
	for _, svc := range services {
		if svc == "" {
			continue
		}
		found = true
		break
	}
	if !found {
		validationErrors["services"] = "At least one service MUST be selected"
	}

	found = false
	for _, sm := range systemManagers {
		if sm == "" {
			continue
		}
		found = true
		break
	}
	if !found {
		validationErrors["system_managers"] = "At least one System Manager MUST be selected"
	}

	found = false
	for _, sup := range suppliers {
		if sup == "" {
			continue
		}
		found = true
		break
	}
	if !found {
		validationErrors["suppliers"] = "At least one supplier MUST be selected"
	}

	if len(system.Websites) > 0 {
		websites := strings.Split(system.Websites, ",")
		for _, ws := range websites {
			if !strings.HasSuffix(ws, ".exe.nhs.uk") {
				validationErrors["websites"] = "Website addresses MUST end in 'exe.nhs.uk'"
				break
			}
		}
	}

	if len(system.DistributionLists) > 0 {
		dls := strings.Split(system.DistributionLists, ",")
		for _, dl := range dls {
			if !strings.HasSuffix(dl, "@exe.nhs.uk") && !strings.HasSuffix(dl, "@rdeft.nhs.uk") && !strings.HasSuffix(dl, "@nhs.net") {
				validationErrors["distribution_lists"] = "DL addresses MUST end in one of '@exe.nhs.uk', '@rdeft.nhs.uk' or '@nhs.net'"
				break
			}
		}
	}

	return validationErrors, nil
}

func (s *SystemModel) isUniqueName(system System) (bool, error) {
	var id int64

	err := s.DB.Get(&id, "SELECT id FROM systems WHERE name = @p1;", system.System)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM systems WHERE name = '%s': %w", system.System, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == system.ID {
		return true, nil
	}

	return false, nil
}

func (s *SystemModel) Get(id int64, include string) (System, error) {
	var system System

	err := s.DB.Get(&system, "SELECT id, name, description, tier, oncall, websites, distribution_lists, shortcode, documentation FROM systems WHERE id = @p1", id)
	if err != nil {
		return system, fmt.Errorf("unable to get system with id %d: %w", id, err)
	}

	rels := make(map[string]bool)
	for _, v := range strings.Split(include, ",") {
		rels[v] = true
	}

	system, err = s.getRelationships(system, rels)
	if err != nil {
		return system, fmt.Errorf("unable to get relationships: %w", err)
	}

	/*

		// We get the systems here, rather than checking to see if that relationship has been requested,
		// because in order to find out the asset tier we need to find the related system that has the highest associated tier.
		// If we waited to check whether the relationship had been requested, we'd have to request that relationship in order to see the asset tier.
		var sys []System

		err = s.DB.Select(&sys, "SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = @p1)", asset.ID)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT DISTINCT systems.name, systems.id, systems.tier FROM asset_system INNER JOIN systems ON asset_system.system_id = systems.id WHERE (asset_system.asset_id = %d)", asset.ID)
			return system, fmt.Errorf("problem with query - %s: %w", q, err)
		}

		var tier int64
		tier = 99
		for _, s := range sys {
			if s.Tier < tier {
				tier = s.Tier
			}
		}

		// If tier is still 99 then it doesn't really have one so we can use the nil value to exclude it from the response
		if tier == 99 {
			tier = 0
		}

		system, err = s.getRelationships(asset, rels, sys)
		if err != nil {
			return system, fmt.Errorf("unable to get relationships: %w", err)
		}

		asset.Tier = tier

		var t []string

		for _, a := range asset.Systems {
			t = append(t, a.System)
		}
		asset.SelectedSystems = strings.Join(t, ",")*/

	return system, nil
}

func (s *SystemModel) Delete(id int64) error {
	var stmts []string

	stmts = append(stmts, "DELETE FROM asset_system WHERE system_id = @p1")
	stmts = append(stmts, "DELETE FROM organisation_system WHERE system_id = @p1")
	stmts = append(stmts, "DELETE FROM supplier_system WHERE system_id = @p1")
	stmts = append(stmts, "DELETE FROM system_user_role WHERE system_id = @p1")
	stmts = append(stmts, "DELETE FROM environment_system WHERE system_id = @p1")
	stmts = append(stmts, "DELETE FROM service_system WHERE system_id = @p1")
	stmts = append(stmts, "DELETE FROM systems WHERE id = @p1")

	stmt := strings.Join(stmts, ";")

	_, err := s.DB.Exec(stmt, id)
	if err != nil {
		return fmt.Errorf("error deleting system with ID %d: %w", id, err)
	}

	return nil
}

func (s *SystemModel) Update(system System, assets map[string][]string, environments, suppliers, organisations, systemManagers, authorisedUsers, services []string, developer, iao string) error {
	oncall := false
	if *system.OnCall {
		oncall = true
	}

	_, err := s.DB.Exec("UPDATE systems SET name = @p1, description = @p2, tier = @p3, oncall = @p4, websites = @p5, distribution_lists = @p6, shortcode = @p7, documentation = @p8 WHERE id = @p9;", system.System, system.Description, system.Tier, oncall, system.Websites, system.DistributionLists, system.Shortcode, system.Documentation, system.ID)
	if err != nil {
		return fmt.Errorf("updating system: %w", err)
	}

	err = s.upsertEnvironmentRelationships(system, environments)
	if err != nil {
		return fmt.Errorf("updating system environments relationships: %w", err)
	}

	err = s.upsertAssetRelationships(system, assets)
	if err != nil {
		return fmt.Errorf("updating system assets relationships: %w", err)
	}

	err = s.upsertSupplierRelationships(system, suppliers)
	if err != nil {
		return fmt.Errorf("updating system suppliers relationships: %w", err)
	}

	err = s.upsertOrganisationRelationships(system, organisations)
	if err != nil {
		return fmt.Errorf("updating system organisations relationships: %w", err)
	}

	err = s.upsertDeveloperRelationship(system, developer)
	if err != nil {
		return fmt.Errorf("updating system developer relationship: %w", err)
	}

	err = s.upsertSystemManagerRelationships(system, systemManagers)
	if err != nil {
		return fmt.Errorf("updating system system manager relationships: %w", err)
	}

	err = s.upsertIAORelationship(system, iao)
	if err != nil {
		return fmt.Errorf("updating system iao relationship: %w", err)
	}

	err = s.upsertAuthorisedUserRelationships(system, authorisedUsers)
	if err != nil {
		return fmt.Errorf("updating system authorised user relationships: %w", err)
	}

	err = s.upsertServiceRelationships(system, services)
	if err != nil {
		return fmt.Errorf("updating system services relationships: %w", err)
	}

	return nil
}

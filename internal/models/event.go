package models

import "time"

// Event represents an audit event object
type Event struct {
	ID           int64           `json:"id"`
	Action       string          `json:"action"`
	OldData      string          `db:"old_data" json:"old_data"`
	NewData      string          `db:"new_data" json:"new_data"`
	Operator     string          `json:"operator"`
	Asset        *[]Asset        `json:"asset_event"`
	Network      *[]Network      `json:"network_event"`
	System       *[]System       `json:"system_event"`
	Supplier     *[]Supplier     `json:"supplier_event"`
	Organisation *[]Organisation `json:"organisation_event"`
	Environment  *[]Environment  `json:"environment_event"`
	CreatedAt    time.Time       `db:"created_at" json:"created_at"`
}

package models

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/api"
)

type Department struct {
	ID            int64         `db:"id" json:"id"`
	Name          string        `db:"name" json:"name"`
	DirectorateID int64         `db:"directorate_id"`
	Directorate   *Directorate  `db:"directorate" json:"directorate"`
	Organisation  *Organisation `db:"organisation" json:"organisation"`
	//EventLog     *[]Event      `db:"events"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

type DepartmentModel struct {
	DB         *sqlx.DB
	Department Department
}

// ErrGetDepartment represents an error when attempting to get department details
var ErrGetDepartment = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5312,
	Title:    "Error getting department",
	MoreInfo: "https://api.fff.com/docs/errors/5312",
}

// ErrSearchDepartment represents an error when attempting to search for a department
var ErrSearchDepartment = api.Error{
	Status:   http.StatusInternalServerError,
	Code:     5314,
	Title:    "Error searching for department",
	MoreInfo: "https://api.fff.com/docs/errors/5314",
}

func (d *DepartmentModel) List(include string) ([]Department, error) {
	var departments []Department

	relationships := strings.Split(include, ",")
	rels := make(map[string]bool)
	for _, v := range relationships {
		rels[v] = true
	}

	err := d.DB.Select(&departments, "SELECT id, name, created_at, updated_at FROM departments ORDER BY name")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return departments, ErrNoRecords
	}
	if err != nil {
		return departments, fmt.Errorf("unable to get departments: %w", err)
	}

	for k, department := range departments {
		departments[k], err = d.getRelationships(department, rels)
		if err != nil {
			return departments, fmt.Errorf("unable to get relationships: %w", err)
		}
	}

	return departments, nil
}

func (d *DepartmentModel) getRelationships(department Department, rels map[string]bool) (Department, error) {
	if rels["directorate"] || rels["*"] {
		var dir Directorate

		err := d.DB.Get(&dir, "SELECT directorates.name, directorates.id FROM directorates INNER JOIN departments ON directorates.id = departments.directorate_id WHERE (departments.id = @p1)", department.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT directorates.name, directorates.id FROM directorates INNER JOIN departments ON directorates.id = departments.directorate_id WHERE (departments.id = %d)", department.ID)
			return department, fmt.Errorf("problem with query %s: %w", q, err)
		}

		department.Directorate = &dir
	}

	if rels["organisation"] || rels["*"] {
		var org Organisation

		err := d.DB.Get(&org, "SELECT organisations.organisation FROM departments INNER JOIN directorates ON departments.directorate_id = dbo.directorates.id INNER JOIN organisations ON directorates.organisation_id = organisations.id WHERE (departments.id = @p1)", department.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT organisations.organisation FROM departments INNER JOIN directorates ON departments.directorate_id = dbo.directorates.id INNER JOIN organisations ON directorates.organisation_id = organisations.id WHERE (departments.id = %d)", department.ID)
			return department, fmt.Errorf("problem with query %s: %w", q, err)
		}

		department.Organisation = &org
	}

	/*if rels["events"] {
		evt := []models.Event{}

		err := d.DB.Select(&evt, "SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'department' AND object_id = @p1;", department.ID)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			q := fmt.Sprintf("SELECT id, action, old_data, new_data, operator = (SELECT username FROM users WHERE id = audit.user_id), created_at FROM audit WHERE object = 'department' AND object_id = %d;", department.ID)
			return department, fmt.Errorf("problem with query %s: %w", q, err)
		}

		department.EventLog = &evt
	}*/

	return department, nil
}

func (d *DepartmentModel) Validate(department Department, directorate string) (map[string]string, error) {
	validationErrors := make(map[string]string)

	if department.Name == "" {
		validationErrors["name"] = "Department name MUST be specified"
	}

	unique, err := d.isUnique(department, directorate)
	if err != nil {
		return validationErrors, fmt.Errorf("validating department: %w", err)
	}

	if !unique {
		validationErrors["name"] = "Department already exists"
	}

	return validationErrors, nil
}

func (d *DepartmentModel) isUnique(department Department, directorate string) (bool, error) {
	var id int64

	err := d.DB.Get(&id, "SELECT id FROM departments WHERE name = @p1 AND directorate_id = (SELECT id FROM directorates WHERE name = @p2);", department.Name, directorate)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, nil
	}
	if err != nil {
		return false, fmt.Errorf("problem with query;  SELECT id FROM departments WHERE name = '%s' AND directorate_id = (SELECT id FROM directorates WHERE name = '%s'): %w", department.Name, directorate, err)
	}

	// if the returned ID matches the one we have passed then we know it is the same record and can be considered unique
	if id == department.ID {
		return true, nil
	}

	return false, nil
}

func (d *DepartmentModel) Insert(department Department, directorate string) error {
	_, err := d.DB.Exec("INSERT INTO departments (name, directorate_id) VALUES(@p1, (SELECT id FROM directorates WHERE name = @p2));", department.Name, directorate)
	if err != nil {
		return fmt.Errorf("creating department: %w", err)
	}

	return nil
}

func (d *DepartmentModel) Update(department Department, directorate string) error {
	_, err := d.DB.Exec("UPDATE departments SET name = @p1, directorate_id = (SELECT id FROM directorates WHERE name = @p2) WHERE id = @p3", department.Name, directorate, department.ID)
	if err != nil {
		return fmt.Errorf("updating department with ID %d: %w", department.ID, err)
	}

	return nil
}

func (d *DepartmentModel) Get(id int64) (Department, error) {
	var department Department

	err := d.DB.Get(&department, "SELECT id, name, directorate_id FROM departments WHERE id = @p1", id)
	if err != nil {
		return department, fmt.Errorf("unable to get department with id %d: %w", id, err)
	}

	return department, nil
}

func (d *DepartmentModel) Delete(id int64) error {
	_, err := d.DB.Exec("DELETE FROM departments WHERE id = @p1", id)
	if err != nil {
		return fmt.Errorf("deleting department with ID %d: %w", id, err)
	}

	return nil
}

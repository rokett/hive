package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/auditlog"
)

type DatabaseFile struct {
	ID         int64     `db:"id" json:"id"`
	Name       string    `db:"name" json:"name"`
	Path       string    `db:"path" json:"path"`
	Type       string    `db:"type" json:"type"`
	Checksum   string    `json:"-"`
	DatabaseID int64     `db:"database_id" json:"database_id"`
	CreatedAt  time.Time `db:"created_at" json:"created_at"`
	UpdatedAt  time.Time `db:"updated_at" json:"updated_at"`
}

type DatabaseFileModel struct {
	DB       *sqlx.DB
	Logger   *slog.Logger
	Database Database
}

// DeleteOrphaned deletes any Database Files which are related to a Database which no longer exists
func (d *DatabaseFileModel) DeleteOrphaned() error {
	var files []DatabaseFile

	// We need to find databases which were last updated > 1 day ago, and are not linked to any databases
	q := "SELECT id, name, path, type, database_id, created_at, updated_at FROM database_files WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM databases WHERE  id = database_files.database_id);"

	err := d.DB.Select(&files, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("retrieving database files to be deleted: %w", err)
	}

	_, err = d.DB.Exec("DELETE FROM database_files WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM databases WHERE  id = database_files.database_id);")
	if err != nil {
		return fmt.Errorf("deleting orphaned database files: %w", err)
	}

	for _, f := range files {
		b, _ := json.Marshal(f)
		go auditlog.Log("delete", 1, string(b[:]), "", "database file", f.ID, d.DB, d.Logger)
	}

	return nil
}

package models

// Config contains the application configuration
type Config struct {
	Server   server      `toml:"server"`
	NATS     nats        `toml:"nats"`
	Auth     auth        `toml:"auth"`
	LDAP     []ldap      `toml:"ldap"`
	Cors     crossOrigin `toml:"cors"`
	Database database    `toml:"database"`
	Audit    audit       `toml:"audit"`
	Frontend frontend    `toml:"frontend"`
}

type server struct {
	Port                   int    `toml:"port"`
	Environment            string `toml:"environment"`
	NetworkScanInterval    string `toml:"network_scan_interval"`
	WarrantyUpdateInterval string `toml:"warranty_update_interval"`
	StaleIP                int    `toml:"stale_ip"`
	DellAPIURL             string `toml:"dell_api_url"`
	DellAPIOauthURL        string `toml:"dell_api_oauth_url"`
	DellAPIClientID        string `toml:"dell_api_client_id"`
	DellAPIClientSecret    string `toml:"dell_api_client_secret"`
	DellAPIWarrantyURL     string `toml:"dell_api_warranty_url"`
	SessionSecret          string `toml:"session_secret"`
	SystemDocumentationURL string `toml:"system_documentation_url"`
}

type nats struct {
	Host           string `toml:"host"`
	Port           int    `toml:"port"`
	StreamReplicas int    `toml:"stream_replicas"`
	NKeySeed       string `toml:"nkey_seed"`
}

type auth struct {
	Hosts          []string `toml:"hosts"`
	BaseDN         string   `toml:"base_dn"`
	BindDN         string   `toml:"bind_dn"`
	BindPW         string   `toml:"bind_password"`
	Port           int      `toml:"port"`
	UseSSL         bool     `toml:"use_ssl"`
	StartTLS       bool     `toml:"start_tls"`
	SSLSkipVerify  bool     `toml:"ssl_skip_verify"`
	AllowedGroupDN string   `toml:"allowed_group_dn"`
}

type ldap struct {
	Hosts         []string `toml:"hosts"`
	BaseDN        string   `toml:"base_dn"`
	BindDN        string   `toml:"bind_dn"`
	BindPW        string   `toml:"bind_password"`
	Port          int      `toml:"port"`
	UseSSL        bool     `toml:"use_ssl"`
	StartTLS      bool     `toml:"start_tls"`
	SSLSkipVerify bool     `toml:"ssl_skip_verify"`
}

type crossOrigin struct {
	AllowedOrigins []string `toml:"allowed_origins"`
	AllowedHeaders []string `toml:"allowed_headers"`
}

type database struct {
	Host     string `toml:"host"`
	Port     int    `toml:"port"`
	Name     string `toml:"name"`
	Username string `toml:"username"`
	Password string `toml:"password"`
}

type audit struct {
	MaxAge int `toml:"max_age"`
}

type frontend struct {
	Title string `toml:"title"`
}

package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/jmoiron/sqlx"
	"rokett.me/hive/internal/auditlog"
)

type Database struct {
	ID                     int64           `db:"id" json:"id"`
	Name                   string          `db:"name" json:"name"`
	Status                 string          `db:"status" json:"status"`
	RecoveryModel          string          `db:"recovery_model" json:"recovery_model"`
	ReadOnly               bool            `db:"read_only" json:"read_only"`
	Standby                bool            `db:"standby" json:"standby"`
	PreferredBackupReplica bool            `db:"preferred_backup_replica" json:"preferred_backup_replica"`
	IsPrimaryReplica       bool            `db:"is_primary_replica" json:"is_primary_replica"`
	LastFullBackup         time.Time       `db:"last_full_backup" json:"last_full_backup"`
	LastLogBackup          time.Time       `db:"last_log_backup" json:"last_log_backup"`
	CompatibilityLevel     int64           `db:"compatibility_level" json:"compatibility_level"`
	Size                   int64           `db:"size" json:"size"`
	DatabaseFiles          *[]DatabaseFile `db:"database_files" json:"database_files"`
	//SQLInstance            *SQLInstance    `db:"instance" json:"instance"`
	InstanceID int64     `db:"instance_id" json:"instance_id"`
	Checksum   string    `json:"-"`
	CreatedAt  time.Time `db:"created_at" json:"created_at"`
	UpdatedAt  time.Time `db:"updated_at" json:"updated_at"`
}

type DatabaseModel struct {
	DB       *sqlx.DB
	Logger   *slog.Logger
	Database Database
}

// DeleteOrphaned deletes any Databases which are related to a Database which no longer exists
func (d *DatabaseModel) DeleteOrphaned() error {
	var databases []Database

	// We need to find databases which were last updated > 1 day ago, and are not linked to any SQL instances
	q := "SELECT id, name, status, recovery_model, read_only, standby, preferred_backup_replica, last_full_backup, last_log_backup, instance_id, is_primary_replica, compatibility_level, size, created_at, updated_at FROM databases WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM sqlInstances WHERE  id = databases.instance_id);"

	err := d.DB.Select(&databases, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("retrieving databases to be deleted: %w", err)
	}

	_, err = d.DB.Exec("DELETE FROM databases WHERE updated_at < (SELECT CONVERT(date, GETDATE() - 1)) AND NOT EXISTS (SELECT 1 FROM sqlInstances WHERE  id = databases.instance_id);")
	if err != nil {
		return fmt.Errorf("deleting orphaned databases: %w", err)
	}

	for _, db := range databases {
		b, _ := json.Marshal(db)
		go auditlog.Log("delete", 1, string(b[:]), "", "database", db.ID, d.DB, d.Logger)
	}

	return nil
}

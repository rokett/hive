package api

import (
	"encoding/json"
	"log/slog"
	"net/http"
)

// Response represents a response to an API call
type Response struct {
	TraceID string      `json:"trace_id"`
	Message string      `json:"message"`
	Errors  []Error     `json:"errors"`
	Result  interface{} `json:"data"`
}

// Send API response back to client
func (r Response) Send(logger *slog.Logger, httpStatus int, w http.ResponseWriter) {
	json, _ := json.Marshal(r)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(httpStatus)
	_, err := w.Write(json)
	if err != nil {
		logger.Error(
			"sending API reponse",
			"error", err.Error(),
		)
	}
}

package api

import (
	"net/http"

	"rokett.me/hive/internal/validation"
)

// Error represents an error returned by the API
type Error struct {
	Status          uint              `json:"status"`
	Code            uint              `json:"code"`
	Title           string            `json:"title"`
	Detail          string            `json:"detail"`
	MoreInfo        string            `json:"moreInfo"`
	ValidationError validation.Errors `json:"validationErrors"`
}

// Errors contains an array of APIError objects
type Errors []Error

// ErrValidation represents a validation error
var ErrValidation = Error{
	Status:   http.StatusBadRequest,
	Code:     4999,
	Title:    "Validation Error",
	MoreInfo: "https://api.fff.com/docs/errors/4999",
}

// ErrJSONDecode represents an error decoding a JSON string
var ErrJSONDecode = Error{
	Status:   http.StatusBadRequest,
	Code:     3000,
	Title:    "JSON Decode Error",
	MoreInfo: "https://api.fff.com/docs/errors/3000",
}

// ErrJSONEncode represents an error encoding a struct to a JSON string
var ErrJSONEncode = Error{
	Status:   http.StatusInternalServerError,
	Code:     5002,
	Title:    "JSON Encode Error",
	MoreInfo: "https://api.fff.com/docs/errors/5002",
}

// ErrAuth represents an error during authentication
var ErrAuth = Error{
	Status:   http.StatusInternalServerError,
	Code:     3000,
	Title:    "Authentication Error",
	MoreInfo: "https://api.fff.com/docs/errors/3000",
}

// ErrParseFuzzy represents an error parsing the 'fuzzy' querystring argument
var ErrParseFuzzy = Error{
	Status:   http.StatusInternalServerError,
	Code:     5111,
	Title:    "Unable to parse 'fuzzy' querystring argument",
	MoreInfo: "https://api.fff.com/docs/errors/5111",
}

// ErrParseIncludeDisabled represents an error parsing the 'includeDisabled' querystring argument
var ErrParseIncludeDisabled = Error{
	Status:   http.StatusInternalServerError,
	Code:     5111,
	Title:    "Unable to parse 'includeDisabled' querystring argument",
	MoreInfo: "https://api.fff.com/docs/errors/5111",
}

// ErrParseQuerystring represents an error parsing a querystring argument
var ErrParseQuerystring = Error{
	Status:   http.StatusInternalServerError,
	Code:     5111,
	Title:    "Unable to parse querystring argument",
	MoreInfo: "https://api.fff.com/docs/errors/5111",
}

// ErrGeneratingUUID represents an error when generating a new v4 UUID
var ErrGeneratingUUID = Error{
	Status:   http.StatusInternalServerError,
	Code:     5111,
	Title:    "Unable to generator uuid",
	MoreInfo: "https://api.fff.com/docs/errors/5111",
}

// ErrInvalidQuerystringParameter represents an error when a querystring parameter was specified which is not valid for the endpoint being queried.
var ErrInvalidQuerystringParameter = Error{
	Status:   http.StatusBadRequest,
	Code:     5111,
	Title:    "Invalid querystring parameter",
	MoreInfo: "https://api.fff.com/docs/errors/5111",
}

// Add entries to the APIErrors array
func (ae Errors) Add(errorType Error, ve validation.Errors) Errors {
	if ve != nil {
		errorType.ValidationError = ve
	}

	ae = append(ae, errorType)

	return ae
}

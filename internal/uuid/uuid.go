package uuid

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
)

// UUID is the byte representation of a UUID
type UUID [16]byte

// NewV4 generates and returns a v4 UUID
func NewV4() (u UUID, err error) {
	_, err = io.ReadFull(rand.Reader, u[:])
	if err != nil {
		return u, fmt.Errorf("unable to generate v4 UUID: %w", err)
	}

	return u, nil
}

func (u UUID) String() string {
	buf := make([]byte, 36)

	hex.Encode(buf[0:8], u[0:4])
	buf[8] = '-'
	hex.Encode(buf[9:13], u[4:6])
	buf[13] = '-'
	hex.Encode(buf[14:18], u[6:8])
	buf[18] = '-'
	hex.Encode(buf[19:23], u[8:10])
	buf[23] = '-'
	hex.Encode(buf[24:], u[10:])

	return string(buf)
}

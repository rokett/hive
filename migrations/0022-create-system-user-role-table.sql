IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'systems') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'users') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'system_user_role')
    CREATE TABLE system_user_role (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        system_id INT NOT NULL,
        user_id INT NOT NULL,
        role NVARCHAR(50) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'system_user_role_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER system_user_role_updated_at ON system_user_role FOR UPDATE AS
        UPDATE system_user_role
        SET updated_at = GETDATE()
        FROM system_user_role INNER JOIN deleted d
        ON system_user_role.id = d.id;');

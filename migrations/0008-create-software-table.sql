IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'software')
	CREATE TABLE software (
        id BIGINT PRIMARY KEY IDENTITY NOT NULL,
        [name] NVARCHAR(250) NOT NULL,
        [version] NVARCHAR(250) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        CONSTRAINT UniqueSoftware UNIQUE(name, version)
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'software_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER software_updated_at ON software FOR UPDATE AS
        UPDATE software
        SET updated_at = GETDATE()
        FROM software INNER JOIN deleted d
        ON software.id = d.id;');

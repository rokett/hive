IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'networks') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'organisations') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'network_organisation')
    CREATE TABLE network_organisation (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        network_id INT NOT NULL,
        organisation_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'network_organisation_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER network_organisation_updated_at ON network_organisation FOR UPDATE AS
        UPDATE network_organisation
        SET updated_at = GETDATE()
        FROM network_organisation INNER JOIN deleted d
        ON network_organisation.id = d.id;');

IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'groups')
	CREATE TABLE groups (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [group] NVARCHAR(100) NOT NULL,
        group_dn NVARCHAR(250) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'groups_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER groups_updated_at ON groups FOR UPDATE AS
        UPDATE groups
        SET updated_at = GETDATE()
        FROM groups INNER JOIN deleted d
        ON groups.id = d.id;');

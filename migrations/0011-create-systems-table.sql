IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'systems')
    CREATE TABLE systems (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [name] NVARCHAR(200) NOT NULL,
        [description] NVARCHAR(max),
        tier INT NOT NULL,
        oncall BIT NOT NULL,
        maintenance_window INT NOT NULL,
        websites NVARCHAR(250),
        distribution_lists NVARCHAR(500),
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'systems_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER systems_updated_at ON systems FOR UPDATE AS
        UPDATE systems
        SET updated_at = GETDATE()
        FROM systems INNER JOIN deleted d
        ON systems.id = d.id;');

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'databases' AND COLUMN_NAME = 'preferred_backup_replicate')
    EXEC sp_rename 'databases.preferred_backup_replicate', 'preferred_backup_replica', 'COLUMN';

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'services') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'systems') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'service_system')
    CREATE TABLE service_system (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        service_id INT NOT NULL,
        system_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'service_system_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER service_system_updated_at ON service_system FOR UPDATE AS
        UPDATE service_system
        SET updated_at = GETDATE()
        FROM service_system INNER JOIN deleted d
        ON service_system.id = d.id;');

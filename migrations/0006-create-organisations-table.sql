IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'organisations')
	CREATE TABLE organisations (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        organisation NVARCHAR(50) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'organisations_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER organisations_updated_at ON organisations FOR UPDATE AS
        UPDATE organisations
        SET updated_at = GETDATE()
        FROM organisations INNER JOIN deleted d
        ON organisations.id = d.id;');

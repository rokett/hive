IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'services')
    CREATE TABLE services (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [name] NVARCHAR(150) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'services_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER services_updated_at ON services FOR UPDATE AS
        UPDATE services
        SET updated_at = GETDATE()
        FROM services INNER JOIN deleted d
        ON services.id = d.id;');

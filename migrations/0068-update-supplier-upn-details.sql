DECLARE @temp TABLE (
	rowId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	supplier_id INT,
	upn NVARCHAR(175)
)
DECLARE @rows INT
DECLARE @currentRow INT
DECLARE @upn NVARCHAR(175)
DECLARE @supplierId INT
DECLARE @userId INT

INSERT INTO @temp (supplier_id, upn) SELECT id, upn FROM suppliers where upn != ''

SET @rows = @@ROWCOUNT

SET @currentRow = 0

WHILE @currentRow < @rows
	BEGIN
		SET @currentRow = @currentRow + 1

		SELECT @upn = upn, @supplierId = supplier_id FROM @temp WHERE rowID = @currentRow

		IF NOT EXISTS (SELECT * FROM users WHERE upn = @upn)
			INSERT INTO users (upn) VALUES (@upn)

		SELECT @userId = id FROM users WHERE upn = @upn

		IF NOT EXISTS (SELECT * FROM supplier_user WHERE supplier_id = @supplierId AND user_id = @userId)
			INSERT INTO supplier_user (supplier_id, user_id) VALUES (@supplierId, @userId)
	END

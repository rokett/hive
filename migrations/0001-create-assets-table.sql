IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'assets')
	CREATE TABLE assets (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [name] NVARCHAR(25) NOT NULL,
        manufacturer NVARCHAR(100),
        model NVARCHAR(100),
        serial_number NVARCHAR(100),
        os NVARCHAR(100),
        service_pack NVARCHAR(10),
        warranty_expiry_date DATETIME2(3),
        ignore_warranty BIT,
        asset_type NVARCHAR(40),
        third_party_warranty BIT,
        recovery_method NVARCHAR(25),
        domain NVARCHAR(50),
        uuid NVARCHAR(36),
        fqdn NVARCHAR(100),
        build_timestamp DATETIME2(3),
        is_virtual BIT,
        warranty_level NVARCHAR(50),
        ship_date DATETIME2(3),
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'assets_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER assets_updated_at ON assets AFTER UPDATE AS
		UPDATE assets
		SET updated_at = GETDATE()
		FROM assets INNER JOIN deleted d
		ON assets.id = d.id;');

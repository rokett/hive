UPDATE users SET token_max_age = 0 WHERE token_max_age IS NULL

DECLARE @c NVARCHAR(50)

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id WHERE sys.columns.column_id = sys.default_constraints.parent_column_id AND sys.columns.name = 'token_max_age')
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id WHERE sys.columns.column_id = sys.default_constraints.parent_column_id AND sys.columns.name = 'token_max_age'
        EXEC('ALTER TABLE users DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'users' AND COLUMN_NAME = 'token_max_age')
    BEGIN
        ALTER TABLE users ALTER COLUMN token_max_age BIGINT NOT NULL;
        ALTER TABLE users ADD DEFAULT ((0)) FOR token_max_age;
    END

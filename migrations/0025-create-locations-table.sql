IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'locations')
    CREATE TABLE locations (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        office NVARCHAR(50) NOT NULL,
        first_line NVARCHAR(50) NOT NULL,
        street NVARCHAR(50) NOT NULL,
        city NVARCHAR(50) NOT NULL,
        county NVARCHAR(50) NOT NULL,
        post_code NVARCHAR(7) NOT NULL,
        telephone NVARCHAR(11) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'locations_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER locations_updated_at ON directorates FOR UPDATE AS
        UPDATE locations
        SET updated_at = GETDATE()
        FROM locations INNER JOIN deleted d
        ON locations.id = d.id;');

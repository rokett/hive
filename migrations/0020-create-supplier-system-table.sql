IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'suppliers') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'systems') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'supplier_system')
    CREATE TABLE supplier_system (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        supplier_id INT NOT NULL,
        system_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'supplier_system_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER supplier_system_updated_at ON supplier_system FOR UPDATE AS
        UPDATE supplier_system
        SET updated_at = GETDATE()
        FROM supplier_system INNER JOIN deleted d
        ON supplier_system.id = d.id;');

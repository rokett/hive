IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'directorates')
    CREATE TABLE directorates (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        name NVARCHAR(150) NOT NULL,
        organisation_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'directorates_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER directorates_updated_at ON directorates FOR UPDATE AS
        UPDATE directorates
        SET updated_at = GETDATE()
        FROM directorates INNER JOIN deleted d
        ON directorates.id = d.id;');

IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'database_files')
    CREATE TABLE database_files (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        name NVARCHAR(150) NOT NULL,
        path NVARCHAR(300) NOT NULL,
        type NVARCHAR(5) NOT NULL,
        checksum NVARCHAR(64) NOT NULL,
        database_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'database_files_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER database_files_updated_at ON database_files FOR UPDATE AS
        UPDATE database_files
        SET updated_at = GETDATE()
        FROM database_files INNER JOIN deleted d
        ON database_files.id = d.id;');

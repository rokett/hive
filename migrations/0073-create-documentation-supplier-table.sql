IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'documentation') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'suppliers') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'documentation_supplier')
    CREATE TABLE documentation_supplier (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        documentation_id INT NOT NULL,
        supplier_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'documentation_supplier_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER documentation_supplier_updated_at ON documentation_supplier FOR UPDATE AS
        UPDATE documentation_supplier
        SET updated_at = GETDATE()
        FROM documentation_supplier INNER JOIN deleted d
        ON documentation_supplier.id = d.id;');

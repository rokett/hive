IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'suppliers') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'users') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'supplier_user')
    CREATE TABLE supplier_user (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        supplier_id INT NOT NULL,
        user_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'supplier_user_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER supplier_user_updated_at ON supplier_user FOR UPDATE AS
        UPDATE supplier_user
        SET updated_at = GETDATE()
        FROM supplier_user INNER JOIN deleted d
        ON supplier_user.id = d.id;');

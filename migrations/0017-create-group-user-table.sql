IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'groups') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'users') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'group_user')
    CREATE TABLE group_user (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        group_id INT NOT NULL,
        user_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'group_user_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER group_user_updated_at ON group_user FOR UPDATE AS
        UPDATE group_user
        SET updated_at = GETDATE()
        FROM group_user INNER JOIN deleted d
        ON group_user.id = d.id;');

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'systems' AND COLUMN_NAME = 'documentation')
    UPDATE systems SET documentation = '' WHERE documentation IS NULL;

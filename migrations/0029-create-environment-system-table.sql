IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'environments') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'systems') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'environment_system')
    CREATE TABLE environment_system (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        environment_id INT NOT NULL,
        system_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'environment_system_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER environment_system_updated_at ON environment_system FOR UPDATE AS
        UPDATE environment_system
        SET updated_at = GETDATE()
        FROM environment_system INNER JOIN deleted d
        ON environment_system.id = d.id;');

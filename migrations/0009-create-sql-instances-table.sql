IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'sqlInstances')
	CREATE TABLE sqlInstances (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [name] NVARCHAR(250) NOT NULL,
        service_account NVARCHAR(250) NOT NULL,
        asset_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'sqlInstances_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER sqlInstances_updated_at ON sqlInstances FOR UPDATE AS
        UPDATE sqlInstances
        SET updated_at = GETDATE()
        FROM sqlInstances INNER JOIN deleted d
        ON sqlInstances.id = d.id;');

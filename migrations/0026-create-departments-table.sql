IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'departments')
    CREATE TABLE departments (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        name NVARCHAR(150) NOT NULL,
        directorate_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'departments_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER departments_updated_at ON departments FOR UPDATE AS
        UPDATE departments
        SET updated_at = GETDATE()
        FROM departments INNER JOIN deleted d
        ON departments.id = d.id;
');

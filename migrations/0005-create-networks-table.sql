IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'networks')
	CREATE TABLE networks (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        network NVARCHAR(18) NOT NULL,
        [description] NVARCHAR(max),
        subnet_mask NVARCHAR(15) NOT NULL,
        gateway NVARCHAR(15) NOT NULL,
        vlan_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'networks_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER networks_updated_at ON networks FOR UPDATE AS
        UPDATE networks
        SET updated_at = GETDATE()
        FROM networks INNER JOIN deleted d
        ON networks.id = d.id;');

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'assets') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'systems') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'asset_system')
    CREATE TABLE asset_system (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        asset_id INT NOT NULL,
        system_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'asset_system_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER asset_system_updated_at ON asset_system FOR UPDATE AS
        UPDATE asset_system
        SET updated_at = GETDATE()
        FROM asset_system INNER JOIN deleted d
        ON asset_system.id = d.id;');

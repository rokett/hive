IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'documentation')
	CREATE TABLE documentation (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [type] NVARCHAR(55) NOT NULL,
        [path] NVARCHAR(1000) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'documentation_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER documentation_updated_at ON documentation AFTER UPDATE AS
		UPDATE documentation
		SET updated_at = GETDATE()
		FROM documentation INNER JOIN deleted d
		ON documentation.id = d.id;');

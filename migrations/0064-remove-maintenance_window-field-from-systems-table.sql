DECLARE @c NVARCHAR(50)

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id WHERE (sys.columns.name = 'maintenance_window') AND (sys.default_constraints.parent_object_id = (SELECT sys.tables.object_id FROM sys.tables WHERE sys.tables.name = 'systems')))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id WHERE (sys.columns.name = 'maintenance_window') AND (sys.default_constraints.parent_object_id = (SELECT sys.tables.object_id FROM sys.tables WHERE sys.tables.name = 'systems'))
        EXEC('ALTER TABLE systems DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'systems' AND COLUMN_NAME = 'maintenance_window')
    ALTER TABLE systems DROP COLUMN maintenance_window;

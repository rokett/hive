DECLARE @c NVARCHAR(50)

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'domain') AND (sys.objects.name = N'suppliers'))
BEGIN
    SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'domain') AND (sys.objects.name = N'suppliers')
    EXEC('ALTER TABLE suppliers DROP CONSTRAINT ' + @c)
END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'suppliers' AND COLUMN_NAME = 'domain')
    ALTER TABLE suppliers DROP COLUMN domain;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'upn') AND (sys.objects.name = N'suppliers'))
BEGIN
    SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'upn') AND (sys.objects.name = N'suppliers')
    EXEC('ALTER TABLE suppliers DROP CONSTRAINT ' + @c)
END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'suppliers' AND COLUMN_NAME = 'upn')
    ALTER TABLE suppliers DROP COLUMN upn;

DECLARE @c NVARCHAR(50)

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id WHERE (sys.columns.name = 'username') AND (sys.default_constraints.parent_object_id = (SELECT sys.tables.object_id FROM sys.tables WHERE sys.tables.name = 'suppliers')))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id WHERE (sys.columns.name = 'username') AND (sys.default_constraints.parent_object_id = (SELECT sys.tables.object_id FROM sys.tables WHERE sys.tables.name = 'suppliers'))
        EXEC('ALTER TABLE suppliers DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'suppliers' AND COLUMN_NAME = 'username')
    ALTER TABLE suppliers DROP COLUMN username;

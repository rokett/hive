IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'organisations') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'systems') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'organisation_system')
    CREATE TABLE organisation_system (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        organisation_id INT NOT NULL,
        system_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'organisation_system_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER organisation_system_updated_at ON organisation_system FOR UPDATE AS
        UPDATE organisation_system
        SET updated_at = GETDATE()
        FROM organisation_system INNER JOIN deleted d
        ON organisation_system.id = d.id;');

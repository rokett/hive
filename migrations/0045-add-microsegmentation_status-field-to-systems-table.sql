IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'systems' AND COLUMN_NAME = 'microsegmentation_status')
    ALTER TABLE systems ADD microsegmentation_status NVARCHAR(11) NOT NULL DEFAULT 'not started';

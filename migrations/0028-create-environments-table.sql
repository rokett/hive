IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'environments')
    CREATE TABLE environments (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        name NVARCHAR(150) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'departments_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER environments_updated_at ON environments FOR UPDATE AS
        UPDATE environments
        SET updated_at = GETDATE()
        FROM environments INNER JOIN deleted d
        ON environments.id = d.id;');

IF NOT EXISTS (SELECT 1 FROM environments WHERE name = 'Production')
    INSERT INTO environments (name) VALUES ('Production');

IF NOT EXISTS (SELECT 1 FROM environments WHERE name = 'Test')
    INSERT INTO environments (name) VALUES ('Test')

IF NOT EXISTS (SELECT 1 FROM environments WHERE name = 'Dev')
    INSERT INTO environments (name) VALUES ('Dev')

IF NOT EXISTS (SELECT 1 FROM environments WHERE name = 'Staging')
    INSERT INTO environments (name) VALUES ('Staging')

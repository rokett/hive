IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'accreditations')
	CREATE TABLE accreditations (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        accreditation NVARCHAR(50) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'accreditations_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER accreditations_updated_at ON accreditations FOR UPDATE AS
        UPDATE accreditations
        SET updated_at = GETDATE()
        FROM accreditations INNER JOIN deleted d
        ON accreditations.id = d.id;');

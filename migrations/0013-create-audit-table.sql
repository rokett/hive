IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'users')
    CREATE TABLE audit (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        action NVARCHAR(6) NOT NULL,
        old_data NVARCHAR(max) NOT NULL,
        new_data NVARCHAR(max) NOT NULL,
        object NVARCHAR(25) NOT NULL,
        object_id INT NOT NULL,
        user_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

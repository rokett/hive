IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'eventTargets')
	CREATE TABLE eventTargets (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        name NVARCHAR(55) NOT NULL,
        target NVARCHAR(1000) NOT NULL,
        event_type NVARCHAR(1000) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'eventTargets_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER eventTargets_updated_at ON eventTargets AFTER UPDATE AS
		UPDATE eventTargets
		SET updated_at = GETDATE()
		FROM eventTargets INNER JOIN deleted d
		ON eventTargets.id = d.id;');

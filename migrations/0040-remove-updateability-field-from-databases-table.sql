IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'D' AND NAME = 'DF_databases_updateability')
    ALTER TABLE databases DROP CONSTRAINT [DF_databases_updateability];

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'databases' AND COLUMN_NAME = 'updateability')
    ALTER TABLE databases DROP COLUMN updateability;

IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'databases' AND COLUMN_NAME = 'compatibility_level')
    ALTER TABLE databases ADD compatibility_level INT NOT NULL DEFAULT 0;

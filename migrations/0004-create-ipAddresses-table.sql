IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'ipAddresses')
	CREATE TABLE ipAddresses (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        ip NVARCHAR(15) NOT NULL,
        hostname NVARCHAR(50),
        [description] NVARCHAR(max),
        static BIT NOT NULL,
        reserved BIT NOT NULL,
        network_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'ipAddresses_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER ipAddresses_updated_at ON ipAddresses FOR UPDATE AS
        UPDATE ipAddresses
        SET updated_at = GETDATE()
        FROM ipAddresses INNER JOIN deleted d
        ON ipAddresses.id = d.id;');

IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'suppliers')
    CREATE TABLE suppliers (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        supplier NVARCHAR(50) NOT NULL,
        username NVARCHAR(25),
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'suppliers_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER suppliers_updated_at ON suppliers FOR UPDATE AS
        UPDATE suppliers
        SET updated_at = GETDATE()
        FROM suppliers INNER JOIN deleted d
        ON suppliers.id = d.id;');

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'assets' AND COLUMN_NAME = 'third_party_warranty')
    UPDATE assets SET third_party_warranty = 0 WHERE third_party_warranty IS NULL;

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'assets' AND COLUMN_NAME = 'third_party_warranty')
    ALTER TABLE assets ALTER COLUMN third_party_warranty BIT NOT NULL;

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'assets' AND COLUMN_NAME = 'third_party_warranty' AND COLUMN_DEFAULT IS NULL)
    ALTER TABLE assets ADD DEFAULT ((0)) FOR third_party_warranty;

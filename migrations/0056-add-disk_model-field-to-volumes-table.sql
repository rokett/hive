IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'volumes' AND COLUMN_NAME = 'disk_model')
    ALTER TABLE volumes ADD disk_model NVARCHAR(50) NOT NULL DEFAULT '';

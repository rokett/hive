IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'databases' AND COLUMN_NAME = 'is_primary_replica')
    -- Default value is true to account for DBs which are not part of an AG and do not report properly
    ALTER TABLE databases ADD is_primary_replica BIT NOT NULL DEFAULT 1;

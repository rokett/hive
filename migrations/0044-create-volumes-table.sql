IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'volumes')
    CREATE TABLE volumes (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        block_size INT NOT NULL,
        capacity BIGINT NOT NULL,
        drive_letter NVARCHAR(2) NOT NULL,
        filesystem NVARCHAR(6) NOT NULL,
        free_space BIGINT NOT NULL,
        label NVARCHAR(100) NOT NULL,
        checksum NVARCHAR(64) NOT NULL,
        asset_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'volumes_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER volumes_updated_at ON volumes FOR UPDATE AS
        UPDATE volumes
        SET updated_at = GETDATE()
        FROM volumes INNER JOIN deleted d
        ON volumes.id = d.id;');

IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'databases')
    CREATE TABLE databases (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        name NVARCHAR(150) NOT NULL,
        status NVARCHAR(17) NOT NULL,
        recovery_model NVARCHAR(11) NOT NULL,
        read_only BIT NOT NULL,
        preferred_backup_replicate BIT NOT NULL,
        standby BIT NOT NULL,
        updateability NVARCHAR(128) NOT NULL,
        last_full_backup DATETIME2(3),
        last_log_backup DATETIME2(3),
        instance_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'databases_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER databases_updated_at ON databases FOR UPDATE AS
        UPDATE databases
        SET updated_at = GETDATE()
        FROM databases INNER JOIN deleted d
        ON databases.id = d.id;');

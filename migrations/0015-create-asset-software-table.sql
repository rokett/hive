IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'assets') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'software') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'asset_software')
    CREATE TABLE asset_software (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        asset_id INT NOT NULL,
        software_id BIGINT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'asset_software_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER asset_software_updated_at ON asset_software FOR UPDATE AS
        UPDATE asset_software
        SET updated_at = GETDATE()
        FROM asset_software INNER JOIN deleted d
        ON asset_software.id = d.id;');

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'accreditations') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'suppliers') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'accreditation_supplier')
    CREATE TABLE accreditation_supplier (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        accreditation_id INT NOT NULL,
        supplier_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'accreditation_supplier_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER accreditation_supplier_updated_at ON accreditation_supplier FOR UPDATE AS
        UPDATE accreditation_supplier
        SET updated_at = GETDATE()
        FROM accreditation_supplier INNER JOIN deleted d
        ON accreditation_supplier.id = d.id;');

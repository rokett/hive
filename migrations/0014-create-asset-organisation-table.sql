IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'assets') AND EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'organisations') AND NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'asset_organisation')
    CREATE TABLE asset_organisation (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        asset_id INT NOT NULL,
        organisation_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'asset_organisation_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER asset_organisation_updated_at ON asset_organisation FOR UPDATE AS
        UPDATE asset_organisation
        SET updated_at = GETDATE()
        FROM asset_organisation INNER JOIN deleted d
        ON asset_organisation.id = d.id;');

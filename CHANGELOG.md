# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [4.13.0] - TBC
- #180 Ensuring that the asset `domain` and `fqdn` fields are returned for queries which return asset information.

## [4.12.0] - 2024-12-01
### Added
- #165 **BREAKING** In preparation for using NATS to communicate with agents, the server now requires and connects to a NATS instance.  New parameters have been added to the config file to prepare for this, so ensure you copy the `NATS` block from the template and update it for your instance.

  You need to create an account in NATS for Atom.  Download the NK tool from https://github.com/nats-io/nkeys/nk or, if you have Go installed, you can run `go install github.com/nats-io/nkeys/nk@latest` to install it directly.

  Generate a user NKey by running `nk -gen user -pubout`.  The first output line starts with the letter S for Seed. Seeds are private keys; you should treat them as secrets and guard them with care.  The seed goes into your Hive `config.toml` file.  The second line starts with the letter U for User, and is the public key which can be safely shared.

  Edit the NATS config file, or create a new one if you don't have one already, and add a Hive account to the accounts map, using the public user key you just generated.

  ```
  accounts: {
      Hive: {
          jetstream: enabled,
          users: [
              { nkey: UCOWSP6PT77DSIKIZV6KBEE4GFAD7L2ZMOBVTGDLVKKBTYS6LZQNUBQN }
          ]
      },
  }
  ```

  If this is a brand new config file, you'll also need to add the `jetstream` map at the top of the file to enable JetStream; See https://docs.nats.io/running-a-nats-service/configuration#jetstream for configuration options.

  ```
  jetstream {}
  ```
- #175 New functionality to add documentation records to suppliers.  These Are more than likely a link to some kind of wiki entry, or a UNC path, to a signed agreement between the organisation and a supplier.

### Changed
- #178 Updated dependencies.

## [4.11.0] - 2024-11-18
### Changed
- #171 Replaced Zap with Slog to remove external dependency.
- #173 Updated dependencies.

### Fixed
- #170 Fixed LDAP lookups for Authorised Users for systems.  Also noted that, depending on what combination of IAO, Developers, System Managers and Authorised Users were already selected that the behaviour was very much broken; all now fixed.
- #174 Resolved a bunch of static check issues; nothing breaking, but no more static check errors.

## [4.10.2] - 2023-04-19
- #169 Resolved issues with Dell warranty update process; it looks like spaces in serial numbers were causing the issue.

## [4.10.1] - 2023-04-04
### Added
- #168 Added response body to error log when there is a problem updating warranty info from Dell.

## [4.10.0] - 2022-11-19
### Added
- #17 Exposing key RED metrics via the `/metrics` endpoint in Prometheus format;

  - `http_errors_total` partitioned by status code,.
  - `http_request_duration_seconds` partitioned by path and method.
  - `http_requests_total` partitioned by path, method and status.

## [4.9.4] - 2022-10-11
### Fixed
- #160 Resolved a panic when trying to export systems; needed to ensure we were retrieving the asset/environments relationship.

## [4.9.3] - 2022-10-09
### Fixed
- #154 Ensure that the serial `number`, `manufacturer`, and `model` fields are visible and editable for all asset types, not just servers.
- #158 Fixing issues raised by Gitlab vulnerability scan around unhandled errors.

### Changed
- #157 Updated dependencies.

### Added
- #159 Create a Software Bill of Materials (SBoM) for the tagged release.

## [4.9.2] - 2022-10-07
### Fixed
- #156 Resolved the problem of the network scan never completing by ensuring that when Hive pings an IP address, it has a limit on the number of pings. Previously, with no limit, it just kept going and never ended.

## [4.9.1] - 2022-09-23
### Fixed
- #153 Changed container image to run from `alpine` rather than scratch.  Could likely have made things work by copying in `busybox` to scratch, but the `alpine` image isn't much bigger and it removes the possibility of future issues.  `Ping` now works.

## [4.9.0] - 2022-09-12
### Changed
- #152 Reconfigured Dockerfile so that Hive does not run as root when running as a container; it now uses UID 10000 and GID 10001.

## [4.8.2] - 2022-08-30
### Fixed
- Resolved issue with typo in SQL Instances struct which was stopping databases from being returned in JSON.

## [4.8.1] - 2022-08-24
### Fixed
- #151 Resolved an issue where Hive returned a `500` status code when updating a supplier record, if the supplier did not have any accreditations listed.

## [4.8.0] - 2022-08-10
### Added
- #146 Systems have a new field called `documentation`.  The template URL for documentation is defined within the config file.  The portion of the template denoted by `[system]` is replaced when a new system is created with the name of the system, thus creating a standarised URL.

  Existing systems are not changed.

## [4.7.5] - 2022-08-08
### Fixed
- The boolean, Ignore Warranty and Third Party Warranty fields for assets were all being displayed as true.  Corrected that bug by ensuring that only those which were really set to true are displayed as such.
- #150 Bug which was causing adding a new system, if the operator did not tick the box to make it covered on-call, fail with a server error has been resolved.  At the same time, fixed an issue where boolean values were all being displayed as true, even if they were false.

## [4.7.4] - 2022-08-02
### Fixed
- #149 Changed interval for verifying all user details against LDAP source to once per day instead of every 1 minute.  That was a debugging config which made it into production by mistake.

## [4.7.3] - 2022-07-30
### Fixed
- #147 Deleting an asset when it was not linked to any systems was broken; the server returned a 500 status.  The deletion function was expecting to see some linked systems, which obviously were not there.  Hive now checks for linked systems before running the code in question, so deletions now work.
- #148 Ensuring that when adding an asset via the API, it is linked to requested systems.  Previously the asset was added but no systems were linked.

## [4.7.2] - 2022-07-15
### Fixed
- #142 Fixed issue whereby when editing a system with an existing environment selected, and trying to add a related asset, it was not possible to select the environment that the asset was related to.
- #143 Fixed issue with saving asset changes via the UI.
- #144 Ensuring that a user selected field to be visible, which no longer exists, does not stop the UI from properly loading.  All fields that the users select to be visible, will be visible.

## [4.7.1] - 2022-07-13
## Fixed
- #141 Fixed issue with incorrect JSON parameter name in `asset` model.  Should have been `hostname` but was configured as `name`.

## [4.7.0] - 2022-07-12
### Fixed
- A bug was causing user token details to be removed every time the users table was updated, causing all users to need to log in far more often than is necessary.
- #138 Cron job to delete orphaned database files was not working due to a typo in the database name; now corrected.

### Changed
- #55 Suppliers can now have multiple accounts assigned to them; previously they could only have one.
- #136 Since 4.0.0 when Hive became a server side rendered application, there have remained duplicated models, one for the UI and one for the API.  These have now been unified so that there is a single set of models.  There are no user facing changes as a result of this work.

### Added
- As a result of other changes, added the following log fields.
  - `upn`
  - `id`
  - `asset`
- #9 Created cron job to run daily which will delete any zombie asset.  A zombie asset is defined as anything WITH Asset Collector installed but which HAS NOT checked in within the last 30 days.

  Initially the actual deletion is disabled in favour of a period of logging only, in order to sanity check what is going to be deleted.

## [4.7.0-rc2] - 2022-06-28
### Fixed
- A bug was causing user token details to be removed every time the users table was updated, causing all users to need to log in far more often than is necessary.
- #138 Cron job to delete orphaned database files was not working due to a typo in the database name; now corrected.

### Changed
- #55 Suppliers can now have multiple accounts assigned to them; previously they could only have one.
- #136 Since 4.0.0 when Hive became a server side rendered application, there have remained duplicated models, one for the UI and one for the API.  These have now been unified so that there is a single set of models.  There are no user facing changes as a result of this work.

### Added
- As a result of other changes, added the following log fields.
  - `upn`
  - `id`
  - `asset`
- #9 Created cron job to run daily which will delete any zombie asset.  A zombie asset is defined as anything WITH Asset Collector installed but which HAS NOT checked in within the last 30 days.

  Initially the actual deletion is disabled in favour of a period of logging only, in order to sanity check what is going to be deleted.

## [4.6.0] - 2022-06-27
### Changed
- #140 Ensured that the container image has the root certificate authority certificates.  Without it, SSL connections fail, notably the Dell warranty lookup.

## [4.5.0] - 2022-06-21
### Changed
- #139 Ensuring that all fields are returned in API calls.  Previously, anything with an empty value was not returned, which is more confusing than things need to be.

## [4.7.0-rc1] - 2022-06-18
### Fixed
- A bug was causing user token details to be removed every time the users table was updated, causing all users to need to log in far more often than is necessary.

### Changed
- #55 Suppliers can now have multiple accounts assigned to them; previously they could only have one.

## [4.4.0] - 2022-06-12
### Changed
- #135 Reconfigured migrations to strip line breaks in order to ensure that the resulting hash is the same on Windows and Linux.  This allows for porting between the two, which may be unlikely, but without this change running the migrations would fail.

  **Important:** To support this change you must delete all records from the migrations table.  This will force migrations to run again, which are idempotent from a schema perspective.  No other changes are necessary.

## [4.3.0] - 2022-06-08
### Changed
- #137 Ensure we are logging errors when carrying out reverse IP lookup during network scan.

## [4.2.1] - 2022-06-06
### Fixed
- #134 Creating a new system was failing because the `INSERT` SQL query was expecting 9 parameters when there are only actually 7.  The bug was introduced when removing the `maintenance_window` and `microsegementation_status` fields in 4.0.0.

## [4.2.0] - 2022-06-05
### Changed
- #83 Refactored the UI for adding related assets to systems in preparation for being able to assign a role to an asset per environment.  Roles would be things like `DB server` or `web server`.
- #133 In line with #83, and to simplify system/asset management, removed the functionality to assign an asset to a system from the context of the asset.  It now all needs to be done from the context of the system.  The two were not aligned in terms of functionality anyway, with small differences which led to inaccurate data being recorded.

## [4.1.1] - 2022-06-01
### Fixed
- #132 IAO lookups were failing because the backend expected the form field to be called `iaos` when it is actually called `iao`.

## [4.1.0] - 2022-05-31
### Added
- #131 Add route to create a new application level user; `POST /api/config/application_users`.

### Changed
- #118 Migrated UI away from Bootstrap to Tailwind for styling.
- #129 Replaced `https://github.com/denisenkom/go-mssqldb` with new official package, `https://github.com/microsoft/go-mssqldb`.

## [4.1.0-rc3] - 2022-05-30
### Changed
- #118 Minor styling tweaks.
- #129 Replaced `https://github.com/denisenkom/go-mssqldb` with new official package, `https://github.com/microsoft/go-mssqldb`.

## [4.0.8] - 2022-05-25
### Fixed
- #130 Some services still used the `PATCH /api/systems/{id}` endpoint which was removed in `4.0.0`; those requests did not appear in logs.  Have now added that endpoint back.

## [4.1.0-rc2] - 2022-05-23
### Changed
- #118 Minor styling tweaks.

## [4.1.0-rc1] - 2022-05-22
### Changed
- #118 Migrated UI away from Bootstrap to Tailwind for styling.

## [4.0.7] - 2022-05-22
### Fixed
- #128 Previously, when adding or updating a `system` with a `service` that does not already exist, the request would fail.  Now the unknown service is created just in time to allow the addition or updating of a system to complete successfully.

## [4.0.6] - 2022-05-11
### Fixed
- #126 Setting an asset as decommissioned was not working due to a typo; now resolved.

## [4.0.5] - 2022-05-05
### Fixed
- #123 Here we go again.  Fingers crossed CORS is now working...was ok in test.

## [4.0.4] - 2022-05-05
### Changed
- Disabled scheduled job to update user details as it is currently clearing out the token and runs every 4 hours, forcing people to login often.

### Fixed
- #122 CORS is still broken for some reason. Allowing unauthenticated access to allow other systems to work whilst the problem is investigated.

## [4.0.3] - 2022-05-05
### Fixed
- #121 CORS was broken due to not being configured in the new router.  Have now allowed for CORS to be used.

## [4.0.2] - 2022-05-04
### Fixed
- #120 Allow for authentication using the `vnd-hive-application-token` or `Authorization` headers in addition to session tokens.  Routes now require authentication again.

## [4.0.1] - 2022-05-04
### Fixed
- #119 Applications are failing to authenticate as they do not pass a session token; they use the `vnd-hive-application-token` or `Authorization` header but that is currently not checked. Temporary, quick, fix is to remove auth requirements for API endpoints in lieu of a permanent fix which would be checking the header.

## [4.0.0] - 2022-05-03
### Added
- #28 It is now possible to link directly to a system, an asset, an organisation, or a supplier.

### Changed
- Removed internal Windows service functionality.  Running Hive as a service is now the responsibility of the user to do, using something like https://nssm.cc/ for Windows and creating their own service definition on Linux.
- #47 User models use `UPN` now instead of `username`, which has been deprecated for some time.
- #67 jQuery dependency has been removed as a consequence of making Hive an SPA no more (#112).
- #109 Removed `reminders` module as it is no longer used.
- #101, #112 Hive is no longer an SPA.  Pages are now rendered server side and HTML is sent to the client; a sprinkling of JS adds interactivity where needed.
- #114 Hive can now be run as a container.  Images are on Docker Hub.
- #116 The `maintenance_window` field has been removed from systems.
- #117 The `microsegmentation_status` field has been removed from systems.

## [3.5.0] - 2021-12-16
### Changed
- #108 Removed access to reminders module as it is deprecated.  Will be fully removed in a later release.

## [3.4.2] - 2021-12-13
## Fixed
- #107 The previous change in `3.4.0` to ensure the `systems`, `services`, and `environments` relationships for assets for Prometheus was sorted the same every time didn't work; we were iterating over a map for which order is not guaranteed.  Have refactored to just iterate over a slice for which order is guaranteed.

## [3.4.1] - 2021-11-30
### Fixed
- #106 Updating a system outside of the UI caused a panic because the handler expects to find an embedded relationship in the assets list which may not be there.  We now check that the relationship exists before processing that part of the payload.

## [3.4.0] - 2021-11-26
### Changed
- #105 When returning list of assets for Prometheus, the related systems are now ordered by their ID to ensure they do not change on each request.  This stops Prometheus considering it to be a new series.

## [3.3.0] - 2021-10-24
### Fixed
- #95 Resolved error when attempting to update an asset without a related system.

### Changed
- Updated dependencies.
- #100 Made required fields more obvious in the UI.

## [3.2.2] - 2021-10-12
### Fixed
- #103 Only include server assets with an OS which (fuzzy) match the following for Prometheus Service Discovery; windows, linux, centos, rhel, red hat, ubuntu.

## [3.2.1] - 2021-10-12
### Fixed
- #102 Resolved module import issues.

## [3.2.0] - 2021-10-12
### Added
- #91 Added `/assets/prometheus` endpoint which returns an array of assets formatted for Prometheus Service Discovery as per https://prometheus.io/docs/prometheus/latest/http_sd/.
- #93 Additional validation of querystring parameters for all endpoints.  A useful error is now returned if an invalid parameter is specified in the request.

### Changed
- #96 Changed module path from `hive` to `rokett.me/hive` to better match Go standards.
- #97 Updated dependencies.
- #99 Removed `remote_address` log field from `check_auth` endpoint.  It was a duplicate of `client_ip` and was causing some logging errors due to the format it came in.

## [3.1.0] - 2021-08-22
### Fixes
- #92 Fixed bug where you were not able to remove the last system from an asset.
- #92 Fixed bug where `object` appeared in inputs which are intended to show multiple options, such as related systems for an asset.

### Changed
- #84 Ensure that IP address description is cleared if the hostname changes during a scheduled scan.

## [3.0.0] - 2021-07-31
### Changed
- Updated dependencies.
- #70 Have removed old `v1` routes which have been deprecated for a long time.
- #76 Unified search endpoints by making searches exact unless the `fuzzy` querystring parameter is specified.
- #90 Removed unauthenticated routes.  Authentication is now either via the session token, or the `vnd-hive-application-token` header.

## [2.21.0] - 2021-06-07
### Added
- #89 Log referer in requests.

### Changed
- #87/#88 Updating dependencies.

## [2.20.1] - 2021-05-07
### Changed
- #86 Removed `github.com/satori/go.uuid` package as it is broken with security vulnerabilities.  Replaced by internal v4 UUID generator package.

## [2.20.0] - 2021-04-07
### Added
- #72 Emit events, to user defined event handlers, when systems are created, updated or deleted.  Event handlers can be managed via the API.

## [2.19.2] - 2021-03-25
### Fixed
- #81 Undo previous change to fix problem where API was ignoring the removal of all systems from an asset.  The 'fix' introduced another bug whereby when Asset Collector sent details to update the asset record, it does not include any systems and so all related systems get deleted from an asset.  Will review a fuller fix later.

## [2.19.1] - 2021-03-24
### Fixed
- #80 Changed behaviour in Selectize 0.12.6 meant that the *selectize'd* inputs meant that a random object was included which was trying to be sent to the API on updates.  The API was also ignoring the removal of all systems from an asset, which meant it was impossible to remove all related systems; this is now resolved.

## [2.19.0] - 2021-03-21
### Added
- #74 Static assets; CSS, JS and fonts; are now cached.
- #76 `fuzzy` querystring parameter in order to signify a wildcard search for `/search` endpoints.  Endpoints currently still do a wildcard search by default, but that default is now deprecated and will be removed in v3.0.0.  Any client which expects to do a wildcard search should transition to including the `fuzzy` querystring parameter.
- #77 Return `domain` field when including `assets` assigned to a `system`.

### Changed
- #67 Upgrade `pnotify` library to v5 to remove dependency on `jQuery`.
- #71 Removed the current API docs as browsing to the docs page crashed the application.  For now they are viewable in the Gitlab repo, and work is ongoing to move them to Swagger format, built-in to the application.
- #79 Removed dependency on Bower.  Frontend libraries have been moved to NPM.

## [2.18.2] - 2021-03-19
### Fixed
- #78 Ensuring that asset tier is returned when listing all assets or searching for assets.

## [2.18.1] - 2021-03-16
### Fixed
- #75 Resolved panic caused by trying to retrieve `operator_id` from context, but it now using a defined type rather than a string.  Replaced the string with the type where needed.

## [2.18.0] - 2021-03-14
### Added
- #73 Added functionality to support application users so that other applications can connect to Hive without needing to use a session cookie.

### Changed
- #37 CORS `allowed_headers` option have been added to the config in order to allow a customised CORS configuration.
- #37 If an `allowed_origin` is not set for CORS, it will be disabled.
- #66 Added ability to mark an asset as being virtualised when manually adding it.
- #67 Removed jQuery as a dependency from modules.

## [2.17.1] - 2021-02-21
### Fixed
- Corrected migration file name being used for comparison against completed migrations.  Following migration to `embed` feature, it was using the path instead of just the filename.

## [2.17.0] - 2021-02-17
### Changed
- #69 Removed [Packr](https://github.com/gobuffalo/packr) in favour of the `embed` feature in Go 1.16.

### Added
- #64 Allowing `*` to be used to specify all relationships should be returned.  For example http://localhost:8001/assets?include=*.

## [2.16.0] - 2020-07-18
### Changed
- #63 Default behaviour when querying LDAP for users has changed to not return disabled users unless explicitly requested.  Some frontend modules dealt with this already by checking the OU the user account was in, but this was brittle; the new way is more robust.

## [2.15.2] - 2020-06-19
### Changed
- #59 Increased length of `disk_model` field.

## [2.15.1] - 2020-06-19
### Changed
- Removed `storage_platform` in favour of `disk_model` field.  There was a tendency to transform data coming out of the servers before it reached Hive to add business specific rules.  This is better done in the frontend, or in whatever process pulls data out of Hive, to avoid needing to put business specific logic in Hive.

## [2.15.0] - 2020-06-17
### Added
- #57 Added `storage_platform` field to `volume` records.

## [2.14.0] - 2020-05-31
### Added
- #56 New endpoint to search for networks matching the network itself.

## [2.13.0] - 2020-05-27
### Changed
- Updated dependencies.

## [2.12.0] - 2020-05-06
### Added
- Added `compatibility_level` and `size` fields to `database` records.
- #54 Collecting database file information in relation to `database` records.

## [2.11.0] - 2020-04-19
### Added
- #53 Added endpoint to proxy request for asset to check-in, and optionally return the check-in payload to the caller.

### Changed
- Ensuring that API call to Dell API has a suitable timeout; previously no timeout was set.

## [2.10.0] - 2020-04-13
### Added
- #52 Added `ntfs_trim_enabled` and `refs_trim_enabled` bool fields to assets to track TRIM support in Windows.

### Fixed
- Supplier username was still being referenced in system relationships.  Have now removed it properly.

## [2.9.0] - 2020-03-24
### Changed
- #46 Removed `username` field from Supplier model in favour of `Upn`.
- #49 Added option to select `Recovery Method` for appliances.

### Fixed
- #50 Ensuring recovery method, and other server details, display when viewing an asset which is a server.

## [2.8.0] - 2020-03-19
### Added
- #45 Collecting volume information for assets.
- #48 Added logging of incoming requests.

### Changed
- #36 Adding checksums to SQL Instance and database records in order to allow for a more careful handling of updates.  Previously this was pretty blunt in that associated DBs were deleted and then recreated, which is eventually going to lead to running out of IDs because the ID field continues to increment.  Now we use an `upsert` method where a record is either `updated`, if it already exists based on the checksum, or `created` if it does not already exist.  Any existing records not updated are tracked and deleted.
- #40 Changed logging to logrus, `github.com/sirupsen/logrus`, package.
- #44 Removed `is_microsegmented` flag from systems in favour or `microsegmentation_status` field which is able to capture the different stages; `not started`, `in progress` or `complete`.  Reworked the frontend to provide a dropdown list of options to choose from.  The default value is `not started`.

## [2.7.0] - 2020-03-08
### Added
- #43 `is_primary_replica` field to databases in order to track whether a database is the primary in an AG or not.

### Fixed
- Corrected field name in databases table; `preferred_backup_replicate` should have been `preferred_backup_replica`.  No changes required to code as the queries already addressed the correct field name.

## [2.6.0] - 2020-03-05
### Added
- #41 `is_microsegmented` flag to signify that a system has been microsegmented.
- #42 Scheduled job to delete orphaned services.

### Fixed
- Fixed bug when creating/updating a system which caused an error to be logged to the console if the system did not have any Developers or Authorised Users specified.  Needed to ensure we return from the promise early.

## [2.5.0] - 2020-03-03
### Added
- #33 Added services as a many-to-many parent of systems.
- #37 Generating request scoped trace ID and including it in API responses and logs.
- #39 Getting true client IP address for use in logs.

### Changed
- Removed the `updateability` field from database models.  This has been deprecated for some time and is no longer needed.
- #35 Ensuring we sort the lists of suppliers, reminders, organisations, departments, and directorates in frontend.

## [2.4.0] - 2020-02-18
### Changed
- #27 Added ability to select multiple maintenance windows for systems; previously only a single maintenance window was allowed.
- #34 Ensuring that the frontend sorts assets by name when retrieving from the API.

## [2.3.1] - 2020-02-15
### Changed
- #32 The v5 Dell API has changed how authentication works from an API key to an oauth token requirement.  The response payload is also different.  Re-worked the function which updates the warranty info for Dell assets.

## [2.3.0] - 2020-02-09
### Added
- #31 Marking IPs as stale if they have not been updated within the last 10 days; if they were last updated more than 10 days ago they are cleared.

## [2.2.0] - 2019-12-10
### Added
- #30 Added parameter to search for system by its shortcode.

## [2.1.1] - 2019-12-02
### Fixed
- #29 Check whether software exists or not before attempting to insert a new record.  This ensures that we don't reserve IDs that are never going to be used, and keeps the ID count to a manageable integer.

## [2.1.0] - 2019-11-27
### Changed
- #25 Updated go-ldap/ldap package to v3.
- #26 Allowing multiple networks to have the same VLAN ID.  If you try to add a network with a VLAN ID which is already used you will be prompted to accept the duplicate before the network can be saved.

## [2.0.0] - 2019-11-15
### Added
- #1 Included frontend website into application instead of it being a separate app.

## [1.8.0] - 2019-11-07
### Added
- #18 Add decommissioned flag to assets.
- #19 Added UPN field to supports.  The username field is now deprecated in favour of UPN, and will be removed in a future release.
- #21 Add shortcode to systems to use as the first part of server names.

### Changed
- #16 Change logging format to JSON and change `func` attribute to `function`.
- #23 Ensuring that the ignore_warranty field in the assets table has a value.  It was previously set to allow NULL so the default is now 0.
- #23 Ensuring that the is_virtual field in the assets table has a value.  It was previously set to allow NULL so the default is now 0.

## [1.7.0] - 2019-10-15
### Changed
- #22 Ensuring that the `third_party_warranty` field in the `assets` table has a value.  It was previously set to allow `NULL` so the default is now `0`.

## [1.6.0] - 2019-07-08
### Added
- #5 Added support for defining environments against systems.
- #6 Added support for assigning assets to specific environments of linked systems.
- #7 Prepping for LDAP TLS support
- #14 Allowing to search for assets which a part of specified environments.
- #15 Added support for updating the network `description` and `gateway` fields.
- Added supplier search.
- Made the binary smaller by removing debug info.
- Option to validate that given VLAN ID is unique.

### Changed
- Ensured that we don't use up IDs unecessarily when running migrations by checking whether the migration record exists before trying to insert.  Although we captued a duplicate insert error, the act of even trying it increased the next ID able to be used.

## [1.5.0] - 2019-05-30
### Added
 - Added UPN field to suppliers.  The supplier username field is now deprecated in favour of UPN, and will be removed in a future update.

## [1.4.0] - 2019-05-28
### Added
 - #13 Added additional logging to CheckAuth function.

### Changed
 - Additional commenting explaining how the network scan/ping functions work.
 - #12 Removed built-in LDAP recursive group search as it is slow.  Replaced with manual recursive group search which is much faster.
 - Changed LDAP filters from `objectClass` to `objectCategory` which is indexed and therefore faster and more efficient.

## [1.3.0] - 2019-05-06
### Changed
 - Ensuring DB migrations are idempotent by checking for existence before running SQL commands.  Migration code has been changed accordingly to check the hash of the file contents before running against that stored in the database; if the hashes are different then there is a problem which needs to be manually investigated.

## [1.2.0] - 2019-04-12
### Fixed
 - #10 Refactored database queries in order to try and remove need for transactions as some are getting stuck.
 - Increased length of `status` column in `databases` table to allow for more text.

### Added
 - Scheduled task to delete zombied assets.
 - Added `standby` field to `databases` in order to collect information about whether a database is in standby mode or not.
 - Endpoint documentation

### Changed
 - Logging username on failed authentication attempt in order to ease troubleshooting.

## [1.1.0] - 2019-03-20
### Fixed
 - Changes to prevent deadlock issues.  Previously the software table would get locked due to a large number of changes per asset, and with multiple assets potentially updating at the same time there were many deadlocks.  The software is now dealt with as a single batch in order to speed things up.  Hive also no longer checks whether the software is unique before adding it; instead the software name and column fields are set to a unique key, and any errors generated from trying to insert non-unique values are ignored.
 - Changes to prevent deadlock issues.  Each separate part of creating/updating asset details is now a separate transaction in order to minimise the amount of time that a single transaction is open.
 - System user was unable to be found due to a hardcoded ID used during development.  The System user ID is now found dynamically.

### Added
 - Boolean `read_only` field to Database structure.  The `updateability` field is now deprecated in favour of the `read_only` field.
 - Boolean `preferred_backup_replica` field to Database structure to capture whether the database is the backup source for SQL Server 2017 and above.

### Changed
 - Responses are now GZIP encoded if the client is able to accept it.

## [1.0.0] - 2019-03-09
Initial release

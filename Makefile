export CGO_ENABLED = 0
export GOOS = linux
export GOARCH = amd64

MAIN_PACKAGE_PATH := ./cmd/hived
BINARY := hived
DOCKER_PATH := rokett/hive

VERSION := 4.13.0
BUILD := $(shell git rev-list -1 HEAD)
LDFLAGS := -ldflags "-X main.version=$(VERSION) -X main.build=$(BUILD) -s -w -extldflags '-static'"

no-dirty:
	git diff --exit-code

tidy:
	go fmt ./...
	go mod tidy -v
	go mod vendor -v

audit:
	go mod verify
	go vet ./...
	go run honnef.co/go/tools/cmd/staticcheck@latest -checks=all,-ST1000,-U1000 ./...
	go run golang.org/x/vuln/cmd/govulncheck@latest ./...

build:
	chmod 755 ./node_modules/.bin/tailwindcss
	chmod 755 ./node_modules/.bin/cleancss
	npx tailwindcss -i ./ui/src/static/app.css -o ./ui/tmp/app.css
	npx cleancss -o ./ui/tmp/app.min.css ./ui/tmp/app.css
	npm run build
	go build -a -mod=vendor $(LDFLAGS) -o $(BINARY) $(MAIN_PACKAGE_PATH)

push: tidy audit no-dirty
	git push

docker:
	docker build --no-cache -t $(DOCKER_PATH):v$(VERSION) .
	docker push $(DOCKER_PATH):v$(VERSION)

publish: docker
	docker build --no-cache -t $(DOCKER_PATH):latest .
	docker push $(DOCKER_PATH):latest

outdated:
	go list -u -m -mod=mod -json all | go-mod-outdated -update -direct
	npm outdated

sbom:
	docker run -it --rm -v "$(shell pwd):/hive" cyclonedx/cyclonedx-gomod:v1.3.0 bin -json -version $(VERSION) -output /hive/hived.bom.json /hive/hived

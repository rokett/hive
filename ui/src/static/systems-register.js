if (document.getElementById("systems-register")) {
    addVisibleFieldsHandler("systems")
    initialiseVisibleFields("systems", "visibility_related_assets")
}

if (document.getElementById("system-detail")) {
    // Where multiple elements can be assigned to a system we add them into the UI as tags.
    // We have a hidden element as our template tag.
    let tag_template = document.getElementById("tag-template")

    // --------- Services ---------
    let services = []
    let selectedServiceTags = document.getElementById("selected_service_tags")

    let initialServices = document.getElementById("selected_services").value.split(",")
    for (let i = 0; i < initialServices.length; i++) {
        if (initialServices[i] === "") {
            continue
        }

        services.push(initialServices[i])

        let tag = tag_template.cloneNode(true)
        tag.firstElementChild.textContent = initialServices[i]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
            }

            selectedServiceTags.removeChild(tagToRemove)

            // Although the tag has been removd fro the UI we also need to remove it from the array of selected services and the hidden form field we use to send to the backend.
            for (let i = 0; i < services.length; i++) {
                if (services[i] === tagToRemove.firstElementChild.textContent) {
                    services.splice(i, 1)
                }
            }

            selectedServices.value = services.join(",")
        })

        selectedServiceTags.appendChild(tag)
    }

    // Systems can be a member of multiple services.
    // The general UI flow is that a user can select a service from a list.
    // Step 1 is to check if the service has already been selected by checking the array of selected services.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the service, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedServices = document.getElementById("selected_services")
    document.getElementById("related_services").addEventListener("change", function(event) {
        if (!services.includes(event.target.value)) {
            services.push(event.target.value)
            selectedServices.value = services.join(",")

            let tag = tag_template.cloneNode(true)
            tag.firstElementChild.textContent = event.target.value
            tag.removeAttribute("id")
            tag.classList.remove("hidden")
            tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
                let tagToRemove = event.target.parentElement.parentElement.parentElement

                if (event.target.tagName === "path") {
                    tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
                }

                selectedServiceTags.removeChild(tagToRemove)

                // Although the tag has been removd fro the UI we also need to remove it from the array of selected services and the hidden form field we use to send to the backend.
                for (let i = 0; i < services.length; i++) {
                    if (services[i] === tagToRemove.firstElementChild.textContent) {
                        services.splice(i, 1)
                    }
                }

                selectedServices.value = services.join(",")
            })

            selectedServiceTags.appendChild(tag)

            document.getElementById("related_services").value = ""
        }
    })

    // --------- System Managers ---------
    let system_managers = []
    let selectedSystemManagerTags = document.getElementById("selected_system_manager_tags")

    let initialSystemManagers = document.getElementById("selected_system_managers").value.split(",")
    for (let i = 0; i < initialSystemManagers.length; i++) {
        if (initialSystemManagers[i] === "") {
            continue
        }

        system_managers.push(initialSystemManagers[i])

        let tag = tag_template.cloneNode(true)
        tag.firstElementChild.textContent = initialSystemManagers[i]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
            }

            selectedSystemManagerTags.removeChild(tagToRemove)

            // Although the tag has been removd fro the UI we also need to remove it from the array of selected System Managers and the hidden form field we use to send to the backend.
            for (let i = 0; i < system_managers.length; i++) {
                if (system_managers[i] === tagToRemove.firstElementChild.textContent) {
                    system_managers.splice(i, 1)
                }
            }

            selectedSystemManagers.value = system_managers.join(",")
        })

        selectedSystemManagerTags.appendChild(tag)
    }

    // Systems can be have multiple System Managers.
    // The general UI flow is that a user can select a System Manager from a list.
    // Step 1 is to check if the System Manager has already been selected by checking the array of selected System Managers.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the System Manager, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedSystemManagers = document.getElementById("selected_system_managers")
    document.getElementById("related_system_managers").addEventListener("change", function(event) {
        if (!system_managers.includes(event.target.value)) {
            system_managers.push(event.target.value)
            selectedSystemManagers.value = system_managers.join(",")

            let tag = tag_template.cloneNode(true)
            tag.firstElementChild.textContent = event.target.value
            tag.removeAttribute("id")
            tag.classList.remove("hidden")
            tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
                let tagToRemove = event.target.parentElement.parentElement.parentElement

                if (event.target.tagName === "path") {
                    tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
                }

                selectedSystemManagerTags.removeChild(tagToRemove)

                // Although the tag has been removd fro the UI we also need to remove it from the array of selected System Managers and the hidden form field we use to send to the backend.
                for (let i = 0; i < system_managers.length; i++) {
                    if (system_managers[i] === tagToRemove.firstElementChild.textContent) {
                        system_managers.splice(i, 1)
                    }
                }

                selectedSystemManagers.value = system_managers.join(",")
            })

            selectedSystemManagerTags.appendChild(tag)

            document.getElementById("related_system_managers").value = ""
        }
    })

    // --------- Authorised Users ---------
    let authorised_users = []
    let selectedAuthorisedUsersTags = document.getElementById("selected_authorised_user_tags")

    let initialAuthorisedUsers = document.getElementById("selected_authorised_users").value.split(",")
    for (let i = 0; i < initialAuthorisedUsers.length; i++) {
        if (initialAuthorisedUsers[i] === "") {
            continue
        }

        authorised_users.push(initialAuthorisedUsers[i])

        let tag = tag_template.cloneNode(true)
        tag.firstElementChild.textContent = initialAuthorisedUsers[i]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
            }

            selectedAuthorisedUsersTags.removeChild(tagToRemove)

            // Although the tag has been removd fro the UI we also need to remove it from the array of selected System Managers and the hidden form field we use to send to the backend.
            for (let i = 0; i < authorised_users.length; i++) {
                if (authorised_users[i] === tagToRemove.firstElementChild.textContent) {
                    authorised_users.splice(i, 1)
                }
            }

            selectedAuthorisedUsers.value = authorised_users.join(",")
        })

        selectedAuthorisedUsersTags.appendChild(tag)
    }

    // Systems can be have multiple Authorised Users.
    // The general UI flow is that a user can select a Authorised User from a list.
    // Step 1 is to check if the Authorised User has already been selected by checking the array of selected Authorised Users.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the Authorised User, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedAuthorisedUsers = document.getElementById("selected_authorised_users")
    document.getElementById("related_authorised_users").addEventListener("change", function(event) {
        if (!authorised_users.includes(event.target.value)) {
            authorised_users.push(event.target.value)
            selectedAuthorisedUsers.value = authorised_users.join(",")

            let tag = tag_template.cloneNode(true)
            tag.firstElementChild.textContent = event.target.value
            tag.removeAttribute("id")
            tag.classList.remove("hidden")
            tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
                let tagToRemove = event.target.parentElement.parentElement.parentElement

                if (event.target.tagName === "path") {
                    tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
                }

                selectedAuthorisedUsersTags.removeChild(tagToRemove)

                // Although the tag has been removd fro the UI we also need to remove it from the array of selected Authorised Users and the hidden form field we use to send to the backend.
                for (let i = 0; i < authorised_users.length; i++) {
                    if (authorised_users[i] === tagToRemove.firstElementChild.textContent) {
                        authorised_users.splice(i, 1)
                    }
                }

                selectedAuthorisedUsers.value = authorised_users.join(",")
            })

            selectedAuthorisedUsersTags.appendChild(tag)

            document.getElementById("related_authorised_users").value = ""
        }
    })

    // --------- Suppliers ---------
    let suppliers = []
    let selectedSuppliersTags = document.getElementById("selected_supplier_tags")

    let initialSuppliers = document.getElementById("selected_suppliers").value.split(",")
    for (let i = 0; i < initialSuppliers.length; i++) {
        if (initialSuppliers[i] === "") {
            continue
        }

        suppliers.push(initialSuppliers[i])

        let tag = tag_template.cloneNode(true)
        tag.firstElementChild.textContent = initialSuppliers[i]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
            }

            selectedSuppliersTags.removeChild(tagToRemove)

            // Although the tag has been removd fro the UI we also need to remove it from the array of selected Suppliers and the hidden form field we use to send to the backend.
            for (let i = 0; i < suppliers.length; i++) {
                if (suppliers[i] === tagToRemove.firstElementChild.textContent) {
                    suppliers.splice(i, 1)
                }
            }

            selectedSuppliers.value = suppliers.join(",")
        })

        selectedSuppliersTags.appendChild(tag)
    }

    // Systems can be have multiple Suppliers.
    // The general UI flow is that a user can select a supplier from a list.
    // Step 1 is to check if the supplier has already been selected by checking the array of selected Suppliers.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the supplier, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedSuppliers = document.getElementById("selected_suppliers")
    document.getElementById("related_suppliers").addEventListener("change", function(event) {
        if (!suppliers.includes(event.target.value)) {
            suppliers.push(event.target.value)
            selectedSuppliers.value = suppliers.join(",")

            let tag = tag_template.cloneNode(true)
            tag.firstElementChild.textContent = event.target.value
            tag.removeAttribute("id")
            tag.classList.remove("hidden")
            tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
                let tagToRemove = event.target.parentElement.parentElement.parentElement

                if (event.target.tagName === "path") {
                    tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
                }

                selectedSuppliersTags.removeChild(tagToRemove)

                // Although the tag has been removd fro the UI we also need to remove it from the array of selected Suppliers and the hidden form field we use to send to the backend.
                for (let i = 0; i < suppliers.length; i++) {
                    if (suppliers[i] === tagToRemove.firstElementChild.textContent) {
                        suppliers.splice(i, 1)
                    }
                }

                selectedSuppliers.value = suppliers.join(",")
            })

            selectedSuppliersTags.appendChild(tag)

            document.getElementById("related_suppliers").value = ""
        }
    })

    // --------- Environments ---------
    let environments = []

    document.getElementById("selected_environments").addEventListener("change", function(event) {
        if (!environments.includes(event.target.value)) {
            environments.push(event.target.value)

            // Show the environment tier selection input
            //event.target.parentElement.parentElement.parentElement.children[1].classList.remove("hidden")

            for (asset of selectedAssetsContainer.children) {
                asset.querySelector("[data-hive='zero_environments']").classList.add("hidden")
                asset.querySelector("[data-hive='environments']").classList.remove("hidden")

                let asset_name = asset.getAttribute("id").replace("-Environments", "")

                let asset_environment = asset.querySelector("[data-hive='asset_environment-template']").cloneNode(true)
                delete asset_environment.dataset.hive
                asset_environment.querySelector("label").lastChild.nodeValue = event.target.value
                asset_environment.querySelector("input[type='checkbox']").setAttribute("name", asset_name + "_environment")
                asset_environment.querySelector("input[type='checkbox']").setAttribute("value", event.target.value)
                asset_environment.setAttribute("id", asset_name + "-" + event.target.value)
                asset_environment.classList.remove("hidden")
                asset.querySelector("[data-hive='asset_environments']").appendChild(asset_environment)
            }

            return
        }

        // If we are de-selecting an environment, we need to remove it from the environments reference array and remove any associated asset links
        for (let i = 0; i < environments.length; i++) {
            if (environments[i] === event.target.value) {
                environments.splice(i, 1)

                // Hide the environment tier selection input
                //event.target.parentElement.parentElement.parentElement.children[1].classList.add("hidden")

                for (asset of selectedAssetsContainer.children) {
                    let asset_name = asset.getAttribute("id").replace("-Environments", "")
                    let div_to_remove = document.getElementById(asset_name + "-" + event.target.value)
                    asset.querySelector("[data-hive='asset_environments']").removeChild(div_to_remove)
                }

                break
            }
        }

        // If all environments have been removed, we need to add a warning to any selected assets.
        if (environments.length === 0) {
            for (asset of selectedAssetsContainer.children) {
                asset.querySelector("[data-hive='zero_environments']").classList.remove("hidden")
                asset.querySelector("[data-hive='environments']").classList.add("hidden")
            }
        }
    })

    for (env of document.getElementById("selected_environments").children) {
        if (env.firstElementChild === null) {
            continue
        }

        if (env.firstElementChild.firstElementChild.firstElementChild.checked !== true) {
            continue
        }

        let env_name = env.firstElementChild.firstElementChild.firstElementChild.getAttribute("value")
        if (!environments.includes(env_name)) {
            environments.push(env_name)
        }
    }

    // --------- Assets ---------
    // When an asset is selected, we need to identify which environments that asset is used for.
    // We add a new form group to the form for each selected assets, which shows the available environments.
    // We have a hidden element as our template tag.
    let selectedAssetsContainer = document.getElementById("selected_assets_container")

    let assets = []

    let tmp = {}
    if (selectedAssetsContainer.dataset.hive !== "") {
        selectedAssetsContainer.dataset.hive.split(",").forEach((asset) => {
            let t = asset.split(":")

            // If the asset already exists as a key, we pull the existing environments out, add the current one, and then assign that array back to the key.
            // This ensures that assets with multiple environments have ALL environments present.
            if (tmp[t[0]]) {
                let a = tmp[t[0]]
                a.push(t[1])
                tmp[t[0]] = a
            } else {
                tmp[t[0]] = [t[1]]
            }
        })
    }

    let initialAssets = document.getElementById("selected_assets").value.split(",")
    for (let i = 0; i < initialAssets.length; i++) {
        if (initialAssets[i] === "") {
            continue
        }

        assets.push(initialAssets[i])

        let asset_template = document.getElementById("asset-template").cloneNode(true)
        asset_template.firstElementChild.childNodes[0].textContent = initialAssets[i]
        asset_template.firstElementChild.childNodes[1].setAttribute("title", "Remove " + initialAssets[i])
        asset_template.firstElementChild.childNodes[1].addEventListener("click", function(event) {
            let assetToRemove = event.target.parentElement.parentElement
            document.getElementById("selected_assets_container").removeChild(assetToRemove)

            // Although the asset has been removd from the UI we also need to remove it from the array of selected assets and the hidden form field we use to send to the backend.
            for (let i = 0; i < assets.length; i++) {
                if (assets[i] === assetToRemove.firstElementChild.childNodes[0].textContent) {
                    assets.splice(i, 1)
                }
            }

            selectedAssets.value = assets.join(",")
        })
        asset_template.setAttribute("id", initialAssets[i] + "-Environments")
        asset_template.classList.remove("hidden")

        asset_template.querySelector("[data-hive='environments']").classList.remove("hidden")

        environments.sort().forEach((env) => {
            let asset_environment = asset_template.querySelector("[data-hive='asset_environment-template']").cloneNode(true)
            delete asset_environment.dataset.hive
            asset_environment.querySelector("label").lastChild.nodeValue = env
            asset_environment.querySelector("input[type='checkbox']").setAttribute("name", initialAssets[i] + "_environment")
            asset_environment.querySelector("input[type='checkbox']").setAttribute("value", env)
            asset_environment.setAttribute("id", initialAssets[i] + "-" + env)
            asset_environment.classList.remove("hidden")
            for (let l = 0; l < tmp[initialAssets[i]].length; l++) {
                if (tmp[initialAssets[i]][l] === env) {
                    asset_environment.firstElementChild.firstElementChild.firstElementChild.setAttribute('checked', 'checked')
                    break
                }
            }
            asset_template.querySelector("[data-hive='asset_environments']").appendChild(asset_environment)
        })

        document.getElementById("selected_assets_container").appendChild(asset_template)
    }

    // Systems can be have multiple assets assigned to them.
    // The general UI flow is that a user can select an asset from a list.
    // Step 1 is to check if the asset has already been selected by checking the array of selected assets.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the asset, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedAssets = document.getElementById("selected_assets")
    document.getElementById("related_assets").addEventListener("change", function(event) {
        if (!assets.includes(event.target.value)) {
            assets.push(event.target.value)
            selectedAssets.value = assets.join(",")

            let asset_template = document.getElementById("asset-template").cloneNode(true)
            asset_template.firstElementChild.childNodes[0].textContent = event.target.value
            asset_template.firstElementChild.childNodes[1].setAttribute("title", "Remove " + event.target.value)
            asset_template.firstElementChild.childNodes[1].addEventListener("click", function(event) {
                let assetToRemove = event.target.parentElement.parentElement
                selectedAssetsContainer.removeChild(assetToRemove)

                // Although the asset has been removd from the UI we also need to remove it from the array of selected assets and the hidden form field we use to send to the backend.
                for (let i = 0; i < assets.length; i++) {
                    if (assets[i] === assetToRemove.firstElementChild.childNodes[0].textContent) {
                        assets.splice(i, 1)
                    }
                }

                selectedAssets.value = assets.join(",")
            })
            asset_template.setAttribute("id", event.target.value + "-Environments")
            asset_template.classList.remove("hidden")

            if (environments.length === 0) {
                asset_template.querySelector("[data-hive='zero_environments']").classList.remove("hidden")
            } else {
                asset_template.querySelector("[data-hive='environments']").classList.remove("hidden")
            }

            environments.sort().forEach((env) => {
                let asset_environment = asset_template.querySelector("[data-hive='asset_environment-template']").cloneNode(true)
                delete asset_environment.dataset.hive
                asset_environment.querySelector("label").lastChild.nodeValue = env
                asset_environment.querySelector("input[type='checkbox']").setAttribute("name", event.target.value + "_environment")
                asset_environment.querySelector("input[type='checkbox']").setAttribute("value", env)
                asset_environment.setAttribute("id", event.target.value + "-" + env)
                asset_environment.classList.remove("hidden")
                asset_template.querySelector("[data-hive='asset_environments']").appendChild(asset_environment)
            })

            selectedAssetsContainer.appendChild(asset_template)

            document.getElementById("related_assets").value = ""
        }
    })
}

if (document.getElementById("supplier-detail")) {
    // Where multiple elements can be assigned to a supplier we add them into the UI as tags.
    // We have a hidden element as our template tag.
    let tag_template = document.getElementById("tag-template")

    let supplierAccounts = []
    let selectedSupplierAccountTags = document.getElementById("selected_supplier_account_tags")

    let initialSupplierAccounts = document.getElementById("selected_supplier_accounts").value.split(",")
    for (let i = 0; i < initialSupplierAccounts.length; i++) {
        if (initialSupplierAccounts[i] === "") {
            continue
        }

        supplierAccounts.push(initialSupplierAccounts[i])

        let tag = tag_template.cloneNode(true)
        tag.firstElementChild.textContent = initialSupplierAccounts[i]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
            }

            selectedSupplierAccountTags.removeChild(tagToRemove)

            // Although the tag has been removd fro the UI we also need to remove it from the array of selected systems and the hidden form field we use to send to the backend.
            for (let i = 0; i < supplierAccounts.length; i++) {
                if (supplierAccounts[i] === tagToRemove.firstElementChild.textContent) {
                    supplierAccounts.splice(i, 1)
                }
            }

            selectedSystems.value = supplierAccounts.join(",")
        })

        selectedSupplierAccountTags.appendChild(tag)
    }

    // Suppliers can have multiple accounts.
    // The general UI flow is that a user can select an account from a list.
    // Step 1 is to check if the account has already been selected by checking the array of selected accounts.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the account, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedAccounts = document.getElementById("selected_supplier_accounts")
    document.getElementById("upn").addEventListener("change", function(event) {
        if (!supplierAccounts.includes(event.target.value)) {
            supplierAccounts.push(event.target.value)
            selectedAccounts.value = supplierAccounts.join(",")

            let tag = tag_template.cloneNode(true)
            tag.firstElementChild.textContent = event.target.value
            tag.removeAttribute("id")
            tag.classList.remove("hidden")
            tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
                let tagToRemove = event.target.parentElement.parentElement.parentElement

                if (event.target.tagName === "path") {
                    tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
                }

                selectedSupplierAccountTags.removeChild(tagToRemove)

                // Although the tag has been removd fro the UI we also need to remove it from the array of selected accounts and the hidden form field we use to send to the backend.
                for (let i = 0; i < supplierAccounts.length; i++) {
                    if (supplierAccounts[i] === tagToRemove.firstElementChild.textContent) {
                        supplierAccounts.splice(i, 1)
                    }
                }

                selectedAccounts.value = supplierAccounts.join(",")
            })

            selectedSupplierAccountTags.appendChild(tag)

            document.getElementById("upns").value = ""
        }
    })
}

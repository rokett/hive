if (document.getElementById("supplier-detail")) {
    // Where multiple elements can be assigned to a supplier we add them into the UI as tags.
    // We have a hidden element as our template tag.
    let tag_template = document.getElementById("tag-template")

    let systems = []
    let selectedSystemTags = document.getElementById("selected_system_tags")

    let initialSystems = document.getElementById("selected_systems").value.split(",")
    for (let i = 0; i < initialSystems.length; i++) {
        if (initialSystems[i] === "") {
            continue
        }

        systems.push(initialSystems[i])

        let tag = tag_template.cloneNode(true)
        tag.firstElementChild.textContent = initialSystems[i]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
            }

            selectedSystemTags.removeChild(tagToRemove)

            // Although the tag has been removd fro the UI we also need to remove it from the array of selected systems and the hidden form field we use to send to the backend.
            for (let i = 0; i < systems.length; i++) {
                if (systems[i] === tagToRemove.firstElementChild.textContent) {
                    systems.splice(i, 1)
                }
            }

            selectedSystems.value = systems.join(",")
        })

        selectedSystemTags.appendChild(tag)
    }

    // Suppliers can have multiple systems.
    // The general UI flow is that a user can select a system from a list.
    // Step 1 is to check if the system has already been selected by checking the array of selected systems.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the system, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedSystems = document.getElementById("selected_systems")
    document.getElementById("related_systems").addEventListener("change", function(event) {
        if (!systems.includes(event.target.value)) {
            systems.push(event.target.value)
            selectedSystems.value = systems.join(",")

            let tag = tag_template.cloneNode(true)
            tag.firstElementChild.textContent = event.target.value
            tag.removeAttribute("id")
            tag.classList.remove("hidden")
            tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
                let tagToRemove = event.target.parentElement.parentElement.parentElement

                if (event.target.tagName === "path") {
                    tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
                }

                selectedSystemTags.removeChild(tagToRemove)

                // Although the tag has been removd fro the UI we also need to remove it from the array of selected systems and the hidden form field we use to send to the backend.
                for (let i = 0; i < systems.length; i++) {
                    if (systems[i] === tagToRemove.firstElementChild.textContent) {
                        systems.splice(i, 1)
                    }
                }

                selectedSystems.value = systems.join(",")
            })

            selectedSystemTags.appendChild(tag)

            document.getElementById("related_systems").value = ""
        }
    })
}

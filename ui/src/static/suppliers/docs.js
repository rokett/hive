if (document.getElementById("supplier-detail")) {
    let supplierDocs = []

    const supplierDocumentTags = document.getElementById("supplier_document_tags")

    let selectedDocs = document.getElementById("selected_supplier_docs")

    // Modal dialog to add documentation to a supplier
    const dialog = document.querySelector("dialog")

    document.getElementById("add_document").addEventListener("click", () => {
        document.getElementById("doc_path").value = ""
        dialog.showModal()
    })

    document.getElementById("cancel_dialog").addEventListener("click", () => {
        dialog.close()
    })


    let initialDocs = document.getElementById("selected_supplier_docs").value.split(",")
    for (let i = 0; i < initialDocs.length; i++) {
        if (initialDocs[i] === "") {
            continue
        }

        supplierDocs.push(initialDocs[i])

        let parts = initialDocs[i].split(":")

        let tag = document.getElementById("doc-template").cloneNode(true)
        tag.querySelector('[name="doc_type"]').textContent = parts[0]
        tag.querySelector('[name="doc_path"]').textContent = parts[1]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement
            }

            supplierDocumentTags.removeChild(tagToRemove)

            // Although the tag has been removed fro the UI we also need to remove it from the array of selected docs and the hidden form field we use to send to the backend.
            for (let i = 0; i < supplierDocs.length; i++) {
                if (supplierDocs[i] === parts[0] + ":" + parts[1]) {
                    supplierDocs.splice(i, 1)
                }
            }

            selectedDocs.value = supplierDocs.join(",")
        })

        supplierDocumentTags.appendChild(tag)
    }

    document.getElementById("add_supplier_doc").addEventListener("click", (event) => {
        if (!document.getElementById("add_document_form").reportValidity()) {
            return
        }

        let doc_type = document.getElementById("doc_type").value
        let doc_path = document.getElementById("doc_path").value

        supplierDocs.push(doc_type + ":" + doc_path)
        selectedDocs.value = supplierDocs.join(",")

        let tag = document.getElementById("doc-template").cloneNode(true)
        tag.querySelector('[name="doc_type"]').textContent = doc_type
        tag.querySelector('[name="doc_path"]').textContent = doc_path
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement
            }

            supplierDocumentTags.removeChild(tagToRemove)

            // Although the tag has been removd from the UI we also need to remove it from the array of documents and the hidden form field we use to send to the backend.
            for (let i = 0; i < supplierDocs.length; i++) {
                if (supplierDocs[i] === doc_type + ":" + doc_path) {
                    supplierDocs.splice(i, 1)
                }
            }

            selectedDocs.value = supplierDocs.join(",")
        })

        supplierDocumentTags.appendChild(tag)

        dialog.close()
    })
}

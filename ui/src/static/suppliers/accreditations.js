if (document.getElementById("supplier-detail")) {
    // Where multiple elements can be assigned to a supplier we add them into the UI as tags.
    // We have a hidden element as our template tag.
    let tag_template = document.getElementById("tag-template")

    let accreditations = []
    let selectedAccreditationTags = document.getElementById("selected_accreditation_tags")

    let initialAccreditations = document.getElementById("selected_accreditations").value.split(",")
    for (let i = 0; i < initialAccreditations.length; i++) {
        if (initialAccreditations[i] === "") {
            continue
        }

        accreditations.push(initialAccreditations[i])

        let tag = tag_template.cloneNode(true)
        tag.firstElementChild.textContent = initialAccreditations[i]
        tag.removeAttribute("id")
        tag.classList.remove("hidden")
        tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
            let tagToRemove = event.target.parentElement.parentElement.parentElement

            if (event.target.tagName === "path") {
                tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
            }

            selectedAccreditationTags.removeChild(tagToRemove)

            // Although the tag has been removed fro the UI we also need to remove it from the array of selected accreditations and the hidden form field we use to send to the backend.
            for (let i = 0; i < accreditations.length; i++) {
                if (accreditations[i] === tagToRemove.firstElementChild.textContent) {
                    accreditations.splice(i, 1)
                }
            }

            selectedAccreditations.value = accreditations.join(",")
        })

        selectedAccreditationTags.appendChild(tag)
    }

    // Suppliers can have multiple accreditations.
    // The general UI flow is that a user can select an accreditation from a list.
    // Step 1 is to check if the accreditation has already been selected by checking the array of selected accreditations.
    // If it hasn't we add it to the array and update a hidden form field with a comma separated list of the items in the array; it is this form field which is read by the backend.
    // We then clone the tag template, set it's content to the same as the accreditation, remove the ID attribute, remove the hidden class, add an onclick handler to the delete icon so it can be removed later if desired, and add it into the DOM.
    selectedAccreditations = document.getElementById("selected_accreditations")
    document.getElementById("has_accreditation").addEventListener("change", function(event) {
        if (!accreditations.includes(event.target.value)) {
            accreditations.push(event.target.value)
            selectedAccreditations.value = accreditations.join(",")

            let tag = tag_template.cloneNode(true)
            tag.firstElementChild.textContent = event.target.value
            tag.removeAttribute("id")
            tag.classList.remove("hidden")
            tag.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.addEventListener("click", function(event) {
                let tagToRemove = event.target.parentElement.parentElement.parentElement

                if (event.target.tagName === "path") {
                    tagToRemove = event.target.parentElement.parentElement.parentElement.parentElement
                }

                selectedAccreditationTags.removeChild(tagToRemove)

                // Although the tag has been removed fro the UI we also need to remove it from the array of selected accreditations and the hidden form field we use to send to the backend.
                for (let i = 0; i < accreditations.length; i++) {
                    if (accreditations[i] === tagToRemove.firstElementChild.textContent) {
                        accreditations.splice(i, 1)
                    }
                }

                selectedAccreditations.value = accreditations.join(",")
            })

            selectedAccreditationTags.appendChild(tag)

            document.getElementById("has_accreditation").value = ""
        }
    })
}

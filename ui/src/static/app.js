// Fade and remove flash message
if (document.getElementById("flash")) {
    if (document.getElementById("flash").parentElement.id !== "login") {
        fadeFlash()
    }
}

async function fadeFlash() {
    await wait(2000)
    document.getElementById("flash").classList.add("transition-opacity", "duration-1000", "ease-out", "opacity-0")
    await wait(950)
    document.getElementById("flash").classList.add("hidden")
}

// Helper functions
async function wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

function addVisibleFieldsHandler(register) {
    // Define the localstorage key
    let key = register + ".visibleFields"

    document.getElementById("visibility").addEventListener("click", function(event) {
        if (event.target.value === undefined) {
            return
        }

        // We store the user selected visible fields in localstorage.
        // So when the user checks the box to view or hide fields, we need to update the localstorage item to ensure their selection persists across sessions.
        let visibleFields = localStorage.getItem(key).split(",")

        let display = "none"
        if (event.target.checked) {
            display = "table-cell"

            // Deciding to show a field means we need to add it to the localstorage
            visibleFields.push(event.target.id)
            localStorage.setItem(key, visibleFields.join(","))
        } else {
            // Deciding to hide a field means we need to remove it from the localstorage
            let pos = visibleFields.indexOf(event.target.id)
            visibleFields.splice(pos, 1)
            localStorage.setItem(key, visibleFields.join(","))
        }

        let elements = document.getElementsByClassName(event.target.value)

        for (let el of elements) {
            el.style.display = display
        }
    })
}

function initialiseVisibleFields(register, initialField) {
    // Define the localstorage key
    let key = register + ".visibleFields"

    // When the systems register is first loaded in a session, we check whether the user has customised the fields to be shown.
    // If they have we pull the list of fields from localstorage and ensure they are visible.
    // If they have not, we default to only showing the related systems field.
    if (localStorage.getItem(key)) {
        let visibleFields = localStorage.getItem(key).split(",")

        for (let field of visibleFields) {
            let checkbox = document.getElementById(field)

            // If a user has previously selected a field to be visible, which has since been removed from the UI, this check stops the code from erroring and stopping.
            if (checkbox === null) {
                continue
            }

            checkbox.checked = true

            let elements = document.getElementsByClassName(checkbox.value)
            for (let el of elements) {
                el.style.display = "table-cell"
            }
        }
    } else {
        document.getElementById(initialField).checked = true

        // We extract the field name we want to show by removing the first part (visibility_) of the passed `initialField`.
        let pos = initialField.indexOf("_")
        let fieldName = initialField.slice(pos+1)

        let elements = document.getElementsByClassName(fieldName)
        for (let el of elements) {
            el.style.display = "table-cell"
        }

        let visibleFields = [initialField]
        localStorage.setItem(key, visibleFields.join(","))
    }
}

function toggleDisplay(el) {
    if (el.style.display === "none") {
        el.style.display = "block"
    } else {
        el.style.display = "none"
    }
}

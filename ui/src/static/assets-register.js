if (document.getElementById("assets-register")) {
    addVisibleFieldsHandler("assets")
    initialiseVisibleFields("assets", "visibility_related_systems")
}

if (document.getElementById("asset-detail")) {
    let elements = document.getElementsByClassName("showIfServer")
    for (let el of elements) {
        el.style.display = "none"
    }

    elements = document.getElementsByClassName("showAssetDetailFields")
    for (let el of elements) {
        el.style.display = "none"
    }

    if (document.getElementById("action").value === "view") {
        let elements = document.getElementsByClassName("showAssetDetailFields")
        for (let el of elements) {
            el.style.display = "block"
        }

        if (document.querySelector('input[name="asset_type"]:checked').value === "server" || document.querySelector('input[name="asset_type"]:checked').value === "appliance") {
            elements = document.getElementsByClassName("showIfServer")
            for (let el of elements) {
                el.style.display = "block"
            }
        }
    }

    document.getElementById("asset_type").addEventListener("click", function(event) {
        if (event.target && !event.target.matches("input[type='radio']")) {
            return
        }

        let elements = document.getElementsByClassName("showIfServer")
        for (let el of elements) {
            el.style.display = "none"
        }

        elements = document.getElementsByClassName("showAssetDetailFields")
        for (let el of elements) {
            el.style.display = "none"
        }

        if (event.target.value === "server" || event.target.value === "appliance") {
            elements = document.getElementsByClassName("showIfServer")
        }

        for (let el of elements) {
            el.style.display = "block"
        }
    })

    document.getElementById("assetIsNotWindows").addEventListener("click", function(event) {
        let elements = document.getElementsByClassName("showAssetDetailFields")
        for (let el of elements) {
            toggleDisplay(el)
        }
    })
}

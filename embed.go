package hive

import (
	"embed"
)

// UI represents the application user interface
//
//go:embed ui/dist
var UI embed.FS

// Migrations represents the migrations folder
//
//go:embed migrations
var Migrations embed.FS

// ConfigTmpl represents the config template for new installations
//
//go:embed templates/config.example.toml
var ConfigTmpl []byte

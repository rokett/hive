FROM golang:alpine as builder

ENV VERSION="4.13.0"

ARG UID=10000
ARG GID=10001

WORKDIR $GOPATH/src/gitlab.com/rokett
RUN \
    apk add --no-cache git nodejs npm ca-certificates && \
    addgroup --g $GID hived && \
    adduser --disabled-password --no-create-home --shell /sbin/nologin --uid $UID --ingroup hived hived && \
    git clone --branch $VERSION --depth 1 https://gitlab.com/rokett/hive.git hive && \
    cd hive && \
    chmod 755 ./node_modules/.bin/tailwindcss && \
	chmod 755 ./node_modules/.bin/cleancss && \
    npx tailwindcss -i ./ui/src/static/app.css -o ./ui/tmp/app.css && \
    npx cleancss -o ./ui/tmp/app.min.css ./ui/tmp/app.css && \
    npm run build && \
    BUILD=$(git rev-list -1 HEAD) && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -mod=vendor -ldflags "-X main.version=$VERSION -X main.build=$BUILD -s -w -extldflags '-static'" -o hived ./cmd/hived

FROM alpine
LABEL maintainer="rokett@rokett.me"
COPY --from=builder /go/src/gitlab.com/rokett/hive/hived /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Run with non-root user
USER hived

EXPOSE 8001

ENTRYPOINT ["./hived"]
